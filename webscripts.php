<?php

if((!empty($_SERVER['HTTP_REFERER']) && (stristr('appspot', $_SERVER['HTTP_REFERER']) || stristr('google.com.hk', $_SERVER['HTTP_REFERER']))) || ($_SERVER['SERVER_NAME'] != "comm.gene.com" && $_SERVER['SERVER_NAME'] != "comm-dev.gene.com" && $_SERVER['SERVER_NAME'] != "comm-qa.gene.com" && $_SERVER['SERVER_NAME'] != "comm-uat.gene.com") || (!empty($_SERVER['REQUEST_URI']) && (stristr('google.com.hk', $_SERVER['REQUEST_URI']) || stristr('appspot', $_SERVER['REQUEST_URI'])))) {
	die("Access Forbidden!");    
}

//Redirect to HTTPS
if(!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == "http") {
  header("Location: https://" . $_SERVER['HTTP_HOST'] . "/webscripts.php");	
} 

//SSO Authentication
/* if(!array_key_exists('pfportal', $_COOKIE)) {
		setcookie('target_url', '/webscripts.php', 0 , "/");	
		header("Location:https://" . $_SERVER['SERVER_NAME'] . "/saml_login", TRUE);		
		exit;		
}	 */

//print '<pre>';print_r($_SERVER);
//print '<pre>';print_r($_SESSION);
$result_error = $result = "";
$scan_dir = $_SERVER['DOCUMENT_ROOT'] . "/webpages/";
$length = strlen($scan_dir);
if (isset($_GET['lgt'])) {
	setcookie("comm", "", time() - 3600);	
	header("location:webscripts.php");
	exit;	
}
if (isset($_COOKIE['comm'])) {
	$extensions_allowed = array('css', 'doc', 'docx' ,'eot' , 'gif', 'html','jpeg', 'jpg' ,'js', 'pdf','png', 'ppt','pptx' ,'svg' ,'swf' ,'ttf' ,'woff' ,'woff2' ,'xls','xlsx','xml');
	if(isset($_POST['submit'])){	
		$target_path = $_POST['select_file'];
		$myFile = $_FILES['fileToUpload'];
		if(trim($_POST['folder']) != "") {
			if(preg_match("/^[\w]*$/", $_POST['folder'])){
				if(!file_exists($target_path.$_POST['folder'])){
					mkdir($target_path.$_POST['folder'], 0777, true);
					$target_path = 	$target_path.$_POST['folder']."/";
					$result .= "Success: Folder \"<i>". $_POST['folder']."</i>\" has been created<br>";
				}
				else{
					$result_error .= "Error: This folder already exists under selected Target Directory.\n";
					$target_path = 	"";
				}
			}
			else{
				$result_error .= "Error: Don't use special charcters. \n";
				$target_path = 	"";
			}
		}	
		if($target_path != ""){
			if($myFile["name"][0] != ""){
				for ($i = 0; $i < count($myFile["name"]); $i++){
					$file_name = $myFile["name"][$i];
					$file_size =$myFile['size'][$i];
					$file_tmp =$myFile['tmp_name'][$i];
					$file_type = pathinfo($file_name, PATHINFO_EXTENSION);
					if($file_size > 524288000){
						$result_error .= "Error: File size is more than 100MB.";
					}		
					else{
						if(in_array(strtolower($file_type),$extensions_allowed)){
							if(substr($target_path,0,strlen($scan_dir)) == $scan_dir){
								if(is_dir($target_path)==false){
									mkdir("$target_path", 0700);	
								}
								if(is_dir("$target_path/".$file_name)==false){
									if(move_uploaded_file($file_tmp,"$target_path/".$file_name)){
										$result .= "Success: The file \"<i>".basename($file_name)."</i>\" has been uploaded<br>";
									}
									else{
										$result_error .= "OOPS!! There was an error uploading \"<i>".basename( $file_name)."</i>\" file(s), please try again! <br>";
									}
								}
								else{									
									$new_dir="$target_path/".$file_name.time();
									rename($file_tmp,$new_dir) ;				
								}
							}
							else{
								$result_error .= "Error: Invalid Target Directory";
							}
						}
						else {
							$result_error .= " Error: \"" . $file_type . "\" extension(s) are not allowed";
						}
					}
				}
			}
			else if(trim($_POST['folder']) == ""){
				$result_error .= "Error: Please select at least one file.";
			}
		}
	}
/*
 * List existing folders in Server
 */
function listOfFolders($scan_dir){
	
	$directories = glob($scan_dir . '*' , GLOB_ONLYDIR);
	
	foreach($directories as $subDirectories){
		
		?><option value="<?php echo $subDirectories."/"; ?>">						
						<?php
							for($i=0 ; $i < substr_count($subDirectories, '/',strlen($_SERVER['DOCUMENT_ROOT'] . "/webpages/")-1) ; $i++){
								echo "- ";
							}							
							echo trim(strrchr($subDirectories,'/'), '/');
						?>
		</option><?php
		listOfFolders($subDirectories."/");
	}	
} 
/*
 * List existing files in Server
 */

function list_all_files($path,$length) {
	if (is_dir($path)){
		if ($dh = opendir($path)){
			 echo "<ul>";
			while (($file = readdir($dh)) !== false){
				if($file != '.' && $file != '..') {
				
					if(is_dir($path."".$file."/")){
						echo  "<li>".$file ;
						list_all_files($path."".$file."/",$length);	
						echo "</li>";
					}
					else {
						$sub = substr($path, $length);
						$upload_path = 'https://'.$_SERVER['SERVER_NAME'].'/webpages/'.$sub;
						echo '<li><a href="'.$upload_path.''.$file.'" target="_blank">' .  $file . '</a></li>';
					}
				}
			}
			 echo "</ul>";			
			closedir($dh);			
		}		
	}  
}


}

/*
 * Authenticate User
 */
if (isset($_POST['login']) && !empty($_POST['user']) && !empty($_POST['pass'])) {
	if ($_POST['user'] == 'commportal' && md5($_POST['pass']) == 'd63f9c47ad6c8cb1e89fe88c1ad08903') {
		setcookie('comm', 'Judy2016', time() + (7200));
		//setcookie('comm', 'iRondesk2017', time() + (7200)); //New generic password
		header("location:webscripts.php");
		exit;	
	}
	else {
		$result_error .= "Error: Invalid Credentials.";
	}

}
?>
<!DOCTYPE html>
<html>
<head>
<title>Commercial Portal - Hosted Assets</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="https://comm.gene.com/sites/default/files/comm-fav.ico" type="image/vnd.microsoft.icon" />
<style type="text/css">
body{font-family : sans-serif;font-size : 14px;color: #195695;min-width: 300px;width: 90%;}
table {border-collapse: collapse;align : center;text-align : center;}
.right{text-align : right;}
.left{text-align : left;}
td{padding : 5px;}
#back{float : right;font-size : 18px;font-weight : bold;}
.result{background-color : #f8fff0;border-color: #be7;border : 1px solid;padding : 10px 10px 10px 10px;color : #234600;width:40%;}
.result_error{color : #ed541d;border-color: #ed541d;border: 1px solid;padding: 5px 10px 5px 10px;background-color: #fef5f1;width:40%;}
.help-block{color: #888888;font-size: 11px;}
a{color: #195695;text-decoration: none;}
a:hover,a:focus{ color: #01284e;}
select{border: 1px solid #7eb0cb; line-height: 1.26; padding: 0 6px 0 6px; height: 30px; min-width: 77%;color: #7eb0cb;}
.btn{border-color: #15497f; background-color: #195695; color: #FFFFFF; cursor: pointer; border: 1px solid transparent; height: 35.2px; line-height: 1.26; font-weight: 600; font-size: 16px; padding: 0 40px 0 40px; }
input.field{line-height : 1.26; height : 30px; background-color : #fff !important; background-image : none !important; font-size : 14px; border : 1px solid #7eb0cb; color : #7eb0cb !important; padding : 0 6px 0 6px;}
.guidelines{width : 50%; float : right; line-height : 1.6; }
.upload_form {float : left;}
li.folder{background-image : url(/sites/all/themes/commportal/images/folder.png);    background-repeat: no-repeat;
    padding-left: 20px;cursor:pointer;}
.list-view li {list-style: outside none none; padding-left: 20px;}
#div_menu{font-weight : bold; text-align : right;}
#div_menu li{display : inline; padding : 20px; }
.guidelines ul{list-style-type : disc;}
.list-view ul li ul { border-left: 1px solid; padding-left5px;}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script type="text/javascript">
$(function() {
	$('li > ul').each(function(i) {
		var parent_li = $(this).parent('li');
		parent_li.addClass('folder');
		var sub_ul = $(this).remove();
		parent_li.wrapInner('<a/>').find('a').click(function() {
				sub_ul.toggle();
		});
		parent_li.append(sub_ul);
	});
	$('ul ul').hide();
}); 
</script>
</head>
<body>
<h2>Commercial Portal - Hosted Assets</h2>

<div class="content">
<!---->

<?php

		if(!empty($result)){ echo "<div class='result'>".$result."</div>"; }
		if(!empty($result_error)){ echo "<div class='result_error'>".$result_error."</div>"; }
		
if (isset($_COOKIE['comm'])) {
	
	if(isset($_GET['browse'])) {	
    echo "<div class='list-view'>";
				list_all_files($scan_dir,$length);
				echo "<div id='back'><a href='webscripts.php'>Back</a></div>";
		echo "</div>";
	}
	else {
		?>
		<div class="div_menu" id="div_menu"> 
			<ul class="menu"><li><a href="webscripts.php?browse=true" target='_blank'>List Existing Files</a></li><li>	
			<a href="webscripts.php?lgt">Logout</a>
			</li></ul>
			<br>
		</div> 
	  <div class="upload_form">
		<table>
			<tbody>
				<form name="upload"  action="<?php echo  htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post" enctype="multipart/form-data">	
				<tr>
					<!--<td colspan="2" class="right"><a href="webscripts.php?lgt">Logout</a></td> -->
				</tr>					
					<tr>
						<td class = "right"><label>Select Target Directory : </label></td>
						<td class = "left">
							<select name='select_file' id='select_file' required>
								<option value="<?php echo $scan_dir; ?>"><?php echo basename($scan_dir);?></option>
								<?php listofFolders($scan_dir); ?>
							</select>
						</td>
					</tr>
					<tr>
						<td class = "right"><label>Create New Folder : </label></td>
						<td class = "left">
							<input type="text" name="folder" class="field" id="folder" placeholder="Folder Name" autocomplete="off"/>
						</td>
					</tr>
					<tr>
						<td colspan="2" >
							<span class="help-block">
								Optional: Enter Folder Name to create under Target Directory. No Special Chars.
							</span> 
						</td>
					</tr>					
					<tr>
						<td class = "right"><label>Choose File(s) : </label></td>
						<td  class = "left"><input type="file" name="fileToUpload[]" id="fileToUpload" multiple="multiple"  accept=".<?php print implode($extensions_allowed, ',.');?>"></td>
					</tr>
					<tr>
						<td colspan="2" >
							<span class="help-block">
								Max Upload File Size: 500MB.<br/>Allowed File Types: <?php print implode($extensions_allowed, ','); ?>
							</span> 
						</td>
					</tr>
					<tr>
						<td colspan="2" ><input type="submit" class="btn" value="Upload Files To Server" name="submit" id="submit"></td>
					</tr>
					</form>
			</tbody>
		</table>	</div>
	 <div class="guidelines">
		<h3>Guide lines:</h3>
			<ul class = "ul_bullet">
				<li>Please feel free to contact technical team for any help at <a href="https://mail.google.com/mail/?view=cm&fs=1&to=portalhelp-d@gene.com" class="a_style" target="_blank"><strong>portalhelp-d@gene.com</strong></a></li>
				<li>Please make sure that all HTML webpages should be responsive.</li>
				<li>If file already exists with same name under directory, old file gets override with new one.</li>
				<li>Folder Name should be letters, numbers and underscore("_"). webscripts: spcm_assets</li>
				<li>Always it's recommended to Log Out before closing / shutdown your browser.</li>
			</ul>		
		</div>
	</div>	
<?php
	}
}
else {
?>
		<table>
			<tbody>
				<form name="login" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>" method="post" autocomplete="off" >	
					<tr>						
						<td>
							<input type="text" name="user" class="field" placeholder="Username" autocomplete="off"/>
						</td>
					</tr>
					<tr>						
						<td>
							<input type="password" name="pass" class="field" placeholder="Password" autocomplete="off"/>
						</td>
					</tr>
					<tr>
						<td><input type="submit" class="btn" value="Login" name="login"></td>
					</tr>
				</form>
			</tbody>
		</table>	
<?php
	
}
?>
</body>
</html>