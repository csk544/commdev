<?php

/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> role="article">

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2 <?php print $title_attributes; ?> class="<?php print $class_attributes;?>">
		<?php //print $title; ?>
		
    </h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php //if ($display_submitted): ?>
    <!--<footer class="meta submitted">
    
      <?php //print $submitted; ?>
    </footer>-->
  <?php //endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content);
     
			$date = date('d F Y',$node->created);
       dsm($node);
    ?>
   
		<h1 class="content__title"><?php print isset($node->field_event_name_videos['und'])? $node->field_event_name_videos['und'][0]['taxonomy_term']->name : '' ; ?></h1>
		<div class="content__body">
			<div class="video">
				<div class="video__feature">
				<!-- Embedd Vimeo videos within this area, even if password protected. Password details can be added where the date is positioned. -->
					<!--<img src="/sites/default/files/video-placeholder.jpg" alt="">-->
					<?php 
					if(($node->field_video_type['und'][0]['value']== 'Upload file') && !empty($node->field_video_file)){
						$url = file_create_url(isset($node->field_video_file['und'][0]['uri']));
						$file_types = array('video/quicktime','video/mp4','video/x-ms-wmv','video/x-m4v','video/x-flv');
						if(in_array(isset($node->field_video_file['und'][0]['filemime']),$file_types)) {
							$poster_img_src = "";
							if(isset($node->field_preview_image['und']) && $node->field_preview_image['und'][0]['uri'] != ''){
								$poster_img_src = file_create_url($node->field_preview_image['und'][0]['uri']);
							}
							echo "<video width='100%' controls='true' poster='".$poster_img_src."'><source src='".file_create_url($node->field_video_file['und'][0]['uri'])."' type='".$node->field_video_file['und'][0]['filemime']."'></video>"; 
							//print render($content['field_video_file']);
						}
					}
          else if(($node->field_video_type['und'][0]['value']== 'vimeo') && !empty($node->field_vimeo_file)){
						$url = $node->field_vimeo_file['und'][0]['value'];
						$vimeo_id = usma_custom_vimeo_video_id($url);
						echo '<div class="videoWrapper"><iframe src="//player.vimeo.com/video/' . $vimeo_id . '?autoplay=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>'; 
					}
          else if(($node->field_video_type['und'][0]['value']== 'youtube') && !empty($node->field_youtube_file)){
						$url = $node->field_youtube_file['und'][0]['value'];
						$youtube_id = usma_custom_youtube_video_id($url);
						echo '<div class="videoWrapper"><iframe src="//www.youtube.com/embed/' . $youtube_id . '?rel=0&hd=1&autoplay=0" frameborder="0" allowfullscreen></iframe></div>'; 
					}
					?>
					<!--<a href="<?php //print $url;?>" class="video__trigger"></a>-->
					<!-- Remove the "icon-play" div if play icon is not needed and native vimeo controls will be used. -->
					<!--<div class="icon-play"><span></span></div>-->
					
				</div>
				<div class="video__info">
					<div class="video__info__date"><?php print $date; ?></div>
					<div class="video__info__details">
						<h1 class="video__info__title"><a href="#"><?php print $node->title; ?></a></h1>
						<div class="video__info__text">
							<?php
              if(isset($node->body['und'])){
                print $node->body['und'][0]['value']; 
              }
              ?>
						</div>
					</div>
				</div>
			</div>
      <div class="event-gallery">
						<div class="grid-sizer"></div>
						<?php
                           
            if(isset($content['field_event_images'])){
              foreach($content['field_event_images']['#items'] as $result){
                   // dsm($content);  
										$title = $result['filename'];
										$src = file_create_url($result['uri']);?>
										<div class="event-gallery__item"><a href="<?php print $src;?>" rel="group" title="<?php print $title; ?>" class="fancybox event-gallery__item__lightbox-reveal">
										<img src="<?php print $src;?>" alt="">
										</a></div>
						<?php } 
            }
            ?>
					</div>
				</div>
		</div>
 

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php //print render($content['comments']); ?>

</article>
