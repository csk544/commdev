<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
 $profile_pic = $fields['guid']->raw;
 $default_profile_pic = file_create_url("public://filegPQEFT");
 $phone = !empty($fields['phone_number']->raw) ? $fields['phone_number']->raw : 'n/a';
 $mobile_number = !empty($fields['mobile_number']->raw) ? $fields['mobile_number']->raw : 'n/a';
 $work_location = !empty($fields['work_location']->raw) ? $fields['work_location']->raw : 'n/a';
 $building = !empty($fields['building']->raw) ? $fields['building']->raw : 'n/a';
 $room_number = !empty($fields['room_number']->raw) ? $fields['room_number']->raw : 'n/a';
 $mail_stop = !empty($fields['mail_stop']->raw) ? $fields['mail_stop']->raw : 'n/a';
 $manager_fullname = !empty($fields['manager_fullname']->raw) ? $fields['manager_fullname']->raw : 'n/a';
 $manager_unixid = !empty($fields['manager_unixid']->raw) ? $fields['manager_unixid']->raw : 'n/a';
 $cost_center_name = !empty($fields['cost_center_name']->raw) ? $fields['cost_center_name']->raw : 'n/a';
 $cost_center_id = !empty($fields['cost_center_id']->raw) ? $fields['cost_center_id']->raw : 'n/a';
 $employee_type = !empty($fields['employee_type']->raw) ? $fields['employee_type']->raw : 'n/a';
 $job_title = !empty($fields['job_title']->raw) ? $fields['job_title']->raw : 'n/a';
 $email_id = $fields['unixid']->raw . '@gene.com';
?>
<li class="row">
	<div class="col-sm-6">
		<div class="people-list__photo-wrapper">
			<img class="" src="<?php echo $profile_pic; ?>" alt="" onerror="this.onerror=null;this.src='<?php echo $default_profile_pic;?>' ;" title="<?php echo $fields['preferred_fullname']->raw;?>"/>
		</div>
		<div class="people-list__details-primary">
			<span class=""><?php echo $fields['preferred_fullname']->raw;?> (<?php echo $fields['unixid']->raw;?>)</span>
			<span><a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=<?php echo $email_id;?>" target="_blank"><?php echo $email_id;?></a></span>
			<span>p. <?php echo $phone;?></span>
			<span>m. <?php echo $mobile_number;?></span>
			<span>Location: <?php echo $work_location;?></span>
			<span>Building: <?php echo $building;?>, Room <?php echo $room_number;?></span>
		</div>
	</div>
	<div class="people-list__details-secondary col-sm-6">
		<span>Mail Stop: <?php echo $mail_stop;?> </span>
		<span>Manager: <?php echo $manager_fullname;?> (<?php echo $manager_unixid;?>)</span>
		<span>Cost Center: <?php echo $cost_center_name;?> (<?php echo $cost_center_id;?>)</span>
		<span>Type: <?php echo $fields['employee_type']->raw;?></span>
		<span>Title: <?php echo $job_title;?></span>
		<!--<span><a href="#">View Org Chart</a></span>-->
	</div>
</li>