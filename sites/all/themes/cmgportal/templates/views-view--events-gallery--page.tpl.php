<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">
  <?php 
	  global $base_url;
		$term = taxonomy_term_load($view->args[0]);
		$term_name = $term->name;
	?>
  <h1 class="content__title"><?php print $term_name; ?></h1>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>
  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>
  <?php
	$tid= arg(1);
	$child_tid_query =db_select('taxonomy_term_hierarchy','t')
											->fields('t', array('tid'))
											->condition('t.parent', $tid, '=');
	$ctid_res = $child_tid_query->execute()->fetchAll();
  if($rows){?>
	<?php if ($rows): ?>
    <div class="view-content">
			<div class="content__body">
				<div class="event-gallery">
					<div class="grid-sizer"></div>
					<?php if($view->current_display == "page"){
						?>
						<?php 
						foreach($view->result as $result){
							foreach($result->field_field_event_images as $field_image){
								$title = $field_image['raw']['filename'];
								$src = $field_image['raw']['uri'];
								$style = 'events_gallery_380_x_257';
				        $src_url = image_style_url($style, $src);?>
								<div class="event-gallery__item"><a href="<?php print $src_url;?>" rel="group" title="<?php print $title; ?>" class="fancybox event-gallery__item__lightbox-reveal">
								<img style="width:374px;" src="<?php print $src_url;?>" alt="">
								</a></div>
							<?php } ?>
					<?php } }
          else if($view->current_display == "page_1"){ 
								?>
						<div class="event-landing">
							<div class="event-landing__grid">
								
								<?php 
								foreach($view->result as $result){ 
                ?>
									<div class="event-landing__grid__item">
										<div class="event-landing-item__thumbnail">
										<?php 
                    if(isset($result->_field_data['nid']['entity']->field_video_file['und']) && $result->_field_data['nid']['entity']->field_video_file['und'][0]['uri'] != "") {
                      $url = file_create_url($result->_field_data['nid']['entity']->field_video_file['und'][0]['uri']);
                      $filetype = $result->_field_data['nid']['entity']->field_video_file['und'][0]['filemime'];
                      $file_types = array('video/quicktime','video/mp4','video/x-ms-wmv','video/x-m4v','video/x-flv');
                      if(in_array($result->_field_data['nid']['entity']->field_video_file['und'][0]['filemime'],$file_types)) {
                    ?>
                      <video width='30%' controls='true'><source src="<?php print $url;?>" type="<?php print $filetype?>"></video>
                    <?php
                      }  
                    }
										elseif(isset($result->_field_data['nid']['entity']->field_vimeo_file['und']) && $result->_field_data['nid']['entity']->field_vimeo_file['und'][0]['value'] != "") {
											$vimeo_url = $result->_field_data['nid']['entity']->field_vimeo_file['und'][0]['value'];
                      $vimeo_id = usma_custom_vimeo_video_id($vimeo_url);
               
                      ?>
						          <div><iframe src="//player.vimeo.com/video/"<?php print $vimeo_id;?>" ?autoplay=0" frameborder="0" height="30%" width="30%"></iframe></div>
										<?php
                    }
										else{
                      $youtube_url = $result->_field_data['nid']['entity']->field_youtube_file['und'][0]['value'];
						          $youtube_id = usma_custom_youtube_video_id($youtube_url);?>
						          <div><iframe src="//www.youtube.com/embed/"<?php print $youtube_id;?>" ?rel=0&hd=1&autoplay=0" frameborder="0" height="30%" width="30%"></iframe></div>
											<?php
										} ?>
											<!--<div class="event-landing-item__video-length">4:10</div>-->
											<a href="<?php print $base_url;?>/node/<?php print $result->nid;?>" class="event-landing-item__link event-landing-item__link--video">
											<!--<div class="icon-play"><span></span></div>--></a>
										</div>
										<div class="event-landing-item__info">
											<h3 class="event-landing-item__info__title"><?php print $result->node_title;?></h3>
										</div>
									</div>
								<?php } ?>
								 
							</div>
						</div>
						
					<?php }?>
						
				</div>
			</div>
      <?php //print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>
	<?php } ?>
  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>