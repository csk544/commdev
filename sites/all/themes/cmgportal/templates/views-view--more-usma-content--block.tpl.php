<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">

  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="view-content">
		
			<?php if($view->current_display == "block"){ ?>
			<div class="event-landing">
				<h2>More USMA Events</h2>
				<div class="event-landing__grid">
					<?php foreach($view->result as $result)	{ 
					$src = file_create_url($result->field_field_thumbnail[0]['raw']['uri']);
					$tid = $result->tid;
					$style = 'events_gallery_380_x_257';
					$src_url = image_style_url($style, $src);
					?>
						<div class="event-landing__grid__item">
            <a href="/events-gallery/images/<?php print $tid; ?>" class="event-landing-item__link">
							<div class="event-landing-item__thumbnail">
								<img src="<?php print $src; ?>" alt="">
							</div>
							<div class="event-landing-item__info">
								<h3 class="event-landing-item__info__title"><?php print $result->taxonomy_term_data_name; ?></h3>
								<h4 class="event-landing-item__info__date"><?php print $result->field_field_date[0]['rendered']['#markup']; ?></h4>
							</div>
							</a>
						</div>
					<?php } ?>
				</div>
			</div>
			<?php }else if($view->current_display == "block_1") { ?>
					<div class="content__body">
						<div class="event-landing">
						<h2>More USMA events</h2>
							<div class="event-landing__grid">
								<?php foreach($view->result as $result){ 
								$src = $result->field_field_add_image[0]['raw']['uri'];
								$style = 'events_gallery_380_x_257';
								$src_url = image_style_url($style, $src);
								?>
								<div class="event-landing__grid__item">
                <a href="/events-gallery/videos/<?php print $result->tid; ?>" class="event-landing-item__link">
									<div class="event-landing-item__thumbnail">
										<img src="<?php print $src_url; ?>" alt="">
									</div>
									<div class="event-landing-item__info">
										<h3 class="event-landing-item__info__title"><?php print $result->taxonomy_term_data_name; ?></h3>
										<h4 class="event-landing-item__info__date"><?php print $result->field_field_date[0]['rendered']['#markup']; ?></h4>
									</div>
									</a>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
			<?php } ?>
      <?php //print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>