<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
 $profile_pic = $fields['guid']->raw;
 $default_profile_pic = file_create_url("public://filegPQEFT");
 $phone = !empty($fields['phone_number']->raw) ? $fields['phone_number']->raw : 'n/a';
 $mailStop = !empty($fields['mail_stop']->raw) ? $fields['mail_stop']->raw : 'n/a';
 $email_id = $fields['unixid']->raw . '@gene.com';
?>

<div class="people-item">
  <div class="image">
    <img class="" src="<?php print $profile_pic; ?>" alt="" title="<?php print $fields['preferred_fullname']->raw;?>" onerror="this.onerror=null;this.src='<?php print $default_profile_pic;?>' ;"/>
  </div>
  <div class="content">
    <a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=<?php print $email_id;?>" target="_blank">
      <div class="name">
        <?php print $fields['preferred_fullname']->raw;?> (<?php print $fields['unixid']->raw;?>)
      </div>
    </a>
    <?php print $email_id;?>
  </div>
</div>