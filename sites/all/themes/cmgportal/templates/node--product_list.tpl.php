<?php

/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> role="article">

  <?php print render($title_prefix); ?>
  <?php if (!$page){ ?>
		<?php if($title != ''){ ?>
			<?php print $title_attributes; ?>
				<a href="<?php print $node_url; ?>"><h3 class= "hd-title"><?php print $title; ?></h3></a>
			
		<?php } ?>
		<?php if($title == '')	{ ?>
			<?php  print $title_attributes; ?>
				<h3 class="no-title"></h3>
			
		<?php }?>
		
	<?php } ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="meta submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
			//$markup = '<h3 class="hd-title">'.$title.'</h3>';
			$prod_count = sizeof($node->field_products['und']); 
			if ($prod_count%2 == 0){
				$left = ($prod_count/2);
				 $i = 0;
					$markup =  '<div class="col-wrapper"><ul class="media-list">';
					$node_wrapper = entity_metadata_wrapper('node', $node);
					foreach ($node_wrapper->field_products as $field_collection_wrapper) {					
						$field_collection = $field_collection_wrapper->value();
						$image = image_style_url('product_logo', $field_collection->field_product_logo['und'][0]['uri']);						
						$markup .= '<li class="media"><div class="media-left media-top"><img alt="'.$field_collection->field_product_logo['und'][0]['filename'].'" src="'.$image.'" /></div><div class="media-body entry"><span><strong>Indications:</strong> '.$field_collection->field_product_indications['und'][0]['value'].'</span><span><strong>Managed by:</strong> '.fetch_dept_microsite_link($field_collection->field_managed_by['und'][0]['taxonomy_term']->name).'</span>';
						if(isset($field_collection->field_product_site['und'])){
							$url = $field_collection->field_product_site['und'][0]['url'];
							$link_title = isset($field_collection->field_link['und']) ? $field_collection->field_link['und'][0]['value'] : t('Product Site');
							$markup .= '<a href="'.$url.'">' . $link_title . '</a>';													
						}
						$i++; 	
						$markup .= '</div></li>';
						if ($i == $left){ 
							  $markup .=  '</ul></div><div class="col-wrapper"><ul class="media-list">';
						}						
					}					
					$markup .= '</ul></div>';
				print $markup;
			}	
			else {
			  $left = round($prod_count/2);
				 $i = 0;
					$markup =  '<div class="col-wrapper"><ul class="media-list">';
					$node_wrapper = entity_metadata_wrapper('node', $node);
					foreach ($node_wrapper->field_products as $field_collection_wrapper) {					
						$field_collection = $field_collection_wrapper->value();
						$image = image_style_url('product_logo', $field_collection->field_product_logo['und'][0]['uri']);						
						$markup .= '<li class="media"><div class="media-left media-top"><img alt="'.$field_collection->field_product_logo['und'][0]['filename'].'" src="'.$image.'" /></div><div class="media-body entry"><span><strong>Indications:</strong> '.$field_collection->field_product_indications['und'][0]['value'].'</span><span><strong>Managed by:</strong> '.fetch_dept_microsite_link($field_collection->field_managed_by['und'][0]['taxonomy_term']->name).'</span>';
						if(isset($field_collection->field_product_site['und'])){
							$url = $field_collection->field_product_site['und'][0]['url'];
							$link_title = isset($field_collection->field_link['und']) ? $field_collection->field_link['und'][0]['value'] : t('Product Site');
							$markup .= '<a href="'.$url.'">' . $link_title . '</a>';													
						}
						$i++; 	
						$markup .= '</div></li>';
						if ($i == $left){ 
							  $markup .=  '</ul></div><div class="col-wrapper"><ul class="media-list">';
						}
						
					}					
					$markup .= '</ul></div>';
				print $markup;
			}
				
    ?>
  </div>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</article>