<?php
$results = $variables['element'];
$video_html = $mobile_markup = "";
?>
<section class="section-hero">
	<div class="hero-block">
		<div class="container hero-container">
			<div class="row">
				<div class="col-lg-6 nopadding">
					<!-- Example of Large Block -->
					<?php if(isset($results[0])){ foreach($results[0]['entity']['field_collection_item'] as $key => $val) { ?>
					<?php
					//$img_url = file_create_url(isset($val['field_add_image']) ? $val['field_add_image'][0]['#item']['uri'] : '');
          $img_url= image_style_url('homapagecollage_mainimage', $val['field_add_image'][0]['#item']['uri']);
					if($val['field_type']['#items'][0]['value'] == 'video' && isset($val['field_video'])){
						$video_url = file_create_url($val['field_video'][0]['#item']['uri']);
							$data_targetting = array('class' => array('hero-item-link'),'data-target' => '.hero-video-modal0','data-toggle' => 'modal');
							$video_html .= '
								   <div class="modal fade hero-video-modal hero-video-modal0" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg" role="document">
									  <div class="modal-content">
										<div class="modal-header">
										  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true"></span>
										  </button>
										</div>
										<div class="modal-body">
										  <video id="media-video0" class="video-js">
											<source src="'.$video_url.'" type="video/mp4">
											  <p class="vjs-no-js">
												To view this video please enable JavaScript, and consider upgrading to a web browser that
												<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
											  </p>
											  <div class="play-button"><i class="fas fa-play"></i></div>
										  </video>

										</div>
									  </div>
									</div>
								  </div>';
								  $video_html .=   "<script type='text/javascript'>
     jQuery(document).ready(function() {
        var mediavideo = videojs('media-video0', {
          controls: true,
          autoplay: false,
          preload: 'none',
          fluid: true,
          poster: '".$img_url."',
        });
        jQuery('.hero-video-modal0 .close').click(function() {
          mediavideo.pause();
        })
        jQuery( '.hero-video-modal0' ).on('shown.bs.modal', function(){
          mediavideo.play();
        });
        jQuery( '.hero-video-modal0' ).on('hidden.bs.modal', function(){
          mediavideo.pause();
        });
      });
              </script>";
              
						}else{
							$data_targetting = array('class' => array('hero-item-link'));
						}
					?>
					<div class="hero-item large-block <?php print isset($val['field_color_scheme']['#items'][0]['value']) ? $val['field_color_scheme']['#items'][0]['value']: "color-white"; ?>" style="background-image: url(<?php print $img_url; ?>)">
						<div class="hero-item-bg" style="background-image: url(<?php print  $img_url;?>)"></div>
						<div class="hero-item-info">
						<h2 class="title"><?php print (isset($val['field_collage_links']) && $val['field_collage_links'][0]['#element']['title']!= "" && $val['field_collage_links'][0]['#element']['title']!= $val['field_collage_links'][0]['#element']['display_url']) ? $val['field_collage_links'][0]['#element']['title'] : ''; ?></h2>
            
						<span class="date"><?php print (isset($val['field_date']) && $val['field_date']['#items'][0]['value']!= "") ? date("F j, Y", strtotime($val['field_date']['#items'][0]['value'])) : '';?></span>
						<p class="excerpt"><?php print (isset($val['field_excerpt']) && $val['field_excerpt'][0]['#markup']!= "") ? $val['field_excerpt'][0]['#markup'] : ''; ?></p>
						</div>
						<div class="overlay style="display: block;""></div>
						<?php print l('',(isset($val['field_collage_links']) && $val['field_collage_links'][0]['#element']['display_url']!= "") ? $val['field_collage_links'][0]['#element']['display_url'] : 'javascript:void(0);',array('attributes' => $data_targetting)); ?>
					</div>
					<?php } } ?>
				</div>

				  
				<div class="col-lg-6 nopadding">
					<div class="row">
					    <?php 
							for($i=1; $i<=4 ;$i++){ 
							?>
								<div class="col-md-6 nopadding">
								<?php if(isset($results[$i])){ 
								foreach($results[$i]['entity']['field_collection_item'] as $key => $val) { ?>
								<?php 
								$video_url = "";
								//$img_url = file_create_url(isset($val['field_add_image']) ? $val['field_add_image'][0]['#item']['uri'] : '');
                $img_url= image_style_url('homepage_collage', $val['field_add_image'][0]['#item']['uri']);
								if($val['field_type']['#items'][0]['value'] == 'video' && isset($val['field_video'])){
									$video_url = file_create_url($val['field_video'][0]['#item']['uri']);
									$data_targetting = array('class' => array('hero-item-link'),'data-target' => '.hero-video-modal'.$i.'','data-toggle' => 'modal');
									$video_html .= '
										   <div class="modal fade hero-video-modal hero-video-modal'.$i.'" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document">
											  <div class="modal-content">
												<div class="modal-header">
												  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true"></span>
												  </button>
												</div>
												<div class="modal-body">
												  <video id="media-video'.$i.'" class="video-js">
													<source src="'.$video_url.'" type="video/mp4">
													  <p class="vjs-no-js">
														To view this video please enable JavaScript, and consider upgrading to a web browser that
														<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
													  </p>
													  <div class="play-button"><i class="fas fa-play"></i></div>
												  </video>

												</div>
											  </div>
											</div>
										  </div>';
										$video_html .=   "<script type='text/javascript'>
											 jQuery(document).ready(function() {
												var mediavideo = videojs('media-video".$i."', {
												  controls: true,
												  autoplay: false,
												  preload: 'none',
												  fluid: true,
												  poster: '".$img_url."',
												});
												jQuery('.hero-video-modal".$i." .close').click(function() {
												  mediavideo.pause();
												})
												jQuery( '.hero-video-modal".$i."' ).on('shown.bs.modal', function(){
												  mediavideo.play();
												});
												jQuery( '.hero-video-modal".$i."' ).on('hidden.bs.modal', function(){
												  mediavideo.pause();
												});
											  });
										</script>";
								}else{
									$data_targetting = array('class' => array('hero-item-link'));
								}
								?>
								<!-- Example of Square Block -->
								<div class="hero-item square-block <?php print isset($val['field_color_scheme']['#items'][0]['value'])? $val['field_color_scheme']['#items'][0]['value']: "color-white";?>" style="background-image: url(<?php print  $img_url; ?>)">
								<div class="hero-item-bg" style="background-image: url(<?php print  $img_url; ?>)"></div>
								<div class="hero-item-info">
								<h2 class="title"><?php print (isset($val['field_collage_links']) && $val['field_collage_links'][0]['#element']['title']!= "" && $val['field_collage_links'][0]['#element']['title']!= $val['field_collage_links'][0]['#element']['display_url']) ? $val['field_collage_links'][0]['#element']['title'] : ''; ?></h2>
                <span class="date"><?php print (isset($val['field_date']) && $val['field_date']['#items'][0]['value']!= "") ? date("F j, Y", strtotime($val['field_date']['#items'][0]['value'])) : '';?></span>
								</div>
								<div class="overlay" style="display: block;"></div>
								<?php print l('',(isset($val['field_collage_links']) && $val['field_collage_links'][0]['#element']['display_url']!= "") ? $val['field_collage_links'][0]['#element']['display_url'] : 'javascript:void(0);',array('attributes' => $data_targetting)); ?>
								</div>
								<?php } } ?>
								</div>
						<?php }	?>
					</div>
				</div>
        <a class="view-all-cmg" href="/CMG-lead-stories">View All CMG News</a>
				</div>
		</div>
	</div>
</section>
<section class="hero-section-mobile">
	<div id="mobileHeroCarousel" class="carousel slide" data-ride="carousel">
	  <div class="carousel-inner" role="listbox">
		   <?php $active_class= "active"; ?>
		   <?php for($i=0; $i<=4 ;$i++){  ?>
			<!-- Example of Large Block -->
				<?php if(isset($results[$i])){ foreach($results[$i]['entity']['field_collection_item'] as $key => $val) { 
				$img_url = file_create_url(isset($val['field_add_image']) ? $val['field_add_image'][0]['#item']['uri'] : '');
				
			if($val['field_type']['#items'][0]['value'] == 'image'){ ?>	 
			<div class="carousel-item <?php print $active_class; ?>">				
			  <div class="hero-item <?php print isset($val['field_color_scheme']['#items'][0]['value']) ? $val['field_color_scheme']['#items'][0]['value']: "color-white"; ?>" style="background-image: url(<?php print $img_url; ?>)">
				<div class="hero-item-bg" style="background-image: url(<?php print  $img_url;?>)"></div>
				<div class="hero-item-info">
				  <h2 class="title"><?php print (isset($val['field_collage_links']) && $val['field_collage_links'][0]['#element']['title']!= "" && $val['field_collage_links'][0]['#element']['title']!= $val['field_collage_links'][0]['#element']['display_url']) ? $val['field_collage_links'][0]['#element']['title'] : ''; ?></h2>
				  <span class="date"><?php print (isset($val['field_date']) && $val['field_date']['#items'][0]['value']!= "") ? date("F j, Y", strtotime($val['field_date']['#items'][0]['value'])) : '';?></span>
				  <p class="excerpt"><?php print (isset($val['field_excerpt']) && $val['field_excerpt'][0]['#markup']!= "") ? $val['field_excerpt'][0]['#markup'] : ''; ?></p>
				</div>
				<div class="overlay style="display: block;""></div>
				<a href="<?php print (isset($val['field_collage_links']) && $val['field_collage_links'][0]['#element']['display_url']!= "") ? $val['field_collage_links'][0]['#element']['display_url'] : 'javascript:void(0);';?>" class="hero-item-link"></a>
			  </div>
		  </div> 
			<?php }elseif($val['field_type']['#items'][0]['value'] == 'video' && isset($val['field_video'])){ 
			$title_video = (isset($val['field_collage_links']) && $val['field_collage_links'][0]['#element']['title']!= "") ? $val['field_collage_links'][0]['#element']['title'] : "";
			$mobile_markup .= 	'<div class="hero-item long-block color-white" style="background-image: url(\''.$img_url.'\')">
				  <div class="hero-item-bg" style="background-image: url(\''.$img_url.'\')"></div>
				  <div class="hero-item-info">
					<h2 class="title">'.$title_video.'</h2>
				  </div>
				  <div class="overlay"></div>
				  <a href="javascript:void(0);" class="hero-item-link" data-toggle="modal" data-target=".hero-video-modal'.$i.'"></a>
				</div>';		
				}
			} }
				$active_class = "";
			?>
			
		   <?php } ?>
	  </div>

	  <a class="carousel-control-prev" href="#mobileHeroCarousel" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	  </a>
	  <a class="carousel-control-next" href="#mobileHeroCarousel" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	  </a>
	</div>

<?php print $mobile_markup; ?>
<a class="view-all-cmg" href="javascript:void(0);">View All CMG News</a>
<script type="text/javascript">
/*      jQuery(document).ready(function() {
  jQuery("#mobileHeroCarousel").swiperight(function() {
	 jQuery(this).carousel('prev');
   });
  jQuery("#mobileHeroCarousel").swipeleft(function() {
	 $(this).carousel('next');
  });
  jQuery(document).on("mobileinit", function(){
	  jQuery.mobile.ajaxEnabled=false;
	  jQuery.mobile.loadingMessage = false;
  });
}); */
</script>
</section>

<?php print $video_html; ?>