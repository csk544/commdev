<?php
global $base_url, $user; 
$user = user_load($user->uid);
$notifications_count = 0; 
$user_tags = $deleted_notification_items = array();
if(isset($user->field_deleted_notification_items['und']) && $user->field_deleted_notification_items['und'][0]['value'] != ""){
	$deleted_notification_items = drupal_explode_tags($user->field_deleted_notification_items['und'][0]['value']);
}
if(isset($user->field_follow_tags['und']) && $user->field_follow_tags['und'][0]['value'] != ""){
	$user_tags = drupal_explode_tags($user->field_follow_tags['und'][0]['value']);
}
if(count(variable_get('users_default_tags'))>0){
	foreach(variable_get('users_default_tags') as $default_tags_val){
		if(! in_array($default_tags_val,$user_tags)){
			$user_tags[] = $default_tags_val;
		}
	}
}
$notify_news = variable_get('notify_news');
if(count($notify_news) > 0){
	foreach($deleted_notification_items as $key => $val){
		unset($notify_news[$val]);
	}
}
$notify_news = array_reverse($notify_news, true);
$notifications_count = count($notify_news);
if(variable_get('notice_status')){
	$notifications_count = $notifications_count + 1;
}
$megamenu = mega_menu_html();
$cmgmegamenu = about_cmg_mega_menu_html();
?>

<header>
      <div class="header-top">
        <div class="container header-top-container">
          <div class="row">
            <div class="col-2">
              <div class="gwiz-logo-block">
                <a href="http://gwiz.gene.com" class="gwiz-link">
                  <img class="gwiz-logo-img" src="/sites/all/themes/cmgportal/library/img/gwiz-logo-@2x.png" />
                </a>
              </div>
            </div>
            <div class="col-10">
              <div class="account-block">
			  <?php $warning_class = "";
				if($notifications_count == 0){
					$warning_class = "disabled";
				}
				elseif(isset($_SESSION['my_module'])){
					$warning_class = $_SESSION['my_module']['warning_notice'];
					unset($_SESSION['my_module']);
				} 
				?>
                <div class="notifications <?php print $warning_class; ?>">
                  <span class="notification-icon">
                  <span class="icon icon-Icon_Notification"></span>
                  <?php $active_class = ($notifications_count > 0) ? "active" : "" ;?>
                  <div class="notif-alert <?php print $active_class; ?>"><?php if($notifications_count > 0) { print $notifications_count; } else { print ""; }?>
                  </div>
                  </span>
                  <div class="notifications-flyout fancy-scrollbar <?php print $warning_class; ?>">
                    <div class="notifications-header">
                      <div class="title">Notifications</div>
                      <a href="javascript:void(0);" class="dismiss-all">Dismiss All</a>
                    </div>

                    <div class="notifications-feed fancy-scrollbar">
                      <div class="no-notifs-text">
                        <span class="text">There Are No New Notifications</span>
                      </div>
                      <?php if($notifications_count > 0) {
                        if(variable_get('notice_status')){
													$type = variable_get('notice_type', ''); 
													$notice_title = variable_get('notice_title', '');
													$notice = variable_get('emergency_notice', '');
													$excerpt = strip_tags($notice['value']);
													//$string = substr($excerpt, 0, 50);
													//$string = substr($string, 0, strrpos($string, ' ')) . " ...";
													if($type) { 
														$notification_class = "notif-red";$icon_class ="icon-Icon_Warning-Alert"; 
													} else {
														$notification_class = "notif-yellow";
														$icon_class ="icon-Icon_Notification-Alert";
													}
											?>
                      <div class="notification-item notif-important emergency_notice">
                        <div class="container">
                          <div class="row">
                            <div class="col-2">
                              <div class="notif-type-icon <?php print $notification_class;?>">
                                <span class="icon <?php print $icon_class;?>"></span>
                              </div>
                            </div>
                            <div class="col-10">
                              <div class="notif-item-content">
                                <h6 class="title"><?php print $notice_title; ?></h6>
                                <p class="excerpt"><?php print $excerpt;?></p>
                                  <?php if(!variable_get('notice_type', '')){ ?>
                                    <a href="javascript:void(0);" class="notif-dismiss emergency_noticedismiss"></a>
                                  <?php }?>
                              </div>
                              <span class="notification-category">Alert Notice</span>
                              <!--<span class="notification-time">3h</span>-->
                            </div>
                          </div>
                        </div>
                        <a href="javascript:void(0);" class="notif-item-link"></a>
                      </div>
											<?php }
											if(isset($notify_news) && count($notify_news) > 0){
												foreach($notify_news as $key => $val){
													foreach($user_tags as $user_tag){
														if(in_array($user_tag,$val['tid'])){
															?>
															<div class="notification-item notif-important">
                        <div class="container">
                          <div class="row">
                            <div class="col-2">
                              <div class="notif-type-icon notif-green">
                                <span class="icon icon-Icon_Calendar-Alert"></span>
                              </div>
                            </div>
                            <div class="col-10">
                              <div class="notif-item-content">
                                <h6 class="title"><?php print $val['title']; ?></h6>
                                <p class="excerpt"><?php print $val['teaser'];?></p>
                                  <?php //if(!variable_get('notice_type', '')){ ?>
                                    <a href="javascript:void(0);" class="notif-dismiss newsdismiss" data-nid="<?php print $key; ?>"></a>
                                  <?php //}?>
                              </div>
                              <span class="notification-category">Alert Notice</span>
                              <!--<span class="notification-time">3h</span>-->
                            </div>
                          </div>
                        </div>
                        <a href="node/<?php print $key; ?>" class="notif-item-link"></a>
                      </div>
															<?php
															break;
														}
													}
												}
											}
											} 
                      else  {
                        print '<div class="no-notifs-text">
                          <span class="text">There Are No New Notifications</span>
                        </div>';
                      }
                      ?>
                    </div>
                    <!--<a class="notif-view-all" href="javascript:void(0);">View All</a>-->
                  </div>
                </div>
                <div class="greeting-block color-white">
                  <div class="greeting-text account-flyout-toggle">Hi, <?php print$first_name;?><span class="icon icon-Icon_Down-Carrot-Filled"></span></div>
                  <div class="account-flyout">
                    <ul>
                      <!-- //Authoring Mode Button -->		
                      <?php 		
                      if (stristr($groups, 'drupal_author') || stristr($groups, 'drupal_superuser') || stristr($groups, 'drupal_business_admin') || stristr($groups, 'drupal_it_admin') || stristr($groups, 'drupal_career_learning_admin') || stristr($groups, 'drupal_microsite_creator')) {
                        $redirect = current_path();
                        if (stristr($redirect, 'node') && stristr($redirect, '/edit')) {
                          $redirect = str_replace("/edit", "", $redirect);
                        }
												$authoring_href = "/author_mode?dest=".$redirect;												
                      ?>
                      <li class="flyout-item"><a href="<?php print $authoring_href;?>" class="flyout-link"><span class="icon icon-Icon_Settings"></span><?php print (count($user->roles) > 1) ? " Deactivate" : " Activate";?> Authoring Mode</a></li>
                      <?php } ?>
                    <?php 
                    if ( stristr($groups, 'drupal_service_desk') || stristr($groups, 'drupal_business_admin') || stristr($groups, 'drupal_it_admin') ) {
                      ?>
                      <li class="flyout-item"><a href="<?php print $base_url; ?>/admin/impersonate" class="flyout-link"><span class="icon icon-Icon_Profile"></span>Impersonate a user</a></li>
                      <?php
                    }
                    ?>
                    <li class="flyout-item"><a href="javascript:void(0);" class="flyout-link settingsModalpersonal" data-toggle="modal" data-target="#settingsModal"><span class="icon icon-Icon_Settings"></span>Settings</a></li>
                      <li class="flyout-item"><a href="<?php print $base_url; ?>/user/logout" class="flyout-link"><span class="icon icon-Icon_Power"></span>Log Out</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php if(isset($department_banner_images) && $department_banner_images != "" ){
				$genentech_logo = "/sites/all/themes/cmgportal/library/img/GenentechUS_Logo_White.png";
				 echo '<div class="header-nav" style="background-image: url('.$department_banner_images.')">';
			}elseif(isset($is_usma) && $is_usma){
				 $genentech_logo = "/sites/all/themes/cmgportal/library/img/GenentechUS_Logo_White.png";
				 echo '<div class="header-nav" style="background-image: url(\'/sites/all/themes/cmgportal/library/img/USMA_Header_bg.jpg\')">';
			} else{
				$genentech_logo = "/sites/all/themes/cmgportal/library/img/genentech-logo-3x.png";
				echo '<div class="header-nav">';
			}
			?>
        <div class="container header-nav-container">
          <div class="row">
		  <?php if(isset($department_banner_images) && $department_banner_images != "" ){
			print '<div class="col-lg-4">';
		  }else{
			  print '<div class="col-lg-3">';
		  }
		  ?>
              <div class="logo-block">
                <a href="<?php print $front_page; ?>" class="logo-link">
                  <img class="logo-img" src= "<?php echo $genentech_logo; ?>"/>
                </a>
								<?php
									if(isset($department_white_logos) && $department_white_logos != "" ){ ?>
										<a href="javascript:void(0);" class="interior-logo-link">
										<img class="interior-logo-img" src="<?php print $department_white_logos; ?>" />
									<?php }elseif(isset($is_usma) && $is_usma){ ?>
										<a href="javascript:void(0);" class="interior-logo-link">
										<img class="interior-logo-img" src="/sites/all/themes/cmgportal/library/img/USMA_Logo.png" />
									</a>
									<?php }
								?>
              </div>
			  <div class="mobile-menu-toggle">
                <span class="line-1"></span>
                <span class="line-2"></span>
                <span class="line-3"></span>
              </div>
            </div>
			 <?php if(isset($department_banner_images) && $department_banner_images != "" ){
			print '<div class="col-8">';
		  }else{
			  print '<div class="col-9">';
		  }
		  ?>
            
              <div class="navigation-menu">
                <ul>
                  <li class="menu-item"><a href="javascript:void(0);" class="menu-link has-submenu about-cmg-menu-toggle">About CMG<span class="icon icon-Icon_Down-Carrot-Filled"></span></a></li>
                  <li class="menu-item"><a href="javascript:void(0);" class="menu-link has-submenu departments-menu-toggle">Departments<span class="icon icon-Icon_Down-Carrot-Filled"></span></a></li>
                  <li class="menu-item"><a href="/calender_events" class="menu-link tour-events">CMG Events</a></li>
                  <li class="menu-item"><a href="/tools" class="menu-link tour-tools">Tools</a></li>
                  <li class="menu-item"><a href="/node/<?php print variable_get('career_landing_page');?>" class="menu-link">Career & Learning</a></li>
                  <li class="menu-item menu-search"><a href="javascript:void(0);" class="menu-link"><span class="icon icon-Icon_Search"></span></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="mobile-navigation">
        <div class="mobile-nav-page default" id="primary-page">
          <div class="mobile-nav-page-header">
            <div class="logo">
              <a href="javascript:void(0);" class="logo-link">
                <img class="logo-img" src="/sites/all/themes/cmgportal/library/img/genentech-logo-3x.png" />
              </a>
            </div>
            <div class="mobile-menu-close"><span class="icon icon-Icon_Close"></span></div>
          </div>
          <div class="mobile-search-bar">
						<form action="/solr-search">
								<input name="queryTerm" class="search-input" type="text" placeholder="Search..." required />
								<!--<div class="search-submit">
									<button type="submit">
										<span class="icon icon-Icon_Search"></span>
									</button>
								</div> -->
						</form>
          </div>
          <ul class="mobile-menu">
            <li class="menu-item mobile-submenu-link"><a href="#<?php print 'cmg-comm'.variable_get("about_comm_term_id").'-page'; ?>">About CMG <span class="icon icon-Icon_Right-Carrot"></span></a></li>
            <li class="menu-item mobile-submenu-link"><a href="#departments-page">Departments <span class="icon icon-Icon_Right-Carrot"></span></a></li>
            <li class="menu-item"><a href="/calender_events">CMG Events</a></li>
            <li class="menu-item"><a href="/tools">Tools</a></li>
            <li class="menu-item"><a href="/node/<?php print variable_get('career_landing_page');?>">Career Learning</a></li>
          </ul>
        </div>
		<?php print $megamenu["mobile"]; ?>
       <?php print $cmgmegamenu["mobile"]; ?>
      </div>
      <section id="departments-menu" class="megamenu">
        <div class="container">
					<?php print $megamenu["desktop"]; ?>
        </div>
        <div class="dep-overlay"></div>
      </section>
      <section id="about-cmg-menu" class="about-cmg-megamenu">
        <div class="container">
          <div class="row about-cmg-row">
						<?php 
							for($bucket = 1 ; $bucket<=3 ;$bucket++){
								$about_cmg_title = variable_get('bucket_'.$bucket.'_title', '');
								$about_cmg_link = variable_get('bucket_'.$bucket.'_Link', 'javascript:void(0);');
								$about_cmg_link_text = variable_get('bucket_'.$bucket.'_Link_text', '');
								if(variable_get('bucket_'.$bucket.'_images', '') != ""){
									$about_cmg_uri = db_select('file_managed', 'f')
									->condition('f.fid', variable_get('bucket_'.$bucket.'_images', ''), '=')
									->fields('f', array('uri'))
									->execute()->fetchField();
									if($about_cmg_uri != ""){
										$about_cmg_image = file_create_url($about_cmg_uri);
									}else{
										$about_cmg_image = "/sites/default/files/CMG_Menu_".$bucket.".jpg";
									}									
								}else{
									$about_cmg_image = "/sites/default/files/CMG_Menu_".$bucket.".jpg";
								}
								print '<div class="col-md-4 about-cmg-bucket" style="background-image: url('.$about_cmg_image.')">
									<div class="cmg-title">'.$about_cmg_title.'</div>
									<a href="'.$about_cmg_link.'" class="cmg-button">'.$about_cmg_link_text.'</a>
								</div>';
							}						
						?>
          </div>
          <div class="row about-cmg-row">
              <?php print $cmgmegamenu["desktop"]; ?>                  
          </div>
        </div>
      </section>

      <section id="search-section" class="search-section">
        <div class="container">

          <div class="row">
            <div class="col-12">
              <div class="search-bar">
				<form action="/solr-search">
					<input name="queryTerm" class="search-input" type="text" placeholder="Search News, People, and Events" required />
					<div class="search-submit">
						<button type="submit">
							<span class="icon icon-Icon_Search"></span>
						</button>
					</div>
				</form>

              </div>
            </div>
          </div>
        </div>


      </section>
      
    </header>
		<div id="sb-search" class="sb-search hide">
			<form action="/search-results">
				<!--<form id="gwiz-search">-->
				<input class="sb-search-input form-control input-lg" placeholder="" type="text" value="" name="keyword" id="search" autocomplete="off">
				<input class="sb-search-submit input-lg" type="submit" value="">
				<span class="sb-icon-search"></span>
			</form>
			<!--<div class="search-suggestions-gwiz">
			<div class="search-suggestions__header"><h3 class="pull-left">Note:<span class="pulse"></span> You will be redirected to gWiz Search. Please make sure to open Pulse Secure to see your search results.</h3></div>
			</div>-->
			<!-- search suggestions -->
			<div class="search-suggestions">

			</div>
			<!-- /search suggestions -->

		</div> <!-- /sb-search	 -->