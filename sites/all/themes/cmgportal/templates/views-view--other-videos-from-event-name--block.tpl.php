<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?> 

  <?php if ($rows): ?>
    <div class="view-content">
			<?php //dsm($view->result); ?>
			<?php if(count($view->result)>3){	?>
			<div class="video-strip">
        <div class="event-landing">
					<h3>Other videos from <?php print $view->result[0]->taxonomy_term_data_node_name; ?></h3>
					<div class="event-landing__grid event-landing__grid--video js-video-carousel">
					<?php foreach($view->result as $result) { 
					$src = "public://-text.png";
					$style = 'events_gallery_380_x_257';
					$src_url = image_style_url($style, $src); ?>
						<div class="event-landing__grid__item">
							<div class="event-landing-item__thumbnail">
								<?php
								if($result->field_field_image_preview[0]['raw']['uri']){
									$src = file_create_url($result->field_field_image_preview[0]['raw']['uri']);
                  $video_url = $result->field_field_image_preview[0]['rendered']['#path']['path']; ?>
									<a href="/<?php print $video_url;?>"><img style="width:371px;height:209px;" src="<?php print $src; ?>" alt=""></a>											
								<?php }
								elseif(isset($result->_field_data['nid']['entity']->field_vimeo_file['und']) && $result->_field_data['nid']['entity']->field_vimeo_file['und'][0]['value'] != ""){
									if(isset($result->field_field_vimeo_preview_image_url[0]) && $result->field_field_vimeo_preview_image_url[0  ]['raw']['value'] != ""){
									  ?><img style="width:371px;height:209px;" src="<?php print $result->field_field_vimeo_preview_image_url[0]['raw']['value']; ?>" alt="">
									<?php 
									}else {
										$vimeo_url = $result->_field_data['nid']['entity']->field_vimeo_file['und'][0]['value'];
										$vimeo_id_arr = explode('https://vimeo.com/',$vimeo_url);
										$imgid = stripos($vimeo_id_arr[1],'/');
										if($imgid){
											$vimeo_id = substr($vimeo_id_arr[1],0,$imgid);
										}else{
											$vimeo_id = $vimeo_id_arr[1];
										}
										$vimeo_api_url ="https://api.vimeo.com/videos/".$vimeo_id."/pictures";
										$viemo_result = curl_get($vimeo_api_url);
										if(isset($viemo_result->data) && $viemo_result->data[0]->sizes[2]->link != ""){
											vimeo_url_save($result->nid,$viemo_result->data[0]->sizes[2]->link);
										  ?><img style="width:371px;height:209px;" src="<?php print $viemo_result->data[0]->sizes[2]->link; ?>" alt="">
										<?php 
										}
										else {
										  ?>
											<img src="/sites/default/files/video-capture.PNG" alt="">
										<?php 
										}
									}
							  }
								else{?>
									<img src="/sites/default/files/video-capture.PNG" alt="">
								<?php } ?>
							  <!-- <div class="event-landing-item__video-length">4:10</div>-->
								<a href="/node/<?php print $result->nid; ?>" class="event-landing-item__link event-landing-item__link--video">
								<!--<div class="icon-play"><span></span></div>-->
								</a>
							</div>
							<div class="event-landing-item__info">
								<h3 class="event-landing-item__info__title"><?php print $result->node_title; ?></h3>
							</div>
						</div>
					<?php } ?>
					</div>
				</div>
			
				<div class="video__carousel-controls">
					<a href="#" class="video__carousel-controls__prev"><img class="svg" src="/sites/default/files/assets/icon.carousel.svg" alt=""></a>
					<a href="#" class="video__carousel-controls__next"><img class="svg" src="/sites/default/files/assets/icon.carousel.svg" alt=""></a>
				</div>				
			</div>
				<?php }else{ ?>
				<div class="video-strip nocarousel">
					<div class="event-landing">
						<h3>Other videos from <?php print $view->result[0]->taxonomy_term_data_node_name; ?></h3>
						<div class="event-landing__grid event-landing__grid--video"> 
						<?php foreach($view->result as $result) { ?>						
							<div class="event-landing__grid__item">
								<div class="event-landing-item__thumbnail">
									<?php
										if($result->field_field_image_preview[0]['raw']['uri']){
											$src = file_create_url($result->field_field_image_preview[0]['raw']['uri']);
                      $video_url = $result->field_field_image_preview[0]['rendered']['#path']['path']; ?>
											<a href="/<?php print $video_url;?>"> <img style="width:371px;height:209px;" src="<?php print $src; ?>" alt="">	</a>										
										<?php }
										elseif(isset($result->_field_data['nid']['entity']->field_vimeo_file['und']) && $result->_field_data['nid']['entity']->field_vimeo_file['und'][0]['value'] != ""){
											if(isset($result->field_field_vimeo_preview_image_url[0]) && $result->field_field_vimeo_preview_image_url[0]['raw']['value'] != ""){
												?><img style="width:371px;height:209px;" src="<?php print $result->field_field_vimeo_preview_image_url[0]['raw']['value']; ?>" alt="">
													<?php 
											}else{
												$vimeo_url = $result->_field_data['nid']['entity']->field_vimeo_file['und'][0]['value'];
												$vimeo_id_arr = explode('https://vimeo.com/',$vimeo_url);
												$imgid = stripos($vimeo_id_arr[1],'/');
												if($imgid){
													$vimeo_id = substr($vimeo_id_arr[1],0,$imgid);
												}else{
													$vimeo_id = $vimeo_id_arr[1];
												}
												$vimeo_api_url ="https://api.vimeo.com/videos/".$vimeo_id."/pictures";
												$viemo_result = curl_get($vimeo_api_url);
												if(isset($viemo_result->data) && $viemo_result->data[0]->sizes[2]->link != ""){
													vimeo_url_save($result->nid,$viemo_result->data[0]->sizes[2]->link);
													?><img style="width:371px;height:209px;" src="<?php print $viemo_result->data[0]->sizes[2]->link; ?>" alt="">
													<?php 
												}
												else{
													?>
													<img style="width:371px;height:209px;" src="/sites/default/files/video-capture.PNG" alt="">
												<?php 
												}
											}
										}
										else{?>
											<img src="/sites/default/files/video-capture.PNG" alt="">
										<?php } ?>
									<!--<div class="event-landing-item__video-length">4:10</div>-->
									<a href="/node/<?php print $result->nid; ?>" class="event-landing-item__link event-landing-item__link--video">
									<!--<div class="icon-play"><span></span></div>--></a>
								</div>
								<div class="event-landing-item__info">
									<h3 class="event-landing-item__info__title"><?php print $result->node_title; ?></h3>
								</div>
							</div>
						<?php } ?>
						</div>
					</div>
				</div>
					<?php }?>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>