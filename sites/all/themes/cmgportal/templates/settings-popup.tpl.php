<?php 
global $base_url, $user; 
$followed_obj = $followed_tags = $mylinks = $term_ids = array();
if (isset($userload)) {
	if (isset($userload->field_favorite_links['und'])) {
		$mylinks = drupal_explode_tags($userload->field_favorite_links['und'][0]['value']);
	}
	if (isset($userload->field_follow_tags['und'])) {
		$term_ids = explode(",", $userload->field_follow_tags['und'][0]['value']);						
		$followed_obj = taxonomy_term_load_multiple($term_ids);
		foreach($followed_obj as $term) {								
			$followed_tags[$term->tid] = $term->name;
		}
		asort($followed_tags);
	}
	$tools = array();
	$tools_added = isset($user->field_favorite_tools['und'][0]['value']) ? drupal_explode_tags($user->field_favorite_tools['und'][0]['value']) : array();
	if(count($tools_added) > 0) {
		foreach($tools_added as $k =>$val) {
			$array_val = explode('$#@',$val);
			$tools[$array_val[0]] = $array_val[1];
		}
	}
}

?>

<div class="modal fade settings-modal" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="settingsModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Settings</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-top">
          <div class="name"><?php echo $userload->field_display_name['und'][0]['value']; ?></div>
          <a href="https://sims.gene.com/" class="update-title">Update Your Title (SIMS)<span class="pulse"></span></a>
          <?php if(drupal_is_front_page()){ ?>
					<a href="javascript:void(0);" class="take-tour">Take the CMG Portal Tour</a>
          <?php } ?>
          <a href="javascript:void(0);" class="expand-all" >Expand All</a>
        </div>
        <div id="manageAccordion" role="tablist">
        <?php if($user->uid == 1 || $user->uid == 36 || $user->uid == 39 || $user->uid == 7818 || $user->uid == 13718 || $user->uid == 13677 || $user->uid == 13675){ ?>
          <div class="card">
            <div class="card-header" role="tab" id="manageLinksHeading">
              <h5 class="">
                <a data-toggle="collapse" href="#manageLinks" role="button" aria-expanded="false" aria-controls="manageLinks">
                  Manage My Links
                  <span class="toggle-icon icon-Icon_Right-Carrot"></span>
                </a>
              </h5>
            </div>

            <div id="manageLinks" class="collapse" role="tabpanel" aria-labelledby="manageLinksHeading">
              <div class="card-body">
                <div class="link-url-bar">
                  <div class="mylinks_valid"></div>
                  <input type="text" class="input-field" placeholder="Enter URL (e.g: https://www.google.com)" />
                  <a href="javascript:void(0);" class="add-url">
                    <span class="add-icon icon-Icon_Plus-Circle"></span>
                    <span>Add URL</span>
                  </a>
                </div>
                <div class="added-links">
                  <?php print display_mylinks($mylinks,$tools); ?>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
          <div class="card">
            <div class="card-header" role="tab" id="manageTopicsHeading">
              <h5 class="">
                <a class="collapsed" data-toggle="collapse" href="#manageTopics" role="button" aria-expanded="false" aria-controls="manageTopics">
                  Manage My News Feed
                  <span class="toggle-icon icon-Icon_Right-Carrot"></span>
                </a>
              </h5>
            </div>
            <div id="manageTopics" class="collapse" role="tabpanel" aria-labelledby="manageTopicsHeading">
              <div class="card-body">
                <div class="container">
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="tag-search-bar">
                        <!-- <input type="text" class="input-field" placeholder="Search Tags" /> -->
                        <?php
                          $search_tags_form = drupal_get_form('search_tags_form');
                          print drupal_render($search_tags_form);
                        ?>
                        <a href="javascript:void(0);" class="add-tag">
                          <span class="add-icon icon-Icon_Plus-Circle"></span>
                          <span>Add Tag</span>
                        </a>
                      </div>

                      <div class="tag-select-block"> 
                        <?php print getTagsForm(); ?>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="active-tags-block">
                        <div class="tag-scroll">
                          <ul>
                            <?php	
                              foreach($followed_tags as $key => $value) {	
                                $term = taxonomy_term_load($key);
                                if ($term->vocabulary_machine_name != 'categories') {
                            ?>
                              <i class="tag-item" term="<?php print $key; ?>"><a href="javascript:void(0);" class="remove-tag"><span class="icon-Icon_Close"></span></a><label><?php print $value; ?></label></i>
                            <?php } } ?>
                          </ul>
                        </div>
                      </div>
                    </div> 
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="card">
            <div class="card-header" role="tab" id="manageDepartmentsHeading">
              <h5 class="">
                <a class="collapsed" data-toggle="collapse" href="#manageDepartments" role="button" aria-expanded="false" aria-controls="manageDepartments">
                  Manage My Department
                  <span class="toggle-icon icon-Icon_Right-Carrot"></span>
                </a>
              </h5>
            </div>
            <div id="manageDepartments" class="collapse" role="tabpanel" aria-labelledby="manageDepartmentsHeading">
              <div class="card-body">
                <div class="container">
                  <div class="row">
                    <?php 
										$department_settings_form = drupal_get_form('cmg_custom_department_settings_form');
										print render($department_settings_form);
										?>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>