<?php

/**
 * @file
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
global $user, $base_url;
if($view->name == "nodequeue_8" || $view->name == "nodequeue_7" || $view->name == "nodequeue_9" || $view->name == "nodequeue_10") { 
?>
<?php //print $wrapper_prefix; ?>
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <?php //print $list_type_prefix; ?>
	<ul class="list-clean margin-bottom20">
    <?php foreach ($view->result as $id => $row): 
		
		$allowed_ldapgroups = $row->field_field_role_targeting;
	  $access_permission = check_secured_permissions($allowed_ldapgroups, true);
	  if ($access_permission == TRUE) {
			
			$options = array();
			$vpn_connect = "";
			if($row->node_type == "secured_link" || $row->node_type == "toolbox") {
				if($row->node_type == "secured_link") {
					$options = array('attributes' => array('target'=>'_blank'), 'query' => array('dest' => 'tools'));  
				}
				if(!empty($row->field_field_requires_gene_vpn_) && $row->field_field_requires_gene_vpn_[0]['raw']['value'] == 1) {
					$vpn_connect = '<span class="pulse"></span>';	
				}
			}
			$edit_link = '';
			if(user_access('edit any secured_link content')) {
				$edit_link = '<a class="icon-builder-edit" title="Edit Tool" href="'.$base_url.'/node/'.$row->nid.'/edit?destination=/tools"></a>';
			}
			else {
				if($row->node_uid == $user->uid) {
					if(user_access('edit own secured_link content')) {
						$edit_link = '<a class="icon-builder-edit" title="Edit Tool" href="'.$base_url.'/node/'.$row->nid.'/edit?destination=/tools"></a>';
					}
				}
			}
	 
			if($row->node_type == "downloads") { ?>
				<li>
				<?php 
				$document_fid = db_select('field_data_field_document', 'd')
					->fields('d',array('field_document_fid'))
					->condition('bundle', 'downloads','=')
					->condition('entity_type', 'node', '=')
					->condition('entity_id', $row->nid, '=')
					->execute()->fetchField();
				$document = file_load($document_fid);
				$options = array('attributes' => array('target'=>'_blank'));  
				print l($row->node_title, file_create_url($document->uri), $options) . $vpn_connect . $edit_link; ?></li>
			<?php }
						else { ?>  
				<li><?php print l($row->node_title, "node/".$row->nid, $options) . $vpn_connect . $edit_link; ?></li>
			<?php }
			} ?>  
    <?php endforeach; ?></ul>
  <?php //print $list_type_suffix; ?>
<?php //print $wrapper_suffix; ?>

<?php
} else {
?>
<?php print $wrapper_prefix; ?>
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <?php print $list_type_prefix; ?>
    <?php foreach ($rows as $id => $row): ?>
      <li class="<?php print $classes_array[$id]; ?>"><?php print $row; ?></li>
    <?php endforeach; ?>
  <?php print $list_type_suffix; ?>
<?php print $wrapper_suffix; ?>
<?php
}
?>