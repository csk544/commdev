<?php

/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> role="article">

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2 <?php print $title_attributes; ?>>
      <a href="<?php print $node_url; ?>"><?php print $title; ?></a>
    </h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="meta submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
	  $data = count($field_org_chart_content)>0 ? unserialize($field_org_chart_content[0]['value']) : array();
	  $associates = isset($data['associates']) ? $data['associates'] : array();
	  $admins = isset($data['admins']) ? $data['admins'] : array();
	  $openPositions = isset($data['openPositions']) ? $data['openPositions'] : array();
	  $total = count($associates);
	  $open = count($openPositions);
	  $unixIds = array_diff($associates, array('Open'));
	  $unixIdsDetails = fetch_details_by_unixids($unixIds);
	  require_once(drupal_get_path('module', 'commportal_org_chart') . '/commportal_org_chart.inc');
	  $orgForm = drupal_get_form('commportal_org_chart_form', $associates, $admins, $unixIdsDetails);
	  $associatesData = isset($data['associatesData']) ? $data['associatesData'] : array();
    ?>
	<div class="org-chart-div-container">
	<div class="row margin-top40">

    <!-- ======================== -->
    <!-- ORG CHART BLOCK -->
    <!-- ======================== -->
    <div class="col-md-12">
      <div class="content org-chart-container">
        <div class="org-chart-header">
          <!--<div class="org-chart-title">
            <p class="h1">Org Chart Div</p>
          </div>-->
          <div class="department-info">			
            <div class="department-total"><?php echo t("Department Total: ") . $total;?></div>
            <div class="open-positions"><?php echo t("Open Positions: ") . $open;?></div>
          </div>
          <div class="clearfix"></div>
          <div class="org-chart-key">
			<div class="cursor add-person open-modal-link" href="#org-form">+ <?php echo t("Add Person"); ?></div>
            <div class="org-chart-color-code-container">
              <div class="org-chart-color-code regular-key"><?php echo t("Regular"); ?></div>
              <div class="org-chart-color-code admin-key"><?php echo t("Admin"); ?></div>
              <div class="org-chart-color-code open-key"><?php echo t("Open Position"); ?></div>
              <div class="org-chart-color-code contractor-key"><?php echo t("Contractor"); ?></div>
            </div>
          </div>
        </div>

        <div class="clearfix"></div>
		<div class="mfp-hide white-popup org-chart-form" id="org-form">
			<div class="show-org-messages">
			</div>
			<h3 class="hd-title"><?php echo t("Add / Update Person");?></h3>
			<?php
				echo render($orgForm);
			?>
		</div>
		<?php if(count($associatesData)>0):?>
		<?php 
			$i=0;
			$associateData = $associatesData[1];
			$directorClass = ($i==0) ? "director" : "";
			$class = '';
			$class .= ($associateData['isOpen']) ? "open-position" : "";
			$adminsInfo = isset($associateData['admin']) ? $associateData['admin'] : array();
		?>
        <div class="department-group <?php echo $class;?>">
          <div class="department-group-container">
            <div class="employee-container cursor <?php echo $directorClass;?>" data-index="1">
				<?php
					$unixIdsDetails[$associateData['unixId']]->FuncTitle = $associateData['FuncTitle'];
					$unixIdsDetails[$associateData['unixId']]->isOpen = $associateData['isOpen'];
					echo theme('employee_container',array('data'=>$unixIdsDetails[$associateData['unixId']]));
				?>              
				<?php if(count($adminsInfo)>0): ?>
					<div class="admin">
						<?php foreach($adminsInfo as $adminIndex => $adminInfo):?>              
						<div class="admin-left-attachment">
							<?php
							$adminContainerClass = ($unixIdsDetails[$adminInfo['unixId']]->manager_unixid != $data['associates'][$associatesData[$adminIndex]['parent']]) ? "diff-manager" : "";
							?>
							<div class="employee-container cursor <?php echo $adminContainerClass;?>" data-index="<?php echo $adminIndex; ?>">
								<?php
									$unixIdsDetails[$adminInfo['unixId']]->FuncTitle = $adminInfo['FuncTitle'];
									$unixIdsDetails[$adminInfo['unixId']]->isOpen = $adminInfo['isOpen'];
									echo theme('employee_container',array('data'=>$unixIdsDetails[$adminInfo['unixId']]));
								?>                    
							</div>
						</div>
						<?php endforeach;?>
					</div>
				<?php endif; ?>
            </div>
            
          </div>        
		</div>
		<?php 
		if(isset($associateData['sub']) && count($associateData['sub'])>0){
			echo prepareOrgChart($data, $associatesData, $associateData['sub'], $unixIdsDetails);
		}
		?>
		<?php endif;?>
      </div>      <div class="clearfix"></div>
    </div>

    <!-- ======================== -->
    <!-- //END ORG CHART -->
    <!-- ======================== -->

  </div><!-- row -->
  </div>
  </div>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</article>
