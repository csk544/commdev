<?php
global $base_url;
$path_to_theme = drupal_get_path('theme', 'commportal');
?>
<!DOCTYPE html>
<!--[if IE 8 ]><html lang="en" class="ie8"><![endif]-->
<!--[if IE 9 ]><html lang="en" class="ie9"><![endif]-->
<!--[if lte IE 9 ]><html lang="en" class="lte-ie9"><![endif]-->
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>
<head profile="<?php print $grddl_profile; ?>">
  <?php print $head; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="MobileOptimized" content="width" />
    <meta name="HandheldFriendly" content="true" />
    <meta http-equiv="cleartype" content="on" />
  <title><?php print $head_title; ?></title>
  
  <?php print $styles; ?>
	<!--[if lt IE 9]>
	<script src="<?php print $base_path . $path_to_resbartik; ?>/js/jquery/jquery.1.10.1.min.js"></script>
	<![endif]-->
	<!--[if gte IE 9]><!-->
	<script src="<?php print $base_path . $path_to_resbartik; ?>/js/jquery/jquery.2.1.3.min.js"></script>
	<!--<![endif]-->
	<!--
	<?php if(arg(0) == "media-archive") { ?>
    <script type="text/javascript" src="<?php print $base_path . $path_to_resbartik; ?>/assets/js/min/base-min.js"></script>	
	<?php } ?>-->
  <?php print $scripts; ?>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if (gte IE 6)&(lte IE 8)]>
    <script src="<?php print $base_path . $path_to_resbartik; ?>/js/selectivizr-min.js"></script>
  <![endif]-->
  <!--[if lt IE 9]>
    <script src="<?php print $base_path . $path_to_resbartik; ?>/js/html5-respond.js"></script>
  <![endif]-->
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <div id="skip-link">
    <a href="#main-content" class="element-invisible element-focusable"><?php // print t('Skip to main content'); ?></a>
  </div>
  <?php if(isset($_GET['showonlycontent']) && $_GET['showonlycontent'] == 'yes'): ?>
<?php print $page; ?>
<?php else: ?>
  <?php print $page_top; ?>
  <?php print $page; ?>
  <?php print $page_bottom; ?>
<?php endif; ?>
</body>
</html>