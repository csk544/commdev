<?php
global $base_url, $user, $theme_path;
$user = user_load($user->uid);
$news_dept_active = "hide";
$following_dept_active = "active";
if(!empty($user->field_user_department['und'][0]['target_id'])){
	$term = taxonomy_term_load($user->field_user_department['und'][0]['target_id']);
	if(empty($term->field_term_alias)){
		$news_dept = $term->name;
	}else{
		$news_dept = $term->field_term_alias['und'][0]['value'];
	}
	$news_dept_active = "active";
	$following_dept_active = "";
}

?>
 <!-- Begin Page -->
  <div class="page home-page">

    <!-- Begin Header -->
		 <?php echo theme('header', array('page' => $page,'first_name' => $first_name, 'groups' => $groups,'front_page' => $front_page)); ?>
    <!-- End Header -->

    <!-- Begin Main Content -->
    <div class="main-container">

      <!-- Begin Hero Section -->
      <?php //print render($page['featured']); 
        $node = node_load(variable_get('homepage_collage_nid'));
        $node_view = node_view($node);
        $rendered_node = drupal_render($node_view); 
        echo $rendered_node; 
	  ?>
      <!-- End Hero Section -->
			<?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
  <?php endif; ?>
      <!-- Begin News Section -->
      <section class="section-news">
        <div class="container news-container">
          <div class="row news-row">
            <div class="col-lg-8 col-md-12 no-padding">
              <div class="news-block">
                <div class="row">
                  <div class="col-md-4">
                    <!-- News Sidebar -->
                    <div class="news-sidebar">
                      <h2 class="news-heading"><?php print$first_name;?>'s Dashboard</h2>
                      <div class="news-menu">
                        <ul>
                          <li class="news-menu-item">
                          <a href="javascript:void(0);" class="news-menu-link <?php print $news_dept_active; ?>" data-menu-target="department">
                            <?php print $news_dept;   ?>
                          </a></li>
                          <li class="news-menu-item"><a href="javascript:void(0);" class="news-menu-link <?php print $following_dept_active; ?>" data-menu-target="following">Following</a></li>
                          <li class="news-menu-item"><a href="javascript:void(0);" class="news-menu-link" data-menu-target="people">People</a></li>
                          <li class="news-menu-item"><a href="javascript:void(0);" class="news-menu-link" data-menu-target="training">Training</a></li>
                        </ul>
                        <div class="pill-slider">
                          <div class="pill">
                          </div>
                        </div>
                      </div>
                      <div class="news-menu-mobile">
                        <select class="news-drop-down" data-role="none">
												  <?php if($news_dept_active == "active"){ ?>
                          <option value="department"><?php print $news_dept; ?></option>
													<?php } ?>
                          <option value="following">Following</option>
                          <option value="people">People</option>
                          <option value="training">Training</option>
                        </select>
                      </div>
                    </div>
                    <!-- End News Sidebar -->

                  </div>
                  <div class="col-md-8">

                    <!-- News Container (this section houses the department, people, and training blocks) -->
                    <div class="news-feed-container fancy-scrollbar">

                      <!-- Begin Department Block -->
                      <div class="news-dynamic department-block <?php print $news_dept_active; ?>">
                        <?php print render($page['homepage_section1']); ?>
                      </div>
                      <!-- End Department Block -->

                      <!-- Begin Following Block -->
                      <div class="news-dynamic following-block <?php print $following_dept_active; ?>">
                        <?php print render($page['homepage_section3']); ?>
                      </div>
                      <!-- End Following Block -->
                      
                      <!-- Begin People Block -->
                      <div class="news-dynamic people-block">
                         <?php print render($page['cmghomepage_left_section']); ?>
                      <!-- <div class="applause-block">
                          <div class="applause-title">
                            <h4>Applause</h4>
                            <span class="view-all">View All</span>
                          </div>
                          <div class="applause-feed">
                            <ul>
                              <li class="applause-feed-item"><a href="javascript:void(0);" class="applause-feed-link">One Item Title For the Genentech US Applause<img src="/sites/all/themes/cmgportal/library/img/right-arrow.svg" onerror="this.onerror=null; this.src='image.png'"></a></li>
                              <li class="applause-feed-item"><a href="javascript:void(0);" class="applause-feed-link">Two Item Title For the Genentech US Applause its lorem ipsum ag...<img src="/sites/all/themes/cmgportal/library/img/right-arrow.svg" onerror="this.onerror=null; this.src='image.png'"></a></li>
                              <li class="applause-feed-item"><a href="javascript:void(0);" class="applause-feed-link">Three Item Title For the Genentech US Applause Lorem Ipsum<img src="/sites/all/themes/cmgportal/library/img/right-arrow.svg" onerror="this.onerror=null; this.src='image.png'"></a></li>
                            </ul>
                          </div>
                        </div> -->
                      </div>
                      <!-- End People Block -->

                      <!-- Begin Training Block -->
                     <div class="news-dynamic training-block">
                        <!-- <div class="your-training-block">
                          <div class="training-title">
                            <h4>Your Trainings</h4>
                            <span class="view-all"><a href="https://oneportal.roche.com" target="_blank">Go to CHRIS</a></span>
                          </div>-->
                          <?php print render($page['homepage_section2']); ?>
                        <!-- </div>-->
                      </div>
                      <!-- End Training Block -->


                    </div>
                    <!-- End News Container -->


                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-12 no-padding">
              <!-- Links Section -->
              <div class="links-block">
                <div class="links-heading">
                  <h3 class="">Tools & Links</h3>
                  <span class="view-all"><a href="/fav_links_more">View All</a></span>
                </div>
				<div class="links-feed-container">
					<div class="links-feed">
						<?php print get_cmg_favorite_links(); ?>
					</div>
					<script type="text/javascript">
          /*(function($){
            $("#linksCarousel").swipeleft(function() {
						 $(this).carousel('next');
					   });
					  $("#linksCarousel").swiperight(function() {
						 $(this).carousel('prev');
					  });
          });*/
					
          
					</script>
				</div>
              </div>
              <!-- End Links Section -->


            </div>
          </div>
        </div>
      </section>
      <!-- End News Section -->

      <!-- Begin Footer -->
      <footer>
        <?php if(isset($page['footer'])): ?>
        <?php print render($page['footer']); ?>
        <?php endif;?>
      </footer>      <!-- End Footer -->
    </div>
  </div> 
  <?php if ($user->uid) { echo theme('settings_popup', array('page' => $page, 'userload' => $user)); } ?>
	  <div class="modal fade tour-modal" id="tourModal" tabindex="-1" role="dialog" aria-labelledby="settingsModal" aria-hidden="true">


    <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="tour-modal-header">
        <div class="logo"><img src="/sites/all/themes/cmgportal/library/img/GenentechUS_Logo_White.png"></div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="tour-modal-body">
        <div class="tour-welcome tour-modal-slide">
          <div class="title">Welcome to the CMG Portal!</div>
          <p>Our new site brings together the former Commercial Portal (comm.gene.com) and USMA Portal (usma.gene.com). Now all CMG information and tools are in one place.</p>

          <p><b>Be sure to bookmark this page!</b></p>
          <p>The easiest way to find what you're looking for is to use our Search. <span class="icon icon-Icon_Search"></span></p>
          <p class="note">IMPORTANT: Note our address has changed. If you have bookmarks pointing to sites within comm.gene.com or usma.gene.com, please update them.</p>

          <a class="start-tour" href="javascript:void(0);">Take the Tour Now</a>
          <a class="skip-tour" href="javascript:void(0);">Skip</a>
        </div>

        <div class="tour-skip-confirm tour-modal-slide">
          <div class="title">Skip Tour</div>
          <p>You can go to your <b><i>Settings</i></b> to take the tour anytime.</p>

          <a class="skip-close" href="javascript:void(0);" data-dismiss="modal" aria-label="Close">Close</a>
        </div>

        <div class="tour-thanks tour-modal-slide">
          <div class="title">Thank you for taking the tour!</div>
          <p>You can go to your <b><i>Settings</i></b> to take the tour again at anytime.</p>

          <a class="skip-close" href="javascript:void(0);" data-dismiss="modal" aria-label="Close">Close</a>
        </div>

      </div>
    </div>
  </div>
  </div>