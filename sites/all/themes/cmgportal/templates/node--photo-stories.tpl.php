<?php
global $user,$base_url;
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> role="article">

  <?php print render($title_prefix); ?>
  <?php if (!$page){ ?>
		<?php if($title != ''){ ?>
			<?php print $title_attributes; ?>
				<!--<a href="<?php //print $node_url; ?>">--><h3 class= "hd-title"><?php print $title; ?></h3><!--</a>-->
			
		<?php } ?>
		<?php if($title == '')	{ ?>
			<?php  print $title_attributes; ?>
				<h3 class="no-title"></h3>
			
		<?php }?>
		
	<?php } ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="meta submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
			//print render($content);
      hide($content['comments']);
      hide($content['links']);
			$lang = $node->language;
			$output = '';
			
			$field_collection_value = entity_load_single('field_collection_item',$node->field_add_your_story['und'][0]['value']);	
			$text = $field_collection_value->field_html[$lang][0]['value'];
			$output .= '<div class="module">';
			if($page){
				$output .=	'<h3 class="hd-title">'.$node->title.'</h3>';
			}
			$output .= '<ul class="media-list"><li class="media">';
			if(isset($field_collection_value->field_photo_story_image[$lang][0]['uri'])) {
				$output .= '<div class="photo-stories_resize_image"><img class="media-object photo-story-image" src="'.file_create_url($field_collection_value->field_photo_story_image[$lang][0]['uri']).'" alt="'.$field_collection_value->field_photo_story_image[$lang][0]['filename'].'"></div>';
			}
			if($view_mode != "teaser"){
				$output .= '<div class="media-body entry"><p>'.$text.'</p>';
			}else {
				$text_strip = strip_tags(nl2br($text));
				$text_with_elipses = array();
				$text_with_elipses = explode("$#strip",wordwrap($text_strip,130,".....$#strip"));
				$pageurl = l('<span class="more-story photo_stories_teaser_more">more</span>', "node/".$node->nid,array('attributes' => array('class' => array('view-more')),'html' => TRUE));
				$output .= '<div class="entry"><p>'.$text_with_elipses[0].''.$pageurl.'</p>
				<div class="text-right">';
				
				$output .= '</div>';
			}
			$photo_storyform = drupal_get_form('add_photo_story_form');
			$form_output = drupal_render($photo_storyform);
			$output .= '<div class="announcement-dept"><a href="#popup_add_photo_story_form" class="open-modal-link view-more">Submit a Photo Story</a><div class="white-popup mfp-hide" id="popup_add_photo_story_form">'.$form_output.' </div></div>';
			$tid = $node->field_channel['und'][0]['tid'];
			$nid = db_query("SELECT n.nid FROM {node} n INNER JOIN {field_data_field_microsite_builder} bi on n.nid = bi.entity_id INNER JOIN {field_data_field_microsite_subnav} sn ON n.nid = sn.entity_id INNER JOIN {field_data_field_microsite_miscellaneous} sm ON n.nid = sm.entity_id INNER JOIN {taxonomy_index} ti ON ti.nid = bi.entity_id AND ti.nid = sn.entity_id AND ti.nid = sm.entity_id WHERE ti.tid = :tid AND sn.field_microsite_subnav_value = 0 AND sm.field_microsite_miscellaneous_value = 0 AND n.type=:type AND n.status = 1", array(":tid" => $tid, ':type' => 'microsite'))->fetchField();
      
			$href = 'photo-stories/'.$tid.'/'.$nid;	
      $view_all = l('View All<span class="icon icon-triangle-right"></span>',$href,array('attributes' => array('class' => array('view-more')),'html' => TRUE));
			$output .=	'<div class="text-right">'.$view_all.'</div>';			
			$output .= '</div></li></ul></div>'
    ?>
  </div>
	<?php print $output; ?>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</article>
