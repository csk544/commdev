<?php

/**
 * @file
 * Theme the more link.
 *
 * - $view: The view object.
 * - $more_url: the url for the more link.
 * - $link_text: the text for the more link.
 *
 * @ingroup views_templates
 */
?>
<?php if(($view->name == "commercial_news" && $view->current_display == "home_page_block") || ($view->name == 'kudos' && $view->current_display == "block_home") || ($view->name == 'my_topics_feed' && $view->current_display == "block")){ ?>
	<span class="view-all"><a href="<?php print $more_url; ?>"><?php print $link_text; ?></a></span>
<?php } else {
	?>
<div class="text-right view-more-container">
	<a href="<?php print $more_url ?>" class="view-more"><?php print $link_text; ?><span class="icon icon-triangle-right"></span></a>
</div>

<?php } ?>
