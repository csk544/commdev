<?php
global $base_url, $user, $theme_path; 
$userload = user_load($user->uid);
$img_path = image_style_url('thumbnail', isset($userload->picture->uri) ? $userload->picture->uri : 'public://pictures/no-profile-pic.jpg');
$sideColumn = '';
$pageColumn = 'col-md-12';
if($page['sidebar_first'] || $page['sidebar_second']) {
	$pageColumn = 'col-md-9';
	if($page['sidebar_first']) {
		$pageColumn .= ' col-md-push-3';
	}
	$sideColumn = 'col-md-pull-9';
}
if($page['sidebar_first'] && $page['sidebar_second']) {
	$pageColumn = 'col-md-6 col-md-push-3';
	$sideColumn = 'col-md-pull-6';
}
?>
<?php if(isset($_GET['showonlycontent']) && $_GET['showonlycontent'] == 'yes'): ?>
<?php
print render($page['content']);
?>
<?php else:
?>
<div class="page interior-page <?php if(isset($attribute_class_page)){ echo $attribute_class_page; }?>">
			    <?php 
		if(!isset($is_usma)){
				$is_usma = 0;
			}
		echo theme('header', array('page' => $page,'first_name' => $first_name, 'groups' => $groups,'front_page' => $front_page,'is_usma' => $is_usma)); ?>
<div class="main-container">
		<!-- Begin subheader section -->
    <?php print $breadcrumb; ?>
      <!-- End subheader -->	
		
		<?php if(!isset($show_featured) || ($show_featured == TRUE)):?>
		<?php if ($page['featured']): ?>
		<div id="featured"><div class="section clearfix">
		<?php print render($page['featured']); ?>
		</div></div> <!-- /.section, /#featured -->
		<?php endif; ?>
		<?php endif; ?>
		
   <section class="main-content">
        <div class="container main">
		<?php if ($page['highlighted']): ?>
		<div id="highlighted"><div class="section clearfix">
		<?php print render($page['highlighted']); ?>
		</div></div> <!-- /.section, /#highlighted -->
		<?php endif; ?>
	  
    <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
  <?php endif; ?>	  

	<div class="row">	
		
    <div id="content" class="column <?php print $pageColumn; ?>">
		<div class="section">
			<?php print render($title_prefix); ?>
			<?php if ($title): ?>
			<h1 <?php print $title_attributes; ?> id="page-title">
			<?php print $title; ?>
			</h1><div class="hidden-md hidden-lg search-anchor"><a class="btn btn-block btn-primary" href="#search-tools"><span class="icon icon-search"></span> Search Tools</a></div>
			<?php endif; ?>
			<?php print render($title_suffix); ?>
			
		  <?php if ($tabs): ?>
			<div class="tabs">
			  <?php print render($tabs); ?>
			</div>
		  <?php endif; ?>
				<?php if(!isset($show_help) || ($show_help == TRUE)):?>
		  <?php print render($page['help']); ?>
				<?php endif; ?>
		  <?php if ($action_links): ?>
			<ul class="action-links">
			  <?php print render($action_links); ?>
			</ul>
		  <?php endif; ?>
				<?php if(!isset($show_content) || ($show_content == TRUE)):?>
		  <?php print render($page['content']); ?>
				<?php endif; ?>
		  <?php print $feed_icons; ?>
		</div>
	</div> <!-- /.section, /#content -->
	
	  <?php if(!isset($show_sidebar_first) || ($show_sidebar_first == TRUE)):?>
    <?php if ($page['sidebar_first']): ?>
      <div id="search-tools" class="column col-md-3 sidebar <?php print $sideColumn; ?>"><div class="section">
        <?php print render($page['sidebar_first']); ?>
      </div></div> <!-- /.section, /#sidebar-first -->
    <?php endif; ?>
    <?php endif; ?>
	
		<?php if(!isset($show_sidebar_second) || ($show_sidebar_second == TRUE)):?>
    <?php if ($page['sidebar_second']): ?>
      <div id="sidebar-second" class="column col-md-3 sidebar"><div class="section">
        <?php print render($page['sidebar_second']); ?>
      </div></div> <!-- /.section, /#sidebar-second -->
    <?php endif; ?>
    <?php endif; ?>
	</div>
	
  </div></section> <!-- /#main, /#main-wrapper, /#page -->
 <!-- Begin Footer -->
      <footer>
        <?php if(isset($page['footer'])): ?>
        <?php print render($page['footer']); ?>
        <?php endif;?>
      </footer>
      <!-- End Footer -->
</div>
</div><!-- /#page-wrapper -->
<?php endif; ?>
<?php if ($user->uid) { echo theme('settings_popup', array('page' => $page, 'userload' => $userload)); } ?>