<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
 
  global $user;
  $tags = drupal_explode_tags($user->field_follow_tags['und'][0]['value']);

  $following_tags = array();
  foreach($row->field_field_tags as $key_tag => $val_tag){
    if(in_array($val_tag['raw']['tid'], $tags)){
      $following_tags[] = $val_tag['rendered']['#title'];
    }
  }

?>
<article class="fav-links-item">
  <div class="item-content">
    <div class="title">
      <?php print l(strip_tags($fields['title']->content), 'node/' . $row->nid, array('absolute' => TRUE)); ?>
    </div>
    <div class="date">
      <?php print date("M.j Y", $row->node_created); ?>
      <span class="news-feed-source"><?php print implode(", ", $following_tags); ?></span>
    </div>
    <div class="excerpt">
      <p>
        <?php print $fields['field_teaser']->content; ?>
      </p>
    </div>
  </div>
</article>