<?php
global $user,$base_url;
if ($user->uid > 0) {
	//$email_article = "https://mail.google.com/mail/?view=cm&to=". $user->mail . "&su=" . $node->title . '&body=' . $base_url . $node_url;
	$share_article = "https://plus.google.com/share?url=". $base_url . '/node/' . $node->nid;	
}
if($view_mode != 'full' && !$teaser) {
	print render($title_prefix);
	if ($title) {
		print '<h1 class="hd-title no-border-top" id="page-title">'.$title.'</h1>';
	}
	print render($title_suffix);
}
$dept_tid = $node->field_channel["und"][0]["tid"];

$next_nid = db_query("SELECT f.entity_id FROM {field_data_field_channel} f where f.bundle = :type and f.field_channel_tid = :tid and f.entity_id < :nid ORDER BY f.entity_id  DESC LIMIT 0,1", array(":nid" => $node->nid, ':type' => 'mcco_stories',':tid' => $dept_tid))->fetchField();
$previous_nid = db_query("SELECT f.entity_id FROM {field_data_field_channel} f where f.bundle = :type and f.field_channel_tid = :tid and f.entity_id > :nid ORDER BY f.entity_id  ASC LIMIT 0,1", array(":nid" => $node->nid, ':type' => 'mcco_stories',':tid' => $dept_tid))->fetchField();
$landing_page_nid = db_select('field_data_field_channel', 'c')
				->fields('c', array('entity_id'))
				->condition('c.field_channel_tid', $node->field_channel['und'][0]['tid'], '=')
				->condition('c.bundle', 'microsite', '=')
				->condition('c.deleted', 0, '=')
				->range(0,1)
				->execute()
				->fetchField();
$color = db_query("SELECT f.field_secondary_color_value  FROM {field_data_field_secondary_color} f WHERE f.bundle = :type AND f.entity_id = :nid LIMIT 0,1", array(":nid" => $landing_page_nid, ':type' => 'microsite'))->fetchField();
if(!isset($color) || $color == ""){
	$color = '#b2bb21';
}
?>
<article id="node-<?php print $node->nid; ?>" class="entry <?php if($view_mode == 'full') { ?> border-top-primary border-bottom-primary <?php } ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?> role="article">
  <?php print render($title_prefix); ?>
  <?php //if (!$page): ?>
    <!--<h2<?php //print $title_attributes; ?>>
      <a href="<?php //print $node_url; ?>"><?php //print $title; ?></a>
    </h2>-->
  <?php //endif; ?>
  <?php print render($title_suffix); ?>
	<?php if($view_mode == 'full') { ?>	
	<div class="article-actions border row">
	<div class="article-meta col-lg-7 no-padding"> 
	<?php 
		//$href_link = $base_url."/commercial-news";
		//$query_string = "?field_category[]=".@$node->field_category[$node->language][0]['taxonomy_term']->tid;
	?>
	  <div class="article-meta__date-author">
		<span class="meta-date"><?php print date('m.d.Y', $node->created); ?></span>
		<span>Posted by <?php print $node->name; ?></span>		
	  </div>
	</div>
	<!--PAGE ACTIONS -->
	<div class="page-actions col-lg-5">
		<div class="action-rating  rate_like">
		 <?php if (isset($node->rate_like)) { print $node->rate_like['#markup']; }?>				
		</div>		
	  <div class="action-rating">
		<?php if (isset($node->rate_vote)) { print $node->rate_vote['#markup']; }?>				
	  </div>
	  <div class="action-icons">
		<a href="javascript:window.print();" class="icon-alone">
		  <span class="icon-printer-1" aria-hidden="true"></span>
		  <span class="screen-reader-text">Print this Page</span>
		</a>		
		<a href='#mail-modal' class="open-modal-link icon-alone">
		  <span class="icon-mail" aria-hidden="true"></span>
		  <span class="screen-reader-text">Email this Page</span>
		</a>
		<a href='<?php print $share_article?>' class="icon-alone" onclick="return false;">
		  <span class="icon-gplus" aria-hidden="true"></span>
		  <span class="screen-reader-text">Share in Google+</span>
		</a>
		<a class="count-shares btn-info btn-icon">
		 <span><?php print $gplus_count;?></span>
		</a>              
	  </div>
	</div>   
	<!--PAGE ACTIONS-->
  </div>	
 
	<?php } ?>	  

  <!--/ARTICLE META--> 
	<?php 
	if($teaser) { ?>
		<h3 class="hd-title"><?php print $title ?></h3>
		<?php if (!empty($content['field_mcco_story_image'])) { ?> <div class="alignleft"><?php  print render($content['field_mcco_story_image']); ?> </div> <?php } ?>
		<div class="content clearfix"<?php print $content_attributes; ?>>
		<?php 
			print render($node->field_teaser['und'][0]['value']);
		?>
		</div>
		<div class="text-right">
		<?php print l(t('Read More'), "node/".$node->nid, array('attributes' => array('rel' => array('tag'), 'title' => $title, 'class' => array('view-more')))); ?>
		</div>
	<?php }
	else {
		if (!empty($content['field_mcco_story_image'])) {
	?> 
	<div class="alignleft"><?php  print render($content['field_mcco_story_image']); ?> </div> 	
	<?php }	?> 
	<div class="content clearfix"<?php print $content_attributes; ?>>
		<?php print render($content['body']); ?>
	 </div>	
		<div class="col-md-12" style="clear:both;">
			 <div style="float:left;">	
					<?php if(isset($previous_nid) && $previous_nid != ""){ ?>
						<a href='/node/<?php print $previous_nid?>'>
							<span class="mcco-prev" style="color:<?php print $color;?>;"><span class="icon icon-triangle-left"></span>Previous</span>
							<span class="screen-reader-text">Previous Photo story</span>
						</a>
					<?php } ?>
				</div>
				<div style="float:right;">
				<?php if(isset($next_nid) && $next_nid != ""){ ?>
						<a href='/node/<?php print $next_nid?>'>
							<span class="mcco-next" style="color:<?php print $color;?>;" >Next<span class="icon icon-triangle-right"></span></span>
							<span class="screen-reader-text">Next Photo story</span>
						</a>
					<?php } ?>
				</div>
		</div>
		<div class="col-md-12" style="clear:both;">
			 <div style="float:right;">
					<a href='/photo-stories/<?php print $dept_tid."/".$landing_page_nid?>' class="icon-alone">
						<span class="mcco-back-landing" style="color:<?php print $color?>;" >View All<span class="icon icon-triangle-right"></span></span>
						<span class="screen-reader-text">Landing page</span>
					</a>
					
				</div>
		</div>
	<?php }	?> 
	

</article>

<?php if($view_mode == 'full') { ?>	
	<div class="article-actions row">	
	  <!--PAGE ACTIONS -->
	  <div class="page-actions col-lg-12">		
		<div class="action-rating rate_like">
		 <?php if (isset($node->rate_like)) { print $node->rate_like['#markup']; }?>				
		</div>		
		<div class="action-rating">		 
		 <?php if (isset($node->rate_vote)) { print $node->rate_vote['#markup']; }?>		
		</div>
		<div class="action-icons">
		  <a href="javascript:window.print();" class="icon-alone" target="_blank">
			<span class="icon-printer-1" aria-hidden="true"></span>
			<span class="screen-reader-text">Print this Page</span>
		  </a>		
		  <a href='#mail-modal' class="open-modal-link icon-alone">
			<span class="icon-mail" aria-hidden="true"></span>
			<span class="screen-reader-text">Email this Page</span>
		  </a>
		  <a href='<?php print $share_article?>' class="icon-alone" onclick="return false;">
			<span class="icon-gplus" aria-hidden="true"></span>
			<span class="screen-reader-text">Share in google+</span>
		  </a>
		  <a class="count-shares btn-info btn-icon">
			<span><?php print $gplus_count;?></span>
		  </a>              
		</div>
	  </div>  
	  <!--PAGE ACTIONS-->
	</div>
	
		<!--Email this Article-->
	<div class="white-popup mfp-hide" id="mail-modal">
		<h2 class="hd-title no-border-top" style="border-bottom:solid 2px #696969">Email this Story:</h2>
	  <div class="row">
		<div class="col-sm-6">
		  <div class="input-group margin-top10">
				<input type="hidden" class="nodeId" value="<?php print $node->nid; ?>">
				<input type="hidden" class="nodeType" value="<?php print $node->type ; ?>"> 
				<input type="email" class="form-control input-btn-right em-holder email_article" placeholder="Email Address">				
				<div class="input-group-btn"><button class="btn btn-primary email_submit" type="submit">Submit</button></div>
		  </div>
		  <p class="email_error" style='color:red;display:none'>Please enter valid email address.</p>
		</div>    
	  </div>   
	</div>
	
<?php }	?>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <!--<div class="link-wrapper">
      <?php /*print $links;*/ ?>
    </div>-->
  <?php endif; ?>

  <?php print render($content['comments']); ?>
