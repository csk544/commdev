<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
 $external_url = NULL;
 $dest_url = '';
 $target = '_self';
?>
<?php foreach ($fields as $id => $field): 
	if($id == 'nid'){
		$dest_url = url('node/' . $field->content);
		continue;
	}else if($id == 'field_external_url'){
		$external_url = $field->content;
		$ext_url_params = parse_url($external_url);
		if(isset($ext_url_params['host']) && $ext_url_params['host'] != $_SERVER['HTTP_HOST']){
			$target = '_blank';				
		}
		continue;
	}else if($id == 'title' && !is_null($external_url)){
		$title = $field->raw;	
		$field->content = str_replace('<a href="' . $dest_url . '">' . $title . '</a>', '<a href="' . $external_url . '" target="' . $target . '">' . $title . '</a>', $field->content);
	}
?>
  <?php if (!empty($field->separator)): ?>
    <?php print $field->separator; ?>
  <?php endif; ?>

  <?php print $field->wrapper_prefix; ?>
    <?php print $field->label_html; ?>
    <?php print $field->content; ?>
  <?php print $field->wrapper_suffix; ?>
<?php endforeach; ?>