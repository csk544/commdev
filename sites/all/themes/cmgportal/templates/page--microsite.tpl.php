<?php
global $base_url, $user, $theme_path; 
$userload = user_load($user->uid);
$img_path = image_style_url('thumbnail', isset($userload->picture->uri) ? $userload->picture->uri : 'public://pictures/no-profile-pic.jpg');
$department_white_logos = $department_banner_images = "";
if(!empty($node->field_channel)){
	$term_channel = taxonomy_term_load($node->field_channel['und'][0]['tid']);
	if(!empty($term_channel->field_banner_image)){
		$department_banner_images = file_create_url($term_channel->field_banner_image['und'][0]['uri']);
		if(!empty($term_channel->field_department_megamenu_white_)){
			$department_white_logos = file_create_url($term_channel->field_department_megamenu_white_['und'][0]['uri']);
		}
	}
}
?>
<?php if(isset($_GET['showonlycontent']) && $_GET['showonlycontent'] == 'yes'): ?>
<?php
print render($page['content']);
?>
<?php else:
?>
<div class="page interior-page <?php if($department_banner_images == "" && isset($attribute_class_page)){ echo $attribute_class_page; }?>">
	<!-- Begin Header -->
    <?php 
		if(!isset($is_usma)){
				$is_usma = 0;
			}
		echo theme('header', array('page' => $page,'first_name' => $first_name, 'groups' => $groups,'front_page' => $front_page,'is_usma' => $is_usma,'department_white_logos' => $department_white_logos,'department_banner_images' => $department_banner_images)); ?>
    <!-- End Header -->
		<!-- Begin Main Content -->
    <div class="main-container microsite">
			<!-- Begin subheader section -->
			<?php print $breadcrumb; ?>
      <!-- End subheader -->
			<div class="container">
				<?php if ($page['highlighted']): ?>
				<div id="highlighted"><div class="section clearfix">
				<?php print render($page['highlighted']); ?>
				</div></div> <!-- /.section, /#highlighted -->
				<?php endif; ?>

				<?php if ($messages): ?>
				<div id="messages"><div class="section clearfix">
				<?php print $messages; ?>
				</div></div> <!-- /.section, /#messages -->
				<?php endif; ?>
				<?php if ($tabs): ?>
				<div class="tabs">
					<?php print render($tabs); ?>
				</div>
				<?php endif; ?>
					<?php if(!isset($show_help) || ($show_help == TRUE)):?>
				<?php print render($page['help']); ?>
					<?php endif; ?>
				<?php if ($action_links): ?>
				<ul class="action-links">
					<?php print render($action_links); ?>
				</ul>
				<?php endif; ?>
			</div>
			<?php if(arg(0) == "node" && (arg(1) != "add" || arg(2) != "edit")){ ?>
			<section class="mobile-subheader-menu">
        <div class="container">
				  <?php if(isset($node->microsite_logo)){ ?>
          <div class="row">

            <div class="sidebar-logo-container">
              <?php print $node->microsite_logo; ?>
            </div>

          </div>
					<?php } ?>
          <div class="row">

            <div class="sidebar-menu">
              <ul class="menu">
                <?php
									if(isset($node->subnav_first) && $node->subnav_first != ""){
										print $node->subnav_first;
									}
								?>
								<?php
									if(isset($node->orgchart) && $node->orgchart != ""){?>
										<li class="menu-item">
										<?php print l('Org Chart', $node->orgchart , array('attributes' => array('class' => array('menu-item-link')), 'html' => TRUE)); ?>								
										</li>	
									<?php
									}
								?>
								<?php
									if(isset($node->subnav) && count($node->subnav)){  
									foreach($node->subnav as $key => $val){
									?>
										<li class="menu-item">
										<?php print l($val->title, 'node/'. $val->nid, array('attributes' => array('class' => array('menu-item-link')), 'html' => TRUE)); ?>														
										</li>	
										<?php
									}
									}
								if (!empty($node->field_ad_hoc_links)) { ?>
										<li class="menu-item has-submenu"><a href="javascript:void(0);" class="menu-item-link"><?php print !empty($node->field_ad_hoc_label) ? $node->field_ad_hoc_label['und'][0]['value'] : 'Ad Hoc'; ?><span class="icon icon-Icon_Down-Carrot-Filled"></span></a>
											<div class="submenu-container">
										<ul class="submenu">
											<?php
											foreach($node->field_ad_hoc_links['und'] as $value) {		
												$adhoc_url = isset($value['display_url']) ? $value['display_url'] : $value['url'];
												print "<li class='submenu-item'>". l($value['title'], $adhoc_url,array('attributes' => array('class' => array('submenu-item-link')), 'html' => TRUE)). "</li>";
											}
											?>
											</ul>
											</div>
										</li>
									<?php }
									if (!empty($node->km_departments)) { ?>
										<li  class="menu-item has-submenu"><a href="javascript:void(0);" class="menu-item-link">PAN GIO Pulse<span class="icon icon-Icon_Down-Carrot-Filled"></span></a>
											<div class="submenu-container">
										<ul class="submenu">
											<?php
												print $node->km_departments;
											?>
											</ul>
											</div>
										</li>
									<?php } 
										if (!empty($node->our_departments)) { ?>
										<li  class="menu-item has-submenu"><a href="javascript:void(0);" class="menu-item-link"><?php echo $node->our_department_heading; ?> Departments<span class="icon icon-Icon_Down-Carrot-Filled"></span></a>
											<div class="submenu-container">
										<ul class="submenu">
											<?php
												print $node->our_departments;
											?>
											</ul>
											</div>
										</li>
									<?php }?>
              </ul>
            </div>

          </div>
        </div>

      </section>
			<?php } ?>
			<!-- Begin main content -->
      <section class="main-content">
        <div class="container">
          <?php if(!isset($show_content) || ($show_content == TRUE)):?>
					<?php print render($page['content']); ?>
					<?php endif; ?>
					<?php print $feed_icons; ?>
				</div>
      </section>
      <!-- End main content -->
			<!-- Begin Footer -->
      <footer>
        <?php if(isset($page['footer'])): ?>
        <?php print render($page['footer']); ?>
        <?php endif;?>
      </footer>
      <!-- End Footer -->
		</div>
</div>
<?php endif; ?>
<?php if ($user->uid) { echo theme('settings_popup', array('page' => $page, 'userload' => $userload)); } ?>