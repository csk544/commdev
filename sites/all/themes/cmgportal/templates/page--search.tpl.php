<?php
global $base_url, $user, $theme_path;
$userload = user_load($user->uid);
$img_path = image_style_url('thumbnail', isset($userload->picture->uri) ? $userload->picture->uri : 'public://pictures/no-profile-pic.jpg');
$sideColumn = '';
$pageColumn = 'col-md-12';
if($page['sidebar_first'] || $page['sidebar_second']) {
	$pageColumn = 'col-md-9';
	if($page['sidebar_first']) {
		$pageColumn .= ' col-md-push-3';
	}
	$sideColumn = 'col-md-pull-9';
}
if($page['sidebar_first'] && $page['sidebar_second']) {
	$pageColumn = 'col-md-6 col-md-push-3';
	$sideColumn = 'col-md-pull-6';
}
$pages = array('Search Training' => 'career-and-learning', 'Search News' => 'commercial-news','Search Department News' => 'commercial_news', 'Filter Your Search' => 'search-people', 'Seach Announcements' => 'kudos-list', 'Search Department Announcements' => 'kudos_list', 'Search Gallery' => 'media-archive');
if(in_array(arg(0), $pages) || arg(0) == 'fav_links_more' || arg(0) == 'calender_events'|| arg(0) == 'my-topics-feed'){
	$page_class = "news-page"; 
}else{
	$page_class = "interior-page";
}
if(!isset($is_usma)) {
  $is_usma = 0;
}
?>
<?php if(isset($_GET['showonlycontent']) && $_GET['showonlycontent'] == 'yes'): ?>
<?php
print render($page['content']);
?>
<?php else:
?>
<div class="page <?php echo $page_class;?> <?php if(isset($attribute_class_page)){ echo $attribute_class_page; }?>">

  <?php	echo theme('header', array('page' => $page,'first_name' => $first_name, 'groups' => $groups,'front_page' => $front_page,'is_usma' => $is_usma)); ?>

	<div class="main-container">
    <!-- Begin subheader section -->
      <section class="subheader">
        <div class="container subheader-container">
          <div class="row">
            <div class="col-lg-9">
                <div class="page-title">
                  <h1>Search Results</h1>
                </div>
              </div>
            <div class="col-lg-3">
              <a href="javascript:void(0);" class="add-to-links"><span class="links-icon icon-Icon_Plus-Circle"></span>Add To My Links</a>
            </div>
          </div>
          <div class="sh-line"></div>
        </div>
      </section>
      <!-- End subheader -->
      
      <?php if ($messages): ?>
      <div id="messages">
        <div class="section clearfix">
          <?php print $messages; ?>
        </div>
      </div> <!-- /.section, /#messages -->
    <?php endif; ?>

      <div class="container">
        <div class="row">
          <div class="col-md-2">
            <?php if ($page['sidebar_first']): ?>
              <div id="sidebar-first">
                <?php print render($page['sidebar_first']); ?>
              </div> <!-- /.section, /#sidebar-first -->
          <?php endif; ?>
          </div>
          <div class="col-md-10">
            <div class="content search-results-section">
              <div class="container">
                <div class="row">
                  <div class="col-12">
                    <div class="search-tabs-nav">
                      <ul>
                        <li class="search-tab-item"><a href="#all-results" class="tab-toggle active all-results"><?php print t("All Results"); ?></a></li>
                        <li class="search-tab-item"><a href="#results" class="tab-toggle"><?php print t("Results"); ?></a></li>
                        <li class="search-tab-item"><a href="#gwiz" class="tab-toggle"><?php print t("gWiz"); ?></a></li>
                        <li class="search-tab-item"><a href="#people" class="tab-toggle"><?php print t("People"); ?></a></li>
                      </ul>
                    </div>
                  </div>
                </div>
                
                <div class="row">
                  <div class="col-12">
                    <div class="search-tabs">
                      <div class="search-tab active" id="all-results" style="display: block;">
                        <?php print render($page['sidebar_second']['views_people-block']); ?>
                        <?php print render($page['content']); ?>
                      </div>
                    </div>
                    <div class="search-tab commercial-content-tab" id="results">
                      <?php if(isset($page['sidebar_second']['views_solr_service_results-block'])): ?>
                        <?php print render($page['sidebar_second']['views_solr_service_results-block']); ?>
                      <?php endif; ?>
                    </div>
                    <div class="search-tab" id="gwiz">
                      <?php if(isset($page['sidebar_second']['views_solr_service_results-block_1'])): ?>
                      <?php print render($page['sidebar_second']['views_solr_service_results-block_1']); ?>
                      <?php endif; ?>
                    </div>
                    <div class="search-tab people-content-tab" id="people">
                      <?php print render($page['sidebar_second']['views_people-block_1']); ?>
                    </div>
                  </div>
                </div>
                
              </div>
            </div>
            
          </div>
        </div>
      </div>
      
      <!-- Begin Footer -->
      <footer>
        <?php if(isset($page['footer'])): ?>
          <?php print render($page['footer']); ?>
        <?php endif;?>
      </footer><!-- /.section, /#footer-wrapper -->
    </div>
  </div><!-- /#page-wrapper -->
  <?php if ($user->uid) { echo theme('settings_popup', array('page' => $page, 'userload' => $user)); } ?>
<?php endif; ?>