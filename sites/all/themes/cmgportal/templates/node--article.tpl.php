<?php
global $user,$base_url;
if ($user->uid > 0) {
	//$email_article = "https://mail.google.com/mail/?view=cm&to=". $user->mail . "&su=" . $node->title . '&body=' . $base_url . $node_url;
	$share_article = "https://plus.google.com/share?url=". $base_url . '/node/' . $node->nid;	
}
if($view_mode != 'full' && !$teaser) {
	print render($title_prefix);
	if ($title) {
		print '<h1 class="hd-title no-border-top" id="page-title">'.$title.'</h1>';
	}
	print render($title_suffix);
}
?>
<article id="node-<?php print $node->nid; ?>" class="entry <?php if($view_mode == 'full') { ?> border-top-primary border-bottom-primary <?php } ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?> role="article">
  <?php print render($title_prefix); ?>
  <?php if (!$page){ ?>
		<?php if($title != ''){ ?>
			<?php print $title_attributes; ?>
				<!--<a href="<?php //print $node_url; ?>">--><h3 class= "hd-title"><?php print $title; ?></h3><!--</a>-->
			
		<?php } ?>
		<?php if($title == '')	{ ?>
			<?php  print $title_attributes; ?>
				<h3 class="no-title"></h3>
			
		<?php }?>
		
	<?php } ?>
  <?php print render($title_suffix); ?>
	<?php if($view_mode == 'full') { ?>	
	<div class="article-actions border row">
	<div class="article-meta col-lg-7 no-padding"> 
	<?php 
		$href_link = $base_url."/commercial-news";
		$query_string = "?field_category[]=".@$node->field_category[$node->language][0]['taxonomy_term']->tid;
	?>
	  <a class="article-cat <?php print $node->field_category[$node->language][0]['taxonomy_term']->field_class_name[$node->language][0]['value']; ?>" href="<?php print $href_link.$query_string; ?>"><?php print @$node->field_category[$node->language][0]['taxonomy_term']->name; ?></a>
	  <div class="article-meta__date-author">
		<span class="meta-date"><?php print date('m.d.Y', $node->created); ?></span>
		<span>Posted by <?php print $node->name; ?></span>		
	  </div>
	</div>
	<!--PAGE ACTIONS -->
	<div class="page-actions col-lg-5">
	  <div class="action-rating">
		<?php if (isset($node->rate_vote)) { print $node->rate_vote['#markup']; }?>				
	  </div>
	  <div class="action-icons">
		<a href="javascript:window.print();" class="icon-alone">
		  <span class="icon-printer-1" aria-hidden="true"></span>
		  <span class="screen-reader-text">Print this Page</span>
		</a>		
		<a href='#mail-modal' class="open-modal-link icon-alone">
		  <span class="icon-mail" aria-hidden="true"></span>
		  <span class="screen-reader-text">Email this Page</span>
		</a>
		<a href='<?php print $share_article?>' class="icon-alone" onclick="return false;">
		  <span class="icon-gplus" aria-hidden="true"></span>
		  <span class="screen-reader-text">Share in Google+</span>
		</a>
		<a class="count-shares btn-info btn-icon">
		 <span><?php print $gplus_count;?></span>
		</a>              
	  </div>
	</div>   
	<!--PAGE ACTIONS-->
  </div>	
	
	<!--Email this Article-->
	<div class="white-popup mfp-hide" id="mail-modal">
		<h2 class="hd-title no-border-top" style="border-bottom:solid 2px #696969">Email this Article:</h2>
	  <div class="row">
		<div class="col-sm-6">
		  <div class="input-group margin-top10">
				<input type="hidden" class="nodeId" value="<?php print $node->nid; ?>">
				<input type="hidden" class="nodeType" value="<?php print $node->type ; ?>"> 
				<input type="email" class="form-control input-btn-right em-holder email_article" placeholder="Email Address">				
				<div class="input-group-btn"><button class="btn btn-primary email_submit" type="submit">Submit</button></div>
		  </div>
		  <p class="email_error" style='color:red;display:none'>Please enter valid email address.</p>
		</div>    
	  </div>   
	</div>
  
	<?php } ?>	  

  <!--/ARTICLE META--> 
	<?php 
	if($teaser) { ?>
		<!--<h3 class="hd-title"><?php //print $title ?></h3>-->
		<?php if (!empty($content['field_image'])) { ?> <div class="alignleft"><?php  print render($content['field_image']); ?> </div> <?php } ?>
		<div class="content clearfix"<?php print $content_attributes; ?>>
		<?php 
			print render($node->field_teaser['und'][0]['value']);
		?>
		</div>
		<div class="text-right">
		<?php print l(t('Read More'), "node/".$node->nid, array('attributes' => array('rel' => array('tag'), 'title' => $title, 'class' => array('view-more')))); ?>
		</div>
	<?php }
	else {
		if (!empty($content['field_image'])) {
	?> 
	<div class="alignleft"><?php  print render($content['field_image']); ?> </div> 	
	<?php }	?> 
	<div class="content clearfix"<?php print $content_attributes; ?>>
		<?php print render($content['body']); ?>
	 </div>	
	<?php }	?> 
	
	<!-- Tagging Functionality-->
    <?php 
		
	if (!empty($node->field_tags) && count(@$node->field_tags[$node->language]) > 0 && $view_mode == 'full') { 
		$user = user_load($user->uid);
		$follow_tags = isset($user->field_follow_tags['und']) ? drupal_explode_tags($user->field_follow_tags['und'][0]['value']) : array();
	?>
	<div class="tagged small">
		<span>Tags: </span>
		<?php  foreach($node->field_tags[$node->language] as $tags) {
				if (!empty($tags['taxonomy_term']->name)) {
		?>
		  <a class="badge badge hasTooltip <?php if (in_array($tags['taxonomy_term']->tid, $follow_tags)) { ?> badge-success <?php } ?>" href="#" term="<?php print $tags['taxonomy_term']->tid ?>" ><?php print $tags['taxonomy_term']->name ?></a>		  
		<?php } } ?>
		<div class="tags_help"><i>Manage your selected tags and news categories in My Portal "Settings."</i></div>
	</div>
	<?php }	?>

</article>

<?php if($view_mode == 'full') { ?>	
	<div class="article-actions row">
		<?php echo $show_mark_as_read_html;?>
	  <!--PAGE ACTIONS -->
	  <div class="page-actions col-lg-12">		
		<div class="action-rating">
		 <?php if (isset($node->rate_vote)) { print $node->rate_vote['#markup']; }?>		
		</div>
		<div class="action-icons">
		  <a href="javascript:window.print();" class="icon-alone" target="_blank">
			<span class="icon-printer-1" aria-hidden="true"></span>
			<span class="screen-reader-text">Print this Page</span>
		  </a>		
		  <a href='#mail-modal' class="open-modal-link icon-alone">
			<span class="icon-mail" aria-hidden="true"></span>
			<span class="screen-reader-text">Email this Page</span>
		  </a>
		  <a href='<?php print $share_article?>' class="icon-alone" onclick="return false;">
			<span class="icon-gplus" aria-hidden="true"></span>
			<span class="screen-reader-text">Share in google+</span>
		  </a>
		  <a class="count-shares btn-info btn-icon">
			<span><?php print $gplus_count;?></span>
		  </a>              
		</div>
	  </div>  
	  <!--PAGE ACTIONS-->
	</div>
<?php }	?>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <!--<div class="link-wrapper">
      <?php /*print $links;*/ ?>
    </div>-->
  <?php endif; ?>

  <?php print render($content['comments']); ?>
