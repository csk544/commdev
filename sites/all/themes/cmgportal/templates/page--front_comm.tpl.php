<?php
global $base_url, $user, $theme_path; 
$userload = user_load($user->uid);
if (isset($userload->field_display_name['und'])) {
	$display_name = $display_name = $userload->field_display_name['und'][0]['value'];;
}
$img_path=image_style_url('thumbnail', isset($userload->picture->uri) ? $userload->picture->uri : 'public://pictures/no-profile-pic.jpg');
?>
<?php if(isset($_GET['showonlycontent']) && $_GET['showonlycontent'] == 'yes'): ?>
<?php
print render($page['content']);
?>
<?php else:
?>
<div id="page-wrapper" class="wrapper">
	<header class="home-header">
			<div class="gwiz relative">
	    <div class="container">
   <a href="http://gwiz.gene.com" target="_blank"><span class="icon-chevron-left"></span><img src="/<?php print $theme_path;?>/assets/images/gWiz_Logo.png" alt="gWiz"></a>
			<?php if (variable_get('notice_status')):
				$notice = variable_get('emergency_notice', '');
				$type = variable_get('notice_type', '');
				if (!empty($notice['value'])):
			?>
			<a class="alert-icon" href="#"><img src="/<?php print $theme_path;?>/assets/images/alert-icon.png" alt="alert"></a>
				<div class="alert-message-container">
					<div class="alert-container">
					<?php 
					if($type == 1) { 
						$alert_type = 'alert-warning-yellow'; 
						$icon = 'alert-technical-icon.png';
					} else { 
						$alert_type = 'alert-warning-red'; 
						$icon = 'alert-warning-icon.png';
					}
					?>
						<div class="alert-item alert-warning-container <?php print $alert_type;?>">
							<div class="alert-message-icon">
								<img src="/<?php print $theme_path;?>/assets/images/<?php print $icon; ?>">
							</div>
							<div class="alert-message">
								<p class="alert-message-title"><a href="#"><?php print variable_get('notice_title', ''); ?></a></p>
								<?php print $notice['value']; ?>
							</div>
							<div class="alert-close-icon">
								<a href="#"><img src="/<?php print $theme_path;?>/assets/images/alert-close-icon.png"></a>
							</div>
						</div>
					</div>
				</div>
			<?php endif; endif; ?>
					</div>
				</div>
			<?php if(!isset($show_featured) || ($show_featured == TRUE)):?>
				<?php if ($page['featured']): ?>
			<div id="featured"><div class="section clearfix">
			<?php print render($page['featured']); ?>
			</div></div> <!-- /.section, /#featured -->
				<?php endif; ?>
			<?php endif; ?>
			<div class="home-nav-container">				<div class="container">
				<div class="row">
					<div class="col-xs-6 col-md-3">
						<?php if ($logo): ?>
						<a class="header__logo" href="<?php print $front_page; ?>" title="Home" rel="home"><img src="/<?php print $theme_path . "/images/logo-white.png"; ?>" alt="Home" /></a>
						<?php endif; ?>
					</div>
			<div class="col-md-10 menu-block">
				<div class="mobile-nav-toggle">
						<a class="mobile-nav-toggle__menu"><span class="icon icon-menu"></span></a>
					</div>
					<div id="sb-search" class="sb-search">
						<form action="/search-results">
						<!--<form id="gwiz-search">-->
              <input class="sb-search-input form-control input-lg" placeholder="" type="text" value="" name="keyword" id="search" autocomplete="off">
              <input class="sb-search-submit input-lg" type="submit" value="">
              <span class="sb-icon-search"></span>
	          </form>
            <!--<div class="search-suggestions-gwiz">
							<div class="search-suggestions__header"><h3 class="pull-left">Note:<span class="pulse"></span> You will be redirected to gWiz Search. Please make sure to open Pulse Secure to see your search results.</h3></div>
						</div>-->
				
				<!-- search suggestions -->
			  <div class="search-suggestions">
				
			  </div>
			  <!-- /search suggestions -->
			  
					</div> <!-- /sb-search	 -->
								<!-- Primary Navigation -->
	<div class="nav-wrap">
	<?php if(!isset($show_top_navigation) || ($show_top_navigation == TRUE)):?>
<?php if ($main_menu): ?>
  <nav role="navigation" class="primary-nav navigation">
	<?php 		
	print theme('links__system_main_menu', array(
	  'links' => $main_menu,
	  'attributes' => array(
		'id' => '',
		'class' => array('sm sm-clean collapsed level-0', 'clearfix'),
	  ),
	));        
	?>
  </nav> <!-- /#main-menu -->
<?php endif; ?>
<?php endif;?>
</div>
					<!-- Logged In -->
							<div class="off-canvas-open  js-off-canvas-open">
							<?php if ($user->uid > 0): ?>
							<div class="off-canvas__user-photo-sm-wrapper"><img alt="<?php print $display_name; ?>" src="<?php print $img_path; ?>" class="off-canvas__user-photo-sm"></div>
							<?php endif; ?>
							<span class="myPortalLabel">My Portal</span>
							</div>
						
							<div class="off-canvas-close  js-off-canvas-close">
								<span class="icon icon-close"></span>
							</div>
						
				  </div><!-- /menu-block -->
					</div><!-- /row		 -->
			</div><!-- /container -->
		</div><!-- /interior nav container -->
		<?php print render($page['header']); ?>
	</header>
<div id="page">
    <?php print $breadcrumb; ?>
  
  <?php if(!isset($show_content) || ($show_content == TRUE)):?>
		  <?php print render($page['content']); ?>
				<?php endif; ?>   
				
  <div id="main-wrapper" class="container main clearfix"><div id="main" role="main" class="clearfix">
<div class="col-md-12 view-more-news"><div class="all-news"><a href="<?php print $base_url; ?>/commercial-news" class="view-more">View All Commercial News <span class="icon-triangle-right"></span></a></div></div>
	    <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
  <?php endif; ?>
		<?php if ($page['highlighted']): ?>
		<div id="highlighted"><div class="section clearfix">
		<?php print render($page['highlighted']); ?>
		</div></div> <!-- /.section, /#highlighted -->
		<?php endif; ?>
	<?php print render($title_prefix); ?>
	  <?php if ($title): ?>
		<h1 class="hd-title no-border-top" id="page-title">
		  <?php print $title; ?>
		</h1>
	  <?php endif; ?>
	<?php print render($title_suffix); ?>
	
	<div class="col-md-9">
		<?php if(!isset($show_sidebar_first) || ($show_sidebar_first == TRUE)):?>
		<?php if ($page['sidebar_first']): ?>
		  <div class="col-md-6"><div class="section">
			<?php print render($page['sidebar_first']); ?>
		  </div></div> <!-- /.section, /#sidebar-first -->
		<?php endif; ?>
		<?php endif; ?>
    <!--<div id="content" class="row column">-->
		<div class="col-md-6 section">
				<?php if(!isset($show_help) || ($show_help == TRUE)):?>
		  <?php print render($page['help']); ?>
				<?php endif; ?>
		  <?php if ($action_links): ?>
			<ul class="action-links">
			  <?php print render($action_links); ?>
			</ul>
		  <?php endif; ?>
				
		  <?php print $feed_icons; ?>
		</div>
	<!--</div>--> <!-- /.section, /#content -->
	</div>	
	<div class="row">
	<div class="col-md-9">
		<div class="row margin-bottom50">
			<?php if ($page['homepage_section1']): ?>
			<div class="col-md-6">
			<?php print render($page['homepage_section1']); ?>
			</div>
			<?php endif; ?>
			<?php if ($page['homepage_section2']): ?>
			<div class="col-md-6">
			<?php print render($page['homepage_section2']); ?>
			</div>
			<?php endif; ?>
		</div>
		<div class="row">
			<?php if ($page['homepage_section3']): ?>
			<div class="col-md-6">
			<?php print render($page['homepage_section3']); ?>
			</div>
			<?php endif; ?>
			<?php if ($page['homepage_section4']): ?>
			<div class="col-md-6">
			<?php print render($page['homepage_section4']); ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="col-md-3 sidebar">
	<?php if(!isset($show_sidebar_second) || ($show_sidebar_second == TRUE)):?>
    <?php if ($page['sidebar_second']): ?>
        <?php print render($page['sidebar_second']); ?>
    <?php endif; ?>
    <?php endif; ?>
	</div>
	</div>
  </div></div></div> <!-- /#main, /#main-wrapper, /#page -->
	<div class="pre-footer">
		<div class="container">
			<div class="row">      
				<div class="col-md-9">
					<div class="row">
						<div class="col-md-6">
						<?php if(!isset($prefooter_first_column) || ($prefooter_first_column == TRUE)):?>
						<?php print render($page['prefooter_first_column']); ?>
						<?php endif; ?>
						</div>
						<div class="col-md-6">
						<?php if(!isset($prefooter_second_column) || ($prefooter_second_column == TRUE)):?>
						<?php print render($page['prefooter_second_column']); ?>
						<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<?php if(!isset($prefooter_third_column) || ($prefooter_third_column == TRUE)):?>
					<?php print render($page['prefooter_third_column']); ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
  <div id="footer-wrapper"><div class="section">
    <?php if(!isset($show_footer) || ($show_footer == TRUE)):?>
    <?php if ($page['footer']): ?>
      <footer class="footer" role="contentinfo" class="clearfix">
        <?php print render($page['footer']); ?>
      </footer> <!-- /#footer -->
    <?php endif; ?>
    <?php endif; ?>
  </div></div> <!-- /.section, /#footer-wrapper -->
</div><!--/#page-wrapper -->
<?php endif; ?>
       <?php echo theme('my_pulse', array('page' => $page, 'userload' => $userload)); ?>
           <!-- search__overlay/Overlay for Search -->
		<div class="search__overlay js-search__overlay"> </div>
