<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> role="article">

  <?php print render($title_prefix); ?>
   <?php if (!$page){ ?>
		<?php if($title != ''){ ?>
			<?php print $title_attributes; ?>
				<!--<a href="<?php //print $node_url; ?>">--><h3 class= "hd-title"><?php print $title; ?></h3><!--</a>-->
			
		<?php } ?>
		<?php if($title == '')	{ ?>
			<?php  print $title_attributes; ?>
				<h3 class="no-title"></h3>
			
		<?php }?>
		
	<?php } ?>
  <?php print render($title_suffix); ?>
<?php 
$cal_view_class = $list_view_class = 'hide';
if(@$node->field_default_view['und'][0]['value'] == 'calendar') {
	$cal_view_class = 'show';
} else if(@$node->field_default_view['und'][0]['value'] == 'list') {
	$list_view_class = 'show';
} else {
	$cal_view_class = 'show';
}
global $base_url;
$markup = '';
$today = $calendar->standard('today')->standard('prev-next');
$month = $today->month;
$year  = $today->year;
if($page){ 
	$markup .= '<h3 class="hd-title">'.strip_tags($title).'</h3>';
}
$markup .= '<div class="cal-widget current_month"><div class="calendar-view '.$cal_view_class.'"><h6 data-nid="'.$nid.'" class="calendar-title widget"><a title="prev" class="'.htmlspecialchars($calendar->prev_month_url()).'" href="'.$base_url."/".htmlspecialchars($calendar->prev_month_url()).'"><span class="icon icon-triangle-left"></span></a>'.$calendar->month().'<a title="next" class="'.htmlspecialchars($calendar->next_month_url()).'" href="'.$base_url."/".htmlspecialchars($calendar->next_month_url()).'"><span class="icon icon-triangle-right"></span></a></h6><table class="calendar"><tr>';
foreach ($calendar->days() as $day) {
	if($day == 'Sunday' || $day == 'Saturday') {
		$markup .= '<th class="weekend">'.substr($day, 0, 1).'</th>';
	} else {
		$markup .= '<th>'.substr($day, 0, 1).'</th>';
	}
}
$markup .= '</tr>';
foreach ($calendar->weeks() as $week) {
	$markup .= '<tr>';
	foreach ($week as $index => $day) {
		if($index == 0 || $index == 6) {
			$class = 'weekend';
		} else {
			$class = '';
		}
		$output = '';
		list($number, $current, $data) = $day; 
		$classes = array();
		if (is_array($data)) {
			$classes = $data['classes'];
		}
		$class .= " ".implode(' ', $classes);
		if(isset($eventdata[$year][substr($calendar->month(), 0, 3)][$number]) && !isset($data['classes']['prev-next'])) {
			$events = $eventdata[$year][substr($calendar->month(), 0, 3)][$number];		
			$output .= '<li><a class="hasTooltip" href="#"></a>';
			if(count($events) > 2) {
				$output .= '<div class="tooltip-content overflow scrollDiv malihu">';
			} else {
				$output .= '<div class="tooltip-content">';
			}
			foreach($events as $key => $event_info) {
				if($key) {
					$output .= '<hr>';
				}
				$output .= '<ul><li><a href="'.$event_info['url'].'" target="_blank" class="cal-date"><strong>'.$event_info['title'].'</strong></a></li>';
				if($event_info['allday']) {
					$output .= '<li> <strong>Time: </strong> All Day Event</li>';
				} else {
					$output .= '<li> <strong>Time: </strong>'.($event_info['start time'] != '' ? $event_info['start time'] : date('H:i', $event_info['start timestamp'])).'-'.($event_info['end time'] != '' ? $event_info['end time'] : date('H:i', $event_info['end timestamp'])).'</li>';
				}
				$output .= '<li><strong>Location: </strong>'.$event_info['where'].'</li>';
				$output .='<!--<li><a href="'.$event_info['url'].'" target="_blank" class="view-more">View Event Page <span class="icon-triangle-right"></span></a></li>--></ul>';
			}
			$output .= '</div></li>';
		}
		$markup .= '<td class="'.$class.'">';
		if (!isset($data['classes']['prev-next'])) {
			$markup .= '<span class="day">'.$number.'</span><ol>'.$output.'</ol>';
		}
		$markup .= '</td>';
	}
	$markup .= '</tr>';
}
$markup .= '</table>';
//$view_list = $base_url . "/events-list/". $nid;
$list = '<ul class="list-clean panel-list panel-list-small">';
foreach($eventslist as $event) {
	$list .= "<li><p>" . strtoupper($event['start date']) . ($event['start date'] != $event['end date'] ? ' - ' . strtoupper($event['end date']) : '') . "<a href='" . $event['url'] . "' class='cal-date' target='_blank'>" . $event['title'] . "</a></p></li>";
}
$list .= '</ul>';
$markup .= '<div class="text-right"><a href="#" class="view-more flip">View as List</a></div></div><div class="list-view '.$list_view_class.'">'.$list.'<div class="text-left"><a href="#" class="view-more flip">View as Calendar</a></div><div class="text-right">
<a href="http://www.google.com/calendar/embed?src='. urlencode($calendarID) .'&mode=AGENDA" class="view-more" target="_blank">View All <span class="icon-triangle-right"></span></a></div></div></div>';
print $markup;

?>
</article>