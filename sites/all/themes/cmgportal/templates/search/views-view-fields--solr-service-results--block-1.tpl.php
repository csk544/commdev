<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
 $keyword = !empty($_GET['queryTerm']) ? $_GET['queryTerm'] : '';
  $alter = array(
    'max_length' => 600,
    'ellipsis' => TRUE,
    'word_boundary' => TRUE,
    'html' => TRUE,
  );

  $data = trim(strip_tags($row->content));//array_map(function($v) {return }, $row->content);
  $excerpt = views_trim_text($alter, $data);
  $excerpt = preg_replace("/\w*?" . preg_quote($keyword) . "\w*/i", "<b>$0</b>", $excerpt);
?>
<h3 class='result-list__title'>
  <a href="<?php print $row->id; ?>" target="_blank">
    <?php print $row->title[0]; ?><span class='pulse'></span>
  </a>
  <p>
    <span class='result-list__date'><?php print date('Y-m-d', $row->timestamp); ?></span> | <span class='result-list__location'><?php print $row->id; ?></span>
  </p>
  <div class="excerpt">
    <?php print $excerpt; ?>
  </div>
</h3>