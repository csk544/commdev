<?php
global $base_url, $user, $theme_path;
$userload = user_load($user->uid);
$img_path = image_style_url('thumbnail', isset($userload->picture->uri) ? $userload->picture->uri : 'public://pictures/no-profile-pic.jpg');
$sideColumn = '';
$pageColumn = 'col-md-12';
if($page['sidebar_first'] || $page['sidebar_second']) {
	$pageColumn = 'col-md-9';
	if($page['sidebar_first']) {
		$pageColumn .= ' col-md-push-3';
	}
	$sideColumn = 'col-md-pull-9';
}
if($page['sidebar_first'] && $page['sidebar_second']) {
	$pageColumn = 'col-md-6 col-md-push-3';
	$sideColumn = 'col-md-pull-6';
}
$pages = array('Search Training' => 'career-and-learning', 'Search News' => 'commercial-news','Search Department News' => 'commercial_news', 'Filter Your Search' => 'search-people', 'Seach Announcements' => 'kudos-list', 'Search Department Announcements' => 'kudos_list', 'Search Gallery' => 'media-archive');
if(in_array(arg(0), $pages) || arg(0) == 'fav_links_more' || arg(0) == 'calender_events'|| arg(0) == 'my-topics-feed'){
	$page_class = "news-page"; 
}elseif(arg(0) == "site-map"){
	$page_class ="sitemap-page";
}else{
	$page_class = "interior-page";
}
$department_white_logos = $department_banner_images = "";
if(isset($node->field_channel) && !empty($node->field_channel)){
	$term_channel = taxonomy_term_load($node->field_channel['und'][0]['tid']);
	if(!empty($term_channel->field_banner_image)){
		$department_banner_images = file_create_url($term_channel->field_banner_image['und'][0]['uri']);
		if(!empty($term_channel->field_department_megamenu_white_)){
			$department_white_logos = file_create_url($term_channel->field_department_megamenu_white_['und'][0]['uri']);
		}
	}
}

?>
<?php if(isset($_GET['showonlycontent']) && $_GET['showonlycontent'] == 'yes'): ?>
<?php
print render($page['content']);
?>
<?php else:
?>
<div class="page <?php echo $page_class;?> <?php if($department_banner_images == "" && isset($attribute_class_page)){ echo $attribute_class_page; }?>">

			<?php	
        if(!isset($is_usma)) {
          $is_usma = 0;
        }
        echo theme('header', array(
          'page' => $page,
          'first_name' => $first_name, 
          'groups' => $groups,
          'front_page' => $front_page,
          'is_usma' => $is_usma,
          'department_white_logos' => $department_white_logos,
          'department_banner_images' => $department_banner_images)
        ); 
      ?>
	<div class="main-container">
    <?php //print $breadcrumb; ?>
		<!-- Begin subheader section -->
    <?php print $breadcrumb; ?>
      <!-- End subheader -->
		<?php if(!isset($show_featured) || ($show_featured == TRUE)):?>
		<?php if ($page['featured']): ?>
		<div id="featured"><div class="section clearfix">
		<?php print render($page['featured']); ?>
		</div></div> <!-- /.section, /#featured -->
		<?php endif; ?>
		<?php endif; ?>
		<section class="main-content">
  <div id="main-wrapper" class="container <?php if(isset($node) && $node->type =='page') { print "microsite page"; } else { print "main";} ?> clearfix"><div id="main" role="main" class="clearfix">

		<?php if ($page['highlighted']): ?>
		<div id="highlighted"><div class="section clearfix">
		<?php print render($page['highlighted']); ?>
		</div></div> <!-- /.section, /#highlighted -->
		<?php endif; ?>

    <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
  <?php endif; ?>
		<?php if (!in_array(arg(0), $pages)): ?>
			<?php if(isset($node) && $node->type == 'page' && arg(2) != "edit") { ?>
			 <div class="<?php print $pageColumn; ?> header page col-md-5 col-xs-12">
				<?php
				if(isset($node->field_microsite_logo['und'][0]['uri'])) {
					print '<a class="header__logo" href="" title="'.$node->title.'"><img src="'.file_create_url($node->field_microsite_logo['und'][0]['uri']).'" alt="'.$node->title.'" /></a>';
				} else {
					print render($title_prefix); ?>
					<?php if ($title): ?>
					<h1 <?php print $title_attributes; ?> id="page-title">
					<?php print $title; ?>
					</h1>
					<?php endif; ?>
					<?php print render($title_suffix); 
				} ?>
				<?php
				if(!empty($node->field_ad_hoc_links)) {
				?>
				<div class="nav-wrap text-right">
					<div class="microsite-mobile-nav-toggle">
						<a class="microsite-mobile-nav-toggle__menu btn btn-primary">Initiative Menu <span class="icon icon-menu"></span></a>
					</div>  
					<!-- Secondary Nav -->
					<nav role="navigation" class="secondary-nav">
						<ul class="sm sm-clean collapsed">
							<li><a href="#"><?php print !empty($node->field_ad_hoc_label) ? $node->field_ad_hoc_label['und'][0]['value'] : 'Ad Hoc'; ?></a>
							<ul>
								<?php
								foreach($node->field_ad_hoc_links['und'] as $value) { print "<li>". l($value['title'], $value['url']) . "</li>"; }
								?>
							</ul>
							</li>
						</ul>
					</nav>				
				</div>
			</div>			
			<?php } } else if(((arg(0) != 'find-class') && (arg(0) != 'dept_rolebasedprograms') && (arg(0) != 'fav_links_more') && (arg(0) != 'calender_events') && (arg(0) != 'my-topics-feed')) && (isset($node) && $node->type != 'events_gallery')) { ?>
			 <div class="<?php print $pageColumn; ?> header page col-md-5 col-xs-12">
				<?php print render($title_prefix); ?>
				<?php if ($title): ?>
				<h1 <?php print $title_attributes; ?> id="page-title">
				<?php print $title; ?>
				</h1>
				<?php endif; ?>
				<?php print render($title_suffix); ?>
				</div>
			<?php } ?>
			<?php if ($tabs): ?>
			<div class="tabs">
			<?php print render($tabs); ?>
			</div>
			<?php endif; ?>
		<?php endif; ?>
			
	<div class="row">
		
    <div id="content" class="column <?php print $pageColumn; ?>">
		<div class="section">
		
			<?php 
			/*Code for titles to be appeared above the content section for find a class and dept role based progs*/
			if(arg(0)=='find-class' || arg(0)=='dept_rolebasedprograms'){?>
				<?php if ($title){ ?>
				<h1 <?php print $title_attributes; ?> id="page-title">
				<?php print $title; ?>
				</h1>
			<?php }} /*End of Code for titles to be appeared above the content section for find a class and dept role based progs*/?>
		<?php if (in_array(arg(0), $pages)): ?>	
			<?php if ($tabs): ?>
			<div class="tabs">
			<?php print render($tabs); ?>
			</div>
			<?php endif; ?>
		<?php endif; ?>
			<?php if(!isset($show_help) || ($show_help == TRUE)):?>
		  <?php print render($page['help']); ?>
				<?php endif; ?>
		  <?php if ($action_links): ?>
			<ul class="action-links">
			  <?php print render($action_links); ?>
			</ul>
		  <?php endif; ?>
				<?php if(!isset($show_content) || ($show_content == TRUE)):?>
		  <?php print render($page['content']); ?>
				<?php endif; ?>
		  <?php print $feed_icons; ?>
		</div>
	</div> <!-- /.section, /#content -->
	
	  <?php if(!isset($show_sidebar_first) || ($show_sidebar_first == TRUE)):?>
    <?php if ($page['sidebar_first']): ?>
      <div id="sidebar-first" class="column col-md-3 sidebar <?php print $sideColumn; ?>"><div class="section">
        <?php print render($page['sidebar_first']); ?>
      </div></div> <!-- /.section, /#sidebar-first -->
    <?php endif; ?>
    <?php endif; ?>
		
		<?php if(!isset($show_sidebar_second) || ($show_sidebar_second == TRUE)):?>
    <?php
		if ($page['sidebar_second']): ?>
      <div id="sidebar-second" class="column col-md-3 sidebar"><div class="section">
        <?php print render($page['sidebar_second']); ?>
      </div></div> <!-- /.section, /#sidebar-second -->
    <?php endif; ?>
    <?php endif; ?>
	</div>
	
  </div></div> <!-- /#main, /#main-wrapper, /#page -->
	</section>
      <footer>
        <?php if(isset($page['footer'])): ?>
        <?php print render($page['footer']); ?>
        <?php endif;?>
      </footer><!-- /.section, /#footer-wrapper -->
</div>
</div><!-- /#page-wrapper -->
<?php endif; ?>
<?php if ($user->uid) { echo theme('settings_popup', array('page' => $page, 'userload' => $userload)); } ?>