<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
$results = $view->result;
$nids = array();
global $base_url, $theme_path;
$output = '<div id="example1" class="slider-pro"><div class="sp-slides">';
$thumbnail = '</div><div class="sp-thumbnails">';
foreach($results as $key => $result) {
	if(in_array($result->nid, $nids)){
		continue;
	}
	$nids[] = $result->nid;
	$news_tid = $result->field_field_category[0]['raw']['tid'];
	$news_term = taxonomy_term_load($news_tid);
	$class = @$news_term->field_class_name['und'][0]['value'];
	$teaser = @$result->_field_data['nid']['entity']->field_teaser['und'][0]['value'];
	$output .= '<div class="sp-slide">
		<img class="sp-image" src="' . $theme_path .'/assets/images/blank.gif"
			data-src="'.image_style_url('featured_image', @$result->field_field_image[0]['raw']['uri']).'"
			data-retina="'.image_style_url('featured_image', @$result->field_field_image[0]['raw']['uri']).'"/>
		<div class="container"><div class="sp-content-container" data-vertical="24%" data-width="350">
			<p class="sp-layer sp-padding hide-medium-screen sp-category '.$class.'" 
				data-horizontal="50" 
				data-show-transition="left" data-hide-transition="left">'.$news_term->name.'</p>

			<h3 class="sp-layer sp-padding sp-title" 
				data-horizontal="40" 
				data-show-transition="left" data-show-delay="150" data-hide-transition="left" data-hide-delay="100"><span class="block">'. l($result->node_title, 'node/' . $result->nid) . '</span></h3>  

			<p class="sp-layer sp-padding hide-small-screen sp-date" 
				data-horizontal="40" 
				data-show-transition="left" data-show-delay="250" data-hide-transition="left" data-hide-delay="150">'.date('m.d.Y', @$result->node_created).'</p>

			<p class="sp-layer sp-padding hide-medium-screen sp-description" 
				data-horizontal="40" 
				data-show-transition="left" data-show-delay="300" data-hide-transition="left" data-hide-delay="200">'.strip_tags($teaser).'</p>
			<p class="sp-layer sp-padding hide-medium-screen sp-category read-story" data-horizontal="50" data-show-transition="left" data-hide-transition="left">'.l('Read the Story', 'node/' . $result->nid).'</p>
		</div></div>
	</div>';
	$thumbnail .= '<div class="sp-thumbnail '.$class.'">
		<div class="sp-thumbnail-title"><div style="float: none; position: static;">'.$result->node_title.'</div></div>
		<div class="sp-thumbnail-description">'.$news_term->name.'</div>
		<div class="sp-thumbnail-count">'.gplus_count($base_url."/node/".$result->nid).'</div>
	</div>';
}
$thumbnail .= '</div>';
print $output.$thumbnail.'</div>';
?>
