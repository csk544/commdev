<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
?>
<article class="news-item">
  <div class="image">
    <?php if (!isset($row->field_field_image[0]['raw']['is_default'])): ?>
      <?php print $fields['field_image']->content; ?>
    <?php elseif (!empty($fields['field_department_news_thumbnail'])): ?>
      <?php print $fields['field_department_news_thumbnail']->content; ?>
    <?php else: ?>
      <?php print $fields['field_image']->content; ?>
    <?php endif; ?>
  </div>

  <div class="item-content">
    <div class="title">
      <?php print $fields['title']->content; ?>
    </div>
    <div class="date">
      <?php print $fields['created']->content; ?>
      <span class="news-feed-source">
        <?php print empty($row->field_field_is_this_a_usma_page[0]['raw']['value']) ? t("Commercial") : t("US Medical Affairs"); ?>
      </span>
    </div>
    <div class="excerpt">
      <?php print $fields['field_teaser']->content; ?>
    </div>
  </div>
</article>