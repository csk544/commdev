	<?php if (trim(strip_tags($secured_crm)) != "" || !empty($ags_html) || trim(strip_tags($ags_links)) != "" || !empty($data_publication) || trim(strip_tags($repots)) != "" || trim(strip_tags($ic_links)) != "") { ?>	
	<div class="row">
		<div class="col-md-12">
		<div class="bg-secure">
			<h2 class="hd-title"><span class="icon icon-lock"></span>Field Tools</h2>
			<div class="padding-side15">
			<div class="row">
				<?php if (trim(strip_tags($secured_crm)) != "" || !empty($ags_html) || trim(strip_tags($ags_links)) != "") { ?>	
				<div class="col-sm-4">
				<?php if (trim(strip_tags($secured_crm)) != "") { ?>
					<div class="module">
						<h3 class="hd-title">Salesforce/CRM</h3>						
						<?php print $secured_crm;?>
					</div>
				<?php }
					if (!empty($ags_html) || trim(strip_tags($ags_links)) != "") {
				?>	
					<div class="module">					
						<h3 class="hd-title">Aggregate Spend</h3>
						<?php if (!empty($ags_html)) { ?>
						<div class="secured_ags"><h3 class="hd-title">Aggregate Spend Tasks</h3><?php 
							//print $ags_html; 
							print '<p>Sorry for the inconvenience, section is under maintenance for a change</p>';
						?>
						</div>
					<?php } ?>	
						<?php print $ags_links;?>
					</div>			
					<?php } ?>	
				</div>				
				<?php }
				
				if (!empty($data_publication) || trim(strip_tags($repots)) != "") { ?>	
				<div class="col-sm-4">
					<div class="module">
						<h3 class="hd-title">Reports</h3>
						<?php /*if (!empty($data_publication)) { ?>
						<div class="secured_data_publication"><h3 class="hd-title">Data Publication Dates<span class="pulse"></span></h3><?php print $data_publication; ?></div>
						<?php } */?>	
						<div class="repots"><?php print $repots; ?></div>									
						<!-- /Downloads Module -->
					</div>
				</div>
				<?php }
				if (trim(strip_tags($ic_links)) != "") { ?>
				<div class="col-sm-4">
					<div class="module">
						<h3 class="hd-title">Incentive Compensation</h3>
						<div class="ic_links"><?php print $ic_links; ?></div>
					</div>
					<!-- /module -->
				</div>
				<?php } ?>
			</div>
			</div>
		</div>
		</div>
	</div>
	<?php } ?>	
	<!-- Toolbox Topics and it's pages -->			
	<h2 class="hd-title toolbox-links">Tools by Topic<div class="tools-buttons"><span class="expand-all">Expand All</span><span class="collapse-all">Collapse All</span></div></h2>
	<div class="sitemap-row">
		<ol>
		<?php $j=0; foreach($tb_links as $topic => $links) { if ($j != 0 && $j%3 == 0) { print "</ol></div><div class='sitemap-row'><ol>"; } $j++;  ?>
				<li class="col-sm-4 col-md-4">  
						<h3 class="h5"><?php print $topic; ?><span class="toggle-icon icon-Icon_Right-Carrot"></span></h3>
						<ol><?php	foreach($links as $link) { print "<li>". $link ."</li>"; } ?></ol>           
				</li>
		<?php } ?>															 
		</ol>   
	</div>