<div class="sitemap-section">
	<div class="sitemap-title">
		<h2>About Commercial</h2>
	</div>

	<div class="sitemap-row"><ol class="row">
		<?php $j=0; foreach($about_commercial as $links) { if ($j != 0 && $j%6 == 0) { print "</ol></div><div class='sitemap-row'><ol class='row'>"; } $j++;  ?>
						<li class="col-sm-4 col-md-2"><h3><?php print l($links->title, 'node/' . $links->nid);?></h3></li>
				<?php } ?>	
				<li class="col-sm-4 col-md-2"><h3><?php print l('Commercial Leadership', 'leadership');?></h3></li>
	</ol></div>
</div>
<div class="sitemap-section">
	<div class="sitemap-title">
		<h2>Archives</h2>
	</div>

	<div class="sitemap-row"><ol class="row">
		<li class="col-sm-4 col-md-2"><h3><?php print l('News Archive', 'commercial-news');?></h3></li>
		<li class="col-sm-4 col-md-2"><h3><?php print l('Kudos Archive', 'kudos-list');?></h3></li>					
		<li class="col-sm-4 col-md-2"><h3><?php print l('Multimedia Archive', 'media-archive');?></h3></li>
	</ol></div>
</div>


<div class="sitemap-section">
	<div class="sitemap-title">
		<h2>Departments</h2>
	</div>

	<div class="sitemap-row">
	<ol class="row">
		<?php print $dept_links;?>	
	</ol>
	</div>
</div>
<div class="sitemap-section">
	<div class="sitemap-title">
		<h2>U.S.Medical Affairs</h2>
	</div>

	<div class="sitemap-row">
	<ol class="row">
		<?php print $usma_dept_links;?>	
	</ol>
	</div>
</div>
<div class="sitemap-section">
	<div class="sitemap-title">
		<h2>Tools</h2>
	</div>

	<div class="sitemap-row">
	<ol class="row">
		<?php $j=0; foreach($tb_links as $topic => $links) { if ($j != 0 && $j%6 == 0) { print "</ol></div><div class='sitemap-row'><ol class='row'>"; } $j++;  ?>
						<li class="col-sm-4 col-md-2">  
								<h3><?php print $topic; ?></h3>
								<ol><?php	foreach($links as $link) { print "<li>". $link ."</li>"; } ?></ol>           
						</li>
				<?php } ?>
	</ol>
	</div>
</div>
<div class="sitemap-section">
	<div class="sitemap-title">
		<h2>Career & Learning</h2>
	</div>

	<div class="sitemap-row">
	<ol class="row">
		<?php $j=0; foreach($cl_links as $topic => $links) { if ($j != 0 && $j%6 == 0) { print "</ol></div><div class='sitemap-row'><ol class='row'>"; } $j++;  ?>
						<li class="col-sm-4 col-md-2">  
								<h3><?php print $topic; ?></h3>
								<ol><?php	foreach($links as $link) { print "<li>". $link ."</li>"; } ?></ol>        
						</li>
				<?php } ?>
	</ol>
	</div>
</div>
