<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> role="article">

  <?php print render($title_prefix); ?>
  <?php //if (!$page): ?>
    <!--<h2<?php //print $title_attributes; ?>>
      <a href="<?php //print $node_url; ?>"><?php //print $title; ?></a>
    </h2>-->
  <?php //endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="meta submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content);
			$language = $node->language;
			$link = isset($node->field_link[$language][0]['value']) ? $node->field_link[$language][0]['value'] : '';
			$file_uri = $node->field_basic_image_image[$language][0]['uri'];
			$file_url = file_create_url($file_uri);
			$markup = "";
			if($node->title){
			$markup = '<div class="module"><h3 class="hd-title">' . $node->title . '</h3><div class="row-col image-link"><div class="col-100">';
			}
			else{
				$markup = '<div class="module"><h3 class="no-title"></h3><div class="row-col image-link"><div class="col-100">';
			}
			if(isset($link) && $link != "") {
				$markup .= '<a href="'.$link.'"><img alt="" src="'.$file_url.'" class="img-responsive"></a>';
			} else {
				$markup .= '<img alt="" src="'.$file_url.'" class="img-responsive">';
			}
			$markup .= '</div></div></div>';
			print $markup;
    ?>
  </div>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</article>
