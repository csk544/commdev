<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
$results = $view->result;
$output = '<div id="promo-slide" class="owl-carousel promo-slider margin-top30 margin-bottom50 owl-loaded owl-drag">';
foreach($results as $key => $result) {
	if (!empty($result->field_field_add_image[0]['raw']['uri'])) {
		$link = @$result->field_field_link[0]['raw']['value'];
		$image = image_style_url('promo_image', @$result->field_field_add_image[0]['raw']['uri']);
		$output .= '<div class="item"><a href="'.$link.'"><img class="img-responsive" src="'.$image.'" alt=""></a></div>';
	}
}
$output .= '</div>';
print $output;
?>