CKEDITOR.on('dialogDefinition', function( ev ) {
  var dialogName = ev.data.name;
  var dialogDefinition = ev.data.definition;
  if(dialogName === 'table') {
		var infoTab = dialogDefinition.getContents('info');
		var width = infoTab.get('txtWidth');
		width['default'] = "100%";
		var advancedTab = dialogDefinition.getContents('advanced');
		var advStyles =  advancedTab.get('advStyles');
		var advCSSClasses = advancedTab.get('advCSSClasses');
		advCSSClasses['default'] = "table-layout";
		dialogDefinition.removeContents('advanced');
  }
	if (dialogName === 'image') {
		var imgInfo = dialogDefinition.getContents('info');
		var fileBrowser = imgInfo.get('browse');
		fileBrowser.label = 'Upload or Browse';
		var linkInfo = dialogDefinition.getContents('Link');
		var linkFileBrowser = linkInfo.get('browse');
		linkFileBrowser.label = 'Upload or Browse';
		dialogDefinition.removeContents('advanced');
	}
  if (dialogName === 'video') {
		var videoInfo = dialogDefinition.getContents('info');
		var fileBrowser = videoInfo.get('browse');
		fileBrowser.label = 'Upload or Browse';
	}
	if (dialogName === 'link') {
		dialogDefinition.removeContents('advanced');
		var targetTab = dialogDefinition.getContents('target');
		var targetType = targetTab.get('linkTargetType');
		targetType.items = [["<not set>", "notSet"], ["New Window (_blank)", "_blank"], ["Same Window (_self)", "_self"]];
		var linkInfo = dialogDefinition.getContents('info');
		var linkType = linkInfo.get('linkType');
		linkType.items = [["URL", "url"], ["E-mail", "email"]];
		var fileBrowser = linkInfo.get('browse');
		fileBrowser.label = 'Upload or Browse';
	}
});
CKEDITOR.editorConfig = function(config) {
	config.font_names = config.font_names+';Trade Gothic;';
};
