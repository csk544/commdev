(function($) {
  // Start - Adding classes to USMA special pages.
  $(document).ready(function() {
    if($('div.content').hasClass( "usma-fullwidth" )) {
      $('.field-name-field-microsite-builder').removeClass('col-md-9').addClass('col-md-12 no-padding');
    }
		$('.usma-submenu li.submenu-item').each(function() {
			if(!($(this).hasClass("has-submenu"))) {
				$(this).find('a').addClass('usma-has-nomenu');
			}
		});

  });
  //End
	if($('body').hasClass('admin-menu')) {
		var $adminMenu = $('#admin-menu');
		Drupal.admin.behaviors.hover = function (context, $adminMenu) {
			// Delayed mouseout.
			$('li.expandable', $adminMenu).hover(
				function () {
					// Stop the timer.
					clearTimeout(this.sfTimer);
					// Display child lists.
					$('> ul', this)
						.css({left: 'auto', display: 'block'})
						// Immediately hide nephew lists.
						.parent().siblings('li').children('ul').css({left: '-999em', display: 'none'});
				},
				function () {
					// Start the timer.
					var uls = $('> ul', this);
					this.sfTimer = setTimeout(function () {
						uls.css({left: '-999em', display: 'none'});
					}, 400);
				}
			);
		};
	}
  Drupal.behaviors.commportal = {
		attach: function(context, settings) {
		var base_url = Drupal.settings.commportal_custom.base_url;
		var uid = Drupal.settings.commportal_custom.uid;
		var sid = Drupal.settings.commportal_custom.sid;
		var isProcessing = false;

    /* $(".group-microsite-settings .fieldset-title").click(function(event) {
      $('html, body').animate({
        scrollTop: $(this).offset().top - $("header").height() - 35
      }, 250);
    }); */
    if(uid == 0) {
		$.removeCookie("emergency_notice");
		} else {
			if($.cookie("emergency_notice") == sid) {
				$('.alert-container .alert-item, .notif-important.emergency_notice').css({"display":"none"});
			} else {
				$('.alert-container .alert-item').css({"display":"block"});
			}
		}
		function addMagnificPopup() {
			$('.open-modal-link').magnificPopup({
				type: 'inline',
				midClick: true
			});
		}
		$('#off-canvas').off('click').on('click', '.open-modal-link', function() {
			addMagnificPopup();
			var menu = $("#off-canvas");
			menu.trigger('close.mm');
			return false;
		});
		
	function removeTag(tagID) {
		$('.settings-modal #manageTopics .active-tags-block i').each(function() {
			if($(this).attr('term') == tagID) {
				$(this).remove();
			}
		});
		$('.settings-modal #manageTopics #tag-scroll .abc-tags .tag-item').each(function() {
			var input = $(this).children('div.checkbox-style').children('input:checkbox');
			if(input.val() == tagID) {
				input.prop('checked', false);
			}
		});
		$('div.tagged.small a').each(function() {
			if($(this).attr('term') == tagID) {
				$(this).removeClass('badge-success');
			}
		});
	}
/*  table border wysiwyg */
	$( "table" ).each(function( i ) {
		if ( $(this).attr("border") > 0 ) {
			$(this).addClass('border_blue');
			var borderwidth = $(this).attr("border")+"px";
			$(this).css("border",borderwidth + ' solid #195695');
		} else if ( $(this).attr("border") == 0 ) {
			$(this).addClass('no_border');
		}
	});
	function addTag(tagID, tagged_data) {
		//$('.settings-modal #manageTopics .active-tags-block .ul').html(tagged_data);
		$('.settings-modal #manageAccordion #manageTopics .active-tags-block .tag-scroll ').html("<ul>"+tagged_data+"</ul>"); 
		$('.settings-modal #manageTopics #tag-scroll .abc-tags .tag-item').each(function() {
			var input = $(this).children('div.checkbox-style').children('input:checkbox');
			if(input.val() == tagID) {
				input.prop('checked', true);
			}
		});
		$('div.tagged.small a').each(function() {
			if($(this).attr('term') == tagID) {
				$(this).addClass('badge-success');
			}
		});
	}
		isProcessing = false;
		$('.settings-modal .tag-scroll input').unbind('click').click(function() {
			if(!isProcessing) {
				//var throbberSpan = $(this).next();
				isProcessing = true;
				var $this = $(this);
				if($this.is(":checked")) {
					action = 'add';
				} else {
					action = 'remove';
				}
				//throbberSpan.addClass('throbber');
				$.ajax({
          type: 'POST',
          url: base_url + '/follow_tags/' + action + '/' + $this.val(),
          async: false,
					success: function(result) {
						//throbberSpan.removeClass('throbber');
						if(result.status == true && action == "add") {
							//$('.followed-tags .tagged').append('<i term="'+$this.val()+'" class="badge badge-delete">'+$this.attr('name')+'</i> ');
							//addTag($this.val(), $this.attr('name'));
							addTag($this.val(), result.tags);
						} else {
							removeTag($this.val());
						}
						if($('body').hasClass('front')){
							$('.front .following-block #block-views-my-topics-feed-block .content').html(result.data);
						}
						addMagnificPopup();
						isProcessing = false;
					}
				});		
			}
		});
		//Toggle Tags	
		function setTooltip() {
			var $tags = $('.tagged a'),
				removeMSG = 'Remove from My Topics',
				addMSG = 'Add to My Topics';

			$tags.each(function() {
				var $this = $(this);
				if ($this.hasClass('badge-success')) {
					$this.qtip('option', 'content.text', removeMSG);
				} else {
					$this.qtip('option', 'content.text', addMSG);
				}
			});
			  var $tools_tags = $('.page-tools a.tool-link'),
				tools_removeMSG = 'Remove from My Dashboard',
				tools_addMSG = 'Add to My Dashboard';

			  $tools_tags.each(function() {
				var $this = $(this);
				if ($this.find('span.links-toggle-icon').hasClass('added icon-Icon_Check-Mark-Circle')) {
				  $this.qtip('option', 'content.text', tools_removeMSG);
				} else {
				  $this.qtip('option', 'content.text', tools_addMSG);
				}
			  });

		}
		setTooltip();
		$('.tagged a').unbind().click(function(event) {	
			if(!isProcessing) {
				isProcessing = true;
				event.preventDefault();			
				var $this = $(this),term=$(this).text(),action = "add",
				removeMSG = 'Remove from My Topics',
				addMSG = 'Add to My Topics';
				$(this).text('Loading...');			
				if ($this.hasClass('badge-success')) {
					action = "remove"; 
				}
				$('.tagged a.badge::after').css({"content":"'' !important"});
				$.ajax({
          type: 'POST',
          url: base_url + '/follow_tags/' + action + '/' + $this.attr('term'),
          async: false,
					success: function(result) {
						if(action == "add") {
							$this.text(term).addClass('badge-success');
							$this.qtip('option', 'content.text', removeMSG);
							addTag($this.attr('term'), result.tags);
						} else {
							$this.text(term).removeClass('badge-success');
							$this.qtip('option', 'content.text', addMSG);
							removeTag($this.attr('term'));
						}
						$('ul.off-canvas__accordion.mm-list li:nth-child(2) > h4').next().html(result.data);
						addMagnificPopup();
						isProcessing = false;
					}
				});	
			}			
		});

		$('.settings-modal #manageTopics .active-tags-block').off('click').on('click', '.remove-tag', function(event) {		
			event.preventDefault();
			var $this = $(this);												
			$(this).html('<span class="throbber"></span>');
			$.ajax({
        type: 'POST',
        url: base_url + '/follow_tags/remove/' + $this.parent().attr('term'),
        async: false,
				success: function(result) {	
					//$this.remove();
					$('.settings-modal #manageTopics .active-tags-block .ul').html(result.tags);
					removeTag($this.parent().attr('term'));
					if($('body').hasClass('front')){
						$('.front .following-block #block-views-my-topics-feed-block .content').html(result.data);
					}					
					addMagnificPopup();
				}
			});		
		});
		$('#popup_add_photo_story_form #preview-modal-popup-story').unbind().click(function(event) {
			event.preventDefault();
			$('#edit-file, #edit-story-text').removeClass('error');
			var err = false;
      if(!($( "body" ).hasClass( "page-node-443" ) || $( "body" ).hasClass( "page-photo-stories-73" ) || $( "body" ).hasClass( "page-node-17841" ))) {
        if($('#edit-file').val()==""){
          $('#edit-file').addClass('error');
          err = true;	
        }	
        else if($('#edit-story-text').val() == ""){
          $('#edit-story-text').addClass('error');
          err = true;	
        }
      }
			if(err){
				return false;
			}
			$('form#add-photo-story-form .form-item,form#add-photo-story-form #preview-modal-popup-story').addClass('hide');
			$('#add-photo-story-form .form-submit , #edit-button-story').removeClass('hide');
			var ann_body = $( "#edit-story-text" ).val();
			if($('input[type=file]').length > 0){
				if($('form#add-photo-story-form #edit-file').val() != "") {
					var filefield = $('input[type=file]')[0].files[0];
					$( "form#add-photo-story-form .form-item-story-text" ).after('<img src="#" id="photo_story_image">');
					var reader = new FileReader();
					reader.onload = function (e) {
						$('#photo_story_image').attr('src', e.target.result);
					}   
					reader.readAsDataURL(filefield);
					$('#photo_story_image').after('<div id="photo_story_body">'+ann_body+'</div>'); 
				} else { //TODO: Remove else condition later.
          $( "form#add-photo-story-form .form-item-story-text" ).after('<div id="photo_story_body">'+ann_body+'</div>');
        }
			} 
		});
    
    
		$('#popup_add_photo_story_form #edit-button-story').unbind().click(function(event) {
			$('#add-photo-story-form .form-submit , #edit-button-story , #photo_story_image ,#photo_story_body').addClass('hide');
			$('form#add-photo-story-form .form-item,form#add-photo-story-form #preview-modal-popup-story').removeClass('hide');
		});
		//MCCO Announcement Modal preview.
		$('#preview-modal-popup').unbind().click(function(event) {
			var error_text = "Please fill the mandatory fields.";
			event.preventDefault();
			$('#edit-announcement-name, #edit-announcement-job-title, #edit-announcement-department, #edit-announcement-body, #edit-announcement-type, #edit-hiring-manager, #edit-announcement-headshot').removeClass('error');
			var err = false;
			if($('#edit-announcement-type').val()==""){
				$('#edit-announcement-type').addClass('error').focus();
				err = true;	
			}					
			else if($('.chosen-single span').html()== "- Select -"){
				$('.chosen-single').parent().addClass('error').focus();
				err = true;	
			}
			else if($('#edit-announcement-name').val()==""){
				$('#edit-announcement-name').addClass('error').focus();
				err = true;	
			}
			else if($('#edit-announcement-job-title').val()==""){
				$('#edit-announcement-job-title').addClass('error').focus();
				err = true;
			} 
			else if($('#edit-hiring-manager').val()=="" && $('#edit-hiring-manager').hasClass( "required" )){
				$('#edit-hiring-manager').addClass('error').focus();
				err = true;	
			}
			else if($('#edit-announcement-department').val()==""){
				$('#edit-announcement-department').addClass('error').focus();
				err = true;
			}					
			else if($('#edit-announcement-headshot').length > 0 && $('#edit-announcement-headshot').val() != "" && $.inArray($('#edit-announcement-headshot').val().split('.').pop().toLowerCase(),['png','jpg','jpeg']) == -1){
				error_text = "Allowed file types: png jpg jpeg.";
				$('#edit-announcement-headshot').addClass('error').focus();
				err = true;	
			}
			else if($('#edit-announcement-body').val()==""){
				$('#edit-announcement-body').addClass('error').focus();
				err = true;
			}
			if(err){
				$('#popup_announcement_form .instructions').html('<div class="messages error">'+error_text+'</div>');
				$('#popup_announcement_form .instructions').removeClass('hide');
				return false;
			}
			$('#popup_announcement_form .instructions').addClass('hide');			
			$('form#announcement_popup h1:first-child,form#announcement_popup .form-item,form#announcement_popup #preview-modal-popup').addClass('hide');
			var ann_type = $( "#edit-announcement-type option:selected" ).text();
			var ann_name = $( "#edit-announcement-name" ).val();
			var ann_job_title = $( "#edit-announcement-job-title" ).val();
			var ann_department = $('#edit-announcement-department').val();
			var ann_body = $( "#edit-announcement-body" ).val();
			if(ann_type == 'People - Job change'|| ann_type == 'People - New hire' || ann_type == 'People - New to Genentech'){
				$( "#popup_announcement_form .form-item-announcement-body" ).after('<div id="ann_title"><h1>'+ann_name+' joins '+ann_department+' as '+ann_job_title+'</h1></div>');
			} else if(ann_type == 'People - Rotation'){
				$( "#popup_announcement_form .form-item-announcement-body" ).after('<div id="ann_title"><h1>'+ann_name+' joins '+ann_department+' as '+ann_job_title+' (Rotation) </h1></div>');
			}
			else if(ann_type == 'People - Promotion'){
				$( "#popup_announcement_form .form-item-announcement-body" ).after('<div id="ann_title"><h1>'+ann_name +' promoted to '+ann_job_title+' in '+ann_department+'</h1></div>');
			}
			if($('input[type=file]').length > 0){
				if($('#edit-announcement-headshot').val() != "") {
					var filefield = $('input[type=file]')[0].files[0];
					$( "#ann_title" ).after('<img src="#" id="ann_image">');
					var reader = new FileReader();
					reader.onload = function (e) {
						$('#ann_image').attr('src', e.target.result);
					}   
					reader.readAsDataURL(filefield);
					$('#ann_image').after('<div id="ann_body">'+ann_body+'</div>'); 
				} else {
					$( "#ann_title" ).after('<img src="/sites/all/themes/cmgportal/images/announcement_headshot.png" id="ann_image">');
					$('#ann_image').after('<div id="ann_body">'+ann_body+'</div>'); 
				}
			}
				else {
					$('#ann_title').after('<div id="ann_body">'+ann_body+'</div>'); 
				}
				$('#edit-submit-button,#edit-button').removeClass('hide');
		});
		$('#popup_announcement_form #edit-button').unbind().click(function(event) {
			$('#edit-submit-button,#edit-button,#ann_image,#ann_title,#ann_body').addClass('hide');	
			$('form#announcement_popup h1:first-child,form#announcement_popup .form-item,form#announcement_popup #preview-modal-popup').removeClass('hide');
		});
		
		
		$('#popup_article_form').off('click').on('click', '#edit-submit', function(event) {
			$('#edit-news-title, #edit-article-body, #edit-news-category').removeClass('error');
			var err = false;
			if($('#edit-news-title').val()==""){
				$('#edit-news-title').addClass('error').focus();
				err = true;	
			}
			else if($('#edit-news-category').val()==""){
				$('#edit-news-category').addClass('error').focus();
				err = true;
			}			
			else if($('#edit-article-body').val()==""){
				$('#edit-article-body').addClass('error').focus();
				err = true;	
			}
			if(err){
				$('#popup_article_form .instructions').html('<div class="messages error">Please fill the mandatory fields.</div>');
				return false;
			}			
			$("#article_popup #edit-submit").val("Loading..");
			return true;
		});
		//Add your story form validation
		$('#popup_add_story_form').off('click').on('click', '#edit-submit-form', function(event) {
			$('#edit-file ,#edit-story-text, #edit-story-collection, #edit-story-type').removeClass('error');
			var err = false;
			if($('#edit-file').val()==""){
				$('#edit-file').addClass('error').focus();
				err = true;	
			}	else if($('#edit-story-text').val()==""){
				$('#edit-story-text').addClass('error').focus();
				err = true;	
			}	else if($('#edit-story-collection').val()==""){
				$('#edit-story-collection').addClass('error').focus();
				err = true;	
			}	else if($('#edit-story-type').val()==""){
				$('#edit-story-type').addClass('error').focus();
				err = true;	
			}
			if(err){
				$('#popup_add_story_form .instructions').html('<div class="messages error">Please fill the mandatory fields.</div>');
				return false;
			}	
			$("#story_popup #edit-submit-form").val("Loading..");
				return true;
		});
    $("#popup_add_photo_story_form form").submit(function () {			
      var clickedForm = $(this); // Select Form
			clickedForm.find("[name='files[file]']").removeClass('error');
			clickedForm.find("[name='story_text']").removeClass('error');
      //TODO: Remove if condition later.
      if(!($( "body" ).hasClass( "page-node-443" ) || $( "body" ).hasClass( "page-photo-stories-73" ) || $( "body" ).hasClass( "page-node-17841" ))) {
        if (clickedForm.find("[name='files[file]']").val() == '') {
          clickedForm.find("[name='files[file]']").addClass('error');
          return false;
        }
        if (clickedForm.find("[name='story_text']").val() == '') {
          clickedForm.find("[name='story_text']").addClass('error');
          return false;
        }
      }
			return true;
    });
	//Limit Text area char count for MCCO stories
  //TODO: Remove the if condition after the removal of Chris burnet block.
  if(!($( "body" ).hasClass( "page-node-443" ) || $( "body" ).hasClass( "page-photo-stories-73" ) || $( "body" ).hasClass( "page-node-17841" ))) {
	if ($(".form-item-story-text .description").length != 0) {
		var characters = 2000;
		$(".form-item-story-text textarea").keyup(function(){ 
			if($(this).val().length > characters){
				$(this).val($(this).val().substr(0, characters));
			}
			var remaining = "";
			var remaining = characters -  $(this).val().length;
			$(".form-item-story-text .description #remaining").html("Remaining <b>"+remaining+"</b>.");
			if(remaining <= 10){
				$("#counter, #remaining").css("color","red");
			} else {
				$("#counter, #remaining").css("color","#666");
			}
		});
	}
  }
		// End of Limit Text area char count for MCCO stories
		//Popup close on clicking cancel button. 
		$('#popup_announcement_form span.cancel,#popup_article_form span.cancel,#popup_add_photo_story_form span.cancel ,#popup_add_story_form span.cancel').on('click', function(e) {
			$.magnificPopup.close();
			$('#edit-announcement-name, #edit-announcement-job-title, #edit-announcement-type, #edit-hiring-manager, #edit-announcement-headshot, #edit-announcement-department, #edit-announcement-body').val("").removeClass("error");
			$('form#announcement_popup h1:first-child,form#announcement_popup .form-item,form#announcement_popup #preview-modal-popup,form#add-photo-story-form .form-item,form#add-photo-story-form #preview-modal-popup-story').removeClass('hide');
			$('#edit-button-story ,#edit-submit-button, #edit-button,#ann_image, #ann_title,#ann_body, #photo_story_image,#photo_story_body,#add-photo-story-form .form-submit ').addClass('hide');
			$(".chosen-single").html("<span>- Select -</span>").parent().removeClass("error");
			$('div.instructions div.messages').remove();
			$('#edit-news-title, #edit-article-body, #edit-news-category').val("").removeClass("error");
			$('#edit-file, #edit-story-text, #edit-story-type').val("").removeClass("error");
			$( "#textarea_helptext" ).text( "1500 characters left" );
		});
		
		$('button.profile_save').unbind().click(function(event) {			
			event.preventDefault();		
			var $this = $(this),cat_ids = [];			
			$(this).text('Loading...');	
			$("#content").attr('disabled','disabled');			
			$('.category_list input').each(function() {				
				cat_ids.push({term: $(this).val(),action:$(this).is(':checked')});	
			});
			$.ajax({
        type: 'POST',
        url: base_url + '/follow_tags/profile/save',
        data: {cat_ids: cat_ids},
        async: false,
				success: function(data) {	
					//$this.text('Saved');				
					location.reload();
				}
			});		
		});	

		//Remove from My topics
	function removeLinks(element) {
		var nid = "nid="+element.attr('data-nid');
		//var remove_element = $(this).parent();
		var remove_element = element.closest('li');
		$.ajax({
      type: 'POST',
      url: base_url + '/remove_from_mytopics',
      data: nid,
      async: false,
      success: function(data) {	
        if(data) {
          remove_element.remove();
        }
      }
		});
	}
	$('#block-system-main .view-my-topics-feed a.remove_mytopics').unbind().click(function(event) {
		event.preventDefault();
		removeLinks($(this));
		return false;
	});
	$('.off-canvas__myPortal').off('click').on('click', 'a.remove_mytopics', function(event) {
	//$('a.remove_mytopics').unbind().click(function(event) {
		event.preventDefault();
		removeLinks($(this));
		return false;
	});
	//Email this Article
	$('#mail-modal').off('click').on('click', 'button.email_submit', function() {
		var $this = $(this);
		var emailId = $('#mail-modal .email_article').val();		
		if(/(.+)@(.+){2,}\.(.+){2,}/.test(emailId)) {
			$('#mail-modal .email_error').hide();		
			var nodeId = $('#mail-modal .nodeId').val();
			var nodeType = $('#mail-modal .nodeType').val(); 
			var data = "emailId="+emailId+"&nodeId="+nodeId+"&nodeType="+nodeType;
			$this.parents('.input-group').html('<p>Sending...</p>');			
			$.ajax({
        type: 'POST',
        url: base_url + '/email_this',
        data: data,
        async: false,
				success: function(data) {
					$('.input-group').html(data);	
					window.location.reload();
				}
			});	
		} else {
			$('#mail-modal .email_error').show();	
		}
	});	
	//Share this article for Announcements.
	$('.action-icons').off('click').on('click', 'a.icon-alone span.icon-gplus', function (event) {
		event.preventDefault();
		window.open($(this).parent().attr("href"), "popupWindow", "width=500,height=500,scrollbars=yes");
	});
	//Add or Manage My links
	function renderDynamicContent(data) {
		if(data.exists == false) {
			$('.settings-modal #manageAccordion #manageLinks div.added-links').html(data.popup);
			if(data.canvas == '') {
				$('#mm-2 > .sm-btm-border').addClass('hide');
				$('#mm-2 > ol.feed.mm-list').replaceWith('<span>No links added</span>');
			} else {
				var linksObj = $('body').hasClass('front');
				if(linksObj) {
					$('.front .links-block .links-feed-container .links-feed').html(data.canvas);
				} 
			}
		} else {
			$('.settings-modal #manageAccordion #manageLinks .link-url-bar .mylinks_valid').html(data.msg);
		}
	}
	var isProcessing = false;
	$('.subheader').off('click').on('click', 'a.add-to-links', function() {
    if(!isProcessing) {
			isProcessing = true;
			var icon_span = $(this).children('span');
			var link = $(this).attr('data-fav_link');
			var class_name = icon_span.attr('class');
			var params = "link="+link+"&class="+class_name+"&ajax=update";	
			icon_span.removeAttr('class').addClass('throbber');
			$.ajax({
        type: 'POST',
        url: base_url + '/add_favorites',
        data: params,
        async: false,
				success: function(data) {	
					isProcessing = false;	
					console.log("hello");
					if(class_name.match('Plus') == 'Plus') {
						$('a.add-to-links').html('<span class="links-icon icon-Icon_Check-Mark-Circle"></span>Link Added');
						$('a.add-to-links').addClass('added');
					} else {
						$('a.add-to-links').html('<span class="links-icon icon-Icon_Plus-Circle"></span>Add To My Links');
						$('a.add-to-links').removeClass('added');
					} 
					renderDynamicContent(data);
				}
			});	
		}	
	});	
	$('.account-block .account-flyout a.settingsModalpersonal').click(function() {
		$('.settings-modal #manageAccordion #manageLinks .link-url-bar .mylinks_valid').html('');
		$('.settings-modal #manageAccordion #manageLinks .link-url-bar .input-field,.settings-modal #manageAccordion #manageTopics .tag-search-bar .input-field').val('');
	});
  $('.settings-modal #manageAccordion #manageLinks').off('click').on('click', 'a.remove-link', function() {
    if(!isProcessing) {
			$(this).closest("li").addClass("temp-remove");
			if($(this).hasClass('tools-fav')) {
				isProcessing = true;
				$(this).html('<span class="throbber"></span>');
				var removeclass = 'links-toggle-icon added icon-Icon_Check-Mark-Circle';
				//var path = encodeURI($(this).attr('rLink'));
				var path = $(this).attr('rLink');
				var linktitle = $(this).attr('rTitle');
				var params = "link="+path+"&class="+removeclass+"&linktitle="+linktitle+"&ajax=update";
				$.ajax({
					type: 'POST', 
					url: base_url + '/add_tools',
					data: params,
					async: false,
					success: function(data) {
						isProcessing = false;
						$('.temp-remove').remove();
						//renderDynamicContent(data);
					}
				});
			} else if($(this).hasClass('secured-tools-fav')){
				isProcessing = true;
				$(this).html('<span class="throbber"></span>');
				var path = $(this).attr('rLink');
				var linktitle = $(this).attr('rTitle');
				var params = "link="+path+"&linktitle="+linktitle+"&ajax=update";
				$.ajax({
					type: 'POST', 
					url: base_url + '/remove_secured_tools',
					data: params,
					async: false,
					success: function(data) {
						isProcessing = false;
						$('.temp-remove').remove();
						//renderDynamicContent(data);
					}
				});			
			}else {
				isProcessing = true;
				var removeclass = "links-icon icon-Icon_Check-Mark-Circle";
				var path = $(this).attr('rLink');		
				var data = "link="+path+"&class="+removeclass+"&ajax=update";
				$(this).html('<span class="throbber"></span>');
				$.ajax({
					type: 'POST',
					url: base_url + '/add_favorites',
					data: data,
					async: false,
					success: function(data) {	
						isProcessing = false;
						$('.temp-remove').remove();
						if(removeclass.match('Check') == 'Check') {
							if($('.subheader .add-to-links').attr('data-fav_link') == path) {
								$('.subheader a.add-to-links').html('<span class="links-icon icon-Icon_Plus-Circle"></span>Add To My Links');
								$('.subheader a.add-to-links').removeClass('added');
							}
						}
						//renderDynamicContent(data);
					}
				});
			}
    }
	});
	
	//Feedback form validation
	$("#commportal-custom-feedback-form #edit-submit").unbind().click(function(event) {
		 var text = $('textarea#comment').val();
		 if(!text) {
			$('textarea#comment').css("border","1px solid red");
			$('p.feedback_error').show();
			event.preventDefault();
		 } else {
			$(this).val('Loading...');
			if ($(this).hasClass('disabled')) { event.preventDefault(); return false; }
			$(this).addClass('disabled');
			$('#edit-clear').attr('disabled',true);
			return true;
		 }
	}); 
	$(".node-advpoll h2 a").unbind().click(function(event) {	
		event.preventDefault();
		return false;
	});
	
	//Media Archive Year Filtering Block to override accordian
	$( ".media-archive-year li").find('a').each(function(index) {
		if($(this).hasClass('active')) {
			$(this).parent().parent().parent().attr("aria-hidden", "false");
			$(this).parent().parent().parent().css("display", "block");
			$(this).parent().parent().parent().siblings("strong").addClass("open");
			$(this).parent().parent().parent().siblings("strong").attr("aria-expanded", "true");
		}
	});
	
	$('.container a.alert-icon, .alert-container div.alert-close-icon, .notif-important.emergency_notice .notif-item-content .notif-dismiss').off('click').on('click', function() {
		$.cookie("emergency_notice", sid);
	});
	
	//Color scheme for Microsite
	/*changeTheme();
	function changeTheme() {
		if ($('body.node-type-microsite').length) {
			var primary_color = "#195695";
			var secondary_color = "#b2bb21";
			var classes_arr = $('#page-wrapper').attr('class').split(' ');
			$(classes_arr).each(function(key, value) {
				if(value.indexOf("color_") == 0) {
					var color_index = value.split("_");
					if(color_index[1] != '') {
						var colors = color_index[1].split("~");
						primary_color = (colors[0] != '' ? colors[0] : primary_color);
						secondary_color = (colors[1] != '' ? colors[1] : secondary_color);
					}
				}
			});
			$(".microsite h1, .microsite h2, .microsite h3, .microsite h4, .microsite h5, .microsite .cal-widget table.calendar th").css({"border-color":primary_color,"color":primary_color});
			$(".microsite .cal-widget h6").css({"background":primary_color+" none repeat scroll 0 0"});
			$("header.microsite").css({"border-color":primary_color});
			$(".microsite .view-more, .microsite .node-readmore > a, .microsite a.node-readmore").css({"color":secondary_color});
		}
	}*/
	
	// Weather Block
	
	// Fixing Quick-edit article float issue
	$('.node-article').off('click').on('click', '.quick-quickedit', function() {
    $('.entry .alignleft').css({"float":"none"});
	});
	$('.quickedit-init-processed').off('click').on('click', '.action-cancel.quickedit-button', function() {
		$('.entry .alignleft').css({"float":"left"});
	});
	
	// Tabber
		function getTabsCount(ele) {
			var count = 0;
			$(ele).find('dt').each(function() {
				count++;
			});
			return count;
		}
		$("div.builder-content").find('dl.ckeditor-tabber').each(function() {
			var tabElement = $("div.builder-content").find('dl.ckeditor-tabber')
			var tabsCount = getTabsCount(tabElement);
			var divWidth = parseFloat(100/tabsCount);
			var maxHeight = 0;
			tabElement.find('dt').each(function() {
				maxHeight = (maxHeight > $(this).height()) ? maxHeight : $(this).height();
			});
			var count = 0;
			var prev_width = 0;
			$(this).find('dt').each(function() {
				var div_width = 0;
				var styles = {'width':divWidth+'%','height':maxHeight+'px'};
				$(this).css(styles);
				if(count) {
				  prev_width = parseInt(prev_width+divWidth);
					div_width = prev_width;
					styles = {'width':divWidth+'%','left':div_width+'%','height':maxHeight+'px'};
					$(this).css(styles);
				}
				count++;
			});
		});
	focusToolTip();
	setTooltip();
	// Calendar
	$('article').off('click').on('click', '.cal-widget > .calendar-view > h6 > a', function() {
		if(!isProcessing) {
			isProcessing = true;
			var athis = $(this);
			var href = athis.attr('class');
			if(!(athis.parent().parent().parent().hasClass('current_month') && athis.attr('title') == "prev")) {
				var url = '';
				var nid = athis.parent().attr('data-nid');
				if(athis.attr('data-type') != 'ajax') {
					url = base_url + '/calendar_navigation?'+href+"&nid="+nid;
				} else {
					url = base_url + '/calendar_navigation'+href+"&nid="+nid;
				}
				athis.parent().parent().parent().prev().append(' <span class="throbber"></span>');
				$.ajax({
          type: 'POST',
          url: url,
          data: '',
          async: false,
          success: function(data) {	
            isProcessing = false;
            var builder_content = athis.parent().parent().parent().parent();
            builder_content.html(data);
            //changeTheme();
            focusToolTip();
          }
				});
			}
		}
		return false;
	});
	function focusToolTip() {
	  //Malihu Custom Scrollbar
		$('.malihu').mCustomScrollbar({
			theme: "dark-3" 
		});
		$('.hasTooltip').each(function() {
			$(this).qtip({
				content: {
					text: $(this).next('.tooltip-content')
				},
				hide: {
					delay: 300,
					fixed: true
				},
				show: {
					event: 'click mouseenter',
					solo: true
				},
				position: {
					viewport: true,
					my: 'top left',
					at: 'bottom center',
          target: ($(this).find('span').length > 0) ? $(this).find('span') : 'event',
				}
			});
		});
	}	
	
	//Hide some fields for microsite subnavigation checkbox	
	var $toggle_fields = $("#microsite-node-form #edit-field-dept-authors,#microsite-node-form #edit-field-microsite-logo,#microsite-node-form #edit-field-org-chart, #microsite-node-form #edit-field-theme-color, #microsite-node-form #edit-field-secondary-color, #microsite-node-form #edit-field-ad-hoc-label, #microsite-node-form #edit-field-ad-hoc-links, #microsite-node-form #edit-field-dept-menu-title");
	if ($('#microsite-node-form input#edit-field-microsite-subnav-und[type="checkbox"], #microsite-node-form input#edit-field-microsite-miscellaneous-und[type="checkbox"]').is(":checked")) {
		$toggle_fields.hide();
	}
	$('#microsite-node-form input#edit-field-microsite-subnav-und[type="checkbox"]').unbind().click(function(event) {
		if($(this).is(":checked")) {
			$toggle_fields.hide();
			$('#microsite-node-form input#edit-field-microsite-miscellaneous-und[type="checkbox"]').attr('checked', false);
		} else if($(this).is(":not(:checked)") && $('#microsite-node-form input#edit-field-microsite-miscellaneous-und[type="checkbox"]').is(":not(:checked)")) {
			$toggle_fields.show();
		}
  });
	$('#microsite-node-form input#edit-field-microsite-miscellaneous-und[type="checkbox"]').unbind().click(function(event) {
		if($(this).is(":checked")) {
			$toggle_fields.hide();
			$('#microsite-node-form input#edit-field-microsite-subnav-und[type="checkbox"]').attr('checked', false);
		} else if($(this).is(":not(:checked)") && $('#microsite-node-form input#edit-field-microsite-subnav-und[type="checkbox"]').is(":not(:checked)")) {
			$toggle_fields.show();
		}
  });	
	//Hide About commercial channels in page content type
	if($('#page-node-form input#edit-field-is-this-a-subpage-of-about-und[type="checkbox"]').is(":not(:checked)")){
		$('#page-node-form #edit-field-channel').hide();
	}else if($('#page-node-form input#edit-field-is-this-a-subpage-of-about-und[type="checkbox"]').is(":checked")){
		$('#page-node-form #edit-field-channel').show();
	}
	$('#page-node-form input#edit-field-is-this-a-subpage-of-about-und[type="checkbox"]').unbind().click(function(event) {
		if($(this).is(":checked")) {
			$('#page-node-form #edit-field-channel').show();
		} else if($(this).is(":not(:checked)")) {
			$('#page-node-form #edit-field-channel').hide();
		}
  });
  //End - About commercial
	if(window.location.pathname.match('/edit') == '/edit' || window.location.pathname.match('/clone') == '/clone') {
		$('body').find('article.node-advpoll > div.content.clearfix input[type=hidden]').remove();
		//Removing hidden input fields 
		$('body div#popup_announcement_form').find('input[type=hidden]').remove();
		$('body div#popup_article_form').find('input[type=hidden]').remove();
		$('body div#popup_add_photo_story_form').find('input[type=hidden]').remove();
	}
	//Init lightbox
  $('.masonry-container-gallery').each(function() {
		$(this).magnificPopup({
			delegate: 'div:not(.inactive) a',
			type: 'image',
			gallery: {
				enabled: true
			}
		});
	});
  $('#admin-menu, #admin-menu .dropdown').mouseover(function() { $(this).addClass('popup_menu'); });
	$('#admin-menu, #admin-menu .dropdown').mouseout(function() { $(this).removeClass('popup_menu'); });
	
	/* Set links from Drupal Menu to open in a separate tab instead of current tab. */
	$('#admin-menu #admin-menu-menu a').attr('target', '_blank');	
	/* To add target blank for all external links who doesn't have target attribute */
	var anchors = document.getElementsByTagName('a');
	for (var i=0; i<anchors.length; i++){
		if ((anchors[i].hostname != window.location.hostname && anchors[i].hostname != "" && typeof anchors[i].getAttribute('target') == "object") || (anchors[i].getAttribute('href') != null && anchors[i].getAttribute('href').substr(0, 7) == "mailto:") ) {
			anchors[i].setAttribute('target', '_blank');
		}
	}
		/* To Mark as Read */
		$('#submit-as-read').unbind('click').bind('click', function (e){
			var mark_as_read = $("#mark_as_read").prop('checked');
			if(mark_as_read){
				$('#mark_as_read').attr('disabled','disabled');
				$('#processing_mark_as_read').addClass('throbber');
				var nid = $('#nid').val();
				$('.mark-as-read-class').remove();
				$.ajax({
					type: 'POST',
					url: Drupal.settings.commportal_custom.base_url + '/mark-as-read',
					data: 'nid='+nid,
          async: false,
					success: function(result) {
						if($.trim(result) == Drupal.t("success")) {
							$('#processing_mark_as_read').remove();						
							$(".marked-success").show().delay(2500).fadeOut();
						}				
					}
				});
			} else {
				alert(Drupal.t("Please select as read"));
			}		
			return false;
		});

		/* Responsive Preview */
		$('#admin-menu-wrapper').off('click').on('click', 'button.responsive-preview-trigger', function (e){
			e.preventDefault();
			$('ul#responsive-preview-navbar-tab').toggleClass('show');
		});
		
		$('.content').off('click').on('click', 'a.flip', function() {
			$(this).parent().parent().addClass('hide').removeClass('show');
			$(this).parent().parent().siblings().addClass('show').removeClass('hide'); 
			return false;
		});
		
		$('.settings-modal #manageTopics .add-tag').click(function() {
			if($('.settings-modal #search-tags-form #edit-search-field').val() != '' && isProcessing == false) {
				isProcessing = true;
				//var info = $('#edit-search-field').val().split('~');
				var info = $('.settings-modal #search-tags-form #edit-search-field').val().split('[ID: ');
				$(this).html('<span class="throbber"></span><span>Add Tag</span>');				
				var term = $.trim(info[0]), action = "add";
				var tid = parseInt(info[1]);
				$.ajax({
          type: 'POST',
          url: base_url + '/follow_tags/' + action + '/' + tid,
          async: false,
					success: function(result) {
						addTag(tid, result.tags);
						
						if($('body').hasClass('front')){
							$('.front .following-block #block-views-my-topics-feed-block .content').html(result.data);
						}
						addMagnificPopup();
						isProcessing = false;
					}
				});
				$('.settings-modal #search-tags-form #edit-search-field').val('');
				$(this).html('<span class="add-icon icon-Icon_Plus-Circle"></span><span>Add Tag</span>');
			}				
			return false;
		});	
		$(window).scroll(function(){
			var scroll = $(window).scrollTop();if (scroll >= 20){ 
			$("body").addClass("darkHeader");
			}
			else {
				$("body").removeClass("darkHeader");
			}
		});
		// Color picker - Show color on entering color code in the textfield
		$('form.node-microsite-form').on('blur', 'input.builder-color-selector-input', function() {
			$(this).next().css({"background-color":$(this).val()}); 
		});
	  if($('#masonry-container').parents().attr('class') == 'mcco_story_page') {
			$('#masonry-container').magnificPopup({
				delegate: 'li:not(.inactive) a.mcco-img',
				type: 'image',
				image: {
					verticalFit: true,
					titleSrc: function(item) {
						return item.el.attr('title');
					}
				},
				gallery: {
					enabled: true
				},
			});
		}
		/* Thumbnail + text changes */
		if($('div.builder-column').hasClass('col-md-12')) {
			$(".margin-bottom15").addClass("auto-align");
		}
		/* End */
		/*Product List alignment issues fix */
		if($('div.builder-column').hasClass('col-md-6') || $('div.builder-column').hasClass('col-md-8')|| $('div.builder-column').hasClass('col-md-4')) {
			$(".col-md-6 .node-product-list .content , .col-md-8 .node-product-list .content , .col-md-4 .node-product-list .content").children().css("width", "100%");
		}		
		if (($(".page-leadership .region-sidebar-second .block-views .content").children().hasClass('node-product-list'))) { 
			$(".node-product-list .content").children().css("width", "100%");
		}	
		if (($("#block-views-related-links-block .content").children().hasClass('node-product-list'))) { 
			$(".node-product-list .content").children().css("width", "100%");
		}
		/* End */
		
		// Start: Career and Learning - Find a class
		$('#findclassform #edit-submit').off().on('click',function(event) {
			event.preventDefault();	
			var choices = [];
			var dates = [];
			var tid;
			var monthnum;
			var for_urself;
			var for_urteam;
			var online;
			$(this).after('<span class="al_throbber throbber"></span>');
			if ($('#edit-intact-teams-0').is(':checked')) {
				var for_urself = 1;
			}
			if ($('#edit-intact-teams-1').is(':checked')) {
				var for_urteam = 1;
			}if ($('#edit-intact-teams-2').is(':checked')) {
				var online = 1;
			}
			$('.form-item-competency li.search-choice span').each(function() {		
				tid =  $('select#edit-competency option:contains('+$(this).text()+')').attr('value');
				choices.push({term: tid+':'+$(this).text()});
			});
			$('.form-item-date li.search-choice span').each(function() {
				monthnum =  $('select#edit-date option:contains('+$(this).text()+')').attr('value');
				dates.push({date: monthnum+':'+$(this).text()});
		  });			
			var ajax_testing = "true";
			$.ajax({
        type: 'POST',
        url: base_url + '/find-class',
        data: {ajax_testing: ajax_testing, choices: choices, dates: dates, for_urself: for_urself, for_urteam: for_urteam, online:online},
        async: false,
				success: function(msg) {	
          $('#replace-find-results').html(msg);						
          $('.al_throbber').remove();
				}
			});
		});
		$('#findclassform #edit-reset').off().on('click',function(event) {
			event.preventDefault();
			$(this).after('<span class="al_throbber throbber"></span>');
			 $('.chosen-processed option').prop('selected', false).trigger('chosen:updated');
			 $('#findclassform input:checkbox').removeAttr('checked');
			 var ajax_testing = "false";
			$.ajax({
        type: 'POST',
        url: base_url + '/find-class',
        data: {ajax_testing: ajax_testing},
        async: false,
				success: function(msg) {	
          $('#replace-find-results').html(msg);						
          $('.al_throbber').remove();
				}
			});
		});
    //View all results - Career and Learning - Featured Courses
		$('.page-find-class #replace-find-results').off().on("click", ".view-more-container a.view-more", function(event) {
			event.preventDefault();
			$(this).after('<span class="al_throbber throbber"></span>');
			 var view_all = "true";
			$.ajax({
        type: 'POST',
        url: base_url + '/find-class',
        data: {view_all: view_all},
        async: false,
				success: function(msg) {	
          $('#replace-find-results').html(msg);						
          $('.al_throbber').remove();
				}
			});
		});
		//End - Find a class
		
		$('div.rolebasedmaindiv div').off().on('click',function(event) {
			var divid = $(this).attr('id');
			$('div.rolebasedmaindiv div.rolebaseddept').css({'background-color':'#962a91','border':'2px solid #962a91'});
			$(this).css({'background-color':'#631c60','border':'2px solid #631c60'});
			var ajax_var = true;
			$.ajax({
        type: 'POST',
        url: base_url + '/dept_rolebasedprograms',
        data: {ajax_var: ajax_var, id:divid},
        async: false,
				success: function(msg) {	
					$('#final_results').html(msg);
				}
		});
		});
		$('.content #final_results').off('click').on('click', 'a.deptm', function(event) {
			event.preventDefault();		
			$('#final_results div.show-div').addClass('hide');
			if($(this).attr('id') != undefined){
				$.ajax({
          type: 'POST',
          url: base_url + '/view_get_career_data',
          data: {id:$(this).attr('id')},
					success: function(msg) {	
						$('#'+id).html(msg);
					}
				});		
			}
			var id = $(this).next().attr('id');
			$('#'+id).removeClass('hide');
			$('#'+id).addClass('show-div');	
		});
		//Career & Learning End.
    
    // Tools - Add to My dashboard
    
       var toolsisProcessing = false;
    $('.page-tools .sitemap-row ol li ol li .links-toggle-icon').off().on('click', function(event) {
      if(!toolsisProcessing){
        toolsisProcessing = true;
        event.preventDefault();
        var $this = $(this);
        //$(this).html('<span class="throbber"></span>');
        var class_name = $(this).attr('class');
        var link = $(this).parent().attr('href');
        var tools_removeMSG = 'Remove from My Dashboard';
        var tools_addMSG = 'Add to My Dashboard';
        var linktitle = $(this).parent().text();
        var params = "link="+link+"&class="+class_name+"&linktitle="+linktitle+"&ajax=update";
        //$(this).html('<span class="throbber"></span>');
        if ($(this).hasClass('added')) {
          $(this).removeClass('added');
          $(this).removeClass('icon-Icon_Check-Mark-Circle');
          $(this).addClass('icon-Icon_Plus-Circle');
          $.ajax({
            type: 'POST',
            url: base_url + '/add_tools',
            data: params,
            async: false,
            success: function(data) {
              $this.parent().qtip('option', 'content.text', tools_addMSG);
              toolsisProcessing = false;
            }
          });
        } else {
          $(this).addClass('added');
          $(this).addClass('icon-Icon_Check-Mark-Circle');
          $(this).removeClass('icon-Icon_Plus-Circle');
          $.ajax({
            type: 'POST',
            url: base_url + '/add_tools',
            data: params,
            async: false,
            success: function(data) {
              $this.parent().qtip('option', 'content.text', tools_removeMSG);
              toolsisProcessing = false;
            }
          });
        }
      }
      
    });
    //End - Tools
    //Tools - Collapsible links
    $('.page-tools .sitemap-row > ol > li > h3.h5').click(function() {
      var container = $(this).closest('.sitemap-row > ol > li');
      if (container.hasClass('active')) {
        container.removeClass('active');
        container.animate({ height: container.find('h3.h5').outerHeight()}, 600);
      } else {
        container.addClass('active');
        var targetheight = container.find('ol').outerHeight();
        container.animate({ height: targetheight + container.find('h3.h5').outerHeight()});
      }
    });
    $('.page-tools .toolbox-links .expand-all').click(function() {
      $('.sitemap-row > ol > li').each(function() {
      var container = $(this).closest('.sitemap-row > ol > li');
	    container.addClass('active');
      var targetheight = container.find('ol').outerHeight();
      container.animate({ height: targetheight + container.find('h3.h5').outerHeight()});
    });
    });
    
    $('.page-tools .toolbox-links .collapse-all').click(function() {
      $('.sitemap-row > ol > li').each(function() {
      var container = $(this).closest('.sitemap-row > ol > li');
      container.removeClass('active');
    	container.animate({ height: container.find('h3.h5').outerHeight()}, 600);
      });
    });
    /*$('.page-tools .sitemap-row > ol > li > h3.h5').each(function() {
      $(this).append('<span class="toggle-icon icon-Icon_Right-Carrot"></span>');
    });*/
    //End - Tools
    
    $('.notifications-flyout .dismiss-all').click(function() {
			var datanid = [];
			var count = 1;
			$(this).fadeOut();
			$('.notifications-flyout .notifications-feed .notification-item').each(function() {
				if(!$(this).find('div.notif-type-icon').hasClass('notif-red')){
					if($(this).find('div.notif-item-content a.notif-dismiss').hasClass('newsdismiss')){
						datanid[count] = $(this).find('div.notif-item-content a.notif-dismiss').attr('data-nid');
						count = count + 1;
					}
					console.log(datanid);
					$(this).fadeOut('fast', function() {
						$(this).remove();
					})
				}
			})
			var nid = "nid="+datanid;
			$.ajax({
				type: 'POST',
				url: base_url + '/remove_from_notifications',
				data: nid,
				async: false,
				success: function(data) {
					
				}
			});
			if ($('.notifications-flyout .notifications-feed .notification-item').length === 1) {
				$('.notif-alert').removeClass('active');
				$('.notif-alert').text('0');
				$('.notifications').addClass('disabled');
			}
	  })

	  $('.notifications-flyout .notifications-feed .notification-item .notif-dismiss').click(function() {
			var datanid = [];
			if($(this).hasClass('newsdismiss')){
				datanid[1] = $(this).attr('data-nid');
				var nid = "nid="+datanid;
				$.ajax({
					type: 'POST',
					url: base_url + '/remove_from_notifications',
					data: nid,
					async: false,
					success: function(data) {
						var notifications_count = $('.notif-alert').html() - 1;
						$('.notif-alert').html(notifications_count);
					}
				});
			}
		  var item = $(this).closest('.notification-item');
		  item.fadeOut('fast', function() {
			item.remove();
		  })
		  if ($('.notifications-flyout .notifications-feed .notification-item').length === 1) {
				$('.notifications-flyout .dismiss-all').fadeOut();
				$('.notif-alert').removeClass('active');
				$('.notif-alert').text('0');
				$('.notifications').addClass('disabled');
		  }
	  })    
	}
}
})(jQuery);
