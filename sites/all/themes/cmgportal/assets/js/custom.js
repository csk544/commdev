
/*global console:true, Wookmark:true, imagesLoaded:true, UISearch:true' */
/*jshint unused:false*/
(function ($) {
$(document).ready(function() {


  // //Smooth Scrolling
  // function scrollTo(element) {
  //   var $scrollDiv = $('.scrollDiv .mCSB_container');
  //   var target = element.attr('href');
  //   console.log(target);
  //   var target = target.length ? '#anchor-' + target.replace(/([#])/, '') : false;
  //   if (target) {
  //     $scrollDiv.css({'border' : '1px solid red'});
  //     $scrollDiv.animate({ scrollTop: 1000 }, 2000);
  //   }

  //   // if (location.pathname.replace(/^\//,'') === element.pathname.replace(/^\//,'') && location.hostname === element.hostname) {
  //   //   var target = $(element.hash);
  //   //   target = target.length ? target : $('[name=' + element.hash.slice(1) +']');
  //   //   if (target.length) {
  //   //     $(element).animate({
  //   //       scrollTop: target.offset().top
  //   //     }, 1000);
  //   //     return false;
  //   //   }
  //   // }
  // }

  var options = {

  url: "tags.json",

  getValue: "name",

  list: { 
    match: {
      enabled: true
    }
  },

  theme: "square"
};

//$("#autocomplete").easyAutocomplete(options);
  

$(document).on("click","a[href^='#']",function(e) {

	var href = $(this).attr("href");
	//var target = $(".mCustomScrollbar"); 
	if($(this).parent().next().hasClass('mCustomScrollbar')) {
		var target = $(this).parent().next();
		var parent = target.attr('id');

		if(target.length) {
			href = href.length ? $('#'+parent+' #anchor-' + href.replace(/([#])/, '')) : false;

			e.preventDefault();
			target.mCustomScrollbar("scrollTo",href);
		}
	}
});

  // Initialize Primary Nav
  $('.primary-nav .sm').smartmenus({
    subMenusMaxWidth: '15em'
  });

  $('.secondary-nav .sm').smartmenus({
    subMenusMaxWidth: '15em',
    rightToLeftSubMenus: true
  });

  //Alerts
  $('.alert-close-icon').on('click', function(e) {
    e.preventDefault();
    $(this).parents('.alert-item').slideUp('fast');
  });

  $('.alert-icon').on('click', function(e) {
    var $alertItems = $('.alert-item');
    if ($alertItems.is(':visible')) {
      $alertItems.fadeOut();
    } else {
      $alertItems.fadeIn();
    }
  });

  // Mobile Nav Hamburger Toggle  
  $(function() {
    $('.mobile-nav-toggle__menu').click(function() {
      var $this = $(this),
        $menu = $('.primary-nav .sm');
      if (!$this.hasClass('collapsed')) {
        $menu.addClass('collapsed');
        $this.addClass('collapsed');
      } else {
        $menu.removeClass('collapsed');
        $this.removeClass('collapsed');
      }
      return false;
    }).click();
  });

  // Microsite Mobile Nav Hamburger Toggle  
  $(function() {
    $('.microsite-mobile-nav-toggle__menu').click(function() {
      var $this = $(this),
        $menu = $('.secondary-nav .sm');
      if (!$this.hasClass('collapsed')) {
        $menu.addClass('collapsed');
        $this.addClass('collapsed');
      } else {
        $menu.removeClass('collapsed');
        $this.removeClass('collapsed');
      }
      return false;
    }).click();
  });

  // Close Mobile Hamburger Menu (if open) when Search is clicked 
  $(function() {
    $('#sb-search').click(function() {
      var $this = $('.mobile-nav-toggle__menu'),
        $menu = $('.primary-nav .sm');
      if (!$this.hasClass('collapsed')) {
        $menu.addClass('collapsed');
        $this.addClass('collapsed');
      }
    });
  });

  // Initialize Off-Canvas content  
  $("#off-canvas").mmenu({
    slidingSubmenus: false,
    offCanvas: {
      position: "right"
    },
    onClick: {
        close: false //Prevent off canvas area from closing after clicking inside
    }
  });

  //  Open Off-Canvas content
  $(".js-off-canvas-open").click(function() {
    var menu = $("#off-canvas");
    menu.trigger('open.mm');
  });


  // Toggle text/icons for Off-Canvas trigger  
  $("#off-canvas").on('opening.mm', function() {
    $('.js-off-canvas-open').fadeOut('fast');
    $('.js-off-canvas-close').fadeIn('fast');
  }).on('closing.mm', function() {
    $('.js-off-canvas-close').fadeOut('fast');
    $('.js-off-canvas-open').fadeIn('fast');
  });

  //Malihu Custom Scrollbar
  $('.malihu').mCustomScrollbar({
    theme: "dark-3" 
  });

  // Off Canvas Accordion
  $(".off-canvas__accordion").collapse({
    open: function() {
      this.slideDown(200);
    },
    close: function() {
      this.slideUp(200);
    },
    query: 'li h4',
    accordion: true,
  });

  // Sidebar List Accordion
  $(".list__accordion").collapse({

    query: 'li strong',
  });
  /*var html; //Set variables
  $.simpleWeather({
    location: 'Roseville, CA',
    woeid: '',
    unit: 'f',
    success: function(weather) {
      html = '<h3><i class="weather-icon icon-' + weather.code + '"></i> <span class="weather-widget--current-lg">' + weather.temp + '' + weather.units.temp + '</span></h3>';
      html += '<ul><li class="weather-widget--city">' + weather.city + ', ' + weather.region + '</li>';
      html += '<li class="weather-widget--currently">Currently: ' + weather.temp + 'F</li>';
      html += '<li class="weather-widget--hi-lo">Today: ' + weather.high + 'F / ' + weather.low + 'F</li></ul>';

      $(".js-weather-widget").html(html);
    },
    error: function(error) {
      $(".js-weather-widget").html('<p>' + error + '</p>');
    }

  });*/

  // searchBar plugin
  var sbSearch = new UISearch(document.getElementById('sb-search'));

  $(".login-form input").keypress(function() {
    $(".off-canvas__logged-out").fadeOut('fast');
    $(".off-canvas__logged-in").addClass('mm-opened').fadeIn('fast');
  });

  $(".drop-input input[type='text']").keypress(function() {
    $(".drop-panel").fadeIn('fast');
  });

  $(".drop-input input[type='text']").focusout(function() {
    $(".drop-panel").fadeOut('fast');
  });

  $(".drop-input button[type='button']").click(function() {
    $(".drop-panel").toggle('fast');
  });

  $(".drop-input button[type='button']").focusout(function() {
    $(".drop-panel").fadeOut('fast');
  });

  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  function debounce(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this,
        args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) {
          func.apply(context, args);
        }
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) {
        func.apply(context, args);
      }
    };
  }

  //Lightbox
  $('.lightbox').magnificPopup({
    type: 'image'
    // other option
  });

  //Open Modals
  $('.open-modal-link').magnificPopup({
    type: 'inline',
    midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
  });
  
  //Close Settings Box
  $('#settings-modal button').on('click', function(e) {
      var action = $(this).data('magnific-action');
      if (action.indexOf('save') > -1 || action.indexOf('cancel') > -1) {
          $.magnificPopup.close();
      }
  });
  //Masonary Wookmark

  var wookmark,
    container = '#masonry-container',
    winWidth = $(window).width(),
    itemWidth = 215,
    offset = 15,
    $moreImagesBtn = $("#load-more-images"),
    $container = $(container);
		
	if($('#masonry-container').parents().attr('class') == 'mcco_story_page'){
		itemWidth = 350; 
	}
  //Breakpoint
  function breakpointWookmark() {
    if ($container.css("marginBottom") !== '0px') {
      initWookmark(itemWidth, offset);
    } else {
      initWookmark((winWidth - 30), 10);
    }
  }

  function initWookmark(itemWidth, offset) {
    wookmark = new Wookmark(container, {
      offset: offset, // Optional, the distance between grid items
      itemWidth: itemWidth, // Optional, the width of a grid item
      flexibleWidth: true,
      align: 'left'
    });

    //Load More Images into Wookmark
    $moreImagesBtn.click(function() {
      var $items = $('li', $container),
      $firstTen = $items.slice(0, 10).clone().css('opacity', 0);
      $container.append($firstTen);

      wookmark.initItems();
      wookmark.layout(true, function() {
        //Fade in items
        setTimeout(function() {
          $firstTen.css('opacity', 1);
        }, 300);
      });
    });
  }

  if ($container.length) {

    //Update the WinWidth variable when window is resized
    window.onresize = function() {
      winWidth = $(window).width();
    };

    //Init lightbox
		if($('#masonry-container').parents().attr('class') != 'mcco_story_page'){
			$('#masonry-container').magnificPopup({
				delegate: 'li:not(.inactive) a',
				type: 'image',
				gallery: {
					enabled: true
				}
			});
		}

    imagesLoaded('#masonry-container', function() {
      breakpointWookmark();
    });

    //Make sure elements adjust correctly (safari)
    $(window).load(function() {
      setTimeout(function() {
        breakpointWookmark();
      }, 500);
    });

    //Resize to mobile view at breakpoint
    var myEfficientFn = debounce(function() {
      breakpointWookmark();
      setTimeout(function() {
        breakpointWookmark();
      }, 500);
    }, 500);

    window.addEventListener('resize', myEfficientFn);

  } //endif
  
  function build_owl() {
    $("#promo-slide").owlCarousel({
      stagePadding: 50,
      loop:true,
      margin:25,
      nav:true,
      responsive: {
        0: {
          items: 1
        },
        768: {
          items: 3
        }, 
        1199: {
          items: 3
        }
      }
    }); 
  }
  
  build_owl();
  
  //Widget Slider
  $('.module-slider').owlCarousel({
    loop:true,
    dots: true,
		autoplay: true,
		fade: true,
		slideAnimationDuration: 400,
    items: 1
  });

  //Widget Slider
  $('.org-chart').owlCarousel({
    items: 7,
    itemsDesktop: [1249, 5],
    itemsDesktopSmall: [991, 3],
    navigation: true,
    pagination: false,
    navigationText: false
  });

  //Calendar functions

  var $eventsCalendar = $('table.calendar'),
      resizeTimer,
      widgetStatus = is_widget();

  function initToolTips() {
    $('.tooltip').each(function() {
    var currentToolTip = $(this);
      currentToolTip.qtip({
        content: {
          text: currentToolTip.nextAll(".tooltip-content:first")
        },
        hide: {
          //event: false,
          delay: 300,
          fixed: true
        },
        show: {
          event: 'click mouseenter',
          solo: true
        },
        position: {
          viewport: $(window),
          my: 'top left',
          at: 'bottom left'
        },
        events: {
          render: function(event, api) {
            if (currentToolTip.closest('.cal-widget').length > 0) {  
              $('.qtip-content').addClass('widget');
            } 
            //Malihu - Works but not on resize
            // $('.qtip-content').mCustomScrollbar({
            //   theme: "minimal-dark" 
            // });
          }
        }
      });
    }); 
  }

  function is_widget() {
    if ($('table.calendar').width() < 600) {
      $('table.calendar').addClass('cal-widget');
      $('.calendar-title').addClass('widget');
      return true;
    } else {
      $('table.calendar').removeClass('cal-widget');
      $('.calendar-title').removeClass('widget');
      return false;
    }
  } 

  //Check for Calendar update on window resize
  $(window).on('resize', function(e) {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {
      //Check if widget breakpoint has changed
      if (widgetStatus !== is_widget()) {
        //Reset Widget Status
        widgetStatus = is_widget();
        //Clear our and reset tooltip
        $('.tooltip').each(function() {
          //console.log($(this).qtip('api'));
          var qtipped = $(this).qtip('api');
          //move element back to original place in dom
          var $origHTML = $('#' + qtipped._id ).find('.qtip-content');
          if ($origHTML) {
            //Remove all 
            if ($origHTML.find('.all-events').length > 0) {
              console.log('all events action');
              $origHTML.find('.all-events').removeAttr('style');
              $(this).parents('td').append($origHTML.html());
            } else {
              console.log('events action');
              $origHTML.find('.event-details').removeAttr('style');
              $(this).parents('li').append($origHTML.html());
            }
            $('.calTooltip').find('.tooltip').removeClass('tooltip');
            $('.calTooltip').find('.tooltip-content').removeClass('tooltip-content');
          }
          $(this).qtip('destroy', true);
        });
        initCalendar();
      } //endif
    }, 250);
  });

  function initCalendar(action) {
    $('.calTooltip').each(function() {      
      //Check if is widget or cal
      $toolTip = is_widget() ? $(this).find('.day') : $(this).find('ol > li > a');
      $toolTipContent = is_widget() ? $(this).find('.all-events') : $toolTip.next();
      //Place classes in thier correct places before init tooltips
      $toolTip.addClass('tooltip');
      $toolTipContent.addClass('tooltip-content');
    });
    //Classes set now init tooltips
    initToolTips();
  }  
  //Initialize Cal
  initCalendar();

  //Trunk8
  function trunk(obj, showlines) {
    showlines = typeof showlines !== 'undefined' ? showlines : 2;
    obj.trunk8({
      lines : showlines
    });
  }

  //Hompage Hero
  $( '#example1' ).sliderPro({
      height: '450',
      width: '100%',
      arrows: false,
      buttons: false,
      waitForLayers: true,
      thumbnailWidth: '25%',
      thumbnailHeight: 120,
      thumbnailPointer: true,
      //thumbnailTouchSwipe: false,
      slideDistance: 0,
      autoplay: true,
      fade: true,
      autoScaleLayers: true,
			slideAnimationDuration: 400,
      captionFadeDuration: 300,
      breakpoints: {
        992: {
          thumbnailWidth: '50%',
          thumbnailHeight: 89,
          thumbnailTouchSwipe: false
        },
        768: {
          thumbnailWidth: '100%',
          thumbnailHeight: 89,
          thumbnailTouchSwipe: false
        }
      },
      showLayersComplete: function() {
        $('.sp-slides, .sp-thumbnails').animate({opacity : 1}, 500);
            trunk($('h3.sp-title > span'));
            trunk($('.sp-thumbnail-title > div'));
      }, 
      gotoSlide: function() {
        trunk($('h3.sp-title > div'));
      }
    });

    //Reinit Trunk8 on window resize for homepage slider
    $(window).resize(function (event) {
      trunk($('.sp-thumbnail-title > div, h3.sp-title > div'));
    });

  //Toggle Tags
  var $tags = $('.tagged a'),
    removeMSG = 'Remove from My Topics',
    addMSG = 'Add to My Topics';

  $tags.each(function() {
    var $this = $(this);
    if ($this.hasClass('badge-success')) {
      $this.qtip('option', 'content.text', removeMSG);
    } else {
      $this.qtip('option', 'content.text', addMSG);
    }
  });
  var $tools_tags = $('.page-tools a.tool-link'),
    tools_removeMSG = 'Remove from My Dashboard',
    tools_addMSG = 'Add to My Dashboard';

  $tools_tags.each(function() {
    var $this = $(this);
    if ($this.find('span.links-toggle-icon').hasClass('added icon-Icon_Check-Mark-Circle-Filled')) {
      $this.qtip('option', 'content.text', tools_removeMSG);
    } else {
      $this.qtip('option', 'content.text', tools_addMSG);
    }
  });

  /*$('.tagged a').click(function(event) {
    event.preventDefault();
    var $this = $(this);
    if ($this.hasClass('badge-success')) {
      $this.removeClass('badge-success');
      $this.qtip('option', 'content.text', addMSG);
    } else {
      $this.addClass('badge-success');
      $this.qtip('option', 'content.text', removeMSG);
    }

  });*/

  //Remove Read Only - Feedback form

  $('input.toggle-readonly').click(function() {
    $(this).parent().parent().find('.readonly').removeAttr('readonly');
  });

  $('a.reset').click(function(event) {
    event.preventDefault();
    $('form#feedback').trigger("reset");
  });

  // IE9 HTML 5 Placeholder polyfill
  $('.container.main input, .container.main textarea, .off-canvas__content input, .off-canvas__content textarea').placeholder();

  $('#parentHorizontalTab').easyResponsiveTabs({
    type: 'default', //Types: default, vertical, accordion
    width: 'auto', //auto or any width like 600px
    fit: true, // 100% fit in a container
    closed: 'accordion', // Start closed if in accordion view
    tabidentify: 'hor_1', // The tab groups identifier
    inactive_bg: '#f6f9fc',
    active_border_color: '#7eb0cb',
    activate: function(event) { // Callback function if tab is switched
      var $tab = $(this);
      var $info = $('#nested-tabInfo');
      var $name = $('span', $info);
      $name.text($tab.text());
      $info.show();
    }
  });

  $('#ChildVerticalTab_1').easyResponsiveTabs({
    type: 'vertical',
    width: 'auto',
    fit: true,
    tabidentify: 'ver_1', // The tab groups identifier
    activetab_bg: '#f6f9fc', // background color for active tabs in this group
    inactive_bg: '#fff', // background color for inactive tabs in this group
    active_border_color: '#fff', // border color for active tabs heads in this group
    active_content_border_color: '#fff' // border color for active tabs contect in this group so that it matches the tab head border
  });


  $('#ChildVerticalTab_2').easyResponsiveTabs({
    type: 'vertical',
    width: 'auto',
    fit: true,
    tabidentify: 'ver_2',
    activetab_bg: '#f6f9fc', // background color for active tabs in this group
    inactive_bg: '#fff', // background color for inactive tabs in this group
    active_border_color: '#fff', // border color for active tabs heads in this group
    active_content_border_color: '#fff' // border color for active tabs contect in this group so that it matches the tab head border
  });


  $('#ChildVerticalTab_3').easyResponsiveTabs({
    type: 'vertical',
    width: 'auto',
    fit: true,
    tabidentify: 'ver_3',
    activetab_bg: '#f6f9fc', // background color for active tabs in this group
    inactive_bg: '#fff', // background color for inactive tabs in this group
    active_border_color: '#fff', // border color for active tabs heads in this group
    active_content_border_color: '#fff' // border color for active tabs contect in this group so that it matches the tab head border
  });

  //Instagram Play Button

 /*  function instalightbox() {
    $('a[rel=magnific-instagram]').magnificPopup({
        type: 'inline',
        preloader: false,
        callbacks: {
        open: function() {
          var $iframe = $('#instagram-popup iframe');
          //Created Loader
          var winHeight = $(window).height();
          $('body').append('<div class="loader"></div>');
          $iframe.css({'opacity' : 0}); //Hide ititially, video takes time to load some times, avoid flash of unstyled content
          $('#instagram-popup').fadeIn(function() {
            var counter = 1;
            function check_condition() {
              if ( $iframe.height() < winHeight || counter === 4 ) { //Only Try 4 Times
                //Height set show iframe
                $('.loader').remove();
                $iframe.animate({'opacity' : 1});
                //Remove Loader
                console.log('height set show iframe');
              } else {
                //Height still not fully loaded try again
                counter++;
                setTimeout(check_condition, 500);
              }
            }
            check_condition();   
          });
        }, 
        close: function() {
          console.log('closed');
          $('#instagram-popup iframe').css({'opacity' : 0});
        }
      }
    });
  }  

  var getURL = $('.instagram-single').find('img').data('insta-url'),
  instaURL = 'https://api.instagram.com/oembed?url=http://instagr.am/p/' + getURL + '/&callback=?';
  if (getURL !== undefined) {
    //Create the button for video popup
    $.getJSON(instaURL).success(function(data) {
      $('body').append('<div id ="instagram-popup">' + data.html + '</div>');
			//var img_width = (data.width/3);
			//data.width = (data.width - img_width);
      //$('#instagram-popup').width(data.width);
			$('#instagram-popup').width(450);
      //Add Thumbnail
      //Below line commented to fix home page instagram issue
      //$(".instagram-single img").attr("src",data.thumbnail_url);
      //Decide What Icon to Show
      var icon = data.thumbnail_width === data.thumbnail_height ? 'icon-expand' : 'icon-play';
      $('.instagram-single__functions').append('<a href="#instagram-popup" rel="magnific-instagram" class="' + icon + '"></a>');
      instalightbox();
      //ERROR
      }).error(function() {
        $(".instagram-single img").attr("src",'/assets/images/instagram-no-image.png');
    }); 
  } */
    


});
})(jQuery);