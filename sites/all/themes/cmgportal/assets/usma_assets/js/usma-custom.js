(function ($) {
var bxSlider = null;
$(document).ready(function(){ 
	$('body .main-content').find('a').css('pointer-events','none');
	//$('body').css('pointer-events','all');
	$('#horizontalTab').easyResponsiveTabs({
    type: 'default', //Types: default, vertical, accordion           
    width: 'auto', //auto or any width like 600px
    fit: true,   // 100% fit in a container
    tabidentify: 'vert',
    closed: 'accordion', // Start closed if in accordion view
    activate: function(event) { // Callback function if tab is switched
        var $tab = $(this);
        var $info = $('#tabInfo');
        var $name = $('span', $info);
        $name.text($tab.text());
        $info.show();
    }
  });
});
$(window).load(function () {
	$('body .main-content').find('a').css('pointer-events','all');
	$('body').css('pointer-events','all');
	//$('.menu__link__dropdown .dropdown__sublink-group .sublinks-toggle').css('pointer-events','all');
/*if(location.pathname == "/hear-landing-page")
{
	 $('section.page-builder-row-section-1').addClass("autoload");
}*/
 //alert("here");
	
bxSlider = $('.bxslider').bxSlider({
    speed: 500,
    pause: 5000,
    auto: true
  });

$('.collapse').collapse({
  toggle: false
});

// NAVGOCO
$('.nav').navgoco({
    slide: {
        duration:300,
        easing: 'swing'
    },
    caretHtml: '<svg class="icon-chevron-right"><use xlink:href="https://usma-portal-qa.gene.com/sites/all/themes/custom/usma/images/icons.svg#icon-chevron-right"></use></svg>',
    accordion: true
});

// JS ACCORDION CONTENT
$("html").addClass("js");
$(function() {
  $("#acc2").accordion({
      obj: "div", 
      wrapper: "div", 
      el: ".h", 
      head: "h4, h5", 
      next: "div", 
      showMethod: "slideFadeDown",
      hideMethod: "slideFadeUp"
    });
  $("#main").accordion({
      obj: "div", 
      wrapper: "div", 
      el: ".h", 
      head: "h4", 
      next: "div", 
      //initShow : "div.outer:first",
      event : 'click'
    });
  //$("html").removeClass("js");
});

$('.content-accordion').navgoco({
    slide: {
        duration:300,
        easing: 'swing'
    },
    caretHtml: '<svg class="icon-chevron-right"><use xlink:href="https://usma-portal-qa.gene.com/sites/all/themes/custom/usma/images/icons.svg#icon-chevron-right"></use></svg>',
    accordion: true
});

// ADD PADDING WHEN MENU COLLAPSED ON DESKTOP
$('#nav-sidepanel-close, #nav-sidepanel-open').click(function() {
    $('.main-content-menu-close').toggleClass("main-content-menu-padding");
});

$('.fotorama').fotorama({
   nav: 'thumbs',
   click: false,
   width: '100%',
   ratio: 770/376,
   maxwidth: '100%',
   thumbwidth: 133,
   thumbheight: 75,
   autoplay: true,
   transition: 'crossfade'
});

/* $('.horizontalTab').easyResponsiveTabs({
    type: 'default', //Types: default, vertical, accordion           
    width: 'auto', //auto or any width like 600px
    fit: true,   // 100% fit in a container
    closed: 'accordion', // Start closed if in accordion view
    activate: function(event) { // Callback function if tab is switched
        var $tab = $(this);
        var $info = $('#tabInfo');
        var $name = $('span', $info);
        $name.text($tab.text());
        $info.show();
    }
}); */


// IMAGE MAP FUNCTION
$('img[usemap]').rwdImageMaps();

// RESPONSIVE VIDEO
$(function() {
    
    var $allVideos = $("iframe[src^='http://player.vimeo.com'], iframe[src^='http://www.youtube.com'], object, embed"),
    $fluidEl = $("figure");
        
  $allVideos.each(function() {
  
    $(this)
      // jQuery .data does not work on object/embed elements
      .attr('data-aspectRatio', this.height / this.width)
      .removeAttr('height')
      .removeAttr('width');
  
  });
  
  $(window).resize(function() {
  
    var newWidth = $fluidEl.width();
    $allVideos.each(function() {
    
      var $el = $(this);
      $el
          .width(newWidth)
          .height(newWidth * $el.attr('data-aspectRatio'));
    
    });
  
  }).resize();

});


// var equalheight = function(container) {

//   var currentTallest = 0;
//   var currentRowStart = 0;
//   var rowDivs = [];
//   var $el;
//   var topPosition = 0;
//   $(container).each(function() {

//    $el = $(this);
//    $($el).height('auto');
//    topPostion = $el.position().top;

//    if (currentRowStart !== topPostion) {
//      for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
//        rowDivs[currentDiv].height(currentTallest);
//      }
//      rowDivs.length = 0; // empty the array
//      currentRowStart = topPostion;
//      currentTallest = $el.height();
//      rowDivs.push($el);
//    } else {
//      rowDivs.push($el);
//      currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
//   }
//    for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
//      rowDivs[currentDiv].height(currentTallest);
//    }
//   });
// };

  // function doEqualHeight() {
  //   if($(window).width() > 976) {
  //     equalheight('.equal-height');
  //   } else {
  //     $('.equal-height').css({'height':'auto'});
  //   }
  // }

  // $(window).load(doEqualHeight);


  // $(window).resize(doEqualHeight);

  $('.primary-nav-wrapper').sidepanel({
    // 'onAfterResize': function() {
    //   doEqualHeight();
    // },
    // 'onAfterOpen': function() {
    //   doEqualHeight();
    //   bxSlider.reloadSlider();
    // },
    'onAfterClose': function() {
      bxSlider.reloadSlider();
    }
  });

  // ADD BORDER RIGHT TO MAIN CONTENT IF SIDEBAR CONTENT EXISTS
  if( $('.content-secondary').length )
  {
       $('.content-primary').addClass('sidebar-border-right');
  }

  function setHeight() {

    // get heights and widths of window and main content
    var windowWidth = $(window).innerWidth();
    var windowHeight = $(window).innerHeight();
    var contentHeight = $('#main-content').height();

    // get new height if content is smaller than window height
    newHeight = (contentHeight < windowHeight) ? windowHeight : contentHeight
    
    // only apply if window is larger than 992
    if(windowWidth >= 992){
      $('.primary-nav-wrapper').css('min-height', newHeight);
    }else{
      $('.primary-nav-wrapper').css('min-height', 0);
    }
  };

  setHeight();

  $('.content-accordion-container a').click(function() {
    setHeight();
  });
  
  $(window).on('resize', function() {
    setHeight();
  });
  
  /******** Script to autoload images in landings pages **********/
var
    $w = $(window),
    $d = $(document);

    $('.autoload').each(function() {
        var
        $that       = $(this),
        page        = 2,
        loading     = false,
        fullyLoaded = false;

        $w.on('scroll', function() {
            var scrollPercentage = 100 * $w.scrollTop() / ($d.height() - $w.height());
            if (isNaN(scrollPercentage))
                scrollPercentage = 0;

            if ( scrollPercentage > 75 ) {
                if (loading || fullyLoaded)
                    return;

                loading = true;
                $.post(window.location.href, { page: page }, function(data) {
                    if (data == '') {
                        fullyLoaded = true;
                        return;
                    }
                    loading = false;
                    $that.append(data);
                    page++;
                },'html');
            }
        });
    });
    $w.scroll();
	
});
})(jQuery);