(function($) {

  $(document).ready(function() {
    //Back to top
    var duration = 500;
    $('.return-top-top').click(function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: 0}, duration);
        return false;
    });


    var
    $drop        = $('.menu__link__dropdown'),
    $menulink    = $('.menu__link__has-dropdown'),
    $close       = $('.close-drop-trigger'),
    closeTimeout = null;

    function setCloseTimeout() {
        closeTimeout = setTimeout(function() {
            $drop.removeClass('show');
            $menulink.removeClass('active');
        }, 5000);
    }

    function clearCloseTimeout() {
        try {
            clearTimeout(closeTimeout);
        } catch (e) {}
    }

    $drop.each(function() {
        var
        $sublinkGroups = $(this).find('.dropdown__sublink-group');
        $sublinkGroups.each(function() {
            var
            // $handle = $(this).find('.dropdown__sublink-handle'),
            $handle = $(this).find('.sublinks-toggle'),
            that    = $(this);

            $handle.click(function(e) {
                e.preventDefault();
                e.stopPropagation();

                var visible = that.hasClass('open');

                $sublinkGroups.removeClass('open');

                if (!visible) {
                    that.addClass('open');
                }
            });
        });
    });

    $(document).click(function() {
        clearCloseTimeout();
        $drop.removeClass('show');
        $menulink.removeClass('active');
    });

    $close.click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        clearCloseTimeout();
        $drop.removeClass('show');
        $menulink.removeClass('active');
    });

    var menu_toggle = $('.page-head__compact__nav-toggle');
    $(menu_toggle).click(function(e){
        e.preventDefault();
        e.stopPropagation();
        $(menu_toggle).toggleClass('menu-active');
        $('body').toggleClass('menu-active');
        return false;
    });

    var mobile_login = $('.menu__link__login'),
        mobile_login_sub = $('.menu-link__sub-menu');
    $(mobile_login).click(function(e) {
        e.preventDefault();
        $(mobile_login_sub).addClass('active');
    });

    $('.menu-link__sub-menu .close-drop-trigger').click(function(e) {
        e.preventDefault();
        $(mobile_login_sub).removeClass('active');
    });

    var
    $carousel = $('.carousel__slides'),
    $carouselPil = $('.carousel__pils__item');
    $carouselPil.eq(0).addClass('selected');

    $carousel.slick({
        arrows: false,
        dots: false,
        swipe: true,
        autoplay: true,
        autoplaySpeed: 4000,
    }).on('afterChange', function(slick, currentSlide) {
        $carouselPil.removeClass('selected').eq(currentSlide.currentSlide).addClass('selected');
    });

    $('.carousel__controls__control--prev').click(function(e) {
        e.preventDefault();
        $carousel.slick("slickPrev");
    });

    $('.carousel__controls__control--next').click(function(e) {
        e.preventDefault();
        $carousel.slick("slickNext");
    });

    $carouselPil.click(function(e){
        e.preventDefault();
        slideIndex = $(this).index();
        $carousel.slick('slickGoTo', slideIndex);
    });

    var
    $journalItems = $('.journal__items');

    $journalItems.slick({
        arrows: false,
        dots: false,
        swipe: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1023,
                settings: "unslick"
            },
        ]
    });

    $('.journal__controls__control--prev').click(function(e) {
        e.preventDefault();
        $journalItems.slick("slickPrev");
    });

    $('.journal__controls__control--next').click(function(e) {
        e.preventDefault();
        $journalItems.slick("slickNext");
    });


    var
    $btnLoadMore       = $('.btn--loadmore'),
    $assetLinksWrapper = $('.asset-links__wrapper'),
    assetLinkPage      = 2;

    $btnLoadMore.on('click', function(e) {
        e.preventDefault();
        $.post(window.location.href, { page: assetLinkPage }, function(data) {
            if (data == '')
            {
                $btnLoadMore.remove();
                return;
            }
            $assetLinksWrapper.append(data);
            assetLinkPage++;
        },'html');
    });

    var
    $w = $(window),
    $d = $(document);

    $('.autoload').each(function() {
        var
        $that       = $(this),
        page        = 2,
        loading     = false,
        fullyLoaded = false;


        $w.on('scroll', function() {
            if (loading || fullyLoaded)
                return;
            loading = true;

            var scrollPercentage = 100 * $w.scrollTop() / ($d.height() - $w.height());
            if ( scrollPercentage > 75 ) {
                $.post(window.location.href, { page: page }, function(data) {
                    loading = false;

                    if (data == '')
                    {
                        fullyLoaded = true;
                        return;
                    }
                    $that.append(data);
                    page++;
                },'html');
            }
        });
    });
    $w.scroll();

    var
    $searchToggle = $('.js-search-toggle'),
    $menuItem     = $('.menu__item');

    $searchToggle.click(function(e) {
        e.preventDefault();
        $body.addClass('inline-search-active');
        $(this).parent().addClass('menu__item--search-active');
    });

    $(document).keyup(function(e) {
         if (e.keyCode == 27) {
            $body.removeClass('inline-search-active');
            $menuItem.removeClass('menu__item--search-active');
        }
    });

    var $body = $('body');
    var $userMenu = $('.user-menu');
    var $userMenuBlocker = $('.user-menu-blocker');
    $('.toggle-user-menu').click(function(e) {
        e.preventDefault();
        $userMenu.toggleClass('user-menu--active');
        $body.toggleClass('user-menu-active');
    });

    $userMenu.find('.user-menu__nav__list-item--with-sub-menu').on('click', '.user-menu__nav__list-item-handle', function(e) {
        e.preventDefault();
        $(this).parent().toggleClass('open');
    });

    // if (!$('html').hasClass('no-touch')) {
        var $mainMenus = $('.menu__item');
        var $subMenus = $('.menu__item > .menu__item__options > div');
        var $subSubMenus = $('.menu__item > .menu__item__options > div > .menu__item__options > div');

        $('.menu__item__options').each(function() {
            var $handle = $(this).prev();
            var $parent = $(this).parent();
            var $self = $(this);
            $handle.click(function(e) {
                e.preventDefault();
                if (!$('html').hasClass('no-touch')) {
                    $parent.toggleClass('hover');
                }
                var active = $parent.hasClass('active');
                if ($parent.hasClass('menu__item')) {
                    $mainMenus.removeClass('active');
                    $subMenus.removeClass('active');
                    $subSubMenus.removeClass('active');
                } else if ($self.hasClass('menu__item__options--secondary')) {
                    $subMenus.removeClass('active');
                    $subSubMenus.removeClass('active');
                } else {
                    $subSubMenus.removeClass('active');
                }

                if (!active) {
                    $parent.toggleClass('active');
                }
            });
        });

        $subMenus.on( 'mouseenter mouseleave', function() {
              $(this).toggleClass('hover');
            }
        );
        $subSubMenus.on( 'mouseenter mouseleave', function() {
              $(this).toggleClass('hover');
            }
        );

        $('.menu__item > .menu__item__options').on('mouseleave', function() {
            $('html, body').animate({scrollTop: 0}, 250);
        });
    // }
    //

    $userMenuBlocker.click(function(e) {
        e.preventDefault();

        $body.removeClass('user-menu-active');
        $body.removeClass('inline-search-active');

        $userMenu.removeClass('user-menu--active');
        $menuItem.removeClass('menu__item--search-active');
    });

    var scrollDiv = $('.scrollDiv');

    scrollDiv.mCustomScrollbar({
        axis: "y",
        theme: "custom"
    });

    var
    $settingsTrigger = $('.js-settings-trigger'),
    $closeSettings   = $('.js-close-settings'),
    $settingsModal   = $('.settings');

    $settingsTrigger.click(function(e) {
        e.preventDefault();

        $body.addClass('settings-active');
        $body.removeClass('user-menu-active');
        $userMenu.removeClass('user-menu--active');
    });

    $('#settings-modal').each(function() {
        var $alphas = $(this).find('.alphas');
        var $availableTags = $(this).find('.available-tags');
        $alphas.on('click', 'a', function(e) {
            e.preventDefault();
            var $anchor = $($(this).attr('href'));
            $availableTags.mCustomScrollbar("scrollTo",$anchor.position().top);
        });
    });

    $closeSettings.click(function(e) {
        e.preventDefault();

        $body.removeClass('settings-active');
    });

    $settingsModal.click(function(e) {
        e.stopPropagation();
    });


    // Masonry Settings

    // init Masonry
    var $grid = $('.event-gallery').masonry({
        itemSelector: '.event-gallery__item',
        columnWidth: '.grid-sizer',
        percentPosition: true
    });
    //layout Masonry after each image loads
    $grid.imagesLoaded().progress( function() {
      $grid.masonry('layout');
    });


    var
    $videoCarousel = $('.js-video-carousel');

    $videoCarousel.slick({
        arrows: false,
        dots: false,
        infinite: true,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });

    var
    $videoNext = $('.video__carousel-controls__next'),
    $videoPrev = $('.video__carousel-controls__prev');

    $videoNext.click(function(e) {
        e.preventDefault();

        $videoCarousel.slick('slickNext');
    });

    $videoPrev.click(function(e) {
        e.preventDefault();

        $videoCarousel.slick('slickPrev');
    });

    // Filter Logic

    var
    $categoryToggle     = $('.js-category-toggle'),
    $yearToggle         = $('.js-year-toggle'),
    $subOptions         = $('.event-filter__sub-options'),
    $subOptionsCategory = $('.event-filter__sub-options__category'),
    $subOptionsYear     = $('.event-filter__sub-options__year'),
    $categorySubItem    = $('.event-filter__sub-options__category .sub-option-item'),
    $yearSubItem        = $('.event-filter__sub-options__year .sub-option-item');

    $categoryToggle.click(function(e) {
        $subOptionsYear.removeClass('event-filter__sub-options__category--open');
        $yearToggle.removeClass('event-filter__options__option--open');
        $yearToggle.find('.chosen').removeClass('chosen--active').text('');
        $(this).toggleClass('event-filter__options__option--open');
        $subOptionsCategory.toggleClass('event-filter__sub-options__category--open');
    });

    $yearToggle.click(function(e) {
        $subOptionsCategory.removeClass('event-filter__sub-options__category--open');
        $categoryToggle.removeClass('event-filter__options__option--open');
        $categoryToggle.find('.chosen').removeClass('chosen--active').text('');
        $(this).toggleClass('event-filter__options__option--open');
        $subOptionsYear.toggleClass('event-filter__sub-options__category--open');
    });

    $categorySubItem.click(function(e) {
        $subOptionsCategory.removeClass('event-filter__sub-options__category--open');
        $categoryToggle.removeClass('event-filter__options__option--open');
        $categoryToggle.addClass('event-filter__options__option--active');
        $categoryToggle.find('.chosen').addClass('chosen--active').text($(this).text());
    });

    $yearSubItem.click(function(e) {
        $subOptionsCategory.removeClass('event-filter__sub-options__category--open');
        $yearToggle.removeClass('event-filter__options__option--open');
        $yearToggle.addClass('event-filter__options__option--active');
        $yearToggle.find('.chosen').addClass('chosen--active').text($(this).text());
    });

    // Gallery fancybox

    (function ($, F) {
        F.transitions.resizeIn = function() {
            var previous = F.previous,
                current  = F.current,
                startPos = previous.wrap.stop(true).position(),
                endPos   = $.extend({opacity : 1}, current.pos);

            startPos.width  = previous.wrap.width();
            startPos.height = previous.wrap.height();

            previous.wrap.stop(true).trigger('onReset').remove();

            delete endPos.position;

            current.inner.hide();

            current.wrap.css(startPos).animate(endPos, {
                duration : current.nextSpeed,
                easing   : current.nextEasing,
                step     : F.transitions.step,
                complete : function() {
                    F._afterZoomIn();

                    current.inner.fadeIn("fast");
                }
            });
        };

    }(jQuery, jQuery.fancybox));

    var

    $fancybox = $('.fancybox');

    $fancybox
        .attr('rel', 'gallery')
        .fancybox({
            nextMethod : 'resizeIn',
            nextSpeed  : 250,
            mouseWheel : false,
            margin     : [20, 100, 20, 100],
            // minWidth   : 280,
            prevMethod : false,

            helpers : {
                title : {
                    type : 'outside'
                }
            }
        });

    if ($('.cmsr').size() > 0) {

        if ($(window).width() >= 768) {
            // Make the hero underlines work correctly when words break
            $('.cmsr__hero h1, .cmsr__hero h2').each(function() {
                var self = $(this);
                // Make sure there are spaces in between words
                var tmp = $('<div />').html($(this).html().replace(/([^<])</, '$1 <').replace('<br>', '|BR| '));
                var words = tmp.text().split(/\s+/);
                var html = [];
                var defaultBreakPoint = null;
                for (var i = 0, n = words.length; i < n; i++) {
                    if (words[i] == '|BR|') {
                        defaultBreakPoint = i;
                        continue;
                    }
                    html.push('<span>'  + words[i] + '</span>');
                }
                $(this).html(html.join(' '));

                function checkWordPosition() {
                    var words = self.find('span');
                    var lastOffsetTop = 0;
                    var lines = [];
                    var line = [];
                    words.each(function(i) {
                        var offsetTop = $(this).offset().top;

                        var useDefaultBreak = $(window).width() > 1000 && offsetTop == lastOffsetTop && i == defaultBreakPoint;

                        if (offsetTop > lastOffsetTop || useDefaultBreak) {
                            if (line.length > 0) {
                                lines.push(line);
                            }
                            line = [];
                        }
                        var word = $(this).html();
                        line.push(word);

                        if (useDefaultBreak) {
                            lastOffsetTop = 10000000000;
                        } else {
                            lastOffsetTop = offsetTop;
                        }
                    });
                    lines.push(line);

                    var html = [];
                    for (var i = 0, n = lines.length; i < n; i++) {
                        html.push('<span class="underline">' + lines[i].join(' ') + '</span>');
                        if (i + 1 < n) {
                            html.push('<br />');
                        }
                    }

                    self.html(html);
                }

                checkWordPosition();

                // $(window).resize(checkWordPosition);
            });
        }

        var cmsrHero = $('.cmsr__hero');
        var cmsrHeroVideo = cmsrHero.find('.cmsr__hero__video');
        // disabled temporarily by client request
        //cmsrHero.on('click', '.cmsr__hero__cell--main', function(e) {
            //e.preventDefault();
            //cmsrHero.addClass('show-video');
        //}).on('click', '.cmsr__hero__video__close', function(e) {
            //e.preventDefault();
            //cmsrHero.removeClass('show-video');
            //cmsrHeroVideo.html(cmsrHeroVideo.html());
        //});

        var heroParticleConfig = {
          "particles": {
            "number": {
              "value": 80,
              "density": {
                "enable": true,
                "value_area": 800
              }
            },
            "color": {
              "value": "#ffffff"
            },
            "shape": {
              "type": "circle",
              "stroke": {
                "width": 0,
                "color": "#000000"
              },
              "polygon": {
                "nb_sides": 5
              },
              "image": {
                "src": "img/github.svg",
                "width": 100,
                "height": 100
              }
            },
            "opacity": {
              "value": 0.25,
              "random": false,
              "anim": {
                "enable": false,
                "speed": 1,
                "opacity_min": 0.1,
                "sync": false
              }
            },
            "size": {
              "value": 4,
              "random": true,
              "anim": {
                "enable": false,
                "speed": 40,
                "size_min": 0.1,
                "sync": false
              }
            },
            "line_linked": {
              "enable": true,
              "distance": 150,
              "color": "#ffffff",
              "opacity": 0.4,
              "width": 1
            },
            "move": {
              "enable": true,
              "speed": 1.5,
              "direction": "none",
              "random": false,
              "straight": false,
              "out_mode": "out",
              "bounce": false,
              "attract": {
                "enable": false,
                "rotateX": 600,
                "rotateY": 1200
              }
            }
          },
          "interactivity": {
            "detect_on": "canvas",
            "events": {
              "onhover": {
                "enable": true,
                "mode": "bubble"
              },
              "onclick": {
                "enable": false,
                "mode": "push"
              },
              "resize": true
            },
            "modes": {
              "grab": {
                "distance": 400,
                "line_linked": {
                  "opacity": 1
                }
              },
              "bubble": {
                "distance": 400,
                "size": 4.06039842659561,
                "duration": 6.009389671361502,
                "opacity": 0.925770841263799,
                "speed": 3
              },
              "repulse": {
                "distance": 200,
                "duration": 0.4
              },
              "push": {
                "particles_nb": 4
              },
              "remove": {
                "particles_nb": 2
              }
            }
          },
          "retina_detect": true
        };


        particlesJS('particles-js', heroParticleConfig);

        var commParticleConfig = {
          "particles": {
            "number": {
              "value": 80,
              "density": {
                "enable": true,
                "value_area": 800
              }
            },
            "color": {
              "value": "#ffffff"
            },
            "shape": {
              "type": "circle",
              "stroke": {
                "width": 0,
                "color": "#000000"
              },
              "polygon": {
                "nb_sides": 5
              },
              "image": {
                "src": "img/github.svg",
                "width": 100,
                "height": 100
              }
            },
            "opacity": {
              "value": 0.1,
              "random": false,
              "anim": {
                "enable": false,
                "speed": 1,
                "opacity_min": 0.1,
                "sync": false
              }
            },
            "size": {
              "value": 8.02608477552044,
              "random": true,
              "anim": {
                "enable": false,
                "speed": 40,
                "size_min": 0.1,
                "sync": false
              }
            },
            "line_linked": {
              "enable": false,
              "distance": 150,
              "color": "#ffffff",
              "opacity": 0.4,
              "width": 1
            },
            "move": {
              "enable": true,
              "speed": 0.8,
              "direction": "none",
              "random": false,
              "straight": false,
              "out_mode": "out",
              "bounce": false,
              "attract": {
                "enable": false,
                "rotateX": 600,
                "rotateY": 1200
              }
            }
          },
          "interactivity": {
            "detect_on": "canvas",
            "events": {
              "onhover": {
                "enable": false,
                "mode": "bubble"
              },
              "onclick": {
                "enable": false,
                "mode": "push"
              },
              "resize": true
            },
            "modes": {
              "grab": {
                "distance": 400,
                "line_linked": {
                  "opacity": 1
                }
              },
              "bubble": {
                "distance": 400,
                "size": 4.06039842659561,
                "duration": 6.009389671361502,
                "opacity": 0.925770841263799,
                "speed": 3
              },
              "repulse": {
                "distance": 200,
                "duration": 0.4
              },
              "push": {
                "particles_nb": 4
              },
              "remove": {
                "particles_nb": 2
              }
            }
          },
          "retina_detect": true
        };
        particlesJS('comm-particles-js', commParticleConfig);

        $('.cmsr__content:not(.no-animation)').each(function() {
            var self = this;

            var $left = $(this).find('.cmsr__content__left');
            var $right = $(this).find('.cmsr__content__right');

            new Waypoint({
                element: self,
                handler: function(direction) {
                    $left.addClass('visible');
                    $right.addClass('visible');
                },
                offset: '50%'
            })
        });

        var currentDiagramSlide = 1;

        (function() {
            var segmentCenter = $('.segment-center');

            var frameworkDiagram = $('.cmsr__framework__diagram');
            var actionCentre = $('.action-centre');
            var actionTopLeft = $('.action-topleft');
            var actionTopRight = $('.action-topright');
            var actionBottomLeft = $('.action-bottomleft');
            var actionBottomRight = $('.action-bottomright');

            actionCentre.click(function(e) {
                e.preventDefault();
            });
            actionTopLeft.click(function(e) {
                e.preventDefault();
            });
            actionTopRight.click(function(e) {
                e.preventDefault();
            });
            actionBottomLeft.click(function(e) {
                e.preventDefault();
            });
            actionBottomRight.click(function(e) {
                e.preventDefault();
            });

            var actionArrowRight = $('.action-arrowright');
            var actionArrowLeft = $('.action-arrowleft');

            // var frameworkSliderHeading = $('.cmsr__framework__slider h2');
            // var frameworkSliderText = $('.cmsr__framework__slider p');
            var frameworkSliderNumber = $('.cmsr__framework__slider span');
            var frameworkSliderSlides = $('.cmsr__framework__slider .cmsr__framework__slide');

            new Waypoint({
                element: $('.cmsr__content--framework').get(0),
                handler: function(direction) {
                    if (frameworkDiagram.hasClass('visible')) {
                        return;
                    }
                    frameworkDiagram.addClass('firstVisible').addClass('visible');

                    setTimeout(function() {
                        frameworkDiagram.removeClass('firstVisible');

                        actionCentre.mouseenter(function() {
                            clearClasses();
                            frameworkDiagram.addClass('centre-selected');
                        }).mouseleave(function() {
                            //frameworkDiagram.removeClass('topleft-selected');
                            clearClasses();
                            restoreClass();
                        });

                        actionTopLeft.mouseenter(function() {
                            clearClasses();
                            frameworkDiagram.addClass('topleft-selected');
                        }).mouseleave(function() {
                            //frameworkDiagram.removeClass('topleft-selected');
                            clearClasses();
                            restoreClass();
                        });

                        actionTopRight.mouseenter(function() {
                            clearClasses();
                            frameworkDiagram.addClass('topright-selected');
                        }).mouseleave(function() {
                            //frameworkDiagram.removeClass('topright-selected');
                            clearClasses();
                            restoreClass();
                        });

                        actionBottomLeft.mouseenter(function() {
                            clearClasses();
                            frameworkDiagram.addClass('bottomleft-selected');
                        }).mouseleave(function() {
                            //frameworkDiagram.removeClass('bottomleft-selected');
                            clearClasses();
                            restoreClass();
                        });

                        actionBottomRight.mouseenter(function() {
                            clearClasses();
                            frameworkDiagram.addClass('bottomright-selected');
                        }).mouseleave(function() {
                            //frameworkDiagram.removeClass('bottomright-selected');
                            clearClasses();
                            restoreClass();
                        });

                        actionCentre.unbind('click').click(function(e) {
                            e.preventDefault();
                            currentDiagramSlide = 1;
                            changeSlide();
                        });

                        actionTopRight.unbind('click').click(function(e) {
                            e.preventDefault();
                            currentDiagramSlide = 2;
                            changeSlide();
                        });

                        actionBottomRight.unbind('click').click(function(e) {
                            e.preventDefault();
                            currentDiagramSlide = 3;
                            changeSlide();
                        });

                        actionBottomLeft.unbind('click').click(function(e) {
                            e.preventDefault();
                            currentDiagramSlide = 4;
                            changeSlide();
                        });

                         actionTopLeft.unbind('click').click(function(e) {
                            e.preventDefault();
                            currentDiagramSlide = 5;
                            changeSlide();
                        });

                        actionArrowLeft.click(function(e) {
                            e.preventDefault();
                            currentDiagramSlide--;
                            changeSlide();
                        });

                        actionArrowRight.click(function(e) {
                            e.preventDefault();
                            currentDiagramSlide++;
                            changeSlide();
                        });


                        function changeSlide() {
                            if (currentDiagramSlide > 5) {
                                currentDiagramSlide = 1;
                            } else if (currentDiagramSlide < 1) {
                                currentDiagramSlide = 5;
                            }

                            clearClasses();

                            frameworkSliderSlides.removeClass('visible');
                            frameworkSliderSlides.filter('[data-area=' + currentDiagramSlide + ']').addClass('visible');

                            switch(currentDiagramSlide) {
                                case 1:
                                    frameworkDiagram.addClass('centre-selected');
                                    break;
                                case 2:
                                    frameworkDiagram.addClass('topright-selected');
                                    break;
                                case 3:
                                    frameworkDiagram.addClass('bottomright-selected');
                                    break;
                                case 4:
                                    frameworkDiagram.addClass('bottomleft-selected');
                                    break;
                                case 5:
                                    frameworkDiagram.addClass('topleft-selected');
                                    break;
                            }

                            frameworkSliderNumber.html(currentDiagramSlide + ' / 5');
                        }

                        function clearClasses() {
                            frameworkDiagram.removeClass('centre-selected');
                            frameworkDiagram.removeClass('topleft-selected');
                            frameworkDiagram.removeClass('topright-selected');
                            frameworkDiagram.removeClass('bottomright-selected');
                            frameworkDiagram.removeClass('bottomleft-selected');
                        }

                        function restoreClass() {
                            switch(currentDiagramSlide) {
                                case 1:
                                    frameworkDiagram.addClass('centre-selected');
                                    break;
                                case 2:
                                    frameworkDiagram.addClass('topright-selected');
                                    break;
                                case 3:
                                    frameworkDiagram.addClass('bottomright-selected');
                                    break;
                                case 4:
                                    frameworkDiagram.addClass('bottomleft-selected');
                                    break;
                                case 5:
                                    frameworkDiagram.addClass('topleft-selected');
                                    break;
                            }
                        }

                        changeSlide(currentDiagramSlide);


                    }, 5000);
                },
                offset: '50%'
            })
        })();

        var visibleSection = false;
        $('.cmsr__belief').find('a[data-core-belief]').each(function() {
            var sectionName = $(this).data('coreBelief');
            var section = $('.cmsr__core-belief--' + sectionName);
            if (section.size() > 0) {
                $(this).click(function(e) {
                    e.preventDefault();

                    if (visibleSection) {
                        visibleSection.removeClass('visible');
                        if (visibleSection == section) {
                            visibleSection = false;
                        } else {
                            setTimeout(function() {
                                section.addClass('visible');
                                visibleSection = section;
                            }, 300);
                        }
                    } else {
                        section.addClass('visible');
                        visibleSection = section;
                    }
                })
            } else {
                $(this).click(function(e) {
                    e.preventDefault();
                });
            }
        });

        $('.cmsr__core-belief').each(function() {
            var self = $(this);

            $(this).find('.cmsr__content__slider-close').click(function(e) {
                e.preventDefault();
                self.removeClass('visible');
            });

        });

        $('.slider').each(function() {
            var self = $(this);
            var parent = $(this).parents('.cmsr__content__slider:first');
            var contentOuter = parent.next();
            var content = contentOuter.find('.cmsr__content__slider-content:first');

            contentOuter.on('click', '.cmsr__content__slider-close', function(e) {
                e.preventDefault();
                //contentOuter.hide();
                contentOuter.removeClass('visible');
            });

            self.find('a.slide-handle').each(function(i) {
                $(this).data('index', i);
            });

            content.slick({
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: contentOuter.find('.cmsr__arrow--left, .cmsr__content__slider-arrow--left'),
                nextArrow: contentOuter.find('.cmsr__arrow--right, .cmsr__content__slider-arrow--right')
            });

            self.slick({
                speed: 300,
                slidesToShow: 2,
                slidesToScroll: 1,
                prevArrow: parent.find('.cmsr__arrow--left'),
                nextArrow: parent.find('.cmsr__arrow--right'),
                asNavFor: content,
                responsive: [
                    {
                      breakpoint: 768,
                      settings: {
                        slidesToShow: 1,
                      }
                    }
                ]
            });

            // content.on('beforeChange', function(e, slick, currentSlide) {
            //     self.slick('goTo', currentSlide);
            // });

            self.on('click', 'a.slide-handle', function(e) {
                e.preventDefault();
                //contentOuter.css('opacity', 0).show();
                setTimeout(function() {
                    //contentOuter.css('opacity', 1);
                    contentOuter.addClass('visible');
                }, 250);
                // contentOuter.slideDown();
                content.slick('goTo', $(this).data('index'));
            });
        });

        $('.cmsr a[href^="#"]').each(function() {
            var section = $($(this).attr('href'));
            if (section.size() > 0) {
                $(this).click(function(e) {
                    e.preventDefault();
                    $('html,body').animate({
                        scrollTop: section.offset().top
                    });
                });
            }
        });

    }



    if ($('.whoweare').size() > 0) {

        $('.js-slider').each(function() {
            var self = $(this);
            var container = self.find('.slider__container');

            container.slick({
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: self.find('.slider__arrow--left'),
                nextArrow: self.find('.slider__arrow--right'),
                responsive: [
                    {
                      breakpoint: 768,
                      settings: {
                        slidesToShow: 1,
                      }
                    }
                ]
            });
        });

        $('.js-slider-readmore').each(function() {

            //js-readmore-btn-back
            //js-readmore-btn-next

            var self = $(this);
            var container = self.find('.slider__container');

            container.slick({
                infinite: false,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                prevArrow: self.find('.js-readmore-btn-back'),
                nextArrow: self.find('.js-readmore-btn-next'),
                responsive: [
                    {
                      breakpoint: 768,
                      settings: {
                        slidesToShow: 1,
                      }
                    }
                ]
            });
        });



        var WatchVideoBtn = $('.whoweare__btncircle');
        var Video = $('.whoweare__video');
        var VideoClose = Video.find('.whoweare__video__close');

        WatchVideoBtn.click(function(e) {
            e.preventDefault();
            //alert('wtf');
            Video.addClass('show-video');
        });

        VideoClose.click(function(e) {
            e.preventDefault();
            Video.removeClass('show-video');
        });

        Video.click(function(e) {
            e.preventDefault();
            Video.removeClass('show-video');
        });


        $('.js-animate-in').each(function() {
            var self = $(this);
            new Waypoint({
                element: self,
                handler: function(direction) {
                    self.addClass('visible');
                },
                offset: '50%'
            });
        });

        var $units = $('.whoweare__units');
        new Waypoint({
            element: $units,
            handler: function(direction) {
                $units.addClass('visible');
            },
            offset: '50%'
        });

        $('svg circle[data-href]').each(function() {
            var href = $(this).data('href');
            $(this).click(function(e) {
                e.preventDefault();
                window.location.href = href;
            });
        });

    }
    $('.js-animate-in').addClass('visible');
    $('.diagram-fade-in').css('opacity', 1);
  });

})(jQuery);