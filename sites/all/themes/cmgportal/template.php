<?php

/**
 * Implementing theme_breadcrumb.
 */
function cmgportal_breadcrumb(&$variables) {
	global $user, $base_url;
	$user = user_load($user->uid);
	$classes = 'breadcrumb';	
	$tid = 0;
	/** Start - display Breadcrumbs for all the pages **/
	if(count($variables['breadcrumb']) > 0) {
		$node = count(menu_get_object())>0 ? menu_get_object() : new stdClass();
		$j = 0;
		$breadcrumbs_formatted = array();				
		$breadcrumb = '<section class="subheader">
        <div class="container subheader-container">
          <div class="row"><div class="col-lg-9">';
		$arg0 = arg(0);
		$arg1 = arg(1);
		$arg2 = arg(2);
		$arg3 = arg(3);
		if(isset($node->type)) {
			$breadcrumbs_formatted[] = '<a href="/" class="bc-parent">Home</a><span class="bc-arrow icon-Icon_Right-Arrow"></span>';
			$breadcrumb .= '<div class="breadcrumbs">';
			if($node->type == "career_learning") { 
				if(isset($node->field_career_learning_topic['und'][0]['tid'])){
					$breadcrumbs_formatted[] = l(t("Career & Learning"), "node/" . variable_get('career_landing_page'),array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
				}
				$breadcrumbs_formatted[] = '<span class="bc-current">' . $node->title . '</span>';
			} else if($node->type == "toolbox") {
				$breadcrumbs_formatted[] = l(t("Tools"), "tools",array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
				$tool_topic_id = (isset($node->field_toolbox_topic['und']) && isset($node->field_toolbox_topic['und'][0]['tid'])) ? $node->field_toolbox_topic['und'][0]['tid'] : 0;
				$tool_topic_details = taxonomy_term_load($tool_topic_id);
				if(isset($tool_topic_details->name)){
					$breadcrumbs_formatted[] = '<span class="bc-current">' . $tool_topic_details->name . '</span><span class="bc-arrow icon-Icon_Right-Arrow"></span>';
				}
				$breadcrumbs_formatted[] = '<span class="bc-current">'. $node->title . "</span>";
			} else if($node->type == "microsite" || $node->type == "faq" || $node->type == "tutorial" || $node->type == "career_profiles" || $node->type == "webform"|| $node->type == "mcco_stories"|| $node->type == "photo_stories") {
				$nodeid = $node->nid;
				$term_id = $node->field_channel['und'][0]['tid'] ? $node->field_channel['und'][0]['tid'] : 0;
				$parent_elements =  array_reverse(taxonomy_get_parents_all($term_id));
				if($term_id>0 && ($node->type == "faq" || $node->type == "tutorial" || $node->type == "career_profiles" || $node->type == "webform"|| $node->type == "mcco_stories"|| $node->type == "photo_stories")){
					$parent_elements[] = taxonomy_term_load($term_id);
				}
				$count = count($parent_elements)-1;			
				foreach($parent_elements as $element) {
					if($element->tid == 9) {
						break;
					} else if($element->tid == 51) {
						$nodeID = variable_get('about_commercial_landing_page', '');
						$breadcrumbs_formatted[] = l($element->name, "node/" . $nodeID,array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
						break;
					} else if($element->tid == 11) {
						$breadcrumbs_formatted[] = l($element->name, "tools",array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
						break;
					} else if($element->tid == 14) {
						$nodeID = variable_get('career_landing_page', '');
						$breadcrumbs_formatted[] = l($element->name, "node/" . $nodeID,array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
						break;
					}
					$nodeID = db_query("SELECT n.nid FROM {node} as n, {taxonomy_index} as ti, {field_data_field_microsite_subnav} as sn, {field_data_field_microsite_miscellaneous} as sm WHERE n.nid=ti.nid AND ti.tid=:tid AND n.nid = sn.entity_id AND n.nid = sm.entity_id AND sn.field_microsite_subnav_value = 0 AND sm.field_microsite_miscellaneous_value = 0 AND n.type=:type AND n.status=1", array(":tid" => $element->tid, ':type' => 'microsite'))->fetchField();
					if($km_nodeID = db_query("SELECT n.nid FROM {node} as n, {taxonomy_index} as ti, {field_data_field_km_sub_navigation} as sn, {field_data_field_km_misc_page} as sm, {field_data_field_km_microsite} as km WHERE n.nid=ti.nid AND n.nid=km.entity_id AND ti.tid=:tid AND n.nid = sn.entity_id AND km.field_km_microsite_value = 1 AND n.nid = sm.entity_id AND sn.field_km_sub_navigation_value = 0 AND sm.field_km_misc_page_value = 0 AND n.type=:type AND n.status=1", array(":tid" => $element->tid, ':type' => 'microsite'))->fetchField()){
						$nodeID = $km_nodeID;
					}
					$link = ($nodeID == "" || empty($nodeID)) ?  "" :  $base_url."/node/".$nodeID ;	
					if(!empty($link) && $link != "") {				
						$breadcrumbs_formatted[] = l($element->name, "node/" . $nodeID,array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';	
						$main_microsite_nid = $nodeID;
					}
					else {
						$breadcrumbs_formatted[] = '<span class="bc-current">'.$element->name.'</span><span class="bc-arrow icon-Icon_Right-Arrow"></span>'; 
					}					
					$j++;
					if($j == $count)  break;				
				}
					/****** Start - Check if the microsite has sub navigation ***********/
				if($node->type == "microsite" && ($node->field_microsite_subnav['und'][0]['value'] == 1 || $node->field_microsite_miscellaneous['und'][0]['value'] == 1)) 
				{
					$get_node = db_query("SELECT n.nid, ttd.name FROM {node} as n, {taxonomy_index} as ti, {taxonomy_term_data} as ttd, {field_data_field_microsite_subnav} as sn, {field_data_field_microsite_miscellaneous} as sm WHERE n.nid=ti.nid AND ti.tid = ttd.tid AND ti.tid=:tid AND n.nid = sn.entity_id AND n.nid = sm.entity_id AND sn.field_microsite_subnav_value = 0 AND sm.field_microsite_miscellaneous_value = 0 AND n.type=:type AND n.status=1", array(":tid" => $node->field_channel['und'][0]['tid'], ':type' => 'microsite'))->fetchAssoc();				
					$breadcrumbs_formatted[] = l($get_node['name'], "node/" . $get_node['nid'],array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';						
				}
				else if($node->type == "microsite" && ((isset($node->field_km_sub_navigation[LANGUAGE_NONE]) && $node->field_km_sub_navigation[LANGUAGE_NONE][0]['value'] == 1) || (isset($node->field_km_misc_page[LANGUAGE_NONE]) && $node->field_km_misc_page[LANGUAGE_NONE][0]['value'] == 1))) {
					if(isset($node->field_channel[LANGUAGE_NONE])){
						$km_tid = $node->field_channel[LANGUAGE_NONE][0]['tid'];
					}
					$get_node = db_query("SELECT n.nid, ttd.name FROM {node} as n, {taxonomy_index} as ti, {taxonomy_term_data} as ttd, {field_data_field_km_sub_navigation} as sn, {field_data_field_km_misc_page} as sm WHERE n.nid=ti.nid AND ti.tid = ttd.tid AND ti.tid=:tid AND n.nid = sn.entity_id AND n.nid = sm.entity_id AND sn.field_km_sub_navigation_value = 0 AND sm.field_km_misc_page_value = 0 AND n.type=:type AND n.status=1", array(":tid" => $km_tid, ':type' => 'microsite'))->fetchAssoc();
					$breadcrumbs_formatted[] = l($get_node['name'], "node/" . $get_node['nid'],array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';	
				}
				/****** End - Check if the microsite has sub navigation ***********/
				if($node->type == "mcco_stories"){
					$breadcrumbs_formatted[] = l("Photo Stories","photo-stories/".$term_id."/".$main_microsite_nid,array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';	
				}
				$breadcrumbs_formatted[] = prepare_title_display();
				//$breadcrumbs_formatted[] = '<span class="bc-current">'.$node->title."</span>";
				//$breadcrumbs_formatted[count($breadcrumbs_formatted)-1] = "<li class='active'>".strip_tags($breadcrumbs_formatted[count($breadcrumbs_formatted)-1])."</li>";
			}
			
			else {
				if($node->type == "article")  { // Breadcrumb for articles		
					$tid = $node->field_category['und'][0]['tid'] ? $node->field_category['und'][0]['tid'] : 0;
					$href_link = $base_url."/commercial-news";
					$breadcrumbs_formatted[] = "<a href='".$href_link."' class='bc-parent'>News</a>".'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
					$query_string = "?field_category[]=".$tid;				
				}	else if($node->type == "announcements") { // Breadcrumb for announcements
					$tid = $node->field_announcement_type['und'][0]['tid'] ? $node->field_announcement_type['und'][0]['tid'] : 0;
					$href_link = $base_url."/kudos-list";
					$breadcrumbs_formatted[] = "<a href='".$href_link."' class='bc-parent'>Kudos</a>".'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
					$query_string = "?field_announcement_type[]=".$tid;			
				} else if($node->type == "page" && $node->nid != 881) { // Breadcrumb for pages
					$href_link = $base_url."/about-commercial";
					$breadcrumbs_formatted[] = "<a href='".$href_link."' class='bc-parent'>About CMG</a></li>".'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
          if(isset($node->field_is_this_a_subpage_of_about[LANGUAGE_NONE]) && $node->field_is_this_a_subpage_of_about[LANGUAGE_NONE][0]['value'] == 1){
						$term_id = $node->field_channel['und'][0]['tid'] ? $node->field_channel['und'][0]['tid'] : 0;
						$parent_elements =  array_reverse(taxonomy_get_parents_all($term_id));	
						foreach($parent_elements as $element) {
							if($element->tid == variable_get('about_comm_term_id') || $element->tid == variable_get('US_for_CMG_term_id') || $element->tid == variable_get('about_cmg_resources_term_id') || $element->tid == $node->field_channel['und'][0]['tid']){
								continue;
							}
							$nodeID = db_query("SELECT n.nid FROM {node} as n, {taxonomy_index} as ti, {field_data_field_is_this_a_subpage_of_about} as sn WHERE n.nid=ti.nid AND ti.tid=:tid AND n.nid = sn.entity_id AND sn.field_is_this_a_subpage_of_about_value = 1 AND n.type=:type AND n.status=1", array(":tid" => $element->tid, ':type' => 'page'))->fetchField();
							$link = ($nodeID == "" || empty($nodeID)) ?  "" :  $base_url."/node/".$nodeID ;	
							if(!empty($link) && $link != "") {				
								$breadcrumbs_formatted[] = l($element->name, "node/" . $nodeID,array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';	
							}
							else {
								$breadcrumbs_formatted[] = '<span class="bc-current">'.$element->name.'</span><span class="bc-arrow icon-Icon_Right-Arrow"></span>'; 
							}
						}
					}
				} else if($node->type == "leadership") {
            $breadcrumbs_formatted[] = l(t("About CMG"), "about-commercial",array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
            //$breadcrumbs_formatted[] = '<li class="active">'. strip_tags(drupal_get_title()) .'</li>';
        }
				if($tid != 0) {
					$term = taxonomy_term_load($tid);
					$term_name = $term->name;		
				}
				if(!empty($term_name)) {	
					$q_string = $query_string ? $query_string : "";
					$breadcrumbs_formatted[] = "<a href='".$href_link.$q_string."' class='bc-parent'>".$term_name.'</a><span class="bc-arrow icon-Icon_Right-Arrow"></span>';			
				}		
				$breadcrumbs_formatted[] = prepare_title_display();
				$breadcrumbs_formatted[count($breadcrumbs_formatted)-1] = '<span class="bc-current">'.strip_tags($breadcrumbs_formatted[count($breadcrumbs_formatted)-1])."</span>";
			}
		} else {
				if(arg(0) == "kudos-list" || arg(0) == "kudos_list" || arg(0) == "commercial-news" || arg(0) == "commercial_news" || arg(0) == "fav_links_more" || arg(0) == "calender_events" || arg(0) == "my-topics-feed" || arg(0) == "home-collage-images" || arg(0) == 'CMG-lead-stories'){
					$breadcrumb .= '<div class="page-title">';
					$breadcrumbs_formatted[] = strip_tags(drupal_get_title());
					$breadcrumbs_formatted[count($breadcrumbs_formatted)-1] = '<h1>'.strip_tags($breadcrumbs_formatted[count($breadcrumbs_formatted)-1])."</h1>";
				}else{
					$breadcrumbs_formatted[] = '<a href="/" class="bc-parent">Home</a><span class="bc-arrow icon-Icon_Right-Arrow"></span>';
					$breadcrumb .= '<div class="breadcrumbs">';
					if(arg(0) == 'career-and-learning' && !empty($arg1)){
						$breadcrumbs_formatted = array();
						$breadcrumbs_formatted[] = l(t("Home"), "",array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
						$career_and_learning_title = "";
						$breadcrumbs_formatted[] = l(t("Career & Learning"), "node/" . variable_get('career_landing_page'),array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
						if(is_numeric($arg1)){
							$parent_elements =  taxonomy_get_parents_all($arg1);					
							$count = count($parent_elements);
							$i = 0;
							foreach(array_reverse($parent_elements) as $element) {
								$link_arrow = ($i++ == $count-1) ? "" : '<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
								$career_and_learning_title = ($link_arrow == "") ? $element->name : "";
								$breadcrumbs_formatted[] = '<span class="bc-current">'. $element->name .'</span>'.$link_arrow;
							}
						}else if(!empty($arg2)){
							$career_and_learning_title = get_monthname($arg2);
							$breadcrumbs_formatted[] = '<span class="bc-current">'. $career_and_learning_title .'</span>';
						}
						drupal_set_title($career_and_learning_title);
					} else if($arg0 == 'media-archive'){
							$media_archive_title = t("Multimedia Archive");
							if(isset($arg1)){
								$breadcrumbs_formatted = array();
								$breadcrumbs_formatted[] = l(t("Home"), "",array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
								$breadcrumbs_formatted[] = l(t("Multimedia Archive"), "/media-archive",array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
								if(!is_numeric($arg1) && (is_numeric($arg2) || is_numeric($arg3))){
									$department_id = is_numeric($arg2) ? $arg2 : $arg3;
									$department_type_details =  taxonomy_term_load($department_id);
									if(isset($department_type_details->name)){
										$media_archive_title = t("Multimedia of ") . $department_type_details->name;						
									}
								}else if(is_numeric($arg1)){
									$gallery_details = node_load($arg1);
									$department_type_id = isset($gallery_details->field_channel['und'][0]['tid']) ? $gallery_details->field_channel['und'][0]['tid'] : 0;
									$department_type_details =  taxonomy_term_load($department_type_id);
									if(isset($department_type_details->name)){
										$breadcrumbs_formatted[] = l(t("Multimedia of ") . $department_type_details->name, "media-archive/all/channel/" . $department_type_id,array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
										$media_archive_title = $gallery_details->title;						
									}
								}else if($arg1 == 'photos' || $arg1 == 'videos'){
									unset($breadcrumbs_formatted[1]);
								}					
							}
							$breadcrumbs_formatted[] = '<span class="bc-current">'. $media_archive_title .'</span>';
							drupal_set_title($media_archive_title);
					} else if($arg0 == 'leadership'){
							$breadcrumbs_formatted[] = l(t("About CMG"), "about-commercial",array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
							$breadcrumbs_formatted[] = '<span class="bc-current">'. strip_tags(drupal_get_title()) .'span';
					} else if($arg0 == 'mcco-stories') {
						$breadcrumbs_formatted[] = '<span class="bc-current">'.t('Departments').'</span><span class="bc-arrow icon-Icon_Right-Arrow"></span>';
						$breadcrumbs_formatted[] = l(t("Managed Care & Customer Operations"),"mcco").'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
						$breadcrumbs_formatted[] = '<span class="bc-current">'. strip_tags(drupal_get_title()) .'</span>';
					}else if($arg0 == 'photo-stories' && is_numeric($arg1) && is_numeric($arg2)){
						$parent_elements =  array_reverse(taxonomy_get_parents_all($arg1));
						if($arg2>0){
							$parent_elements[] = taxonomy_term_load($arg1);
						}
						$count = count($parent_elements)-1;		
							foreach($parent_elements as $element) {	
								$nodeID = db_query("SELECT n.nid FROM {node} as n, {taxonomy_index} as ti, {field_data_field_microsite_subnav} as sn, {field_data_field_microsite_miscellaneous} as sm WHERE n.nid=ti.nid AND ti.tid=:tid AND n.nid = sn.entity_id AND n.nid = sm.entity_id AND sn.field_microsite_subnav_value = 0 AND sm.field_microsite_miscellaneous_value = 0 AND n.type=:type AND n.status=1", array(":tid" => $element->tid, ':type' => 'microsite'))->fetchField();
								if($km_nodeID = db_query("SELECT n.nid FROM {node} as n, {taxonomy_index} as ti, {field_data_field_km_sub_navigation} as sn, {field_data_field_km_misc_page} as sm WHERE n.nid=ti.nid AND ti.tid=:tid AND n.nid = sn.entity_id AND n.nid = sm.entity_id AND sn.field_km_sub_navigation_value = 0 AND sm.field_km_misc_page_value = 0 AND n.type=:type AND n.status=1", array(":tid" => $element->tid, ':type' => 'microsite'))->fetchField()){
									$nodeID = $km_nodeID;
								}
								$link = ($nodeID == "" || empty($nodeID)) ?  "" :  $base_url."/node/".$nodeID ;	
								if(!empty($link) && $link != "") {				
									$breadcrumbs_formatted[] = l($element->name, "node/" . $nodeID,array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';	
									$main_microsite_nid = $nodeID;
								}
								else {
									$breadcrumbs_formatted[] = '<span class="bc-current">'.$element->name.'</span><span class="bc-arrow icon-Icon_Right-Arrow"></span>'; 
									
								}					
								$j++;
								if($j == $count)  break;		
							}
							$breadcrumbs_formatted[] = '<span class="bc-current">'. strip_tags(drupal_get_title()) .'</span>';
					}else if($arg0 == 'gsearch' && !empty($_POST['km_microsite_url']) && !empty($_POST['km_microsite_title'])) {
						$breadcrumbs_formatted[] = '<span class="bc-current">'.t('Departments').'</span><span class="bc-arrow icon-Icon_Right-Arrow"></span>';
						$breadcrumbs_formatted[] = '<a href="'.$_POST['km_microsite_url'].'" class="bc-parent">'.$_POST['km_microsite_title'].'</a><span class="bc-arrow icon-Icon_Right-Arrow"></span>';
						$breadcrumbs_formatted[] = '<span class="bc-current">'.t('KM Search').'</span>';
					}	else {
						if(($arg0 == 'find-class' || $arg0 == 'dept_rolebasedprograms') && (!isset($arg1) || $arg1 == '')){
							$breadcrumbs_formatted[] = l(t("Career & Learning"), "node/" . variable_get('career_landing_page'),array('attributes' => array('class' => 'bc-parent'))).'<span class="bc-arrow icon-Icon_Right-Arrow"></span>';
						}
						$breadcrumbs_formatted[] = strip_tags(drupal_get_title());
						$breadcrumbs_formatted[count($breadcrumbs_formatted)-1] = '<span class="bc-current">'.strip_tags($breadcrumbs_formatted[count($breadcrumbs_formatted)-1])."</span>";
					}
				}
			}
		$breadcrumb .= implode("", $breadcrumbs_formatted);
		$breadcrumb .= '</div></div>';
		/** End - Breadcrumbs for all the pages **/
		
		//Checking and adding the path to add "Add to my links".
		$current_path = current_path();
		if(!empty($current_path)) {
			$path = str_replace('node/', '', $current_path);
			$fav_links = array();
			if (!empty($user->field_favorite_links['und'][0]['value'])) {       
				$fav_links = drupal_explode_tags($user->field_favorite_links['und'][0]['value']); 			
			}
			if (!preg_match("/user/i", $current_path)) {
				$breadcrumb .= '<div class="col-lg-3">';
				if (in_array($path, $fav_links)) {
					$breadcrumb .= '<a class="add-to-links added" href="javascript:void(0);" data-fav_link="'.$path.'"><span class="links-icon icon-Icon_Check-Mark-Circle"></span>Link Added</a>';
				} else {
					$breadcrumb .= '<a class="add-to-links" href="javascript:void(0);" data-fav_link="'.$path.'"><span class="links-icon icon-Icon_Plus-Circle"></span>Add To My Links</a>';
				}
				$breadcrumb .= '</div>';
			}
		}
		$breadcrumb .= '</div><div class="sh-line"></div></div></section>';
		return $breadcrumb;
  }
}

/**
 * Common function; to get/set cache and retrieve data
 */
function cache_code($cachekey, $time) {
  $current_time = time();	
  if ($cache = cache_get($cachekey)) {
    if ($cache->expire > $current_time) {
      $my_data = $cache->data;
	  return $my_data;
	}  
  }
  if (!empty($time)) {
	$customtime = time() + ($time * 3600);  
	switch ($cachekey) {
	  /* case 'instagram':
	    $block = module_invoke('instagram_block', 'block_view', 'instagram_block');
        $my_data = render($block['content']);
      break;  */	  
	  
	  case 'slider':
	    $block = module_invoke('views', 'block_view', 'nodequeue_1-featured_panel');
        $my_data = render($block['content']);
      break; 	  
	  
	  /* case 'gene_press':
        $pressrelease_output = '<h4>Genentech in the Press</h4>';
        $pressrequest = drupal_http_request('http://api.gene.com/press-releases?sortBy=start_date&sort=desc');
        $pressdata = drupal_json_decode($pressrequest->data);
        if (!empty($pressdata)) {
          $pressrelease_output .= '<ul class="list-clean list-spaced">';
          $i = 1;
          foreach ($pressdata['data'] as $key =>$value) {
			$presstitle = $value['title'];  
			$presslink = "http://www.gene.com/media/press-releases/" . $value['id'];
            $pressrelease_output .= "<li><a target='_BLANK' href=\"$presslink\">$presstitle</a></li>";
            if($i == 5) {break;}
              $i++;
          }
          $pressrelease_output .= '</ul>';
        } 
        $my_data = $pressrelease_output;
	  break; 	 */  
	  
	  case 'gwiz':
	    $my_data = "<h2 class='hd-title'>What's New on gWiz</h2>";
  	    $data = get_gwiznews();
		if(!empty($data)) {
		  $i = 1;
		  foreach($data->entries as $value) {
			$title = $value->title;	
			$url = $value->url;	
			$published = substr($value->published, 0, 10);
			$date_arr = explode("-", $published);
			$published_date = $date_arr[1].".".$date_arr[2].".".$date_arr[0];
			if($i == 5) {break;}  
			if($i == 1) {
			  $image = $value->content->mainimage->url;
			  $my_data .= "<p><img alt=\"$title\" src=\"$image\" /></p><ul class='list-clean panel-list no-border-list'>";
			}
			$my_data .= "<li><a target=\"_BLANK\" href=\"$url\">$title<span class='pulse'></span><span>$published_date</span></a></li>";
			$i++;
		  }
		  $my_data .= '</ul><div class="text-right view-more-container margin-bottom20"><a class="view-more" href="http://gwiz.gene.com" target="_blank">View More <span class="icon-triangle-right"></span></a></div>';
		}  else {
			$my_data .= '<p style="font-size:18px;"><span style="font-size:20px;font-weight: bold;">Whoops! </span> <br/>Our gWiz Feed is temporarily disabled. We\'re working to bring it back soon.</p>';
		}
	  break;	
	} 
	  cache_set($cachekey, $my_data, 'cache', $customtime);
  } 
  
  return $my_data;
}


/*
function cmgportal_webform_mail_headers($variables) {
  $headers = array(
    'Content-Type' => 'text/html; charset=UTF-8; format=flowed; delsp=yes',
    'X-Mailer' => 'Drupal Webform (PHP/'. phpversion() .')',
  );
  return $headers;
}
*/

/**
 * THEME_preprocess_html(&$variables)
 */
function cmgportal_preprocess_html(&$variables) {	
  // Add variables for path to theme.
  global $base_url, $user;
	
	$variables['base_path'] = $base_url . "/";
  $variables['path_to_resbartik'] = drupal_get_path('theme', 'commportal');
  
  // Add body classes if certain regions have content.
  if (!empty($variables['page']['featured'])) {
    $variables['classes_array'][] = 'featured';
  }

  if (!empty($variables['page']['triptych_first'])
    || !empty($variables['page']['triptych_middle'])
    || !empty($variables['page']['triptych_last'])) {
    $variables['classes_array'][] = 'triptych';
  }

  if (!empty($variables['page']['footer_firstcolumn'])
    || !empty($variables['page']['footer_secondcolumn'])
    || !empty($variables['page']['footer_thirdcolumn'])
    || !empty($variables['page']['footer_fourthcolumn'])) {
    $variables['classes_array'][] = 'footer-columns';
  } 	

	if($user->uid != 1) {
		$variables['classes_array'][] = "not-admin-user";
	}
}

/**
 * Override or insert variables into the page template for HTML output.
 */
function cmgportal_process_html(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($variables);
  }
}

/**
 * Override or insert variables into the page template.
 */
function cmgportal_process_page(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }
  
  $variables['page']['category_list'] = db_query('SELECT t.tid, t.name FROM {taxonomy_term_data} t, {taxonomy_vocabulary} v WHERE v.vid=t.vid and v.machine_name=:mname', array(':mname'=>'categories'))->fetchAllKeyed();    
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function cmgportal_preprocess_maintenance_page(&$variables) {
  // By default, site_name is set to Drupal if no db connection is available
  // or during site installation. Setting site_name to an empty string makes
  // the site and update pages look cleaner.
  // @see template_preprocess_maintenance_page
  if (!$variables['db_is_active']) {
    $variables['site_name'] = '';
  }
  drupal_add_css(drupal_get_path('theme', 'commportal') . '/css/maintenance-page.css');
}

/**
 * Override or insert variables into the maintenance page template.
 */
function cmgportal_process_maintenance_page(&$variables) {
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
}

/**
 * Override or insert variables into the node template.
 */
function cmgportal_preprocess_node(&$variables) {
  global $user,$base_url;
  if($variables['node']->type == "secured_link") {
    if(!empty($variables['node']->field_target_url) && array_key_exists('und', $variables['node']->field_target_url) && !empty($_GET)) {
	  if(!empty($_GET['dest']) && $_GET['dest'] == "tools")	{
		if(stristr($variables['node']->field_target_url['und'][0]['value'], 'comm-jboss')) {
		  if(stristr($variables['node']->field_target_url['und'][0]['value'], '?')) {
			header("Location: " . $variables['node']->field_target_url['und'][0]['value'] . "&userId=" . $_COOKIE['userId']);  
		  }	 else {
            header("Location: " . $variables['node']->field_target_url['und'][0]['value'] . "?userId=" . $_COOKIE['userId']);
		  }
	      exit;
	    } else if(stristr($variables['node']->field_target_url['und'][0]['value'], 'bo3') || stristr($variables['node']->field_target_url['und'][0]['value'], 'bo4')) {
		    header("Location: " . $variables['node']->field_target_url['und'][0]['value'] . "&user-id=" . $user->name . "&brand=&sfacode=&link-proxy-url=/common-service/SFDCLinkProxy/&link-proxy-host=http://commportal.gene.com");  
		    exit;
	    }  else {
		  drupal_goto($variables['node']->field_target_url['und'][0]['value']);		
		}
	  }
	}
  }
	if($variables['node']->type == "article" || $variables['node']->type == "announcements" || $variables['node']->type == "career_profiles") {		
		$variables['gplus_count'] = gplus_count($base_url . '/node/' . $variables['node']->nid);  			
		if(count($variables['node']->field_author) > 0) {
			$variables['node']->name = get_display_name($variables['node']->field_author[$variables['node']->language][0]['value']);
		}
		else {			
			$variables['node']->name = get_display_name($variables['node']->uid);
		}
	}
	else if ($variables['node']->type == "mcco_stories") {		
		$variables['gplus_count'] = gplus_count($base_url . '/node/' . $variables['node']->nid);  			
		$variables['node']->name = get_display_name($variables['node']->uid);
	}
	
	
	//Submit an announcement form for Container tpl.
	if($variables['node']->type == "containers")	{
		if($variables['node']->field_container_type['und'][0]['value'] == "announcements" || $variables['node']->field_container_type['und'][0]['value'] == "article"){
			$variables['submit_announcement_popup'] = get_announcement_form($variables['node']->field_container_type['und'][0]['value'], $variables['node']->nid);
		}
	}
	
	//Mandatory readers
	if ($variables['node']->type == "article" && $variables['view_mode'] == 'full'){
		$show_mark_as_read = FALSE;
		$show_mark_as_read_html = '';
		$field_mandate_users_list = field_get_items('node', $variables['node'], 'field_mandate_users_list');
		if(is_array($field_mandate_users_list) && count($field_mandate_users_list)>0){
			$field_mandate_users_list_arr = drupal_explode_tags($field_mandate_users_list[0]['value']);
			if(in_array($user->name,$field_mandate_users_list_arr)){
				$show_mark_as_read = TRUE;
			}	
		}
		if($show_mark_as_read){
			$field_content_read_users = field_get_items('node', $variables['node'], 'field_content_read_users');
			if(is_array($field_content_read_users) && count($field_content_read_users)>0){
				$field_content_read_users_arr = drupal_explode_tags($field_content_read_users[0]['value']);
				if(in_array($user->name, $field_content_read_users_arr)){
					$show_mark_as_read = FALSE;
				}
			}				
		}
		if($show_mark_as_read){
			$show_mark_as_read_html =   '<div id="mark-as-read-div">
											<span class="mark-as-read-class">
												<label><input type="checkbox" id="mark_as_read" name="read" value="Read" /> 
													' . t("Mark as Read") . '</label>
												<input type="hidden" id="nid" name="nid" value="' . $variables['node']->nid .'" />
												<a href="#" id="submit-as-read" class="button">' . t("Submit") . '</a>
											</span>
											<span id="processing_mark_as_read"></span>
											<span class="marked-success" style="display:none;">
											' . t("Thank you for reading the content") . '
											</span>
										</div>';
		}
		$variables['show_mark_as_read_html'] = $show_mark_as_read_html;
	}
	
	if($variables['node']->type == "advpoll" || $variables['node']->type == "faq") {
		if($variables['node']->title) {
			$variables['title'] = '<h3 class="hd-title">'.$variables['node']->title.'</h3>';
		}
		if($variables['node']->title == '')	{
			$variables['title'] = '<h3 class="no-title"></h3>';
		}
	}
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
	if($variables['node']->type == "tutorial") {
		if(isset($variables['content']['links']['node']['#links']['node-readmore'])) {
		$variables['content']['links']['node']['#links']['node-readmore']['href'] = $base_url."/".$variables['content']['links']['node']['#links']['node-readmore']['href']."?ms=".arg(1);	
		}
	}
	if($variables['node']->type == "career_learning") {
		if(isset($variables['content']['field_course_date']['#items'])) {
			foreach($variables['content']['field_course_date']['#items'] as $k => $v) {
				$current_date = date("Y-m-d H:i:s");
				$date = $v['value'];
				if($date < $current_date) {
					unset($variables['content']['field_course_date']['#items'][$k]);
				}
			}
			if(!count($variables['content']['field_course_date']['#items'])) {
					unset($variables['content']['field_course_date']);
			}
		}
	}
	$content_types = array('event_calendar','people','preview_blurb','links','image_gallery','advpoll','product_list','thumbnail_text','downloads','image','video','containers');
	if(in_array($variables['node']->type, $content_types) && isset($variables['field_hide_title']['und'][0]) && $variables['field_hide_title']['und'][0]['value'] == 1){
		$variables['classes_array'][] = 'hide-title-block';		
	}
}

function gplus_count($url) {
    $url = sprintf('https://plusone.google.com/u/0/_/+1/fastbutton?url=%s', urlencode($url));
    preg_match_all('/{c: (.*?),/', file_get_contents($url), $match, PREG_SET_ORDER);
    return (1 === sizeof($match) && 2 === sizeof($match[0])) ? '+'. intval($match[0][1]) : '+'. 0;
}

/**
 * Override or insert variables into the block template.
 */
function cmgportal_preprocess_block(&$variables) {
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
	if ($variables['block']->region == 'canvas') {
		$variables['title_attributes_array']['class'][] = 'hd-title hd-reversed';
	}
	if ($variables['block']->region == 'homepage_section2') {
		$variables['title_attributes_array']['class'][] = 'hd-title';
	}
}
function cmgportal_block_view_alter(&$data, $block) {
	if($block->module == 'views' && $block->delta == 'my_topics_feed-block') {
		$viewObj = get_MyTopics();
		$data['content']['#markup'] = $viewObj['markup'];
	}
}
/**
 * Implements theme_menu_tree().
 */
function cmgportal_menu_tree($variables) {
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function cmgportal_field__taxonomy_term_reference($variables) {
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] .'>' . $output . '</div>';

  return $output;
}

function cmgportal_file_link($variables) {
  $file = $variables['file'];
  $icon_directory = $variables['icon_directory'];
  $url = file_create_url($file->uri);
  $icon = theme('file_icon', array('file' => $file, 'icon_directory' => $icon_directory));
  // Set options as per anchor format described at
  // http://microformats.org/wiki/file-format-examples
  $options = array(
    'attributes' => array(
      'type' => $file->filemime . '; length=' . $file->filesize,
    ),
  );
  // Use the description as the link text if available.
  if (empty($file->description)) {
    $link_text = $file->filename;
  }
  else {
    $link_text = $file->description;
    $options['attributes']['title'] = check_plain($file->filename);
  }
  //open files of particular mime types in new window
  $options['attributes']['target'] = '_blank';
  return '<span class="file">' . $icon . ' ' . l($link_text, $url, $options) . '</span>';
}

/**
 * Implements theme_preprocess_region().
 */
function cmgportal_preprocess_region(&$variables) {
	if($variables['region'] == 'canvas_accordion') {
		$data = block_get_blocks_by_region($variables['region']);
		$markup = '<ul class="off-canvas__accordion mm-list">';
		$count = 0;
		foreach($data as $block) {
			$count++;
			if($count == 2) {
				$collapse = "open";
			} else {
				$collapse = "close";
			}
			if(is_array($block)) {
				$title = $block['#block']->title;
				$markup .= '<li>';
				if($title != '') {
					$markup .= '<h4 class="'.$collapse.'"><a href="#">'.$title.'<span class="accordion-open icon icon-triangle-down"></span><span class="accordion-closed icon icon-triangle-right"></span></a></h4>';
				}
				$markup .= $block['#markup'].'</li>';
			}
		}
		$markup .= '</ul>';
		$variables['content'] = $markup;
	}
}

/********** Start - theme function to use single tpl file for multiple views *******/
function cmgportal_preprocess_views_view(&$vars) {
  $suggestions = array('commercial_news','kudos','career_and_planning','toolbox_browse_by','career_category_view'); 
  if (in_array($vars['view']->name, $suggestions) && (($vars['view']->current_display == "page") || ($vars['view']->current_display == "page_1"))) {  
		if($vars['view']->name !== 'career_and_planning') {
			$vars['theme_hook_suggestions'][] = "views_view__commercial_news__page";	
		}
  }
  else if (in_array($vars['view']->name, $suggestions) && (($vars['view']->current_display == "block_1") || ($vars['view']->current_display == "block_2") || ($vars['view']->current_display == "block_3")|| ($vars['view']->current_display == "block_4"))) { 
		$vars['theme_hook_suggestions'][] = "views_view__mediaarchive__block_1";
  }
	else if($vars['view']->name == 'career_category_view' && $vars['view']->current_display == "block") {
		$vars['theme_hook_suggestions'][] = "views_view__mediaarchive__block_1";
	}
	if($vars['view']->name == 'mediaarchive' && $vars['view']->current_display == "block_2") {
		$vars['rows'] = '<ul class="mediaarchive-department">'.$vars['rows'].'</ul>';
	}
}

function cmgportal_preprocess_views_exposed_form(&$vars) {	
	$suggestions = array('views-exposed-form-commercial-news-page','views-exposed-form-kudos-page','views-exposed-form-kudos-page-1'); 
	if(in_array($vars['form']['#id'], $suggestions)) {
		$vars['theme_hook_suggestions'][] = "views_exposed_form__commercial_news";  
	}	
}
/********** End - theme function to use single tpl file for multiple views *******/

/** 
 * MYTHEME_links__system_main_menu - Overriding primary links
 */
function cmgportal_links__system_main_menu($variables) {
	
	if ($cache = cache_get("departmentmenu")) {	
		return $cache->data;
	}
	else {
		$html = "<ul class='sm sm-clean collapsed level-0'>";  
		foreach ($variables['links'] as $link) {
			if(!empty($link['title']) && strtolower($link['title']) == "about commercial"){
				$dept_menu = '<li><a href="javascript:void(0);" class="has-submenu">About Commercial</a><ul class="level-1"><li><a href="/'.$link['href'].'" title="">'.$link['title'].'</a></li>';
				$channel_vid = db_query('SELECT vid FROM {taxonomy_vocabulary} where machine_name = :channel', array(':channel' => 'channel'))->fetchField();
				$dept_term_id = variable_get('about_comm_term_id');
				$menu = taxonomy_get_tree($channel_vid, $dept_term_id);
				if (!empty($menu)) {
					$terms_names = array();
					$menudepth_one = $menudepth_two = $menudepth_three = $menudepth_four = $nodearray = array();
					foreach($menu as $key=>$term) {									
						$terms_names[$term->tid] = $term->name;
						$nid = db_query("SELECT n.nid FROM {node} n INNER JOIN {field_data_field_is_this_a_subpage_of_about} sn ON n.nid = sn.entity_id INNER JOIN {taxonomy_index} ti ON ti.nid = sn.entity_id WHERE ti.tid = :tid AND sn.field_is_this_a_subpage_of_about_value = 1 AND n.type=:type AND n.status = 1 order by ti.tid asc", array(":tid" => $term->tid, ':type' => 'page'))->fetchField();
						if ($nid) {	
							if($term->depth == 0) {
								$dept_menu_title = db_query("SELECT field_dept_menu_title_value FROM field_data_field_dept_menu_title WHERE entity_id=:nid", array(":nid" => $nid))->fetchField();
								if (!empty($dept_menu_title)) {
									$menudepth_one[$term->tid] = $dept_menu_title;										
								}
								else {
									$menudepth_one[$term->tid] = $term->name;  	   
								}									
							}
							if($term->depth == 1) {
								$parent_id = $term->parents[0];
								if(!isset($menudepth_two[$parent_id])){
									$menudepth_two[$parent_id][$parent_id] = $terms_names[$parent_id];									
								}									
								$menudepth_two[$term->parents[0]][$term->tid] = $term->name;  	   
							}
							$nodearray[$term->tid] = $nid; 
						}
					}
				}
				if(!empty($menudepth_one)) {												
					foreach($menudepth_one as $key => $value) {
						if(array_key_exists($key, $menudepth_two) && !empty($menudepth_two)) {
							$dept_menu .= "<li><a>" . $value . "</a><ul class='level-2'>";
							$menudepth_two_array_values = array_values($menudepth_two[$key]);
							$menudepth_two_array_keys = array_keys($menudepth_two[$key]);
              /*My code*/
							$temp_tax_val = $menudepth_two[$key];
							if($value == "Initiatives"){
								$temp_tax_val['about-gcareer'] = 'gCareer';
								natcasesort($temp_tax_val);
								$temp_tax_val = array($key => $value) + $temp_tax_val;
							}
							foreach($temp_tax_val as $twokey => $twovalue) {
                if($twovalue == 'gCareer') {
                  $dept_menu .= "<li>".l(t('gCareer'), $twokey)."</li>";
                } else {
                  $dept_menu .= "<li>".l($twovalue, "node/" . $nodearray[$twokey], $link)."</li>";
                }
							}
							/*End of my code*/
							$dept_menu .= "</ul></li>";		
						} else {
							$dept_menu .= "<li>".l($value, "node/" . $nodearray[$key], $link)."</li>";	
						}
					}  
				}
				$dept_menu .="<li>".l(t('US Leadership Team'), '/leadership')."</li>";
				$dept_menu .='</ul></li>';
				$html .= $dept_menu;
			}
			elseif (!empty($link['title']) && strtolower($link['title']) == "departments") {
					$dept_menu = '<li><a href="javascript:void(0);" class="has-submenu">Departments</a><ul class="level-1">'; 
					$channel_vid = db_query('SELECT vid FROM {taxonomy_vocabulary} where machine_name = :channel', array(':channel' => 'channel'))->fetchField();
					$dept_term_id = variable_get('dept_term_id');
					$menu = taxonomy_get_tree($channel_vid, $dept_term_id);
					$current_path = (isset($_GET['q']) && $_GET['q'] != 'home') ? url($_GET['q']) : '';
					if (!empty($menu)) {
						$terms_names = array();
						$partners = variable_get('department_partners', array());
						$menudepth_one = $menudepth_two = $menudepth_three = $menudepth_four = $nodearray = array();
								foreach($menu as $key=>$term) {									
								$terms_names[$term->tid] = $term->name;
								$nid = db_query("SELECT n.nid FROM {node} n INNER JOIN {field_data_field_microsite_subnav} sn ON n.nid = sn.entity_id INNER JOIN {field_data_field_microsite_miscellaneous} sm ON n.nid = sm.entity_id INNER JOIN {taxonomy_index} ti ON ti.nid = sn.entity_id WHERE ti.tid = :tid AND sn.field_microsite_subnav_value = 0 AND sm.field_microsite_miscellaneous_value = 0 AND n.type=:type AND n.status = 1 order by ti.tid asc", array(":tid" => $term->tid, ':type' => 'microsite'))->fetchField();
								$km_misc_nodeid = db_query("SELECT entity_id from {field_data_field_km_misc_page} mp where mp.entity_id = :nid AND mp.field_km_misc_page_value = 1",array(":nid" => $nid))->fetchField();
								$km_subnav_nodeid = db_query("SELECT entity_id from {field_data_field_km_sub_navigation} mp where mp.entity_id = :nid AND mp.field_km_sub_navigation_value = 1",array(":nid" => $nid))->fetchField();
								$km_main_nodeid = db_query("SELECT entity_id from {field_data_field_km_microsite} mp where mp.entity_id = :nid AND mp.field_km_microsite_value = 1",array(":nid" => $nid))->fetchField();
								if($km_misc_nodeid || $km_subnav_nodeid || $km_main_nodeid){
									continue;
								}
							if ($nid) {	
								if($term->depth == 0) {
									$dept_menu_title = db_query("SELECT field_dept_menu_title_value FROM field_data_field_dept_menu_title WHERE entity_id=:nid", array(":nid" => $nid))->fetchField();
									if (!empty($dept_menu_title)) {
										$menudepth_one[$term->tid] = $dept_menu_title;										
									}
									else {
										$menudepth_one[$term->tid] = $term->name;  	   
									}									
								}
								if($term->depth == 1) {
									$parent_id = $term->parents[0];
									if(!isset($menudepth_two[$parent_id])){
										$menudepth_two[$parent_id][$parent_id] = $terms_names[$parent_id];										
									}									
									$menudepth_two[$term->parents[0]][$term->tid] = $term->name;  	   
								}
								$nodearray[$term->tid] = $nid; 
							}		
						}
					}
					$partner_exists = false;
					if(!empty($menudepth_one)) {												
						foreach($menudepth_one as $key => $value) {
							if(in_array($key, $partners)){
								$partner_exists = true;
								continue;
							}
							if(array_key_exists($key, $menudepth_two) && !empty($menudepth_two)) {
								$dept_menu .= "<li><a>" . $value . "</a><ul class='level-2'>";
								$menudepth_two_array_values = array_values($menudepth_two[$key]);
								$menudepth_two_array_keys = array_keys($menudepth_two[$key]);
								foreach($menudepth_two_array_values as $twokey => $twovalue) {
									$dept_menu .= "<li>".l($twovalue, "node/" . $nodearray[$menudepth_two_array_keys[$twokey]], $link)."</li>";	
								}
								$dept_menu .= "</ul></li>";		
							} else {
								$dept_menu .= "<li>".l($value, "node/" . $nodearray[$key], $link)."</li>";	
							}
						}  
					}
					if($partner_exists){
						$dept_menu .= "<li><a>Partners</a><ul class='level-2'>";
						foreach($partners as $partner_id){
							if(isset($menudepth_one[$partner_id])){
								$dept_menu .= "<li>".l($menudepth_one[$partner_id], "node/" . $nodearray[$partner_id], $link)."</li>";
							}
							else {
								$dept_menu .= "<li>".l("U.S. Medical Affairs", "http://usma.gene.com", $link)."</li>";
							}
						}
						$dept_menu .= "</ul></li>";
					}
					$dept_menu .= "</ul></li>"; 
					$html .= $dept_menu;				 
			}
			else {
				if(array_key_exists('href', $link)) {	
					$html .= "<li>".l($link['title'], $link['href'], $link)."</li>";
				} else {
					$html .= "<li>".l($link['title'], 'home', $link)."</li>";	  
				}
			}
		}
		$html .= "  </ul>";		
		cache_set("departmentmenu", $html, 'cache'); 
		return $html;
	}		
}

/**
 * Implements theme_preprocess_page().
 */
function cmgportal_preprocess_page(&$vars) {
  /* Start - CMG code*/
  global $user;
  $groups = "";
  $groups = isset($user->field_author_groups['und'][0]['value']) ? $user->field_author_groups['und'][0]['value'] : "";
  $vars['groups'] = $groups;
  $vars['first_name'] = isset($user->field_first_name['und'][0]['value']) ? $user->field_first_name['und'][0]['value'] : $user->name;
  $vars['attribute_class_page'] = "";
	if(isset($vars['node'])){
		if((isset($vars['node']->field_is_this_a_usma_page['und']) && $vars['node']->field_is_this_a_usma_page['und'][0]['value'] == 0) || !isset($vars['node']->field_is_this_a_usma_page['und'])){
			$vars['attribute_class_page'] = "commercial-page";
		}
		if(isset($vars['node']->field_is_this_a_usma_page['und'])){
			$vars['is_usma'] = $vars['node']->field_is_this_a_usma_page['und'][0]['value'];
		}
	}else{
		$vars['attribute_class_page'] = "commercial-page";
	}
  //Start - USMA Special Pages
  $nodeids_array = drupal_explode_tags(variable_get('nodeids', ''));
    if(in_array(arg(1),$nodeids_array) || $vars['node']->type == "events_gallery") {
      drupal_add_css(drupal_get_path('theme', 'cmgportal').'/assets/usma_assets/css/main.css');
      drupal_add_js(drupal_get_path('theme', 'cmgportal').'/assets/usma_assets/js/vendor/modernizr-2.6.2.min.js');
      drupal_add_js(drupal_get_path('theme', 'cmgportal').'/assets/usma_assets/js/vendor/jquery-1.10.2.min.js');
      drupal_add_js(drupal_get_path('theme', 'cmgportal').'/assets/usma_assets/js/vendor/jquery.fancybox.js');
      drupal_add_js(drupal_get_path('theme', 'cmgportal').'/assets/usma_assets/js/plugins.js');
      drupal_add_js(drupal_get_path('theme', 'cmgportal').'/assets/usma_assets/js/main.js');
    }
  //End - USMA Special Pages
  /* End - CMG code*/
  if(arg(0)=="photo-stories"&& (arg(1) != NULL) && (arg(2) != NULL) && is_numeric(arg(1)) && is_numeric(arg(2))){
		$node = node_load(arg(2));
		$vars['node'] = $node;
	}
	if(@$vars['node']->field_display_km_departments['und'][0]['value'] == 1 && (@$vars['node']->type == 'microsite')){
		$km_links = "";
		$km_microsite_nids = explode(',', variable_get('km_search_microsites'));			
		foreach($km_microsite_nids as $nid){ 			
		$options = array('absolute' => TRUE);			
		$title = node_load($nid)->title;
		$url = url('node/' . $nid, $options);			
		$km_links .= '<li><a href="' . $url . '" class="">' . $title . '</a></li>';				
		$vars['node']->km_departments =  $km_links;
		}
	}

	$vars['title_attributes_array']['class'][] = 'hd-title';
	$vars['title_attributes_array']['class'][] = 'no-border-top';
	//$vars['title_attributes_array']['class'][] = 'top-flush';	
	$arg0 = arg(0);
	$arg1 = arg(1);
	$arg2 = arg(2);
	$arg3 = arg(3);

	if(arg(0) == 'search-results' || arg(0) == 'search-people') {
		$vars['title_attributes_array']['class'][] = 'hide';
	}
	else if ($arg0 == 'media-archive' || $arg0 == 'commercial-news'|| $arg0 == 'commercial_news' || $arg0 == 'my-topics-feed' ||  $arg0 == 'kudos-list' || $arg0 == 'kudos_list' || ($arg0 == 'career-and-learning' && !empty($arg1))) {
		$vars['title_attributes_array']['class'][] = 'border-bottom-default';
		$vars['title_attributes_array']['class'][] = 'margin-bottom-default';
	}
	else if(isset($vars['node']) && isset($vars['node']->type)){ 
		if (@$vars['node']->type == 'career_learning' || @$vars['node']->type == 'toolbox') {
			$vars['title_attributes_array']['class'][] = 'border-bottom-default';
			$vars['title_attributes_array']['class'][] = 'margin-bottom-default';
			if (@$vars['node']->type == 'career_learning') {
				$vars['theme_hook_suggestions'][] = 'page__career_learning';
			} if (@$vars['node']->type == 'toolbox') {
				$vars['theme_hook_suggestions'][] = 'page__tools';
			}
		}
		if (@$vars['node']->type == 'microsite' || @$vars['node']->type == 'tutorial' || @$vars['node']->type == 'faq' || @$vars['node']->type == 'announcements' || @$vars['node']->type == 'article' || @$vars['node']->type == 'career_profiles' || @$vars['node']->type == 'webform' || @$vars['node']->type == 'mcco_stories') {
			//$vars['title_attributes_array']['class'][] = 'col-md-9';
			$vars['title_attributes_array']['class'][] = 'col-md-12';
			drupal_add_css(drupal_get_path('module', 'commportal_custom').'/css/commportal_admin.css');
			if (@$vars['node']->type == 'microsite') {
				$vars['theme_hook_suggestions'][] = 'page__microsite';
			} else {
				$vars['theme_hook_suggestions'][] = 'page__microsite_content';
			}
			/* Microsite second level navigation */
			$primary_color = '#58595B'; $secondary_color = '#205692';
			if (@$vars['node']->type == 'microsite' && (@$vars['node']->field_microsite_subnav['und'][0]['value'] == 0 || @$vars['node']->field_km_sub_navigation['und'][0]['value'] == 0)) {
				if($arg2 != 'edit'){
					if (!empty($vars['node']->field_microsite_logo)) {
							$vars['node']->microsite_logo = '<img src="'.image_style_url("microsite_logo", $vars['node']->field_microsite_logo['und'][0]['uri']).'"  class="sidebar-logo"/>';
					}
					else {
						$vars['node']->microsite_logo = l($vars['node']->title, 'node/'. $vars['node']->nid, array('attributes' => array('class' => array('sidebar-logo')), 'html' => TRUE));
					}
				}
				$primary_color = (isset($vars['node']->field_theme_color['und'][0]['value']) ? ($vars['node']->field_theme_color['und'][0]['value']) : $primary_color);
				$secondary_color = (isset($vars['node']->field_secondary_color['und'][0]['value']) ? ($vars['node']->field_secondary_color['und'][0]['value']) : $secondary_color);
				if (!empty($vars['node']->field_org_chart)) {
					$vars['node']->orgchart = file_create_url($vars['node']->field_org_chart['und'][0]['uri']);
				}					
			} 
			if ((@$vars['node']->type == 'microsite' && (@$vars['node']->field_microsite_subnav['und'][0]['value'] == 1 || @$vars['node']->field_microsite_miscellaneous['und'][0]['value'] == 1 || @$vars['node']->field_km_misc_page['und'][0]['value'] == 1 || @$vars['node']->field_km_sub_navigation['und'][0]['value'] == 1)) || @$vars['node']->type == 'tutorial' || @$vars['node']->type == 'faq' || @$vars['node']->type == 'announcements' || @$vars['node']->type == 'article' || @$vars['node']->type == 'career_profiles' || @$vars['node']->type == 'career_profiles' || @$vars['node']->type == 'webform' || @$vars['node']->type == 'mcco_stories') {
				$microsite_nodeID = db_query("SELECT n.nid FROM {node} n INNER JOIN {field_data_field_microsite_subnav} sn ON n.nid = sn.entity_id  INNER JOIN {field_data_field_microsite_miscellaneous} sm ON n.nid = sm.entity_id INNER JOIN {taxonomy_index} ti ON ti.nid = sn.entity_id WHERE ti.tid = :tid AND sn.field_microsite_subnav_value = 0 AND sm.field_microsite_miscellaneous_value = 0 AND n.type=:type AND n.status = 1", array(":tid" => @$vars['node']->field_channel['und'][0]['tid'], ':type' => 'microsite'))->fetchField();
				if(@$vars['node']->field_km_misc_page['und'][0]['value'] == 1 || @$vars['node']->field_km_sub_navigation['und'][0]['value'] == 1){
					$microsite_nodeID = db_query("SELECT n.nid FROM {node} n INNER JOIN {field_data_field_km_sub_navigation} sn ON n.nid = sn.entity_id  INNER JOIN {field_data_field_km_misc_page} sm ON n.nid = sm.entity_id INNER JOIN {taxonomy_index} ti ON ti.nid = sn.entity_id WHERE ti.tid = :tid AND sn.field_km_sub_navigation_value = 0 AND sm.field_km_misc_page_value = 0 AND n.type=:type AND n.status = 1", array(":tid" => @$vars['node']->field_channel['und'][0]['tid'], ':type' => 'microsite'))->fetchField();
				}
				if ($microsite_nodeID) {
					$microsite_node = node_load($microsite_nodeID);
					if(arg(2) != 'edit'){
						if (!empty($microsite_node->field_microsite_logo)) {	
									$vars['node']->microsite_logo = '<img src="'.image_style_url("microsite_logo", $microsite_node->field_microsite_logo['und'][0]['uri']).'"  class="sidebar-logo"/>';
						}
						else {
							$vars['node']->microsite_logo = l($microsite_node->title, 'node/'. $microsite_node->nid, array('attributes' => array('class' => array('sidebar-logo')), 'html' => TRUE));
						}
					}											
					$primary_color = (isset($microsite_node->field_theme_color['und'][0]['value']) ? $microsite_node->field_theme_color['und'][0]['value'] : $primary_color);
					$secondary_color = (isset($microsite_node->field_secondary_color['und'][0]['value']) ? $microsite_node->field_secondary_color['und'][0]['value'] : $secondary_color);
					$vars['node']->field_ad_hoc_links = $microsite_node->field_ad_hoc_links;
					$vars['node']->field_ad_hoc_label = $microsite_node->field_ad_hoc_label;
					if (!empty($microsite_node->field_org_chart)) {
						$vars['node']->orgchart = file_create_url($microsite_node->field_org_chart['und'][0]['uri']);
					}	
				}					
			}
      drupal_add_css('.microsite h1, .microsite h2, .microsite h3, .microsite h4, .microsite h5, .microsite .cal-widget table.calendar th {color:'.$primary_color.' !important; border-color:'.$primary_color.' !important;} header.microsite{border-color:'.$primary_color.' !important;} .microsite .cal-widget h6 {background:'.$primary_color.' none repeat scroll 0 0 !important;} .microsite .view-more, .microsite .node-readmore > a, .microsite a.node-readmore {color: '.$secondary_color.' !important;}', array('type' => 'inline'));
			if(arg(2) != 'edit'){				
					$vars['node']->subnav = db_query("SELECT n.nid, n.title FROM {node} n INNER JOIN {field_data_field_microsite_subnav} sn ON n.nid = sn.entity_id   INNER JOIN {taxonomy_index} ti ON ti.nid = sn.entity_id WHERE ti.tid = :tid AND sn.field_microsite_subnav_value = 1 AND n.type=:type AND n.status = 1 order by n.created asc", array(":tid" => @$vars['node']->field_channel['und'][0]['tid'], ':type' => 'microsite'))->fetchAll();
				  if(count($vars['node']->subnav) <= 0){
						$vars['node']->subnav = db_query("SELECT n.nid, n.title FROM {node} n INNER JOIN {field_data_field_km_sub_navigation} sn ON n.nid = sn.entity_id   INNER JOIN {taxonomy_index} ti ON ti.nid = sn.entity_id WHERE ti.tid = :tid AND sn.field_km_sub_navigation_value = 1 AND n.type=:type AND n.status = 1 order by n.created asc", array(":tid" => @$vars['node']->field_channel['und'][0]['tid'], ':type' => 'microsite'))->fetchAll();
					} 			
				// TODO: Modify below code after PHP upgrade to > 5.5
				foreach($vars['node']->subnav as $key => $value) {
					if (stristr($value->title, 'about')) {						
							$vars['node']->subnav_first = "<li class='menu-item'>". l($value->title, "node/" . $value->nid,array('attributes' => array('class' => array('menu-item-link')))) . "</li>";						
							//$vars['node']->subnav_first = "<li>". l($value->title, "node/" . $value->nid) . "</li>";						
						unset($vars['node']->subnav[$key]);break;
					}
				}			
				$vars['node']->our_departments = "";
				$vars['node']->our_department_heading = "";
				$channel_vid = db_select('taxonomy_vocabulary', 'tv')
				->fields('tv',array('vid'))
				->condition('tv.machine_name','channel')
				->execute()
				->fetchField();
				$dept_links="";
				$our_dept_exist = FALSE;
				if(isset($vars['node']->field_channel['und'][0]['tid'])){
					$all_parents = taxonomy_get_parents_all($vars['node']->field_channel['und'][0]['tid']);
					$dept_tids = array(variable_get('dept_term_id',10),variable_get('us_medical_dept_term_id'),variable_get('ga_dept_term_id'));
					$department_exists = (count($all_parents)>0 && in_array($all_parents[count($all_parents)-1]->tid ,$dept_tids) ) ? TRUE : FALSE;
					if($department_exists){
						$main_department_details = $all_parents[count($all_parents)-2];
						$dept_term_id = $main_department_details->tid;
						$vars['node']->our_department_heading = variable_get('department_' . $dept_term_id . '_shorten_form', 'Our');
						if ($terms = taxonomy_get_tree($channel_vid, $dept_term_id, 1)) {
							array_unshift($terms, $main_department_details);						
							foreach ($terms as $term) { 
								$department_link = "";
								$department_class = "";
								$nodeID = db_query("SELECT n.nid FROM {node} n INNER JOIN {field_data_field_microsite_subnav} sn ON n.nid = sn.entity_id INNER JOIN {taxonomy_index} ti ON ti.nid = sn.entity_id INNER JOIN {field_data_field_microsite_miscellaneous} sm ON n.nid = sm.entity_id WHERE ti.tid = :tid AND sn.field_microsite_subnav_value = 0 AND sm.field_microsite_miscellaneous_value = 0 AND n.type=:type AND n.status = 1", array(":tid" => $term->tid, ':type' => 'microsite'))->fetchField();
								
								
								$km_misc_nodeid = db_query("SELECT entity_id from {field_data_field_km_misc_page} mp where mp.entity_id = :nid AND mp.field_km_misc_page_value = 1",array(":nid" => $nodeID))->fetchField();								
								$km_subnav_nodeid = db_query("SELECT entity_id from {field_data_field_km_sub_navigation} mp where mp.entity_id = :nid AND mp.field_km_sub_navigation_value = 1",array(":nid" => $nodeID))->fetchField();				
								if($km_misc_nodeid || $km_subnav_nodeid){
									$nodeID = db_query("SELECT n.nid FROM {node} n INNER JOIN {field_data_field_km_sub_navigation} sn ON n.nid = sn.entity_id INNER JOIN {taxonomy_index} ti ON ti.nid = sn.entity_id INNER JOIN {field_data_field_km_misc_page} sm ON n.nid = sm.entity_id WHERE ti.tid = :tid AND sn.field_km_sub_navigation_value = 0 AND sm.field_km_misc_page_value = 0 AND n.type=:type AND n.status = 1", array(":tid" => $term->tid, ':type' => 'microsite'))->fetchField();
								}
								if($vars['node']->field_channel['und'][0]['tid'] == $term->tid){
									$department_link = "javascript:void(0);";
									$department_class = "active";
								}else if($nodeID){
									$our_dept_exist = TRUE;
									$department_link = url("node/" . $nodeID);
								}else{
									continue;
								}
								$term_name = $term->name;
								if(isset($term->depth)){
									$term_name = str_repeat('- ', $term->depth + 1) . $term->name;
								}
								if($vars['node']->field_is_this_a_usma_page['und'][0]['value'] == 1){
									$dept_links .= '<li  class="submenu-item"
									><a href="' . $department_link . '" class="' . $department_class . ' submenu-item-link">' . $term_name . '</a></li>';
								}else{
									$dept_links .= '<li><a href="' . $department_link . '" class="' . $department_class . '">' . $term_name . '</a></li>';
								}
							}
						}
					}									
				}
				$vars['node']->our_departments = ($our_dept_exist == TRUE) ? $dept_links : "";				
			}			
		}
	}
	if($arg0 == 'more_fav_links'|| $arg0 == 'find-class' || $arg0 == 'dept_rolebasedprograms') {
		$vars['title_attributes_array']['class'][] = 'border-bottom-default';
		$vars['title_attributes_array']['class'][] = 'margin-bottom-default';
	}	
}
 
function getTagsForm() {
	$followed_tags = getUserFollowTags();
	$alphas = '';
	$markup = '<div class="scroll-alphabet" id="tag-scroll-alphas"><ul>';
	$form = '</ul></div><div class="tag-scroll" id="tag-scroll"><ul class="abc-tags">';
	$tags_vocabulary = taxonomy_vocabulary_machine_name_load('tags');
	$tags_list = taxonomy_get_tree($tags_vocabulary->vid);
	$tags = array();
	foreach($tags_list as $key => $value) {
		$tags[] = $value->name."~".$value->tid;
	}
	$tag_count_latest = 1;
	$indices = array();
	$letters = range('A', 'Z');
	$index = $str = "";
	sort($tags);
	foreach($tags as $key => $tag) {
		$tag_arr = explode('~', $tag);
		if(strtoupper($tag[0]) != $index) {
			if($key != 0 || $key == count($tags)-1) { //$form .= '<hr>';
			}
			$index = strtoupper($tag[0]);
			$form .= '<div id="anchor-'.strtolower($index).'" class="alpha-anchor"></div>';
		}
		if(array_key_exists($tag_arr[1], $followed_tags)) {
			$form .= '<li class="tag-item"><div class="checkbox-style test"><input type="checkbox" name="'.$tag_arr[0].'" id="tag' . $tag_count_latest .'" value="'.$tag_arr[1].'" checked="checked" data-enhanced="true" /><label for="tag' . $tag_count_latest . ' ">'.$tag_arr[0].'</label> <span class="checkmark"></span>   </div></li>';
		} else {
			$form .= '<li class="tag-item"><div class="checkbox-style"><input type="checkbox" name="'.$tag_arr[0].'" id="tag' . $tag_count_latest . '" value="'.$tag_arr[1].'" data-enhanced="true" /><label for="tag' . $tag_count_latest . '">'.$tag_arr[0].'</label> <span class="checkmark"></span>   </div></li>';
		}
		$tag_count_latest++;
		$indices[] = $index;
	}
	foreach($letters as $letter) {
		if(in_array($letter, $indices)) {
			//$alphas .= '<a href="#'.$letter.'">'.$letter.'</a>';
			$alphas .= '<li class="tag-scroll-letter" data-scroll="'.strtolower($letter).'">'.$letter.'</li>';
		} else {
			//$alphas .= '<a class="disable">'.$letter.'</a>';
			$alphas .= '<li class="tag-scroll-letter disable" data-scroll="'.strtolower($letter).'">'.$letter.'</li>';
		}
	}
	$form .= '</ul></div>';
	$form = $markup.$alphas.$form;
	return $form;
}

/**
 * fetch latest news from gWiz API. Used in home and search result pages.
 */
function get_gwiznews() {
  $url = 'https://gwiznews.gene.com/dnanews/api/index.json';
	$username = variable_get('generic_unix', 'COMMPORT');
	$password = variable_get('generic_pass');
  $context = stream_context_create(array(
    'http' => array(
    'header'  => "Authorization: Basic " . base64_encode("$username:$password")
    )
  ));
	/* $options = array(
	 'headers' => array(
		 'Content-Type' => 'application/json',
	 ),
	 'method' => 'GET',
	);
	$base_url= 'https://bandlaps:Welcom44@gwiznews.gene.com/dnanews/api/index.json';
	$response = drupal_http_request($base_url,$options);
	$data = stripslashes($response->data);*/
  $data = file_get_contents($url, false, $context);
  $data = json_decode($data);	
  return $data;
}
function cmgportal_preprocess_comment(&$variables) {
	$posted_author = user_load($variables['elements']['#comment']->uid);
	$variables['author'] = $posted_author->field_display_name['und'][0]['value'];
	$variables['changed'] = format_interval(REQUEST_TIME - $variables['elements']['#comment']->changed);
}
function cmgportal_js_alter(&$scripts) {
	if(arg(0) == 'imce') {
		$theme = drupal_get_path('theme', 'commportal');
		unset($scripts[$theme.'/assets/js/min/base-min.js']);
		unset($scripts[$theme.'/assets/js/custom.js']);
		unset($scripts[$theme.'/js/commportal.js']);
		//unset($scripts[$theme.'/assets/js/tour.js']);
	}
	//$scripts['sites/all/modules/contrib/jquery_update/replace/jquery/2.1/jquery.min.js']['data'] = drupal_get_path('theme', 'cmgportal') . '/library/js/jquery-3.3.1.min.js';
	//$scripts['sites/all/modules/contrib/jquery_update/replace/jquery/2.1/jquery.min.js']['version'] = '3.3.1';
	//$javascript['misc/jquery.js']['data'] = drupal_get_path('theme', 'cmgportal') . '/library/js/jquery-3.3.1.min.js';
    /* $javascript['misc/jquery.js']['scope'] = 'header';
    $javascript['misc/jquery.js']['group'] = 'JS_LIBRARY';
    $javascript['misc/jquery.js']['weight'] = '100';
    $javascript['misc/jquery.js']['every_page'] = TRUE;
    $javascript['misc/jquery.js']['type'] = 'file';
    $javascript['misc/jquery.js']['preprocess'] = 'TRUE';
    $javascript['misc/jquery.js']['cache'] = 'TRUE';
    $javascript['misc/jquery.js']['defer'] = 'TRUE'; */
	//dsm($scripts);
}



/**
 * Preprocess function for the thumbs_up_down template.
 */
function cmgportal_preprocess_rate_template_thumbs_up(&$variables) {
  extract($variables);

  $variables['up_button'] = theme('rate_button', array('text' => 'Like', 'href' => $links[0]['href'], 'class' => 'rate-thumbs-up-btn-up'));
	
  $variables['info'] = $results['count'];
}

/**
 * To Place Page Title in Breadcrumb
 */
function prepare_title_display(){
	$page_title = strip_tags(menu_get_active_title());
	if(strlen($page_title) > 45) {	// Strip the length of title
		$wrapped = wordwrap($page_title, 45, "...#", true);
		$wrapped = explode("#", $wrapped);
		return '<span class="bc-current">'.$wrapped[0].'</span>';	
	} else {
		return '<span class="bc-current">'.$page_title.'</span>';	
	}
}

function fetch_dept_microsite_link($dept){
	$nid = db_select('node', 'n')
				->fields('n',array('nid'))
				->condition('type', 'microsite','=')
				->condition('title', $dept, '=')
				->condition('status', 1, '=')
				->execute()->fetchField();
	if(!empty($nid)){
		return l($dept, 'node/'.$nid);
	}else{
		return $dept;
	}	
}