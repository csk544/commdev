(function($){
  $(document).ready(function() {
  var base_url = Drupal.settings.commportal_custom.base_url;
	var portal_tour_status = Drupal.settings.commportal_custom.cmg_portal_tour_status;
	var portal_tools_status = Drupal.settings.commportal_custom.cmg_portal_tools_status;
 
  setFooterOffset()
  function setFooterOffset() {
    $('.main-container').css('padding-bottom', $('footer').outerHeight());
  }
  $(window).on('resize', function(){
    setFooterOffset()
  });


  function openDepartmentsMenu() {
    if ($('#search-section').hasClass('active')) {
      closeSearchBar();
    }
    if ($('#about-cmg-menu').hasClass('active')) {
      //closeAboutMenu(true);
    }


    var menu = $('#departments-menu');
    $('.departments-menu-toggle').find('span.icon').addClass('icon-Icon_Up-Carrot-Filled');
    menu.addClass('active');
    $('.header-nav').removeClass('sticky');
    $('header').addClass('departments-active');
    $('header').scrollTop(0);
    // if (!nofix) {
      if ($(document).height() > $(window).height()) {
         var scrollTop = (parseInt($('body').css('top')) > 0) ? $('body').css('top') : ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop();
         $('body').addClass('overlay-active').css('top',-scrollTop);
      }
    // }


  }

  function closeDepartmentsMenu(nofix) {
    var menu = $('#departments-menu');
    $('.departments-menu-toggle').find('span.icon').removeClass('icon-Icon_Up-Carrot-Filled');
    menu.removeClass('active');
    $('.departments-col.active').removeClass('active');
    $('.dep-overlay').fadeOut();
    if (!$('#about-cmg-menu').hasClass('active')) {
      $('header').removeClass('departments-active');

      if (!nofix) {
       var scrollTop = (parseInt($('body').css('top'))) ? parseInt($('body').css('top')) : ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop();
       $('body').removeClass('overlay-active');
       $('html,body').scrollTop(-1 * scrollTop);
       $('body').css('top', 0);
      }
    }

  }


  var depTimer;
  $('.departments-menu-toggle').mouseenter(function() {
    var menu = $('#departments-menu');
    if (!menu.hasClass('active')) {
      depTimer = setTimeout(function() {
        openDepartmentsMenu();
      }, 200);
    } else {
      clearTimeout(depTimer);
      clearTimeout(abtTimer);
    }
  });

  $('.departments-menu-toggle').mouseleave(function() {
    var menu = $('#departments-menu');
    if (!menu.hasClass('active')) {
      clearTimeout(depTimer);
    } else {
      depTimer = setTimeout(function() {
        closeDepartmentsMenu();
      }, 200);

    }

  });

  var submenuTimer;
  $('.department-toggle-link').mouseleave(function() {
    var col = $(this).parent('.departments-col');
    if (col.hasClass('active')) {
      submenuTimer = setTimeout(function() {
        $('.dep-overlay').fadeOut('fast');
        col.removeClass('active');
        setTimeout(function() {
          col.find('.dep-submenu').hide(0);
        }, 200)
      }, 150);
    }
  });

  $('.department-toggle-link').mouseenter(function() {
    clearInterval(submenuTimer);
  });

  $('#departments-menu').mouseenter(function() {
    var menu = $('#departments-menu');
    if (menu.hasClass('active')) {
      clearTimeout(depTimer);
    }
  });

  $('#departments-menu').mouseleave(function() {
    var menu = $('#departments-menu');
    if (menu.hasClass('active') && (!$('.departments-col.active').length)) {
      depTimer = setTimeout(function() {
        closeDepartmentsMenu();
      }, 300);
    }
  });

  $('#departments-menu .dep-submenu').mouseenter(function() {
    clearInterval(submenuTimer);
    clearTimeout(depTimer);
  });

  $('#departments-menu .dep-submenu').mouseleave(function() {
    var col = $(this).parent('.departments-col');
    if (col.hasClass('active')) {
      submenuTimer = setTimeout(function() {
        $('#departments-menu .dep-overlay').fadeOut('fast');

        col.removeClass('active');
        setTimeout(function() {
          col.find('.dep-submenu').hide(0);
        }, 200)
      }, 150);
      clearTimeout(depTimer);
    }
  });

  $('#departments-menu .tertiary-menu').mouseenter(function() {
    clearInterval(submenuTimer);
    clearTimeout(depTimer);
  });

  $('#departments-menu .tertiary-menu').mouseleave(function() {
    var col = $(this).parent('.departments-col');
    if (col.hasClass('active')) {
      submenuTimer = setTimeout(function() {
        $('#departments-menu .dep-overlay').fadeOut('fast');
        col.removeClass('active');
      }, 150);
      clearTimeout(depTimer);
    }
  });

  $('#departments-menu .department-toggle-link').click(function() {
    var target = $(this).parent('.departments-col');

      $('.departments-col.active').not(target).toggleClass('active', false);
      if ($(target).hasClass('active')) {
        $(target).removeClass('active');
        setTimeout(function() {
          $(target).find('.dep-submenu').hide(0);
        }, 200)

      } else {
        $(target).find('.dep-submenu').show(0, function() {
          $(target).addClass('active');
        });
      }


      $('#departments-menu .submenu-item.has-submenu.active').toggleClass('active', false);
      if ($(target).hasClass('active')) {
        $('#departments-menu .dep-overlay').fadeIn();
      } else {
        $('#departments-menu .dep-overlay').fadeOut('fast');
      }
  });

  $('#departments-menu .dep-overlay').click(function() {
    $(this).fadeOut('fast');
    $('#departments-menu .departments-col.active').toggleClass('active', false);
    $('#departments-menu .submenu-item.has-submenu.active').toggleClass('active', false);
  });

  $('#departments-menu .submenu-item.has-submenu > a').click(function(e) {
    var target = $(this).parent('.has-submenu');
    $('#departments-menu .submenu-item.has-submenu.active').not(target).toggleClass('active', false);
    $(target).toggleClass('active');
  });





  function openAboutMenu() {
    if ($('#search-section').hasClass('active')) {
      closeSearchBar();
    }
    //
    if ($('#departments-menu').hasClass('active')) {
      //closeDepartmentsMenu(true);
    }

      var menu = $('#about-cmg-menu');
      $('.about-cmg-menu-toggle').find('span.icon').addClass('icon-Icon_Up-Carrot-Filled');
      menu.addClass('active');
      $('header').addClass('departments-active');
      $('header').scrollTop(0);
      $('.departments-col.active').removeClass('active');
      //if (!nofix) {
        if ($(document).height() > $(window).height()) {
           var scrollTop = (parseInt($('body').css('top')) > 0) ? $('body').css('top') : ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop();
           $('body').addClass('overlay-active').css('top',-scrollTop);
        }
      //}




  }

  function closeAboutMenu(nofix) {
    var menu = $('#about-cmg-menu');
    $('.about-cmg-menu-toggle').find('span.icon').removeClass('icon-Icon_Up-Carrot-Filled');
    menu.removeClass('active');
    if (!$('#departments-menu').hasClass('active')) {
      $('header').removeClass('departments-active');
       if (!nofix) {
        var scrollTop = (parseInt($('body').css('top'))) ? parseInt($('body').css('top')) : ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop();
        $('body').removeClass('overlay-active');
        $('html,body').scrollTop(-1 * scrollTop);
        $('body').css('top', 0);
       }
    }


    var slideMenuContainer = $('#about-cmg-menu .submenu .has-submenu').closest('.submenu-item');
    if (slideMenuContainer.hasClass('active')) {
      slideMenuContainer.removeClass('active');
      slideMenuContainer.animate({ height: slideMenuContainer.find('a.submenu-item-link').outerHeight()}, 600);

    }
  }


  // $('.about-cmg-menu-toggle').click(function() {
  //   var menu = $('#about-cmg-menu');
  //   if (menu.hasClass('active')) {
  //     closeAboutMenu()
  //   } else {
  //     clearInterval(abtTimer);
  //     openAboutMenu();
  //   }
  // });

  var abtTimer;
  $('.about-cmg-menu-toggle').mouseenter(function() {
    var menu = $('#about-cmg-menu');
    if (!menu.hasClass('active')) {
      abtTimer = setTimeout(function() {
        openAboutMenu();
      }, 200);
    } else {
      clearTimeout(abtTimer);
      clearInterval(depTimer);
    }
  });

  $('.about-cmg-menu-toggle').mouseleave(function() {
    var menu = $('#about-cmg-menu');
    if (!menu.hasClass('active')) {
      clearTimeout(abtTimer);
    } else {
      abtTimer = setTimeout(function() {
        closeAboutMenu();
      }, 200);

    }
  });


  $('#about-cmg-menu').mouseenter(function() {
    var menu = $('#about-cmg-menu');
    if (menu.hasClass('active')) {
      clearTimeout(abtTimer);
    }
  });

  $('#about-cmg-menu').mouseleave(function() {
    var menu = $('#about-cmg-menu');
    if (menu.hasClass('active')) {
      abtTimer = setTimeout(function() {
        closeAboutMenu();
      }, 300);
    }
  });

  var searchBarOpen;

  function openSearchBar() {
    var bar = $('#search-section');
    $('.menu-search').find('span.icon').addClass('icon-Icon_Close').removeClass('icon-Icon_Search');
    $('.menu-search').addClass('active');
    bar.addClass('active');
    $('header').addClass('departments-active');
    if ($(document).height() > $(window).height()) {
       var scrollTop = (parseInt($('body').css('top')) > 0) ? $('body').css('top') : ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop();
       $('body').addClass('overlay-active').css('top',-scrollTop);
    }
    searchBarOpen = true;
  }


  function closeSearchBar(nofix) {
    var bar = $('#search-section');
    $('.menu-search').find('span.icon').removeClass('icon-Icon_Close').addClass('icon-Icon_Search');
    bar.removeClass('active');
    $('.menu-search').removeClass('active');
    if (!$('#departments-menu').hasClass('active')) {
      $('header').removeClass('departments-active');



    }
    if (!$('#about-cmg-menu').hasClass('active')) {
      $('header').removeClass('departments-active');

    }

    if (!nofix) {
     var scrollTop = (parseInt($('body').css('top'))) ? parseInt($('body').css('top')) : ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop();
     $('body').removeClass('overlay-active');
     $('html,body').scrollTop(-1 * scrollTop);
     $('body').css('top', 0);
    }
    searchBarOpen = false;
  }


  $('.menu-search .menu-link').click(function() {
    var bar = $('#search-section');
    var icon = $('.menu-search');
    if (icon.hasClass('active')) {
      closeSearchBar();
    } else {
      openSearchBar();
    }
  });

  $(document).mouseup(function(e) {
    var container = $("#search-section");
    var icon = $('.menu-search .menu-link');
    if (searchBarOpen === true) {
      if ((!container.is(e.target) && container.has(e.target).length === 0) && !icon.is(e.target) && icon.has(e.target).length === 0)
      {
          closeSearchBar();
      }
    }
    // if the target of the click isn't the container nor a descendant of the container

  });









  $.fn.reverse = [].reverse;
  $('.sidebar-menu .menu-item.has-submenu a.menu-item-link').click(function() {
    if ($(this).parent('.menu-item').hasClass('active')) {
      $(this).parent('.menu-item').toggleClass('active', false);
      $(this).find('span.icon').toggleClass('icon-Icon_Up-Carrot-Filled', false);
      $(this).parent('.menu-item').find('.submenu-container').animate({ height: 0}, 600);
      var subcount = 0;
      $(this).parent().find('.submenu-item-link').reverse().each(function() {
        $(this).delay(50*subcount).animate({opacity: 0});

        subcount++;
      });
    } else {
      $(this).parent('.menu-item').toggleClass('active', true);
      $(this).find('span.icon').toggleClass('icon-Icon_Up-Carrot-Filled', true);
      var targetheight = $(this).parent('.menu-item').find('ul.submenu').outerHeight();
      $(this).parent('.menu-item').find('.submenu-container').animate({ height: targetheight});
      var subcount = 1;
      $(this).parent().find('.submenu-item-link').each(function() {

        $(this).delay(50*subcount).animate({opacity: 1});

        subcount++;
      });
    }
  });
/* 
  $('.add-to-links').click(function() {
    if ($(this).hasClass('added')) {
      $(this).removeClass('added');
      var icon = '<span class="links-icon icon-Icon_Plus-Circle"></span>';
      $(this).html(icon + 'Add To My Links')
    } else {
      $(this).addClass('added');
      var icon = '<span class="links-icon icon-Icon_Check-Mark-Circle"></span>';
      $(this).html(icon + 'Link Added');
    }

  }); */


  $('.rsvp-button').click(function() {
    if ($(this).hasClass('added')) {
      $(this).removeClass('added');
      var icon = '<span class="icon icon-Icon_Plus-Circle"></span>';
      $(this).html(icon + '<span class="txt">RSVP</span>')
    } else {
      $(this).addClass('added');
      var icon = '<span class="icon icon-Icon_Check-Mark-Circle"></span>';
      $(this).html(icon + '<span class="txt">RSVP</span>');
    }
  });



  $('.account-flyout-toggle').click(function() {
    var menu = $('.account-flyout');
    var toggle = $(this);
    if (menu.hasClass('active')) {
      toggle.find('i').removeClass('icon-Icon_Up-Carrot-Filled');
      menu.removeClass('active');
      toggle.removeClass('active');
    } else {
      toggle.find('i').addClass('icon-Icon_Up-Carrot-Filled');
      menu.addClass('active');
      toggle.addClass('active');

      var notifmenu = $('.notifications-flyout');
      var notiftoggle = $('.notification-icon');
      notifmenu.removeClass('active');
      notiftoggle.removeClass('active');
    }
  });
  var acctTimer;
  $('.account-flyout-toggle').mouseenter(function() {
    clearTimeout(acctTimer)
  });

  var notiftour, accttour;
  $('.account-flyout').mouseleave(function() {

    if (!accttour) {
      acctTimer = setTimeout(function() {
        var menu = $('.account-flyout');
        var toggle = $('.account-flyout-toggle');
        toggle.find('i').removeClass('icon-Icon_Up-Carrot-Filled');
        menu.removeClass('active');
        toggle.removeClass('active');
      }, 100);
    }

  });

  $('.notification-icon').click(function() {
    var menu = $('.notifications-flyout');
    var toggle = $(this).parent('.notifications');
    if (menu.hasClass('active')) {
      menu.removeClass('active');
      toggle.removeClass('active');
    } else {
      menu.addClass('active');
      toggle.addClass('active');

      var accmenu = $('.account-flyout');
      var acctoggle = $('.account-flyout-toggle');
      accmenu.removeClass('active');
      acctoggle.removeClass('active');
    }
  });

  var notifTimer;
  $('.notification-icon').mouseenter(function() {
    clearInterval(notifTimer);
  });

  $('.notifications-flyout').mouseleave(function() {
    if (!notiftour) {
      notifTimer = setTimeout(function() {
        var menu = $('.notifications-flyout');
        var toggle = $('.notification-icon').parent('.notifications');
        menu.removeClass('active');
        toggle.removeClass('active');
      }, 100);
    }


  });




   if ($('.page').hasClass('home-page')) {
     var firstLink = $('.news-menu-link.active')
     var firstLinkOffset = firstLink.position().top + 30;
     var firstLinkHeight = firstLink.outerHeight();

     $('.news-menu .pill').animate({
       top: firstLinkOffset,
       height: firstLinkHeight
     }, 0);

     var currentActive = $('.news-menu-link').not('.active');
     var caTarget = '.' + currentActive.attr('data-menu-target') + '-block';
     var thisTarget = '.' + firstLink.attr('data-menu-target') + '-block';


       currentActive.removeClass('active');
       $(caTarget).removeClass('active');
       firstLink.addClass('active');
       $(thisTarget).addClass('active');
       $(caTarget).hide();
       $(thisTarget).fadeIn();

   }



   $('.news-dynamic.active').fadeIn();
   $('.news-menu-link').click(function() {
     var topOffset = $(this).position().top + 30;
     var itemHeight = $(this).outerHeight();

     $('.news-menu .pill').stop().animate({
       top: topOffset,
       height: itemHeight
     }, 100);


     // console.log(itemHeight);
     // switch($(this).attr('data-menu-target')) {
     //   case 'department':
     //     $('.news-menu .pill').animate({top: '34px'}, 100);
     //     break;
     //   case 'following':
     //     $('.news-menu .pill').animate({top: '84px'}, 100);
     //     break;
     //   case 'people':
     //     $('.news-menu .pill').animate({top: '134px'}, 100);
     //     break;
     //   case 'training':
     //     $('.news-menu .pill').animate({top: '184px'}, 100);
     //     break;
     // }

     var currentActive = $('.news-menu-link.active');
     var caTarget = '.' + currentActive.attr('data-menu-target') + '-block';
     var thisTarget = '.' + $(this).attr('data-menu-target') + '-block';

     if ($(this).hasClass('active')) {

     } else {
       currentActive.removeClass('active');
       $(caTarget).removeClass('active');
       $(this).addClass('active');
       $(thisTarget).addClass('active');
       $(caTarget).hide();
       $(thisTarget).fadeIn();
     }
   });





  $('.news-menu-mobile .news-drop-down').on('change', function() {
    var target = '.' + this.value + '-block';
    $('.news-dynamic.active').hide().removeClass('active');
    $(target).addClass('active').fadeIn();
  });


  $('.back-to-top').on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });


  $('.notifications').click(function() {

  });

  $('.search-tabs-nav .tab-toggle').click(function(e) {
    e.preventDefault();
    var target = $($(this).attr('href'));
    $(this).addClass('active');
    $('.tab-toggle').not($(this)).removeClass('active');
    $('.search-tab').not(target).removeClass('active').hide();
    target.addClass('active');
    target.fadeIn();
  });


  $(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    if (scroll > 2) {
      $('.header-nav').addClass('sticky');
    } else if ((scroll < 2 ) && ($('.header-nav').hasClass('sticky'))) {
      $('.header-nav').removeClass('sticky');
    }
  });





  $('.mobile-menu-toggle').click(function() {
    $('body').addClass('mobile-menu-active');
  });

  var mostRecentParentPage;
  $('.menu-item.mobile-submenu-link a').click(function(e) { e.preventDefault()
  	var targetMenu = $($(this).attr('href'));
  	targetMenu.addClass('active');
    var parent = $(this).closest('.mobile-nav-page');
    parent.addClass('parent');
    mostRecentParentPage = parent;
  });

   $('.mobile-nav-page .back-link').click(function() {
  	$(this).closest('.mobile-nav-page').removeClass('active');
    $('#' + $(this).closest('.mobile-nav-page').attr('data-parent')).removeClass('parent');

  });


  $('.mobile-menu-close').click(function() {
	$('.mobile-nav-page').each(function() {
		$(this).removeClass('active');
    $(this).removeClass('parent');
    });
	   $('body').removeClass('mobile-menu-active');
  });


  // $(".fancy-scrollbar > div").hover(function() {
  // if ($(window).width() >= 768) {
  //   var oldScrollPos = $(window).scrollTop();
  //
  //   $(window).on('scroll.scrolldisabler', function(event) {
  //     $(window).scrollTop(oldScrollPos);
  //     event.preventDefault();
  //   });
  // }
  //
  // }, function() {
  //   $(window).off('scroll.scrolldisabler');
  // });


  var expandAll = false;
  $('.settings-modal .expand-all').click(function() {
    expandAll = true;
    if ($('#manageLinksHeading a').attr('aria-expanded') === 'false') {
      $('#manageLinksHeading a').click();
    }
    if ($('#manageTopicsHeading a').attr('aria-expanded') === 'false') {
      $('#manageTopicsHeading a').click();
    }
    if ($('#manageDepartmentsHeading a').attr('aria-expanded') === 'false') {
      $('#manageDepartmentsHeading a').click();
    }
    expandAll = false;
  });

  $('.settings-modal .card-header a').click(function() {
    if (expandAll === false) {
      if ($(this).attr('aria-expanded') === 'false') {
        $('.settings-modal .card-header a').not($(this)).each(function() {
          if ($(this).attr('aria-expanded') === 'true') {
            $(this).click();
          }
        })
      }
    }
  });

  // $('.slide-up-menu .slide-up-head').click(function() {
  //   $(this).closest('.slide-up-menu').toggleClass('active');
  // })

  $('.slide-up-menu .slide-up-head').click(function() {
  var container = $(this).closest('.slide-up-menu');
  if (container.hasClass('active')) {
    container.removeClass('active');
    container.animate({ height: container.find('.slide-up-head').outerHeight()}, 600);

  } else {
    container.addClass('active');
    var targetheight = container.find('.slide-up-slide').outerHeight();
    container.find('.slide-up-slide').css('top', container.find('.slide-up-head').outerHeight() + 10)
    container.animate({ height: targetheight + container.find('.slide-up-head').outerHeight() + 10});
  }
});


$('.about-menu-col .submenu-item .has-submenu').click(function() {
var container = $(this).closest('.submenu-item');
if (container.hasClass('active')) {
  container.removeClass('active');
  container.animate({ height: container.find('a.submenu-item-link').outerHeight()}, 600);

} else {
  container.addClass('active');
  var targetheight = container.find('a.submenu-item-link').outerHeight();
  // container.find('.tertiary-menu').css('top', container.find('a.submenu-item-link').outerHeight() + 10)
  container.animate({ height: targetheight + container.find('.tertiary-menu').outerHeight() + 10});
}
});



  $('.settings-modal #tag-scroll-alphas .tag-scroll-letter').click(function() {
    var target = $(this).attr('data-scroll');
    var amt = $('#tag-scroll').scrollTop() + $('#anchor-' + target).position().top;
    $('#tag-scroll').animate({scrollTop: amt}, 300);
  })


  var base = $('#tag-scroll').offset().top;
  var offs = [ ];
  $('.alpha-anchor').each(function() {
      var $this = $(this);
      offs.push($(this));
  });
  $("#tag-scroll").scroll(function() {
      var y = this.scrollTop;
      var alpha;
      for(var i = 0; i < offs.length; ++i) {

          if(y > (y + offs[i].position().top))
              continue;
              alpha = offs[i].attr('id').split('anchor-')[1];
              $('.tag-scroll-letter').not($('.tag-scroll-letter[data-scroll="' + alpha + '"]')).removeClass('active');
              $('.tag-scroll-letter[data-scroll="' + alpha + '"]').addClass('active');
          return;
      }
  });



  $('.tertiary-menu .ter-menu').each(function() {
    var height = $(this).outerHeight();
    $(this).parent('.tertiary-menu').css('height', height);
  })



/* 
  $('.page-tools .sitemap-row ol li ol li').each(function() {
    $(this).prepend('<span class="links-toggle-icon icon-Icon_Plus-Circle"></span>');
  });


  $('.page-tools .sitemap-row ol li ol li .links-toggle-icon').off().on('click', function(event) {
    event.preventDefault();
    if ($(this).hasClass('added')) {
        $(this).removeClass('added');
        $(this).removeClass('icon-Icon_Check-Mark-Circle');
        $(this).addClass('icon-Icon_Plus-Circle');

        // remove link

      } else {
        $(this).addClass('added');
        $(this).addClass('icon-Icon_Check-Mark-Circle');
        $(this).removeClass('icon-Icon_Plus-Circle');

        // add link

      }
  }); */

  /* $('.notifications-flyout .dismiss-all').click(function() {
    $(this).fadeOut();
    $('.notif-alert').removeClass('active');
    $('.notif-alert').text('0');
    $('.notifications').addClass('disabled');
    $('.notifications-flyout .notifications-feed .notification-item').each(function() {
      $(this).fadeOut('fast', function() {
        $(this).remove();
      })
    })
  })

  $('.notifications-flyout .notifications-feed .notification-item .notif-dismiss').click(function() {
      var item = $(this).closest('.notification-item');
      item.fadeOut('fast', function() {
        item.remove();
      })
      if ($('.notifications-flyout .notifications-feed .notification-item').length === 1) {
        $('.notifications-flyout .dismiss-all').fadeOut();
        $('.notif-alert').removeClass('active');
        $('.notif-alert').text('0');
        $('.notifications').addClass('disabled');
      }
  }) */

  var template = '<div class="popover tour">'
  + '<h3 class="popover-title"></h3>'
  + '<div class="popover-content"></div>'
  + '<div class="popover-navigation">'
  + '<a href="javascript:void(0);" data-role="prev" class="tour-prev"><span class="icon icon icon-Icon_Left-Arrow"></span>Previous</a>'
  + '<a href="javascript:void(0);" data-role="next" class="tour-next">Next</a>'
  + '</div>'
  + '<a href="javascript:void(0);" data-role="end" class="tour-end">&times;</a>'
  + '</div>';

  // Tour
  var tour = new Tour({
    template: template,
    steps: [
    {
      element: ".account-flyout",
      title: "Your Settings",
      content: "<p>Take a few minutes to update your preferences in Settings:</p>"
      + "<ul><li><b><i>News Feed</i></b> to add tags and follow topics of interest</li>"
      + "<li><b><i>Department</i></b> to update your Unit and Department manually</li></ul>",
      backdrop: true,
      backdropPadding: {
        top: 56,
      },
      placement: 'left',
      onShow: function() {
        accttour = true;
        var menu = $('.account-flyout');
        var toggle = $('.account-flyout-toggle');
        toggle.find('i').addClass('icon-Icon_Up-Carrot-Filled');
        menu.addClass('active');
        toggle.addClass('active');
      },
      onHide: function() {
        accttour = false;
        var menu = $('.account-flyout');
        var toggle = $('.account-flyout-toggle');
        toggle.find('i').removeClass('icon-Icon_Up-Carrot-Filled');
        menu.removeClass('active');
        toggle.removeClass('active');
      },
    },
    {
      element: ".notifications-flyout",
      title: "Notifications",
      content: "When you have important messages about training requirements, system notifications and other alerts, you'll see them here.",
      backdrop: true,
      backdropPadding: {
        top: 56,
      },
      placement: 'left',
      onShow: function() {
        notiftour = true;
        var menu = $('.notifications-flyout');
        var toggle = $('.notification-icon').parent('.notifications');
        menu.addClass('active');
        toggle.addClass('active');
      },
      onHide: function() {
        notiftour = false;
        var menu = $('.notifications-flyout');
        var toggle = $('.notification-icon').parent('.notifications');
        menu.removeClass('active');
        toggle.removeClass('active');
      }
    },
    {
      element: ".header-nav",
      title: "Navigation",
      content: "Getting around the CMG Portal is easy...",
      backdrop: true,
      backdropPadding: {

      },
      placement: 'bottom',
    },

    {
      element: ".about-cmg-menu-toggle",
      title: "Navigation",
      content: "Open <b><i>About CMG</i></b> to find our Mission, Operating Principles, Leadership information and more.",
      backdrop: true,
      backdropPadding: {
        top: 4,
        bottom: 2
      },
      placement: 'bottom',
    },

    {
      element: ".departments-menu-toggle",
      title: "Navigation",
      content: "Explore all the CMG <b><i>Departments</i></b>, including org charts and team-specific information.",
      backdrop: true,
      backdropPadding: {
        top: 4,
        bottom: 2
      },
      placement: 'bottom',
    },

    {
      element: ".tour-events",
      title: "Navigation",
      content: "View <b><i>CMG Events</i></b> in one place.",
      backdrop: true,
      backdropPadding: {
        top: 4,
        bottom: 2
      },
      placement: 'bottom',
    },

    {
      element: ".tour-tools",
      title: "Navigation",
      content: "Access important business <b><i>Tools</i></b> and pick your favorites to appear in your \"Tools & Links\" list.",
      backdrop: true,
      backdropPadding: {
        top: 4,
        bottom: 2
      },
      placement: 'bottom',
    }

    ,

    {
      element: ".news-block",
      title: "Your Personal Dashboard",
      content: "This <b><i>News Feed</i></b> includes stories from your Department and Business unit, as well as the topics you're following. You'll also find your required trainings and people news here.  ",
      backdrop: true,
      backdropPadding: {
        top: 0,
        bottom: 0
      },
      placement: 'top',
    }

    ,

    {
      element: ".links-block",
      title: "Your Personal Dashboard",
      content: "<p>Your <b><i>Tools & Links</i></b> holds all the essential links you've bookmarked throughout the Portal.</p>"
      + "<p>Take some time to favorite your most used items in Tools. You'll also find important links for the CMG community.</p>"
      + "<p>Use <b><i>Settings</i></b> to control your list.</p>",
      backdrop: true,
      backdropPadding: {
        top: 0,
        bottom: 0
      },
      template: '<div class="popover tour">'
      + '<h3 class="popover-title"></h3>'
      + '<div class="popover-content"></div>'
      + '<div class="popover-navigation">'
      + '<a href="javascript:void(0);" data-role="prev" class="tour-prev"><span class="icon icon icon-Icon_Left-Arrow"></span>Previous</a>'
      + '<a href="javascript:void(0);" data-role="end" class="tour-next">Next</a>'
      + '</div>'
      + '<a href="javascript:void(0);" data-role="end" class="tour-end">&times;</a>'
      + '</div>',
      placement: 'top',
    }




  ],
  storage: false,
  onEnd: function() {
    //document.cookie = 'cmgtour=complete';
    var data = "cmgtourstatus=tour-complete";
    if (portal_tour_status !== "tour-complete") {
       $.ajax({type: 'POST',url: base_url+'/update-tour-status', data: data,
        success: function(result) {
          //console.log('Successfully saved');
			}
		});
    }
    
    $('.tour-welcome').hide();
    $('.tour-skip-confirm').hide();
    $('.tour-thanks').show();

    $('#tourModal').modal('show');
  }
});

  if (portal_tour_status !== "tour-complete") {
    $('#tourModal').modal({
      backdrop: 'static'
    });
  }

// Settings Modal - Start tour
  $('#settingsModal .take-tour').click(function() {
    $('#settingsModal').modal('hide');
    $('.tour-skip-confirm').hide();
    $('.tour-thanks').hide();
    $('.tour-welcome').fadeIn();
    $('#tourModal').modal('show');
  })

// Modal start tour - On page load
  
  $('.start-tour').click(function() {
    $('#tourModal').modal('hide');

    tour.init();
    tour.restart();
    tour.start();
  })

  $('.skip-tour').click(function() {
    var data = "cmgtourstatus=tour-complete";
    if (portal_tour_status !== "tour-complete") {
       $.ajax({type: 'POST',url: base_url+'/update-tour-status', data: data,
        success: function(result) {
        }
      });
    }
    //document.cookie = 'cmgtour=complete';
    $('.tour-welcome').hide();
    $('.tour-skip-confirm').fadeIn();
  })
  
  // Tools tour script
  var toolstourmenu = $('.sitemap-row').first().find('li.col-sm-4:nth-child(2)');
  var toolsTour = new Tour({
    onStart: function() {
      var toggle = toolstourmenu.find('h3.h5');
      var container = toggle.closest('.sitemap-row > ol > li');
      container.addClass('active');
      var targetheight = container.find('ol').outerHeight();
      container.animate({ height: targetheight + container.find('h3.h5').outerHeight()});
    },
    onEnd: function() {
      var data = "cmgtourstatus=tools-complete";
      if (portal_tools_status !== "tools-complete") {
         $.ajax({type: 'POST',url: base_url+'/update-tour-status', data: data,
          success: function(result) {
            //console.log('Successfully saved');
        }
      });
      }
    },
    storage: false,
    steps: [
      {
      element: toolstourmenu.find('li:nth-child(4) > a > span.links-toggle-icon'),
      title: "Tools",
      content: "Be sure to select <span class='icon icon icon-Icon_Plus-Circle'></span> your frequently used tools here so they'll appear within your \"Tools & Links\" area. ",
      backdrop: true,
      backdropPadding: {
        top: 6,
        bottom: 6,
        left: 4,
        right: 4
      },
      placement: 'left',
      template: '<div class="popover tour">'
      + '<h3 class="popover-title"></h3>'
      + '<div class="popover-content"></div>'

      + '<a href="javascript:void(0);" data-role="end" class="tour-last-end">Close</a>'
      + '</div>'
    }]
  });

  if (portal_tools_status !== "tools-complete" && $(toolstourmenu).length > 0) {
    toolsTour.init();
    toolsTour.start();
  }
  
/*   function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
          var c = ca[i];
          while (c.charAt(0) == ' ') {
              c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
              return c.substring(name.length, c.length);
          }
      }
      return "";
  	} */


});

})(jQuery);