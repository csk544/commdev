/*
Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

/*
 * This file is used/requested by the 'Styles' button.
 * The 'Styles' button is not enabled by default in DrupalFull and DrupalFiltered toolbars.
 */
if(typeof(CKEDITOR) !== 'undefined') {
    CKEDITOR.addStylesSet( 'drupal',
    [
			{name : 'Highlight Text',	element : 'span', attributes : { 'class' : 'panel highlight-panel'} },
			{name : 'Button', element : 'span', attributes : { 'class' : 'btn btn-primary btn-sm'} },
			/*{name : 'object-left-align',	element : 'span', attributes : { 'class' : 'object-left-align'} },
			{name : 'object-center', element : 'span', attributes : { 'class' : 'object-center'} },
			{name : 'object-right-align', element : 'span', attributes : { 'class' : 'object-right-align'} },
			{name : 'LARGE PRIMARY BUTTON',	element : 'span', attributes : { 'class' : 'btn btn-primary btn-lg'} },
			{name : 'LARGE DEFAULT BUTTON',	element : 'span', attributes : { 'class' : 'btn btn-default btn-lg'} },	
			{name : 'Default size, primary button', element : 'span', attributes : { 'class' : 'btn btn-primary'} },
			{name : 'Default size, default button', element : 'span', attributes : { 'class' : 'btn btn-default'} },
			{name : 'SMALL BUTTON', element : 'span', attributes : { 'class' : 'btn btn-primary btn-sm'} },
			{name : 'SMALL BUTTON PLAIN', element : 'span', attributes : { 'class' : 'btn btn-default btn-sm'} },
			{name : 'EXTRA SMALL BUTTON', element : 'span', attributes : { 'class' : 'btn btn-primary btn-xs'} },
			{name : 'EXTRA SMALL BUTTON PLAIN', element : 'span', attributes : { 'class' : 'btn btn-default btn-xs'} },
			{name : 'BLOCK LEVEL BUTTON', element : 'span', attributes : { 'class' : 'btn btn-primary btn-lg btn-block'} },
			{name : 'BLOCK LEVEL BUTTON PLAIN', element : 'span', attributes : { 'class' : 'btn btn-default btn-lg btn-block'} },
			{name : 'BLOCK JUST ON MOBILE', element : 'span', attributes : { 'class' : 'btn btn-primary btn-mobile-block'} },*/
    ]);
}