(function ($) {
  Drupal.behaviors.commportal_search = {
    attach: function (context, settings) {
		$('.search-suggestions').fadeOut('fast');		
		if(Drupal.settings.commportal_custom.first_argument == "media-archive") {
			$('.quick-drop-input .input-btn-right').attr("placeholder", "Type a gallery name...");
		}
		else if(Drupal.settings.commportal_custom.first_argument == "career-and-learning") {
			$('.quick-drop-input .input-btn-right').attr("placeholder", "Enter title or keyword...");
		}
		else if(Drupal.settings.commportal_custom.first_argument == "tools") {
			$('.quick-drop-input .input-btn-right').attr("placeholder", "Type a tool name...");
		}
		else if(Drupal.settings.commportal_custom.first_argument == "find-class") {
				$('.quick-drop-input .input-btn-right').attr("placeholder", "Enter title or keyword...");
		} else if(Drupal.settings.commportal_custom.first_argument == "dept_rolebasedprograms"){
				$('.quick-drop-input .input-btn-right').attr("placeholder", "Enter title or keyword...");
		}
		else if(Drupal.settings.commportal_custom.first_argument == "node") {
			if(Drupal.settings.commportal_custom.content_type == "career_learning") {
				$('.quick-drop-input .input-btn-right').attr("placeholder", "Enter title or keyword...");
			} else if (Drupal.settings.commportal_custom.content_type == "toolbox") {
			   $('.quick-drop-input .input-btn-right').attr("placeholder", "Type a tool name...");
			}
		}
    
    /* //Redirecting to gwiz search results.
    $('#gwiz-search .sb-search-submit').click(function(event){        
			//event.preventDefault();	 
			var keyword = $("input:text#search").val();
			if(keyword.length != 0){
        $("input:text#search").val("");
				window.open("http://gwiz.gene.com/search/" + keyword);   
      }			
		}); */
		/*var autosuggestionRequest = null;
		function searchAutoSuggestions(searchtext, abort){
			if(abort){
				if(autosuggestionRequest != null){
					autosuggestionRequest.abort();
				}				
			}else{
				autosuggestionRequest = $.ajax({
					url: Drupal.settings.commportal_custom.base_url + "/search-results-autocomplete",
					method: "POST",
					data: { searchtext : searchtext },
					dataType: "html",
					beforeSend : function()    {           
						if(autosuggestionRequest != null) {
							autosuggestionRequest.abort();
						}
					},
					success: function(msg) {
						if(msg){
							$('.search-suggestions').html(msg);
						 
						   $('.search-suggestions').fadeIn('fast');						
						}
						else {
						   $('.search-suggestions').fadeOut('fast');	
						   $('.search-suggestions').html("");					   
						}
					}
				});
			}
			
		}*/
		/* $("#sb-search" ).off('keyup').on('keyup', '#search', function() {
			var searchedInputLength = $.trim(this.value).length;
			if(searchedInputLength > 1){
				$('.search-suggestions').html('<div class="search-suggestions__header"><h3 class="pull-left">'+Drupal.t('Loading Search Results...')+'</h3></div>');
				searchAutoSuggestions(this.value, false);
			}else{
				searchAutoSuggestions(this.value, true);
				$('.search-suggestions').fadeIn('fast');
				$('.search-suggestions').html('<div class="search-suggestions__header"><h3 class="pull-left">'+Drupal.t('Please enter minimum two characters for auto suggestions')+'</h3></div>');
			}
			
		}); */
		/*$( "#search-term" ).keyup(function() {
			searchAutoSuggestions(this.value);
		});*/
		//Media Archive Autocomplete
		$(".quick-drop-input input[type='text']").keyup(function() {
			if(Drupal.settings.commportal_custom.first_argument == "media-archive") {
				type = "media";	
			}
			else if(Drupal.settings.commportal_custom.first_argument == "career-and-learning") {
				type = "career_learning";	
			}
			else if(Drupal.settings.commportal_custom.first_argument == "find-class") {
				type = "career_learning";	
			} 
			else if(Drupal.settings.commportal_custom.first_argument == "dept_rolebasedprograms"){
				type = "career_learning";	
			}
			else if(Drupal.settings.commportal_custom.first_argument == "tools") {
				type = "toolbox";	
			}
			else if(Drupal.settings.commportal_custom.first_argument == "node") {
	        	if(Drupal.settings.commportal_custom.content_type == "career_learning") {
			       type = "career_learning";	
		        } else if(Drupal.settings.commportal_custom.content_type == "toolbox") {
	               type = "toolbox";
		        }
	    }
			$.ajax({
				url: Drupal.settings.commportal_custom.base_url + "/quick-search-autocomplete",
				method: "POST",
				data: { searchtext : this.value, type: type },
				dataType: "html",
				async: true,
				success: function(msg) {
					if(msg){
					   $('.drop-panel').html("<div class='drop-inner'><ul>" +  msg + "</ul></div>");	
					   $('.drop-panel').fadeIn('fast');
					} else {
					   $('.drop-panel').fadeOut('fast');	
					   $('.drop-panel').html("");	
					}
				}
			});
		});

		$(".quick-drop-input input[type='text']").focusout(function() {
			$(".drop-panel").fadeOut('fast');
		});

		$("#commercial-page").change(function() {
			if(this.checked) {
				$('#people-page').prop('checked', false);
				$('#gwiz-page').prop('checked', false);
				constructURL("search-people", "search-results", "search-gwiz");
			}else{
				constructURL("search-people", "search-results");
			}
		});
		$("#people-page").change(function() {
			if(this.checked) {
				$('#commercial-page').prop('checked', false);
				$('#gwiz-page').prop('checked', false);
				constructURL("search-results", "search-people", "search-gwiz");
			}else{
				constructURL("search-people", "search-results");
			}
		});
		$("#gwiz-page").change(function() {
			if(this.checked) {
				$('#commercial-page').prop('checked', false);
				$('#people-page').prop('checked', false);
				constructURL("search-results", "search-gwiz", "search-people");
			}else{
				constructURL("search-gwiz", "search-results");
			}
		});
		$("#commercial-news").change(function() {
			if(this.checked) {
				$('.commercial-news-terms').each(function(){
					$(this).prop('checked', true);
				});
			}else {
				$('.commercial-news-terms').each(function(){
					$(this).prop('checked', false);
				});
			}
			constructURL();
		});
		$('.commercial-news-terms').change(function() {
			constructURL();
		});	
		$("#people-announcements").change(function() {
			if(this.checked) {
				$('.people-announcements-terms').each(function(){
					$(this).prop('checked', true);
				});
			}else {
				$('.people-announcements-terms').each(function(){
					$(this).prop('checked', false);
				});
			}
			constructURL();
		});
		$('.people-announcements-terms').change(function() {
			constructURL();
		});
		$('#microsites-content').change(function() {
			constructURL();
		});
		$('#downloads-content').change(function() {
			constructURL();
		});
		$("#relevance").click(function() {
			setGetParameter('sort_by', 'search_api_relevance');
		});
		$("#mostrecent").click(function() {
			setGetParameter('sort_by', 'created');
		});
		function setGetParameter(paramName, paramValue, url, returnFlag)
		{
			if(typeof url == 'undefined'){
				var url = window.location.href;				
			}
			if (url.indexOf(paramName + "=") >= 0)
			{
				var prefix = url.substring(0, url.indexOf(paramName));
				var suffix = url.substring(url.indexOf(paramName));
				suffix = suffix.substring(suffix.indexOf("=") + 1);
				suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
				url = prefix + paramName + "=" + paramValue + suffix;
			}
			else
			{
			if (url.indexOf("?") < 0)
				url += "?" + paramName + "=" + paramValue;
			else
				url += "&" + paramName + "=" + paramValue;
			}
			if(typeof returnFlag == 'undefined'){
				window.location.href = url;		
			}else{
				return url;
			}			
		}
		$(".search-again-button").click(function() {
			constructURL();
			return false;
		});
		function constructURL(searchURL, replaceURL, searchURL2){
			var url = location.protocol + '//' + location.hostname + location.pathname;
			var searchInDepth = true;
			if(typeof searchURL != 'undefined' && typeof replaceURL != 'undefined'){
				url = url.replace(searchURL, replaceURL);
				if(typeof searchURL2 != 'undefined'){
					url = url.replace(searchURL2, replaceURL);
				}
				if(replaceURL == 'search-people' || replaceURL == 'search-gwiz'){
					searchInDepth = false;
				}
			}else if (url.indexOf('search-people') > -1 || url.indexOf('search-gwiz') > -1){
				searchInDepth = false;
			}
			var searchText = $.trim($('#search-term').val());
			if(searchText != '' && searchInDepth == true){
				var i=0;				
				var commercialNewsChecked = $("#commercial-news").prop('checked');
				if(commercialNewsChecked){
					url = setGetParameter('commercialNews', 1, url, true);
				}
				var peopleAnnouncChecked = $("#people-announcements").prop('checked');
				if(peopleAnnouncChecked){
					url = setGetParameter('peopleAnnouncments', 1, url, true);
				}
				var commercialPageChecked = $("#commercial-page").prop('checked');
				if(commercialPageChecked){
					url = setGetParameter('type', 'commercial', url, true);
				}
				var micrositesContentChecked = $("#microsites-content").prop('checked');
				var j=0;
				if(micrositesContentChecked){
					url = setGetParameter('contenttype['+j+++']', 'microsite', url, true);
					url = setGetParameter('contenttype['+j+++']', 'career_learning', url, true);
					url = setGetParameter('contenttype['+j+++']', 'toolbox', url, true);
				}
				var downloadsContentChecked = $("#downloads-content").prop('checked');
				if(downloadsContentChecked){
					url = setGetParameter('contenttype['+j+++']', 'downloads', url, true);
				}
				var sortField = $("#sortfield").attr('data-sortfield');
				url = setGetParameter('sort_by', sortField, url, true);
				var commercialNewsTermsIndex = 0;
				$('.commercial-news-terms').each(function(){
					if(this.checked) {
						var fieldName = $(this).attr('data-field');
						var fieldTid = $(this).attr('data-tid');
						url = setGetParameter(fieldName+'['+commercialNewsTermsIndex+++']', fieldTid, url, true);
					}
				});
				var peopleAnnouncementsTermsIndex = 0;
				$('.people-announcements-terms').each(function(){
					if(this.checked) {
						var fieldName = $(this).attr('data-field');
						var fieldTid = $(this).attr('data-tid');
						url = setGetParameter(fieldName+'['+peopleAnnouncementsTermsIndex+++']', fieldTid, url, true);
					}
				});				
			}
			setGetParameter('keyword', searchText, url);
		}
     // Append Subpage add button in Microsite content type.
		$(".node-microsite-form div#edit-field-ad-hoc-links .link-field-subrow").each(function(index) {
			if ($(this).find("button").length == 0) {
				if(!($('#edit-field-km-microsite-und').prop('checked'))){
					$('<img class = "add_icon" title="Add Subpage" src = "/sites/default/files/assets/add.png"><button type="submit" class="clear_links">Clear</button>').appendTo($(this));
				}  
			}
		});
		$('table#field-ad-hoc-links-values div.form-type-link-field .add_icon').click(function(event){           
			event.preventDefault(); 
			var url_id = "";
			$(this).closest('div.link-field-subrow').find('input').each(function() {
				url_id = $(this).attr('id');
			});	 
			var val = '/node/add/microsite/misc/popup/'+ url_id;
			window.open(val, "popupWindow",  "width=600,height=600,scrollbars=yes");           
		});
		$('button.clear_links').unbind().click(function(event) {
			event.preventDefault();
			var $this = $( this ).parent().find('input');
			$this.val("");
		});
    }
  };
}(jQuery));