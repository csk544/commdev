<?php
$markup = '';
global $base_url;
$language = $node->language;
//$entity_val = (array_key_exists($node->language, $obj['entity']->body) && !empty($obj['entity']->body)) ? $obj['entity']->body[$node->language][0]['value'] : '';
$container_type = $node->field_container_type[$node->language][0]['value'];
$container_type_field = 'field_choose_' . $container_type;
$data = isset($node->{$container_type_field}[$node->language]) ? $node->{$container_type_field}[$node->language] : (isset($node->field_container[$node->language]) ? $node->field_container[$node->language] : array());

//Sort List of content based on published date
$data_arr = array();
if($container_type != 'downloads' && $container_type != 'image') {
	foreach($data as $key => $obj) {
		$data_arr[$key] = $data[$key]['entity']->created;
	}
	array_multisort($data_arr, SORT_DESC, SORT_NUMERIC, $data);	
}

/*  else {
	foreach($data as $key => $obj) {
		$data_arr[$key] = $data[$key]['entity']->field_document[$language][0]['timestamp'];
	}
} */

if($container_type == 'downloads') {
	if(!$page){
		if($title != ''){
			$markup .= '<h3 class="hd-title">'.$title.'</h3>';
		}
		if($title == ''){
			$markup .= '<h3 class="no-title"></h3>';
		}
	}
	if($page){
		$markup .= '<h3 class="hd-title">'.$title.'</h3>';	
	}
 $markup .= '<ol class="result-list result-list-dividers">';	
		foreach($data as $key => $obj) {
		$status = $obj['entity']->status;
		if($status) {
			if(isset($obj['entity']->field_document[$language][0]['filename'])) {
				//$author = user_load($obj['entity']->field_document[$node->language][0]['uid']);
				$file_url = file_create_url($obj['entity']->field_document[$language][0]['uri']);
				$file_info = pathinfo($obj['entity']->field_document[$language][0]['filename']);
				$desc = isset($obj['entity']->body[$node->language])? $obj['entity']->body[$node->language][0]['value'] : '';
				$markup .= '<li class="clearfix">
				<span class="result-list__icon icon icon-'. strtolower($file_info['extension']) .'"></span>
				<div class="result-list__details">
				<h5 class="no-margin-bottom">'. l($obj['entity']->title, $file_url).'</h5>
				<p><span class="result-list__location">Uploaded on '. date('m.d.Y', $obj['entity']->field_document[$node->language][0]['timestamp']).'<span class="download-author">, by '. get_display_name($obj['entity']->uid) .'</span></span></p><p class="results-list__desc">'.$desc.'</p>
				</div>
				</li>';
			}
		}
		}
	$markup .= '</ol>';
}
 else if($container_type == 'image') {	 
 if(!$page){
		if($title != ''){
			$markup .= '<h3 class="hd-title">'.$title.'</h3>';
		}
		if($title == ''){
			$markup .= '<h3 class="no-title"></h3>';
		}
	}
	if($page){
		$markup .= '<h3 class="hd-title">'.$title.'</h3>';
	}
	$markup .= '<div class="module-slider widget owl-carousel owl-theme">';
	foreach($data as $key => $obj) {
		$status = $obj['entity']->status;
		if($status) {
			if (!empty($obj['entity']->field_basic_image_image)) {			
				$img_link = !empty($obj['entity']->field_link) ? $obj['entity']->field_link[$language][0]['value'] : '#';		
				$markup .= '<div class="item"><a href="'.$img_link.'"><img src="'.file_create_url($obj['entity']->field_basic_image_image[$node->language][0]['uri']).'" alt="'.$obj['entity']->field_basic_image_image[$node->language][0]['filename'].'"></a></div>';
			}
		}
	}
	$markup .= '</div>';
}
 else if($container_type == 'article' || $container_type == 'announcements' || $container_type == 'career_profiles') {
	 if(!$page){
		if($title != ''){
			$markup .= '<h3 class="hd-title">'.$title.'</h3>';
		}
		if($title == ''){
			$markup .= '<h3 class="no-title"></h3>';
		}
	}
	if($page){
		$markup .= '<h3 class="hd-title">'.$title.'</h3>';
	}
  $markup .= '<ul class="list-clean panel-list panel-list-small">';
	$item_list = isset($node->field_items_list_size['und'][0]['value']) ? $node->field_items_list_size['und'][0]['value'] : '';
	$count = 0;
	foreach($data as $key => $obj) {
		if($item_list != '' && $count == $item_list){
			break;
		}
		$article_title = $obj['entity']->title;
		$status = $obj['entity']->status;
		if($status) {
			if(strlen($obj['entity']->title) > 55)	{	//wrap the title 
				$title_arr = wordwrap($obj['entity']->title, 55, "...#", true);
				$title_arr = explode("#", $title_arr);
				$article_title = $title_arr[0];
			}
			$markup .= '<li><p><span>'. l($article_title, 'node/'.$obj['entity']->nid).'</span><span class="feed__date">'.date('m.d.Y', $obj['entity']->created).'</span></span></p></li>';
			$count++;
		}
	}
	$markup .= '</ul>';
	if($container_type == 'article' || $container_type == 'announcements'){
		$nid = arg(1);
		if($nid && is_numeric($nid)) {
			$tid_node = node_load($nid);
			$tid = isset($tid_node->field_channel['und'][0]['tid']) ? $tid_node->field_channel['und'][0]['tid'] : "";
		}
	}
	//submit announcement and view more labels under kudos block
	if($node->field_container_type[$language][0]['value'] == "announcements") {
		if (!empty($tid)) {
			$href = $base_url.'/kudos_list/dept/'.$tid;
		} else {
			$href = $base_url.'/kudos-list';
		}
		$markup .= $submit_announcement_popup.'<div class="text-right"><a href="'.$href.'" class="view-more">';
		if(($item_list != "" && count($data) > $count) || empty($tid)){
			$markup .= 'View More<span class="icon icon-triangle-right"></span>';
		}
		$markup .= '</a></div>';
	}
	else if($node->field_container_type[$language][0]['value'] == "article") {
		if (!empty($tid)) {
			$href = $base_url.'/commercial_news/dept/'.$tid;
		} else {
			$href = $base_url.'/commercial-news';
		}
		$markup .= $submit_announcement_popup.'<div class="text-right"><a href="'.$href.'" class="view-more">';
		if(($item_list != "" && count($data) > $count) || empty($tid)){
			$markup .= 'View More<span class="icon icon-triangle-right"></span>';
		}
		$markup .= '</a></div>';
	}
} 
?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> role="article">
  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
   <!-- <h2<?php //print $title_attributes; ?>>-->
     <!-- <a href="<?php //print $node_url; ?>"><?php //print $title; ?></a>-->
   <!-- </h2>-->
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php //if ($display_submitted): ?>
   <!-- <div class="meta submitted">
      <?php //print $user_picture; ?>
      <?php //print $submitted; ?>
    </div>-->
  <?php //endif; ?> 

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content);
			print $markup;
    ?>
  </div>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</article>