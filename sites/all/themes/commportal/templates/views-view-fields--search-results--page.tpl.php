<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
 $titleAttr = $pulse = ''; 
 if($fields['type']->content == 'image_gallery'){
	 if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") { 
		$url = 'https://' . $_SERVER['HTTP_HOST'] . '/';
	} else { 
		$url = 'http://' . $_SERVER['HTTP_HOST'] . '/';
	}
	 $fields['url']->content = $url . 'media-archive/' . $fields['nid']->content;
 }else if($fields['type']->content == 'career_learning' || $fields['type']->content == 'toolbox'){
	  if(!is_null($fields['field_external_url']->content)){
			$field_external_url = $fields['field_external_url']->content;
			$fields['url']->content = $field_external_url;
			$ext_url_params = parse_url($field_external_url);
			if(isset($ext_url_params['host']) && $ext_url_params['host'] != $_SERVER['HTTP_HOST']){
				$titleAttr = 'target="_blank"';
			}
			if(isset($fields['field_requires_gene_vpn_']->content) && $fields['field_requires_gene_vpn_']->content == 1) {
				$pulse = "<span class='pulse'></span>";
			}
		}
 }
 $titleURL = $fields['url']->content;
?>
<li>
	<?php if($fields['type']->content == 'downloads' || ($fields['type']->content == 'video' && is_null($fields['field_video_type']->content))):
					$titleURL = ($fields['type']->content == 'downloads') ? $fields['field_document']->content : $fields['field_video_file']->content;
					$titleAttr = 'target="_blank"';
	?>
					<span class="result-list__icon icon icon-<?php echo get_file_extension($titleURL);?>"></span>
					<div class="result-list__details">
	<?php endif;?>
						<h3 class="result-list__title">
							<a href="<?php echo url($titleURL); ?>" <?php echo $titleAttr;?>>
								<?php echo $fields['title']->content;?>
							</a>
							<?php if($pulse != "") {
								echo $pulse;
							}
							?>
						</h3>
						<p>
							<span class="result-list__date">
								<?php echo $fields['created']->content;?>
							</span> | <span class="result-list__location">
													<?php echo $fields['url']->content;?>
												</span>
						</p>
						<p>
							<?php echo str_replace($_GET['keyword'],"<b>".$_GET['keyword']."</b>",$fields['search_api_excerpt']->content);?>
						</p>
	<?php if($fields['type']->content == 'downloads' || ($fields['type']->content == 'video' && is_null($fields['field_video_type']->content))):?>
					</div>
	<?php endif;?>
</li>