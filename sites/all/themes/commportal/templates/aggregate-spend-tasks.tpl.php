<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
global $theme_path;
$imgs_path = '/' . $theme_path . '/images/';
?>
	<?php if ($status['state'] == 'failed') { ?>
		<div class="failed"><p class="small">Sorry! We are not able to display your data at this time. If you would like immediate assistance, please contact the service desk at 1-877-225-8111. (Error code: <?php print $status['code']; ?>) <?php print $status['message']; ?></p></div>
	 <?php }
	 elseif ($status['state'] == 'success') {
		$dhtmlgoodies_a1 = $dhtmlgoodies_a2 = $bullet = "";
		$tasks_base_url = $data['sfdcinfo']['sfdcserverurl'] . "secur/frontdoor.jsp?sid=" . $data['sfdcinfo']['sfdcsessionid'] . "&retURL=";
		if (isset($data['payload']['totalagstasks']) > 0) {
			foreach ($data['payload']['tasks'] as $key => $record) {				
				if($record["latency"]=="No Latency"){
					$bullet = 'nolatency.gif';
				}else if($record["latency"]=="Reminder"){
					$bullet = 'reminder.gif';
				}else if($record["latency"]=="Overdue"){
					$bullet = 'overdue.gif';
				}else{
					$bullet = 'escalated.gif';
				}
				$desc_link = '<a href="' . $tasks_base_url . $record["caseid"] . '" target="_blank">' . $record["description"] . '</a>';
			$dhtmlgoodies_a2 .= <<<EOF
			<li><span><img src="{$imgs_path}{$bullet}"> {$desc_link}</span>
				<p>{$record["summary"]} ({$record["name"]})</p></li>
EOF;
			}
		}
		else {
			$dhtmlgoodies_a2 .= '<p class="small">There are no alerts available at the moment.</p>';
		}
		
		/** Notifications **/
		if (isset($data['payload']['totalagsnotifications']) > 0) {
			foreach ($data['payload']['notifications'] as $key => $record) {				
				$desc_link = '<a href="' . $tasks_base_url . $record["caseid"] . '" target="_blank">' . $record["description"] . '</a>';

			$dhtmlgoodies_a1 .= <<<EOF
			<li><span><img src="{$imgs_path}nolatency.gif"> {$desc_link}</span>
									<p>{$record["summary"]} ({$record["name"]})</p>
								</li>
EOF;
			}
		}
		else {
			$dhtmlgoodies_a1 .= '<p class="small">There are no notifications available at the moment.</p>';
		}
		
		$escalated_cnt = isset($data['payload']['count']['escalated']) ? $data['payload']['count']['escalated'] : 0;
		$overdue_cnt = isset($data['payload']['count']['overdue']) ? $data['payload']['count']['overdue'] : 0;
		$reminder_cnt = isset($data['payload']['count']['reminder']) ? $data['payload']['count']['reminder'] : 0;
		$nolatency_cnt = isset($data['payload']['count']['nolatency']) ? $data['payload']['count']['nolatency'] : 0;
		print <<<EOF
				<div class="ags-container" id="dhtmlgoodies_q1">					
					<p class="bold">Notifications</p>
				</div>
				<ul class="list-clean panel-list panel-list-small" id="dhtmlgoodies_a1" >
							{$dhtmlgoodies_a1}
				</ul>
				<div class="ags-container" id="dhtmlgoodies_q2">					
					<p class="bold">Action Required </p>
					<span class="escalated"><img src="{$imgs_path}escalated.gif" title="Manager Escalated" alt="Manager Escalated"> <label class="small">({$escalated_cnt})</label></span>
					<span class="overdue"><img src="{$imgs_path}overdue.gif" title="Overdue" alt="Overdue"> <label class="small">({$overdue_cnt})</label></span>
					<span class="reminder"><img src="{$imgs_path}reminder.gif" title="Reminder" alt="Reminder"> <label class="small">({$reminder_cnt})</label></span>
					<span class="nolatency"><img src="{$imgs_path}nolatency.gif" title="No Latency" alt="No Latency"> <label class="small">({$nolatency_cnt})</label></span>
				</div>
				<ul class="list-clean panel-list panel-list-small" id="dhtmlgoodies_a2">
							{$dhtmlgoodies_a2}
				</ul>
EOF;
		
	
	print '<div class="text-right"><a href="' . $tasks_base_url . $data['payload']['archiveurl'] . '" target="_blank" class="view-more">View archived alerts</a></div>';

	}
	?>	 
