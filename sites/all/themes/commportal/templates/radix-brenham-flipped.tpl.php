<?php
/**
 * @file
 * Template for Radix Brenham Flipped.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>

<div class="container microsite panel-display brenham-flipped clearfix <?php if (!empty($classes)) { print $classes; } ?><?php if (!empty($class)) { print $class; } ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
	
	<div class="row module margin-top-default">
		<div class="col-sm-12 block">
			<?php print $content['header']; ?>
		</div>
	</div>

	<div class="row margin-top40">
		<div class="col-md-9">
			<?php print $content['contentmain']; ?>
		</div>
		<div class="col-md-3 sidebar">
				<?php print $content['sidebar']; ?>
		</div>
	</div>
    
</div><!-- /.brenham-flipped -->