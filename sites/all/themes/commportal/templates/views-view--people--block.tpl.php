<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php //if ($more): ?>
    <?php //print $more; ?>
  <?php //endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>
<?php
	if ($more):
	$query_params = array();
	if(isset($_GET['keyword']) && trim($_GET['keyword']) != ''):
	$query_params['keyword'] = trim($_GET['keyword']);
	if(isset($_GET['type']) && trim($_GET['type']) == 'commercial'){
		$query_params['type'] = trim($_GET['type']);
	}
		
?>
	<a href="<?php echo url('search-people', array('query' => $query_params));?>" class="pull-right view-more margin-ll"><?php echo t('More People');?> <span class="icon-triangle-right"></span></a>
<?php
	else:
?>
	<a href="<?php echo url('search-people');?>" class="pull-right view-more margin-ll"><?php echo t('More People');?> <span class="icon-triangle-right"></span></a>
<?php
	endif;
	endif;
?>
</div><?php /* class view */ ?>