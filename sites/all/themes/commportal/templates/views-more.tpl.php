<?php

/**
 * @file
 * Theme the more link.
 *
 * - $view: The view object.
 * - $more_url: the url for the more link.
 * - $link_text: the text for the more link.
 *
 * @ingroup views_templates
 */
?>
<div class="text-right view-more-container">
	<a href="<?php print $more_url ?>" class="view-more"><?php print $link_text; ?><span class="icon icon-triangle-right"></span></a>
</div>
