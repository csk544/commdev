<?php
$output = '<ol class="result-list commercial-news">';
if(count($view->result) <= 0) {
	$output .= "<div class='no_results'>No results found</div>";
}
else {
	foreach($view->result as $result) {
		$author_uid = !empty($result->field_field_author[0]['rendered']['#markup']) ? $result->field_field_author[0]['rendered']['#markup'] : $result->node_uid;
		$category = isset($result->field_field_category[0]['rendered']['#markup']) ? $result->field_field_category[0]['rendered']['#markup'] : $result->field_field_announcement_type[0]['rendered']['#markup'];
		$output .= '<li><div><h3 class="result-list__title">'. l($result->node_title, 'node/'. $result->nid).'</h3>
		<p><span class="result-list__location">'. date("m.d.Y", $result->node_created) .' |  By '. get_display_name($author_uid) .'  |  In '. $category .'</span></p>
		<p>'. $result->field_field_teaser[0]['rendered']['#markup'] .'</p></div></li>';			
	}
}
$output .= "</ol>";
if(!empty($pager)) {
	$output .= $pager;
}
print $output;
?>