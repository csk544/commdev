<?php
$builder_class = ''; 
if($element['#field_name'] == "field_faq") {
	$builder_class = "col-md-9";
}else if($element['#field_name'] == "field_microsite_builder") {
	$builder_class = "col-md-9  order-md-12 order-1";
} else if($element['#field_name'] == "field_microsite_rightside") {
	$builder_class = "col-md-3 order-md-1 order-12";
}
$classes .= " ".$builder_class;
?>
<div class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
    <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>:&nbsp;</div>
  <?php endif;
if($element['#field_name'] == "field_microsite_rightside" && arg(0) == "node") {
  if(is_numeric(arg(1)) && !arg(2)){
		$node = node_load(arg(1));
		if($node->type == "microsite"){ ?>
	<div class="interior-sidebar">
                <div class="subheader-menu">
                  <div class="row">

                    <div class="sidebar-logo-container">
                      <?php print $node->microsite_logo; ?>
                    </div>
                    
                  </div>
                  <div class="row">

                    <div class="sidebar-menu">
                      <ul class="menu">
											<?php
												if(isset($node->subnav_first) && $node->subnav_first != ""){
													print $node->subnav_first;
												}
											?>
												<?php
													if(isset($node->orgchart) && $node->orgchart != ""){?>
														<li class="menu-item">
														<?php print l('Org Chart', $node->orgchart , array('attributes' => array('class' => array('menu-item-link')), 'html' => TRUE)); ?>								
														</li>	
													<?php
													}
												?>
												<?php
													if(isset($node->subnav) && count($node->subnav)){  
													foreach($node->subnav as $key => $val){
													?>
														<li class="menu-item">
														<?php print l($val->title, 'node/'. $val->nid, array('attributes' => array('class' => array('menu-item-link')), 'html' => TRUE)); ?>														
														</li>	
														<?php
													}
													}
												if (!empty($node->field_ad_hoc_links)) { ?>
														<li class="menu-item has-submenu"><a href="javascript:void(0);" class="menu-item-link"><?php print !empty($node->field_ad_hoc_label) ? $node->field_ad_hoc_label['und'][0]['value'] : 'Ad Hoc'; ?><span class="icon icon-Icon_Down-Carrot-Filled"></span></a>
															<div class="submenu-container">
                            <ul class="submenu">
															<?php
															foreach($node->field_ad_hoc_links['und'] as $value) {		
																$adhoc_url = isset($value['display_url']) ? $value['display_url'] : $value['url'];
																print "<li class='submenu-item'>". l($value['title'], $adhoc_url,array('attributes' => array('class' => array('submenu-item-link')), 'html' => TRUE)). "</li>";
															}
															?>
															</ul>
															</div>
														</li>
													<?php }
														if (!empty($node->our_departments)) { ?>
														<li  class="menu-item has-submenu"><a href="javascript:void(0);" class="menu-item-link"><?php echo $node->our_department_heading; ?> Departments<span class="icon icon-Icon_Down-Carrot-Filled"></span></a>
															<div class="submenu-container">
                            <ul class="submenu">
															<?php
																print $node->our_departments;
															?>
															</ul>
															</div>
														</li>
													<?php }?>
                      </ul>
                    </div>

                  </div>
                </div>
								<div class="field-items"<?php print $content_attributes; ?>>
    <?php foreach ($items as $delta => $item): ?>
      <div class="field-item <?php print $delta % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>><div <?php print $attributes; ?>><?php print render($item); ?></div></div>
    <?php endforeach; ?>
  </div>
	<?php
	$excluded_nids = explode(",",variable_get('feedback_nid_exclude',''));
	if($element['#field_name'] == "field_microsite_rightside" && arg(0) == "node") {
		if(!(arg(1) && is_numeric(arg(1)) && !arg(2) && in_array(arg(1),$excluded_nids))){
			$block = module_invoke('commportal_custom', 'block_view', 'feedback_right');
			print render($block['content']);
		}
	}
	?>
</div>
		<?php		
		}
	}
 }else{?>
  <div class="field-items"<?php print $content_attributes; ?>>
    <?php foreach ($items as $delta => $item): ?>
      <div class="field-item <?php print $delta % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>><div <?php print $attributes; ?>><?php print render($item); ?></div></div>
    <?php endforeach; ?>
  </div>
 <?php } ?>
</div>