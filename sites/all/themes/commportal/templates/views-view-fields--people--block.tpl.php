<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */
 $profile_pic = $fields['guid']->raw;
 $default_profile_pic = file_create_url("public://filegPQEFT");
 $phone = !empty($fields['phone_number']->raw) ? $fields['phone_number']->raw : 'n/a';
 $mailStop = !empty($fields['mail_stop']->raw) ? $fields['mail_stop']->raw : 'n/a';
 $email_id = $fields['unixid']->raw . '@gene.com';
?>
<li class="">
	<div class="people-list__photo-wrapper">
		<img class="" src="<?php echo $profile_pic; ?>" alt="" title="<?php echo $fields['preferred_fullname']->raw;?>" onerror="this.onerror=null;this.src='<?php echo $default_profile_pic;?>' ;"/>
	</div>
	<div class="people-list__details-primary">
		<span class="people-list__name"><?php echo $fields['preferred_fullname']->raw;?> (<?php echo $fields['unixid']->raw;?>)</span>
		<span><a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=<?php echo $email_id;?>" target="_blank"><?php echo $email_id;?></a></span>
		<span>p. <?php echo $phone;?></span>
		<span>Mail Stop: <?php echo $mailStop;?></span>
		<span><?php echo $fields['work_location']->raw;?></span>
	</div>
</li>