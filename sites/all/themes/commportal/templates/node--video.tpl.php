<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> role="article">

  <?php print render($title_prefix); ?>
  <?php if (!$page){ ?>
		<?php if($title != ''){ ?>
			<?php print $title_attributes; ?>
				<!--<a href="<?php // print $node_url; ?>">--><h3 class= "hd-title"><?php print $title; ?></h3><!--</a>-->
			
		<?php } ?>
		<?php if($title == '')	{ ?>
			<?php  print $title_attributes; ?>
				<h3 class="no-title"></h3>
			
		<?php }?>
		
	<?php } ?>
  <?php print render($title_suffix); ?>
	<?php if($page){ print "<h3 class='hd-title test'>".$title."</h3>"; } ?>

  <?php if ($display_submitted): ?>
    <div class="meta submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
	
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
			
      //If video uploaded into system
			if(isset($node->field_video_file['und'][0]['uri'])) {
				if($node->field_video_file[$node->language][0]['type'] == "video") {
					if($node->field_video_file[$node->language][0]['filemime'] == "video/quicktime") {
					  $videourl = file_create_url($node->field_video_file['und'][0]['uri']);
					  //echo "<embed src=\"$videourl\" type='video/quicktime' pluginspage='http://www.apple.com/quicktime/download/' />";
					  echo "<video width='100%' controls='controls' controlsList='nodownload' preload='auto'><source src='".file_create_url($node->field_video_file['und'][0]['uri'])."' type='".$node->field_video_file['und'][0]['filemime']."'></video>";
					} else {
					  echo "<video width='100%' controls='controls' controlsList='nodownload' preload='auto'><source src='".file_create_url($node->field_video_file['und'][0]['uri'])."' type='".$node->field_video_file['und'][0]['filemime']."'></video>";
					}
				} // Audio file
				else if($node->field_video_file[$node->language][0]['type'] == "audio") {
					echo "<audio controls=''><source src='".file_create_url($node->field_video_file['und'][0]['uri'])."' type='".$node->field_video_file['und'][0]['filemime']."'></audio>";
				}
			} //Youtube Embed Video
			else if(@$node->field_video_type[$node->language][0]['value'] == "youtube") {
				$youtube_id = builder_youtube_video_id($node->field_video_url[$node->language][0]['value']);
				echo '<div class="videoWrapper"><iframe src="//www.youtube.com/embed/' . $youtube_id . '?rel=0&hd=1&autoplay=0" frameborder="0" allowfullscreen></iframe></div>';
			} //Vimeo Embed Video
			else if(@$node->field_video_type[$node->language][0]['value'] == "vimeo") {
				$vimeo_id = builder_vimeo_video_id($node->field_video_url[$node->language][0]['value']);
				echo '<div class="videoWrapper"><iframe src="//player.vimeo.com/video/' . $vimeo_id . '?autoplay=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
			}
    ?>
  </div>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</article>

