<?php
global $base_url, $user, $theme_path; 
$userload = user_load($user->uid);
$img_path = image_style_url('thumbnail', isset($userload->picture->uri) ? $userload->picture->uri : 'public://pictures/no-profile-pic.jpg');
?>
<?php if(isset($_GET['showonlycontent']) && $_GET['showonlycontent'] == 'yes'): ?>
<?php
print render($page['content']);
?>
<?php else:
?>
<div class="page interior-page <?php if(isset($attribute_class_page)){ echo $attribute_class_page; }?>">
	<!-- Begin Header -->
    <?php 
		if(!isset($is_usma)){
				$is_usma = 0;
			}
		echo theme('header', array('page' => $page,'first_name' => $first_name, 'groups' => $groups,'front_page' => $front_page,'is_usma' => $is_usma)); ?>
    <!-- End Header -->
		<!-- Begin Main Content -->
    <div class="main-container">
			<!-- Begin subheader section -->
			<?php print $breadcrumb; ?>
      <!-- End subheader -->
			<div class="container">
				<?php if ($page['highlighted']): ?>
				<div id="highlighted"><div class="section clearfix">
				<?php print render($page['highlighted']); ?>
				</div></div> <!-- /.section, /#highlighted -->
				<?php endif; ?>

				<?php if ($messages): ?>
				<div id="messages"><div class="section clearfix">
				<?php print $messages; ?>
				</div></div> <!-- /.section, /#messages -->
				<?php endif; ?>
				<?php if ($tabs): ?>
				<div class="tabs">
					<?php print render($tabs); ?>
				</div>
				<?php endif; ?>
					<?php if(!isset($show_help) || ($show_help == TRUE)):?>
				<?php print render($page['help']); ?>
					<?php endif; ?>
				<?php if ($action_links): ?>
				<ul class="action-links">
					<?php print render($action_links); ?>
				</ul>
				<?php endif; ?>
			</div>
			<section class="mobile-subheader-menu">
        <div class="container">
          <div class="row">

            <div class="sidebar-logo-container">
              <?php print $node->microsite_logo; ?>
            </div>

          </div>
          <div class="row">

            <div class="sidebar-menu">
              <ul class="menu">
                <?php
									if(isset($node->subnav_first) && $node->subnav_first != ""){
										print $node->subnav_first;
									}
								?>
								<?php
									if(isset($node->orgchart) && $node->orgchart != ""){?>
										<li class="menu-item">
										<?php print l('Org Chart', $node->orgchart , array('attributes' => array('class' => array('menu-item-link')), 'html' => TRUE)); ?>								
										</li>	
									<?php
									}
								?>
								<?php
									if(isset($node->subnav) && count($node->subnav)){  
									foreach($node->subnav as $key => $val){
									?>
										<li class="menu-item">
										<?php print l($val->title, 'node/'. $val->nid, array('attributes' => array('class' => array('menu-item-link')), 'html' => TRUE)); ?>														
										</li>	
										<?php
									}
									}
								if (!empty($node->field_ad_hoc_links)) { ?>
										<li class="menu-item has-submenu"><a href="javascript:void(0);" class="menu-item-link"><?php print !empty($node->field_ad_hoc_label) ? $node->field_ad_hoc_label['und'][0]['value'] : 'Ad Hoc'; ?><span class="icon icon-Icon_Down-Carrot-Filled"></span></a>
											<div class="submenu-container">
										<ul class="submenu">
											<?php
											foreach($node->field_ad_hoc_links['und'] as $value) {		
												$adhoc_url = isset($value['display_url']) ? $value['display_url'] : $value['url'];
												print "<li class='submenu-item'>". l($value['title'], $adhoc_url,array('attributes' => array('class' => array('submenu-item-link')), 'html' => TRUE)). "</li>";
											}
											?>
											</ul>
											</div>
										</li>
									<?php }
										if (!empty($node->our_departments)) { ?>
										<li  class="menu-item has-submenu"><a href="javascript:void(0);" class="menu-item-link"><?php echo $node->our_department_heading; ?> Departments<span class="icon icon-Icon_Down-Carrot-Filled"></span></a>
											<div class="submenu-container">
										<ul class="submenu">
											<?php
												print $node->our_departments;
											?>
											</ul>
											</div>
										</li>
									<?php }?>
              </ul>
            </div>

          </div>
        </div>

      </section>
			<!-- Begin main content -->
      <section class="main-content">
        <div class="container">
          <?php if(!isset($show_content) || ($show_content == TRUE)):?>
					<?php print render($page['content']); ?>
					<?php endif; ?>
					<?php print $feed_icons; ?>
				</div>
      </section>
      <!-- End main content -->
			<!-- Begin Footer -->
      <footer>
        <div class="container">
          <div class="row">
            <div class="col-md-2">
              <div class="footer-logo-block">
                <img class="footer-logo" src="/sites/all/themes/cmgportal/library/img/genentech-logo-white-@2x.png" />
              </div>
            </div>
            <div class="col-md-8">
              <p class="footer-links text-sm-center color-white"><a href="javascript:void(0);">Sitemap</a>   |   <a href="javascript:void(0);">Technical Support</a>   |   <a href="javascript:void(0);">Contact Us</a></p>
            </div>
            <div class="col-md-2">
              <div class="back-to-top color-white">
                <a href="javascript:void(0);" class="top-link"><span class="icon icon-Icon_Up-Arrow"></span>Back To Top</a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              <p class="disclaimer text-sm-center">Please note, this information is confidential and proprietary. The information is only intended as a reference for internal use at Genentech. It must not be shared with external audiences. &copy; Genentech USA, Inc. Last updated: January 31, 2016</p>
            </div>
            <div class="col-md-2"></div>
          </div>
        </div>
      </footer>
      <!-- End Footer -->
		</div>
</div>
<?php endif; ?>