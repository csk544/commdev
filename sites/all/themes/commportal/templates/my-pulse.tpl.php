<?php
global $base_url, $user; 
$display_name = $role_title = $groups = "";
$followed_obj = $followed_tags = $mylinks = $term_ids = array();
if (isset($userload)) {
	$display_name = $userload->field_display_name['und'][0]['value'];
	$first_name = $userload->field_first_name['und'][0]['value'];
	$role_title = isset($userload->field_role_title['und']) ? $userload->field_role_title['und'][0]['value'] : '';	
	if (isset($userload->field_follow_tags['und'])) {
		$term_ids = explode(",", $userload->field_follow_tags['und'][0]['value']);						
		$followed_obj = taxonomy_term_load_multiple($term_ids);
		foreach($followed_obj as $term) {								
			$followed_tags[$term->tid] = $term->name;
		}
		asort($followed_tags);
	}
	$groups = isset($userload->field_author_groups['und']) ? $userload->field_author_groups['und'][0]['value'] : '';	
	if (isset($userload->field_favorite_links['und'])) {
		$mylinks = drupal_explode_tags($userload->field_favorite_links['und'][0]['value']);
	}
	$img_path=image_style_url('thumbnail', isset($userload->picture->uri) ? $userload->picture->uri : 'public://pictures/no-profile-pic.jpg');
}	


?>
<nav id="off-canvas" class="mm-menu mm-vertical mm-offCanvas mm-right"><!-- //Logged OUT Content -->

			<?php
			if (!$user->uid && !(arg(0) == 'user' && !is_numeric(arg(1)))) {
				echo '<div class="off-canvas__content off-canvas__logged-out mm-panel mm-opened mm-current" id="mm-0"><div class="has-padding"><h2 class="hd-title hd-reversed">Log In</h2>';
				$elements = drupal_get_form("user_login"); 
				$form = drupal_render($elements);
				echo $form.'</div></div><!-- // End Logged Out Content --><!-- //Logged IN Content -->';
			} else {
			?>
  <div class="off-canvas__content off-canvas__logged-in mm-panel mm-opened" id="mm-0">
 <div class="off-canvas__welcome has-padding row-col">
        <div class="off-canvas__user-photo col-30">
          <div class="off-canvas__user-photo-wrapper"><img alt="<?php print $display_name; ?>" src="<?php print $img_path; ?>" /></div>
        </div>
        <div class="off-canvas__greeting col-70">
          <h3 class="hd-reversed">Welcome, <span><?php print $first_name; ?></span>!</h3>
          <p class="off-canvas__title"><?php print ($role_title != '' ? $role_title : 'Hey, you can show your functional title here. <br /><a href="https://sims.gene.com" target="_blank">Click to change your profile in SIMS.</a>'); ?></p>
          <ul class="off-canvas__admin-nav clearfix mm-list">
            <li class="off-canvas__logout pull-right"><a href="<?php print $base_url; ?>/user/logout"><span class="icon icon-logout icon--left"></span>Log Out</a></li>
            <li class="off-canvas__settings pull-left"><a class="open-modal-link" href="#settings-modal"><span class="icon icon-settings icon--left"></span>Settings</a></li>
          </ul>
        </div>
        <div class="clearfix"></div>
        
        
         <!-- //Settings Modal/Popup -->
         <div id="settings-modal" class="white-popup mfp-hide">
					 <div class="settings-modal">
            <h2 class="hd-title no-border-top">Settings</h2>     
            <div class="settings__user">
                    <div class="settings__user-photo-wrapper"><a target="_blank" href="https://plus.google.com/me"><span>Edit in Google+</span><img alt="<?php print $display_name; ?>" src="<?php print $img_path; ?>" class=""></a></div>

                    <div class="settings__user-details">
                      <h3 class="h4 settings__user-name"><?php print $display_name; ?></h3>
                     <p><?php print $role_title; ?></p>
										 <p><a target="_blank" href="<?php print $base_url; ?>/update-googlepicture">Sync your picture (Google+)</a> / <a target="_blank" href="https://sims.gene.com/">Update your title (SIMS)</a></p>
											<!--<a href="#" id="tour-restart" class="btn btn-default btn-wide">Reset Guided Tour</a>-->
                    </div>
                 <div class="clearfix"></div>
             </div>
		<!-- //Profile Settings - My Links -->	 
				<h4 class="border-bottom-default padding-bottom10">Manage My Links</h4>
			<div id="profile-my-links" class="row">		
				<?php print display_mylinks($mylinks); ?>				
			</div>
            <h4 class="border-bottom-default padding-bottom10">Manage My Topics Feed</h4>
						<p class="help-block em small">Items including your selected categories and tags will appear in "My Topics Feed."</p>
            <div class="row">
                <div class="col-sm-4 category_list">
                    <p class="bold">Select News Categories</p>
					<?php foreach($page['category_list'] as $cterm_tid => $cterm_name) { ?>
					<div class="checkbox">
                        <label><input type="checkbox" value="<?php print $cterm_tid; ?>" <?php print array_key_exists($cterm_tid, $followed_tags) ? 'checked' : ''; ?>><?php print $cterm_name; ?><span></span></label>
                    </div> 
					<?php }	?>	                                                             
                </div>
                <div class="col-sm-8 cp-tour-tags">
                    <!--<p class="bold">Select Tags</p>-->
										<div class="row">
											<div class="col-sm-6">                    
											<!--<p class="bold">Search Tags</p>
											<div class="input-group-autocomplete">
                      <div class="text-input">
                        <input type="text" placeholder="Start Typing Term..." id="autocomplete_tags" class="form-control input-btn-right em-holder">
                      </div>
                      <div class="submit-input">
                        <button type="submit" class="btn btn-primary">Add</button>
                      </div>
                    </div>-->
										<?php 
											$search_tags_form = drupal_get_form('search_tags_form');
											print drupal_render($search_tags_form); 
											print getTagsForm(); 
										?>
                    </div></div>
											<div class="col-sm-6">
												<p class="bold">Your Selected Tags</p>
												<div class="followed-tags malihu panel panel-default tag-panel margin-top10">
													<div class="tagged">
														<?php												
														foreach($followed_tags as $key => $value) {	
															$term = taxonomy_term_load($key);
															if ($term->vocabulary_machine_name != 'categories') {
														?>
														<i class="badge badge-delete" term="<?php print $key?>"><?php print $value?></i>
														<?php } } ?>	                            							
													</div>
												</div>
											</div>
										</div>
                </div> 
            </div>       
            <div class="">    
							<button class="btn btn-primary btn-wide profile_save pull-right" type="button">Save</button>
							<!--<button class="btn btn-default btn-wide" type="button">Cancel</button>-->
            </div>
					</div>
        </div><!-- //End Modal/Popup -->
				
		<!-- //Authoring Mode Button -->		
		<?php 		
		if (stristr($groups, 'drupal_author') || stristr($groups, 'drupal_superuser') || stristr($groups, 'drupal_business_admin') || stristr($groups, 'drupal_it_admin') || stristr($groups, 'drupal_career_learning_admin')) {
			$redirect = current_path();
			if (stristr($redirect, 'node') && stristr($redirect, '/edit')) {
				$redirect = str_replace("/edit", "", $redirect);
			}
		?>
		<a role="button" href="/author_mode?dest=<?php print $redirect;?>" class="btn btn-success btn-block btn-icon margin-m0 hidden-mobile"><span class="icon icon-tools"></span><?php print (count($user->roles) > 1) ? " Deactivate" : " Activate";?> Authoring Mode</a>
		<?php } ?>
      </div>
			
				<div class="off-canvas__myPortal has-padding clearfix margin-xx">
				
				
				<!-- To display Pending announcement in the My portal drawer -->
				<?php 
					if((isset($user->field_author_groups) && !empty($user->field_author_groups['und'][0]['value'])) || $user->uid == 1) {				
						$count = count(views_get_view_result('pending_announcements', 'page'));
				 ?>
				 <h4 class="hd-reversed" id="draft-announcements"> <a href=<?php echo (count($user->roles) > 1) ? '/admin/my-content/draft-announcements' : '/author_mode?dest=/admin/my-content/draft-announcements'; ?>> Draft Announcements</a>  <span class="badge badge-success"> <?php echo ($count > 0) ? $count : 0;?></span></h4>			
				<?php } ?>
			
			
					<h2 class="hd-title hd-reversed">My Portal</h2>
					<?php print render($page['canvas_accordion']); ?>
				</div>
					<?php print render($page['canvas']); ?>
          </div>
					<?php } ?><!-- //End Logged IN Content -->
					</nav>
  <!-- TOUR TEMPLATES -->

 <!-- <script id="tour-global-template" type="text/template">
    <div class="popover popover-style tour">
    	  	<div class="arrow"></div>
            <div class="popover-header">            
    	  	    <span class="popover-title"></span>
    		      <span class="popover-close" data-role="end">x</span>
            </div>          
    	  	<div class="popover-content"></div>
            <div class="popover-footer">
                <span class="popover-progress"></span>
                <div class="popover-navigation pull-right">
                    <button data-role="prev">Previous</button>
                    <button data-role="next">Next</button>
                </div>
                <div class="clearfix"></div>
            </div>
    	</div>
  </script>

  <script id="tour-start-template" type="text/template">
    <div class="popover tour-start">
    	  	<div class="welcome-center text-center">
					<p>Welcome to</p>
    	  		<div class="tour-heading">Commercial Portal</div>
    		  	<p class="text-center">Explore our revamped site, starting with the new Homepage.</p>
    		  	<div class="popover-navigation">
    			    <button class="btn" data-role="next">Start Tour!</button>
    			    <button class="btn" data-role="end">Skip Tour</button>
    		  	</div>
    		</div>
    	</div>
  </script>

  <script id="tour-end-template" type="text/template">
    <div class="popover tour-end">
				<div class="welcome-center text-center">
    	  		<div class="tour-heading">So far, so good!</div>
    		  	<p class="text-center">More tips will appear as you click around the new Commercial Portal. If you'd like to take the full tour again, click "Reset Guided Tour" in My Portal Settings.</p>

    		  	<div class="popover-navigation">
    			    <button class="btn" data-role="end">End Tour</button>
    		  	</div>
    		</div>
    	</div>
  </script> -->


  <!-- END TOUR TEMPLATES -->