<?php

/**
 * @file
 * Bartik's theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
 
  $leadership_units_entities = $ceo_details = array();
	$ceo_details['image_url'] = isset($node->field_headshot['und'][0]['uri']) ? file_create_url($node->field_headshot['und'][0]['uri']) : '';
	$ceo_details['unix_id'] = isset($node->field_ceo_unix_id['und']) ? $node->field_ceo_unix_id['und'][0]['value']  : '';
	$ceo_details['view_bio'] = isset($node->field_view_bio_link['und']) ? $node->field_view_bio_link['und'][0]['url']  : '';
	$ceo_details['related_url'] = isset($node->field_related_link['und']) ? $node->field_related_link['und'][0]['url']  : '';
	$ceo_details['related_title'] = isset($node->field_related_link['und']) ? $node->field_related_link['und'][0]['title']  : '';
	$ceo_details['admin'] = isset($node->field_clc_admin['und']) ? $node->field_clc_admin['und'][0]['value']  : '';
	$ceo_details['role_title'] = isset($node->field_role_title['und']) ? $node->field_role_title['und'][0]['value']  : '';
  $field_col_ids = array_column($node->field_leadership_team_info['und'], 'value');
  $leadership_units_entities[] = entity_load('field_collection_item', $field_col_ids); 
  $leadership_units_entities[0]['unit'] = 'Commercial Units'; 
	if(isset($node->field_commercial_partners_info['und'])){
		$partners_field_col_ids = array_column($node->field_commercial_partners_info['und'], 'value');
		$leadership_units_entities[] = entity_load('field_collection_item', $partners_field_col_ids);
		$leadership_units_entities[1]['unit'] = 'Commercial Partners';
	}
  $unix_id_details = array($ceo_details['unix_id'],$ceo_details['admin']);
  $ceo_unixid_details = fetch_details_by_unixids($unix_id_details);
  $ceo_preferred_name = isset($ceo_unixid_details[$ceo_details['unix_id']]->preferred_fullname) ? $ceo_unixid_details[$ceo_details['unix_id']]->preferred_fullname: " ";
  ?>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> role="article">
  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
		<?php if($title != ''): ?>
			<h2<?php print $title_attributes; ?>>
				<a href="<?php print $node_url; ?>"><?php print "US Leadership Team"; ?></a>
			</h2>
		<?php endif; ?>
	<?php endif; ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="meta submitted">
      <?php //print $user_picture; ?>
      <?php //print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content);
    ?>
<section class="entry">
  <ul class="staff">
  <li class="row no-margin">
  <ul class="staff__subgroup row no-padding">	
	<?php foreach($leadership_units_entities as $k => $v){ ?>
		<li class="full">
		<?php if($v['unit'] == 'Commercial Partners'){ ?>
			<h2 class="hd-title no-border-top"><?php echo variable_get('leadership_commercial_partner','Commercial Partners'); ?></h2>
			<?php
		} ?>
		<ul class="subgroup__member-halfs">
		<!--STAFF member-half -->
		<?php if($v['unit'] == 'Commercial Units'){ ?>
			<li class="member-half">
			<div class="member__block">
			<div class="member__profile-image">
			<img src="<?php echo $ceo_details['image_url'];?>" alt="<?php echo $ceo_preferred_name;?>" title="<?php echo $ceo_preferred_name;?>">
			</div><!--/col-->
			<div class="member__profile-info">
			<h4><?php echo $ceo_preferred_name; ?></h4>
			<span><?php echo ($ceo_details['role_title'] != '') ? $ceo_details['role_title'] : $ceo_unixid_details[$ceo_details['unix_id']]->job_title; ?></span><br>
			<div class="member__bio-links">
			<div class="member__more-info">
			<span class="icon-info-circled hasTooltip"></span>
			<div class="tooltip-content">
			<ul>                                
			<li><strong>UnixID:</strong> <?php echo $ceo_details['unix_id'];?></li>
			<li><a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=<?php echo $ceo_unixid_details[$ceo_details['unix_id']]->email;?>"  title="<?php echo t("Mail ". $ceo_preferred_name);?>"><?php echo$ceo_unixid_details[$ceo_details['unix_id']]->email;?></a>
			</li>
			<li><strong>p:</strong> <?php echo $ceo_unixid_details[$ceo_details['unix_id']]->phone_number;?></li>
			<li><strong>m:</strong> <?php echo $ceo_unixid_details[$ceo_details['unix_id']]->mobile_number;?></li>
			<li><strong>Mail Stop:</strong> <?php echo $ceo_unixid_details[$ceo_details['unix_id']]->mail_stop;?></li>
			<li><strong>Building:</strong> <?php echo $ceo_unixid_details[$ceo_details['unix_id']]->building;?>, Room <?php echo $ceo_unixid_details[$ceo_details['unix_id']]->room_number;?></li>
			</ul>                                   
			</div>
			</div>
			<a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=<?php echo $ceo_unixid_details[$ceo_details['unix_id']]->email;?>"  title="<?php echo t("Mail ". $ceo_preferred_name);?>"><span class="icon-mail" aria-hidden="true"></span></a><a href="<?php echo $ceo_details['view_bio'];?>"><?php echo empty($ceo_details['view_bio']) ? "" : t("View Bio") ;?></a><a href="<?php echo $ceo_details['related_url'];?>"><?php echo $ceo_details['related_title'];?></a>
			</div>
			
			<?php if (!empty($ceo_unixid_details[$ceo_details['admin']]->unixid)) { ?>
				<div class="member__bio-links">
				<div class="member_admin_info">
				<p class="small">
				<i>
				Admin: <?php echo $ceo_unixid_details[$ceo_details['admin']]->preferred_fullname . "&nbsp;";?>
				</i>
				</p>
				</div>
				<div class="member__more-info">
				<span class="icon-info-circled hasTooltip"></span>
				<div class="tooltip-content">
				<ul>
				<li><strong>UnixID:</strong> <?php echo $ceo_unixid_details[$ceo_details['admin']]->unixid;?></li>
				<li><a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=<?php echo $ceo_unixid_details[$ceo_details['admin']]->email;?>"  title="<?php echo t("Mail ". $ceo_unixid_details[$ceo_details['admin']]->preferred_fullname);?>"><?php echo $ceo_unixid_details[$ceo_details['admin']]->email;?></a>
				</li>
				<li><strong>p:</strong> <?php echo $ceo_unixid_details[$ceo_details['admin']]->phone_number;?></li>
				<li><strong>m:</strong> <?php echo $ceo_unixid_details[$ceo_details['admin']]->mobile_number;?></li>
				<li><strong>Mail Stop:</strong> <?php echo $ceo_unixid_details[$ceo_details['admin']]->mail_stop;?></li>
				<li><strong>Building:</strong> <?php echo $ceo_unixid_details[$ceo_details['admin']]->building;?>, Room <?php echo $ceo_unixid_details[$ceo_details['admin']]->room_number;?></li>
				</ul>
				</div>
				</div>
				</div>
				<?php 
			} ?>
			</div><!--/col-->
			</div>                   
			</li>
			<?php 
		} 
		foreach($v as $key => $val){
			if(is_numeric($key)){
				$unixid_details_leader = array();
				$leadership_image_url = isset($val->field_leader_headshot['und'][0]['uri']) ? file_create_url($val->field_leader_headshot['und'][0]['uri']) : '';
				$leader_unixid = isset($val->field_unix_id['und']) ? $val->field_unix_id['und'][0]['value']  : '';
				if($leader_unixid != ''){
					$unixid_details_leader[] = $leader_unixid;
				}
				$leader_admin = isset($val->field_leader_admin['und']) ? $val->field_leader_admin['und'][0]['value']  : '';
				if($leader_admin != ''){
					$unixid_details_leader[] = $leader_admin;
				}
				$leadership_unixid_details = fetch_details_by_unixids($unixid_details_leader);
				$leadership_role_title = (isset($val->field_functional_title['und']) && $val->field_functional_title['und'][0]['value'] != '') ? $val->field_functional_title['und'][0]['value'] : $leadership_unixid_details[$leader_unixid]->job_title;
				$leader_name = isset($leadership_unixid_details[$leader_unixid]->preferred_fullname) ? $leadership_unixid_details[$leader_unixid]->preferred_fullname: " ";
				$bio_link = isset($val->field_bio_link['und']) ? $val->field_bio_link['und'][0]['url'] : '';
				$related_link = isset($val->field_leadership_related_link['und']) ? $val->field_leadership_related_link['und'][0]['url'] : '';
				$related_link_title = isset($val->field_leadership_related_link['und']) ? $val->field_leadership_related_link['und'][0]['title'] : '';
				?>
				<li class="member-half">
				<div class="member__block">
				<div class="member__profile-image">
				<img src="<?php echo $leadership_image_url;?>" alt="<?php echo $leader_name;?>" title="<?php echo $leader_name;?>">
				</div><!--/col-->
				<div class="member__profile-info">
				<h4><?php echo $leader_name; ?></h4>
				<span><?php echo $leadership_role_title; ?></span><br>
				<div class="member__bio-links">
				<div class="member__more-info">
				<span class="icon-info-circled hasTooltip"></span>
				<div class="tooltip-content">
				<ul>                                
				<li><strong>UnixID:</strong> <?php echo $leadership_unixid_details[$leader_unixid]->unixid;?></li>
				<li><a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=<?php echo $leadership_unixid_details[$leader_unixid]->email;?>"  title="<?php echo t("Mail ". $leader_name);?>"><?php echo $leadership_unixid_details[$leader_unixid]->email;?></a>
				</li>
				<li><strong>p:</strong> <?php echo $leadership_unixid_details[$leader_unixid]->phone_number;?></li>
				<li><strong>m:</strong> <?php echo $leadership_unixid_details[$leader_unixid]->mobile_number;?></li>
				<li><strong>Mail Stop:</strong> <?php echo $leadership_unixid_details[$leader_unixid]->mail_stop;?></li>
				<li><strong>Building:</strong> <?php echo $leadership_unixid_details[$leader_unixid]->building;?>, Room <?php echo $leadership_unixid_details[$leader_unixid]->room_number;?></li>
				</ul>                                   
				</div>
				</div>
				<a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=<?php echo $leadership_unixid_details[$leader_unixid]->email;?>"  title="<?php echo t("Mail ". $leader_name);?>"><span class="icon-mail" aria-hidden="true"></span></a><a href="<?php echo $bio_link;?>"><?php echo empty($bio_link) ? "" : t("View Bio") ;?></a><a href="<?php echo $related_link;?>"><?php echo $related_link_title;?></a>
				<div class="member__bio-links">
				</div>
				<?php 
				if (!empty($leadership_unixid_details[$leader_admin]->unixid)) { ?>
					
					<div class="member_admin_info">
					<p class="small">
					<i>
					Admin: <?php echo $leadership_unixid_details[$leader_admin]->preferred_fullname . "&nbsp;";?>
					</i>
					</p>
					</div>
					<div class="member__more-info">
					<span class="icon-info-circled hasTooltip"></span>
					<div class="tooltip-content">
					<ul>
					<li><strong>UnixID:</strong> <?php echo $leadership_unixid_details[$leader_admin]->unixid;?></li>
					<li><a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=<?php echo $leadership_unixid_details[$leader_admin]->email;?>"  title="<?php echo t("Mail ". $leadership_unixid_details[$leader_admin]->preferred_fullname);?>"><?php echo $leadership_unixid_details[$leader_admin]->email;?></a>
					</li>
					<li><strong>p:</strong> <?php echo $leadership_unixid_details[$leader_admin]->phone_number;?></li>
					<li><strong>m:</strong> <?php echo $leadership_unixid_details[$leader_admin]->mobile_number;?></li>
					<li><strong>Mail Stop:</strong> <?php echo $leadership_unixid_details[$leader_admin]->mail_stop;?></li>
					<li><strong>Building:</strong> <?php echo $leadership_unixid_details[$leader_admin]->building;?>, Room <?php echo $leadership_unixid_details[$leader_admin]->room_number;?></li>
					</ul>
					</div>
					</div>
					
					<?php 
				} else{ ?>
					<div class="member_admin_info">
					<p class="small">
					<i>&nbsp;</i>
					</p>
					</div>
					<?php
				}?>
				</div>
				</div><!--/col-->
				</div>                   
				</li>
				<?php 
			} 
		} ?>
		<!--/STAFF member-half -->
		</ul>
		</li>
		<?php 
	} 
	?>
</ul>
</li>
</ul>
</section>
  </div>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</article>
