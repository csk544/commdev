<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $GLOBALS['language']->language; ?>" xml:lang="<?php print $GLOBALS['language']->language; ?>" class="imce">

<head>
  <title><?php print t('File Browser'); ?></title>
  <meta name="robots" content="noindex,nofollow" />
  <?php if (isset($_GET['app'])): drupal_add_js(drupal_get_path('module', 'imce') .'/js/imce_set_app.js'); endif;?>
  <?php print drupal_get_html_head(); ?>
  <?php print drupal_get_css(); ?>
  <?php print drupal_get_js('header'); ?>
  <style media="all" type="text/css">/*Quick-override*/</style>
</head>

<body class="imce">
<?php 
//Custom Function; to create directories based on department channels in node edit page
if(empty($_GET['dir'])) {
  $url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; 
  $parent = $_SERVER["HTTP_REFERER"];
  $channel = 'assets';
  if (!empty($parent)) {
    $splitparenturl = explode("/", $parent); 
  } 
  if ($splitparenturl[4] == "add") {
    $channel = 'assets';
  } 
  else if ($splitparenturl[4] && is_numeric($splitparenturl[4]))  { 
    $channel_details = db_query("SELECT td.name, td.tid FROM {taxonomy_index} as ti, {taxonomy_term_data} as td where td.tid=ti.tid and ti.nid=:nid and td.vid=2", array(':nid' => $splitparenturl[4]))->fetchObject();
	if(!empty($channel_details)) {
      if ($channel_details->tid) {
	    $parent_channel = db_query("SELECT th.parent,td.name FROM taxonomy_term_hierarchy as th, taxonomy_term_data as td WHERE td.tid=th.parent AND th.tid=:tid and td.vid=2", array(':tid' => $channel_details->tid))->fetchObject();
	  } 
      $parentchannel = "";
      if (!empty($parent_channel) && $parent_channel->name != "Departments") {
	    $parentchannel = $parent_channel->name . "/";
	  }  
      $channel = 'assets/' . $parentchannel . $channel_details->name;
	  $mydir = "public://$channel"; 
      file_prepare_directory($mydir, FILE_CREATE_DIRECTORY);
	  $channel = urlencode($channel);
	}  
  }
  $url = $url . "&dir=$channel"; 
  
  header("Location: $url");
  exit();
}
?>

<div id="imce-messages"><?php print theme('status_messages'); ?></div>
<?php print $content; ?>
<?php print drupal_get_js('footer'); ?>
</body>

</html>
