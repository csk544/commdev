<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
$markup = '';
$container = array();
if(count($view->result)>0){
	if(count($view->result[0]->field_field_articles_container)>0){
		$container = $view->result[0]->field_field_articles_container;
	} else if(count($view->result[0]->field_field_announcements_container)>0){
		$container = $view->result[0]->field_field_announcements_container;
	}else if(count($view->result[0]->field_field_links_container)>0){
		$container = $view->result[0]->field_field_links_container;
	}
}
if(count($container)){
	$markup = '<h3 class="hd-title">Related Content</h3>';
}
$title_view = array('article', 'announcements');
foreach($container as $key => $result) {
	$title = $result['rendered']['#markup'];
	$entity = $result['raw']['entity'];
	$view_mode = (in_array($entity->type, $title_view)) ? 'title' : 'full';	
	if($entity->type == 'downloads'){
		$markup .= '<p class="top_border"></p>';
	}
	if($view_mode == 'title'){
		$node_view = '<p class="top_border sidebar_title"><span><a href="' . url('node/' . $entity->nid) . '">' . $entity->title . '</a></span><span class="feed__date">' . date('m.d.Y', $entity->created) . '</span></p>';
	}else{
		$node_view = node_view($entity, $view_mode);	
	}  
	$markup .= render($node_view);
}
print $markup;
?>