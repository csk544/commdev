<?php
global $base_url;
?>
<section class="">        
	<h1 class="no-border-top page-title"><?php echo t("Site Map");?></h1>

			<!-- About Commercial -->		
			<h2 class="sitemap__section-title hd-title no-border-top"><?php echo t("About Commercial");?></h2> 
			<div class="sitemap-row">
				<ol>
				<?php $j=0; foreach($about_commercial as $links) { if ($j != 0 && $j%6 == 0) { print "</ol></div><div class='sitemap-row'><ol>"; } $j++;  ?>
						<li class="col-sm-4 col-md-2"><h3 class="h5"><?php print l($links->title, 'node/' . $links->nid);?></h3></li>
				<?php } ?>	
				<li class="col-sm-4 col-md-2"><h3 class="h5"><?php print l('Commercial Leadership', 'leadership');?></h3></li>
				</ol>   
			</div>	


			<!-- Archives -->			
			<h2 class="sitemap__section-title hd-title no-border-top"><?php echo t("Archives");?></h2>
			<div class="sitemap-row">
					<ol>
							<li class="col-sm-4 col-md-2"><h3 class="h5"><?php print l('News Archive', 'commercial-news');?></h3></li>
							<li class="col-sm-4 col-md-2"><h3 class="h5"><?php print l('Kudos Archive', 'kudos-list');?></h3></li>					
							<li class="col-sm-4 col-md-2"><h3 class="h5"><?php print l('Multimedia Archive', 'media-archive');?></h3></li>
					</ol> 
			</div>
			
			<!-- Microsite Departments and Sub Pages -->
			<h2 class="sitemap__section-title hd-title no-border-top microsite-links"><?php echo t("Departments");?></h2>
			<div class="sitemap-row">
					<ol>
					<?php print $dept_links;?>							                              
					</ol>   
			</div>

			<!-- Toolbox Topics and it's pages -->			
			<h2 class="sitemap__section-title hd-title no-border-top toolbox-links"><?php echo t("Tools");?></h2>
			<div class="sitemap-row">
				<ol>
				<?php $j=0; foreach($tb_links as $topic => $links) { if ($j != 0 && $j%6 == 0) { print "</ol></div><div class='sitemap-row'><ol>"; } $j++;  ?>
						<li class="col-sm-4 col-md-2">  
								<h3 class="h5"><?php print $topic; ?></h3>
								<ol><?php	foreach($links as $link) { print "<li>". $link ."</li>"; } ?></ol>           
						</li>
				<?php } ?>															 
				</ol>   
			</div>
			
			<!-- Career & Learning Topics and it's pages -->			
			<h2 class="sitemap__section-title hd-title no-border-top career-links"><?php echo t("Career & Learning");?></h2>
			<div class="sitemap-row">
				<ol>
				<?php $j=0; foreach($cl_links as $topic => $links) { if ($j != 0 && $j%6 == 0) { print "</ol></div><div class='sitemap-row'><ol>"; } $j++;  ?>
						<li class="col-sm-4 col-md-2">  
								<h3 class="h5"><?php print $topic; ?></h3>
								<ol><?php	foreach($links as $link) { print "<li>". $link ."</li>"; } ?></ol>           
						</li>
				<?php } ?>															 
				</ol>   
			</div>	
</section>