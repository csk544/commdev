<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?> role="article">

  <?php print render($title_prefix); ?>
  <?php if (!$page){ ?>
		<?php if($title != ''){ ?>
			<?php print $title_attributes; ?>
				<!--<a href="<?php //print $node_url; ?>">--><h3 class= "hd-title"><?php print $title; ?></h3><!--</a>-->
			
		<?php } ?>
		<?php if($title == '')	{ ?>
			<?php  print $title_attributes; ?>
				<h3 class="no-title"></h3>
			
		<?php }?>
		
	<?php } ?>
  <?php print render($title_suffix); ?>

  <?php if ($display_submitted): ?>
    <div class="meta submitted">
      <?php print $user_picture; ?>
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content clearfix"<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
			//print render($content);
      hide($content['comments']);
      hide($content['links']);
			$lang = $node->language;
			$output = '';
			$text = $node->field_text[$lang][0]['value']; 
			$output .= '<div class="module">';
			if($page){
				$output .=	'<h3 class="hd-title">'.$node->title.'</h3>';
			}
			$output .= '<ul class="media-list"><li class="media">';
			if(isset($node->field_preview_image[$lang][0]['uri'])) {
				$output .= '<div class="media-left media-top"><img class="media-object" src="'.file_create_url($node->field_preview_image[$lang][0]['uri']).'" alt="'.$node->field_preview_image[$lang][0]['filename'].'"></div>';
			}
			$output .= '<div class="media-body entry"><p>'.$text.'</p>';
			if(!empty($node->field_view_all[$lang][0]['url'])) {
				$output .= '<div class="text-right"><a href="'.$node->field_view_all[$lang][0]['url'].'" class="view-more">View More <span class="icon-triangle-right"></span></a></div>';
			}
			$output .= '</div></li></ul></div>';
    ?>
  </div>
	<?php print $output; ?>

  <?php
    // Remove the "Add new comment" link on the teaser page or if the comment
    // form is being displayed on the same page.
    if ($teaser || !empty($content['comments']['comment_form'])) {
      unset($content['links']['comment']['#links']['comment-add']);
    }
    // Only display the wrapper div if there are links.
    $links = render($content['links']);
    if ($links):
  ?>
    <div class="link-wrapper">
      <?php print $links; ?>
    </div>
  <?php endif; ?>

  <?php print render($content['comments']); ?>

</article>
