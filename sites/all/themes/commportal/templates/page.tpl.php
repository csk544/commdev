<?php
global $base_url, $user, $theme_path;
$userload = user_load($user->uid);
$img_path = image_style_url('thumbnail', isset($userload->picture->uri) ? $userload->picture->uri : 'public://pictures/no-profile-pic.jpg');
$sideColumn = '';
$pageColumn = 'col-md-12';
if($page['sidebar_first'] || $page['sidebar_second']) {
	$pageColumn = 'col-md-9';
	if($page['sidebar_first']) {
		$pageColumn .= ' col-md-push-3';
	}
	$sideColumn = 'col-md-pull-9';
}
if($page['sidebar_first'] && $page['sidebar_second']) {
	$pageColumn = 'col-md-6 col-md-push-3';
	$sideColumn = 'col-md-pull-6';
}
$pages = array('Search Training' => 'career-and-learning', 'Search News' => 'commercial-news','Search Department News' => 'commercial_news', 'Filter Your Search' => 'search-people', 'Seach Announcements' => 'kudos-list', 'Search Department Announcements' => 'kudos_list', 'Search Gallery' => 'media-archive');
?>
<?php if(isset($_GET['showonlycontent']) && $_GET['showonlycontent'] == 'yes'): ?>
<?php
print render($page['content']);
?>
<?php else:
?>
<div class="page interior-page <?php if(isset($attribute_class_page)){ echo $attribute_class_page; }?>">
		
			<?php	
			if(!isset($is_usma)){
				$is_usma = 0;
			}
			echo theme('header', array('page' => $page,'first_name' => $first_name, 'groups' => $groups,'front_page' => $front_page,'is_usma' => $is_usma)); ?>
	<div class="main-container">
    <?php //print $breadcrumb; ?>
		<!-- Begin subheader section -->
    <?php print $breadcrumb; ?>
      <!-- End subheader -->
		<?php if(!isset($show_featured) || ($show_featured == TRUE)):?>
		<?php if ($page['featured']): ?>
		<div id="featured"><div class="section clearfix">
		<?php print render($page['featured']); ?>
		</div></div> <!-- /.section, /#featured -->
		<?php endif; ?>
		<?php endif; ?>
		<section class="main-content">
  <div id="main-wrapper" class="container <?php if(isset($node) && $node->type =='page') { print "microsite page"; } else { print "main";} ?> clearfix"><div id="main" role="main" class="clearfix">
	
		<?php if ($page['highlighted']): ?>
		<div id="highlighted"><div class="section clearfix">
		<?php print render($page['highlighted']); ?>
		</div></div> <!-- /.section, /#highlighted -->
		<?php endif; ?>
	  
    <?php if ($messages): ?>
    <div id="messages"><div class="section clearfix">
      <?php print $messages; ?>
    </div></div> <!-- /.section, /#messages -->
  <?php endif; ?>
		<?php if (!in_array(arg(0), $pages)): ?>
			<?php if(isset($node) && $node->type == 'page' && arg(2) != "edit") { ?>
			 <div class="<?php print $pageColumn; ?> header page col-md-5 col-xs-12">
				<?php
				if(isset($node->field_microsite_logo['und'][0]['uri'])) {
					print '<a class="header__logo" href="" title="'.$node->title.'"><img src="'.file_create_url($node->field_microsite_logo['und'][0]['uri']).'" alt="'.$node->title.'" /></a>';
				} else {
					print render($title_prefix); ?>
					<?php if ($title): ?>
					<h1 <?php print $title_attributes; ?> id="page-title">
					<?php print $title; ?>
					</h1>
					<?php endif; ?>
					<?php print render($title_suffix); 
				} ?>
				<?php
				if(!empty($node->field_ad_hoc_links)) {
				?>
				<div class="nav-wrap text-right">
					<div class="microsite-mobile-nav-toggle">
						<a class="microsite-mobile-nav-toggle__menu btn btn-primary">Initiative Menu <span class="icon icon-menu"></span></a>
					</div>  
					<!-- Secondary Nav -->
					<nav role="navigation" class="secondary-nav">
						<ul class="sm sm-clean collapsed">
							<li><a href="#"><?php print !empty($node->field_ad_hoc_label) ? $node->field_ad_hoc_label['und'][0]['value'] : 'Ad Hoc'; ?></a>
							<ul>
								<?php
								foreach($node->field_ad_hoc_links['und'] as $value) { print "<li>". l($value['title'], $value['url']) . "</li>"; }
								?>
							</ul>
							</li>
						</ul>
					</nav>				
				</div>
			</div>			
			<?php } } else if((arg(0) != 'find-class') && (arg(0) != 'dept_rolebasedprograms')) { ?>
			 <div class="<?php print $pageColumn; ?> header page col-md-5 col-xs-12">
				<?php print render($title_prefix); ?>
				<?php if ($title): ?>
				<h1 <?php print $title_attributes; ?> id="page-title">
				<?php print $title; ?>
				</h1>
				<?php endif; ?>
				<?php print render($title_suffix); ?>
				</div>
			<?php } ?>
			<?php if ($tabs): ?>
			<div class="tabs">
			<?php print render($tabs); ?>
			</div>
			<?php endif; ?>
		<?php endif; ?>
			
	<div class="row">
		
    <div id="content" class="column <?php print $pageColumn; ?>">
		<div class="section">
		
			<?php 
			/*Code for titles to be appeared above the content section for find a class and dept role based progs*/
			if(arg(0)=='find-class' || arg(0)=='dept_rolebasedprograms'){?>
				<?php if ($title){ ?>
				<h1 <?php print $title_attributes; ?> id="page-title">
				<?php print $title; ?>
				</h1>
			<?php }} /*End of Code for titles to be appeared above the content section for find a class and dept role based progs*/?>
		<?php if (in_array(arg(0), $pages)): ?>
			<?php print render($title_prefix); ?>
			<?php if ($title): ?>
			<h1 <?php print $title_attributes; ?> id="page-title">
			<?php print $title; ?>
			</h1><div class="hidden-md hidden-lg search-anchor"><a class="btn btn-block btn-primary" href="#sidebar-first"><span class="icon icon-search"></span> <?php print array_search(arg(0), $pages); ?></a></div>
			<?php endif; ?>
			<?php print render($title_suffix); ?>		
			<?php if ($tabs): ?>
			<div class="tabs">
			<?php print render($tabs); ?>
			</div>
			<?php endif; ?>
		<?php endif; ?>
			<?php if(!isset($show_help) || ($show_help == TRUE)):?>
		  <?php print render($page['help']); ?>
				<?php endif; ?>
		  <?php if ($action_links): ?>
			<ul class="action-links">
			  <?php print render($action_links); ?>
			</ul>
		  <?php endif; ?>
				<?php if(!isset($show_content) || ($show_content == TRUE)):?>
		  <?php print render($page['content']); ?>
				<?php endif; ?>
		  <?php print $feed_icons; ?>
		</div>
	</div> <!-- /.section, /#content -->
	
	  <?php if(!isset($show_sidebar_first) || ($show_sidebar_first == TRUE)):?>
    <?php if ($page['sidebar_first']): ?>
      <div id="sidebar-first" class="column col-md-3 sidebar <?php print $sideColumn; ?>"><div class="section">
        <?php print render($page['sidebar_first']); ?>
      </div></div> <!-- /.section, /#sidebar-first -->
    <?php endif; ?>
    <?php endif; ?>
		
		<?php if(!isset($show_sidebar_second) || ($show_sidebar_second == TRUE)):?>
    <?php
		if ($page['sidebar_second']): ?>
      <div id="sidebar-second" class="column col-md-3 sidebar"><div class="section">
        <?php print render($page['sidebar_second']); ?>
      </div></div> <!-- /.section, /#sidebar-second -->
    <?php endif; ?>
    <?php endif; ?>
	</div>
	
  </div></div> <!-- /#main, /#main-wrapper, /#page -->
	</section>
        
  <footer>
        <div class="container">
          <div class="row">
            <div class="col-md-2">
              <div class="footer-logo-block">
                <img class="footer-logo" src="/sites/all/themes/cmgportal/library/img/genentech-logo-white-@2x.png" />
              </div>
            </div>
            <div class="col-md-8">
              <p class="footer-links text-sm-center color-white"><a href="javascript:void(0);">Sitemap</a>   |   <a href="javascript:void(0);">Technical Support</a>   |   <a href="javascript:void(0);">Contact Us</a></p>
            </div>
            <div class="col-md-2">
              <div class="back-to-top color-white">
                <a href="javascript:void(0);" class="top-link"><span class="icon icon-Icon_Up-Arrow"></span>Back To Top</a>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
              <p class="disclaimer text-sm-center">Please note, this information is confidential and proprietary. The information is only intended as a reference for internal use at Genentech. It must not be shared with external audiences. &copy; Genentech USA, Inc. Last updated: January 31, 2016</p>
            </div>
            <div class="col-md-2"></div>
          </div>
        </div>
      </footer><!-- /.section, /#footer-wrapper -->
</div>
</div><!-- /#page-wrapper -->
<?php endif; ?>