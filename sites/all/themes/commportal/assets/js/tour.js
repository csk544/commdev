var tours = {};

(function ($) {
$(document).ready(function() {
	var base_url = Drupal.settings.commportal_custom.base_url;
	var portal_tour_status = Drupal.settings.commportal_custom.portal_tour_status;
  // disable tours
  var disableTours = false;

  // force tour start
 var forceStart = false;

  // tour names
  var tourNameHome = 'home';
  var tourNameArticle = 'article';
  var tourNameCareer = 'career';
  var tourNamePortal = 'portal';

  // map body classes to tours
  var tourNames = {};
  tourNames[tourNameHome] = 'page-home';
  tourNames[tourNameArticle] = 'node-type-article';
  tourNames[tourNameCareer] = 'node-type-career-learning';

  // tour templates - footer.php
  var tourGlobalTmpl = $('#tour-global-template').html();
  var tourStartTmpl = $('#tour-start-template').html();
  var tourEndTmpl = $('#tour-end-template').html();

  // tour restart button
  var $tourRestart = $('#tour-restart');

  // portal tab
  var $portalTab = $('.off-canvas-open');

  // mobile nav button
  var $mobileNav = $('.mobile-nav-toggle');

  // dynamic dom element selectors
  var popover = '.popover';
  var popoverProgress = '.popover-progress';
  var popoverNavButtons = '.popover-navigation button';
  var mfpWrap = '.mfp-wrap';
  var mfpBackground = '.mfp-bg';
  var settingsModal = '#settings-modal';
  var offCanvasContent = '.off-canvas__content';
  var tourBackdrop = '.tour-backdrop';

  // show outro after portal tour on home page
  var showOutro = false;

	
  // restart tour
  $tourRestart.on('click', function(e) {
    e.preventDefault();
    $.each(tourNames, function(key, val){
      removeTourComplete(key);
    });
    removeTourComplete(tourNamePortal);
    window.location.href = '/';
  });

  // start portal tour
  $portalTab.on('click', function(){
    if((!getTourComplete(tourNamePortal) || forceStart) && !disableTours && !$mobileNav.is(':visible')){
      Tours.portalTour();
    }
  });

  // tours namespace
  var Tours = Tours || {};

  // home tour
  Tours.homeTour = function() {
    // tour selectors
    var $tourHomepage = $('.header__logo > img');
    var $tourKudos = $('.cp-tour-kudos');
    var $tourTrending = $('.cp-tour-trending');
    var $tourFooter = $('.pre-footer');
    var $tourNav = $('.primary-nav > ul');
    var $tourNavItems = $('.primary-nav > ul > li');
    var $tourNavAbout = $tourNavItems.eq(1);
    var $tourNavDept = $tourNavItems.eq(2);
    var $tourNavToolbox = $tourNavItems.eq(3);
    var $tourNavCareer = $tourNavItems.eq(4);
    var $tourNavSearch = $('.sb-search-submit');

    // tour steps
    var steps = [{
      template: tourStartTmpl,
      title: 'Welcome to the new Commercial Portal',
      content: 'Here is a quick tour of the website.',
      orphan: true
    }, {
      // nav - about
      element: $tourNavAbout,
      title: 'About Commercial',
      content: 'Learn more about the Commercial Organization, our leaders, priorities and portfolio.',
      placement: 'bottom',
      onShown: function() {
        updateProgress();
      }
    }, {
      // nav - departments
      element: $tourNavDept,
      title: 'Departments',
      content: 'Find your department’s site or learn more about other departments within Commercial.',
      placement: 'bottom',
      onShown: function() {
        updateProgress();
      }
    }, {
      // nav - tools
      element: $tourNavToolbox,
      title: 'Tools',
      content: 'Find the tools, resources, and policies you need everyday in our Tools Area.',
      placement: 'bottom',
      onShown: function() {
        updateProgress();
      }
    }, {
      // nav - career
      element: $tourNavCareer,
      title: 'Career & Learning',
      content: 'Visit the Career & Learning section for training, development, and performance review resources.',
      placement: 'bottom',
      onShown: function() {
        updateProgress();
      }
    }, {
      // nav - search
      element: $tourNavSearch,
      title: 'Search',
      content: 'Find what you need quickly within Commercial Portal, gWiz and the Corporate Directory.',
      placement: 'bottom',
      onShown: function() {
        updateProgress();
      }
    },{
      // nav - search
      element: $('.front .row .col-md-9 .row.margin-bottom50'),
      title: 'Learn More',
      content: "Scroll down the Homepage for news feeds from gWiz, trending stories in Commercial, and Genentech's latest posts on Google News, Twitter and Instagram.",
      placement: 'top',
      onShown: function() {
        updateProgress();
        hideNavButton(0);
      },
      onNext: function() {
        if(isPortalOpen() && !getTourComplete(tourNamePortal)){
          tour.end();
        }
      }
    }, {
      // end modal
      template: tourEndTmpl,
      orphan: true
    }];

    // create the tour object
    var tour = new Tour({
      name: tourNameHome,
      storage: false,
      backdrop: true,
      keyboard: false,
      backdropPadding: 5,
      template: tourGlobalTmpl,
      steps: steps,
      onEnd: function(tour) {
	if(tour._state.current_step == 0) {
	  setTourComplete('skip');
	} else {
	  setTourComplete(tourNameHome);
	}
      }
    });

    function updateProgress(){
      updateProgressUI(tour, steps.length - 2); // minus 2 for start/end steps
    }

    //start the tour
	if(portal_tour_status.indexOf(tourNameHome) == -1) {
	  tour.init();
	  tour.start();
	}
  }

  // my portal tour
  Tours.portalTour = function() {
    var $offCanvas = $('#off-canvas');
    var $tourMyPortal = $('.off-canvas__myPortal');
    var $tourMyPortalChris = $('.off-canvas__notifications a.action-link');
    var $tourMyPortalSettings = $('.off-canvas__greeting > ul > li.off-canvas__settings');
    var $tourSettingsTags = $('.cp-tour-tags');

    var steps = [{
      element: $offCanvas,
      title: 'Tip: My Portal',
      backdropPadding: 0,
      content: 'Check here regularly for news from your department and tags you\'re following. Keep up-to-date on your scheduled training.',
      placement: 'left',
      onShown: function() {
        updateProgress();
        hideNavButton(0)
      },
      onNext: function() {
        var offset = $tourMyPortalSettings.offset().top + $tourMyPortalSettings.height() / 2;
        if(offset < 0){
          return scrollPortalPanel(offset); // promise
        }
      }
    }, {
      // my portal settings
      element: $tourMyPortalSettings,
      title: 'Tip: Settings',
      backdropPadding: 5,
      content: 'Click here to update your title and synch your Google+ photo. Manage your links, tags and categories here, too.',

      placement: 'left',
      autoscroll: false,
      onShown: function() {
        updateProgress();
      },
      onNext: function() {
        return showSettingsModal(); // promise
      }
    }, {
      // settings modal
      element: $tourSettingsTags,
      title: 'Tip: Manage Tags',
      backdropPadding: 5,
      content: 'Add new tags and bookmarks to your favorite sites and pages.',
      placement: 'left',
      onShown: function(){
        if(!showOutro){
          replaceNavButton(1);
        }
        updateProgress();
      },
      onPrev: function() {
        toggleMagnificOverlay(true);
        $.magnificPopup.close();
        var offset = $tourMyPortalSettings.offset().top + $tourMyPortalSettings.height() / 2;
        var windowHeight = $(window).height();
        if(offset < 0){
          return scrollPortalPanel(offset); // promise
        }
      },
      onNext: function() {
        toggleMagnificOverlay(true);
        $.magnificPopup.close();
      }
    }, {
      // end modal
      template: tourEndTmpl,
      orphan: true
    }];

    // create the tour object
    var tour = new Tour({
      name: tourNamePortal,
      storage: false,
      backdrop: true,
      keyboard: false,
      backdropPadding: 10,
      template: tourGlobalTmpl,
      steps: steps,
      onEnd: function() {
        toggleMagnificOverlay(true);
        $.magnificPopup.close();
        setTourComplete(tourNamePortal);
      }
    });

    function updateProgress(){
      updateProgressUI(tour, steps.length - 1); // minus 1 for end step
    }
		
    //start the tour
	if(portal_tour_status.indexOf(tourNamePortal) == -1) {
	  tour.init();
	  tour.start();
	}
  }

  Tours.articleTour = function() {
    // tour selectors
    var $tourSocial = $('.page-actions').eq(0);
    var $tourTags = $('article > .tagged');
    var $tourFeedback = $('.feedback');

    // tour steps
    var steps = [{
      // rating
      element: $tourSocial,
      title: 'Rate, Comment and Share',
      content: 'How did you like this article? Let everyone know! And share this article with your colleagues via email or Google+.',
      placement: 'top'
    }, {
      // comments
      element: $tourTags,
      title: 'Tagging',
      content: 'Follow tags to receive related stories in your Personal Portal',
      placement: 'top'
    }, {
      // feedback
      element: $tourFeedback,
      title: 'Feedback',
      content: 'We really appreciate your feedback!',
      placement: 'left',
      onShown: function(){
        replaceNavButton(1);
        updateProgress();
      }
    }];

    // create the tour object
    var tour = new Tour({
      name: tourNameArticle,
      storage: false,
      backdrop: true,
      keyboard: false,
      backdropPadding: 10,
      template: tourGlobalTmpl,
      steps: steps,
      onEnd: function() {
        setTourComplete(tourNameArticle);
        if(isPortalOpen() && !getTourComplete(tourNamePortal)){
          Tours.portalTour();
        }
      },
      onShown: function() {
        updateProgress();
      }
    });

    function updateProgress(){
      updateProgressUI(tour, steps.length);
    }
	//start the tour
	if(portal_tour_status.indexOf(tourNameArticle) == -1) {
		tour.init();
		tour.start();
	}
  }

  Tours.careerTour = function() {
    // tour selectors
    var $tourSidebar = $('#search-training .module');
    var $tourSearch = $tourSidebar.eq(0);
    var $tourSort = $('#search-training');


  // tour steps
   var steps = [{
      // search
      element: $tourSearch,
      title: 'Tip: Quick Search and Browsing.',
      content: 'Find offerings within Career & Learning by name or keyword. Or select your role, your department, a desired competency or a month.',
      placement: 'right',
      onShown: function(){
        hideNavButton(0);
        replaceNavButton(1);
        updateProgress();
      }
    }];
    // create the tour object
    var tour = new Tour({
      name: tourNameCareer,
      storage: false,
      backdrop: true,
      keyboard: false,
      backdropPadding: 10,
      template: tourGlobalTmpl,
      steps: steps,
      onEnd: function() {
        setTourComplete(tourNameCareer);
        if(isPortalOpen()){
          Tours.portalTour();
        }
      }
    });

    function updateProgress(){
      updateProgressUI(tour, steps.length);
    }

    //start the tour
		if(portal_tour_status.indexOf(tourNameCareer) == -1) {
			tour.init();
			tour.start();
		}
  }

  // set tour finish cookie
  function setTourComplete(tour){
    //window.localStorage.setItem(tour + '_tour_completed', 1);
		// Update Tour Status
		var data = "tour="+tour;	
		$.ajax({type: 'POST',url: base_url+'/update-tour-status', data: data,
			success: function(result) {
				portal_tour_status = result;
				if(tour == 'reset') {
					window.location.reload();
				}
			}
		});
  }

  // get tour finish cookie
  function getTourComplete(tour){
    return window.localStorage.getItem(tour + '_tour_completed');
  }

  // remove tour finish cookie
  function removeTourComplete(tour){
    window.localStorage.removeItem(tour + '_tour_completed');
  }

  // check if portal panel is open
  function isPortalOpen(){
    var padding = $('body').css('padding-right');
    return parseInt(padding) > 0;
  }
  
  // update popover top/left values
  function adjustPopover(top, left) {
    var $popover = $(popover);
    $popover.css({
      top: '+=' + top,
      left: '+=' + left
    });
  }

  // remove step progress from dom
  function removeProgressBar(){
    $(popoverProgress).remove();
  }

  // hide step nav button from view
  function hideNavButton(idx){
    $(popoverNavButtons).eq(idx).hide();
  }

  // swap nav button for end tour button
  function replaceNavButton(idx){
    var $button = $(popoverNavButtons).eq(idx);
    $button.text('End');
    $button.attr({
      'data-role': 'end',
      'disabled': false,
      'class': ''
    });
  }

  // hide/show magnific background
  function toggleMagnificOverlay(show){
    $(mfpBackground).css('width', (show ? '100%' : '0px'));
  }

  // update popover step progress value
  function updateProgressUI(tour, stepCount) {
		/*var firstIsOrphan = tour._options.steps[0].orphan;
		if(tour.getCurrentStep() != 0 && firstIsOrphan == true) {
			var text = (tour.getCurrentStep()) + '/' + (stepCount);
		} else {
			var text = (tour.getCurrentStep() + 1) + '/' + (stepCount);
		}
    $(popoverProgress).text(text);*/
		var text = (tour.getCurrentStep() + (tour._options.name === tourNameHome ? 0 : 1)) + '/' + (stepCount);
		$(popoverProgress).text(text);
		if(!tour._options.steps[tour.getCurrentStep()].orphan) {
			$(tourBackdrop).css('background', 'none');
		}
  }

  // scroll off canvas panel
  function scrollPortalPanel(offset){
    var promise = new jQuery.Deferred();
    var scrollTop = Math.max(0, offset - ($(window).height() / 2));
    $(offCanvasContent).animate({ scrollTop: Math.ceil(scrollTop) }, "slow", function(){
      promise.resolve();
    });
    return promise.promise();
  }

  // show settings modal
  function showSettingsModal(){
    var promise = new jQuery.Deferred();
    $.magnificPopup.open({
      items: {
        src: $(settingsModal)
      }
    });
    setTimeout(function(){
      $(mfpWrap).animate({ scrollTop: $(document).height() }, "slow", function(){
        toggleMagnificOverlay(false);
        promise.resolve();
      });
    }, 250);
    return promise.promise();
  }


  function init() {
    // determine which tour to load from body class
    var classes = $('body').attr('class').split(' ');
    var tour = null;

    $.each(tourNames, function(key, val){
      if(classes.indexOf(val) > -1){
        tour = key;
        return false;
      }
    });

		if(portal_tour_status.indexOf(tour) == -1) {
			if(tour){
				if(!getTourComplete(tour) || forceStart){
					Tours[tour + 'Tour']();
					return;


				}
			}

			if(!getTourComplete(tourNamePortal) && isPortalOpen()){
				Tours[tourNamePortal + 'Tour']();

			}
		}
  }

  // check/start tour if not mobile
  if(!$mobileNav.is(':visible') && !disableTours){
    init();
  }

  // start tour through js console
  tours.start = function(tour){
    Tours[tour + 'Tour']();
  }
	
	$('#tour-restart').click(function() {
		setTourComplete('reset');
		return false;
	});
});
})(jQuery);
