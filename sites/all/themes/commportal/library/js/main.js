$(document).ready(function() {
  $('.departments-menu-toggle').click(function() {
    var menu = $('#departments-menu');
    var backdrop = '<div class="modal-backdrop megamenu-backdrop"></div>';
    if (menu.hasClass('active')) {
      $(this).find('span.icon').removeClass('icon-Icon_Up-Carrot-Filled');
      menu.removeClass('active');
      backdrop = $('.megamenu-backdrop');
      backdrop.remove();
      $('body').removeClass('overlay-active');
    } else {
      $(this).find('span.icon').addClass('icon-Icon_Up-Carrot-Filled');
      menu.addClass('active');
      $('.main-container').append(backdrop);
      $('body').addClass('overlay-active');
      $('.header-nav').removeClass('sticky');
    }
  });


  $('.asdfaccount-flyout-toggle').click(function() {
    var menu = $('.account-flyout');
    var toggle = $(this);
    var backdrop = '<div class="modal-backdrop megamenu-backdrop"></div>';
    if (menu.hasClass('active')) {
      toggle.find('i').removeClass('icon-Icon_Up-Carrot-Filled');
      menu.removeClass('active');
      toggle.removeClass('active');
      //backdrop = $('.megamenu-backdrop');
      //backdrop.remove();
    } else {
      toggle.find('i').addClass('icon-Icon_Up-Carrot-Filled');
      menu.addClass('active');
      toggle.addClass('active');
      //$('.main-container').append(backdrop);
    }
  });

  $('.news-dynamic.active').fadeIn();
  $('.news-menu-link').click(function() {

    switch($(this).attr('data-menu-target')) {
      case 'department':
        $('.news-menu .pill').animate({top: '34px'}, 100);
        break;
      case 'people':
        $('.news-menu .pill').animate({top: '84px'}, 100);
        break;
      case 'training':
        $('.news-menu .pill').animate({top: '134px'}, 100);
        break;
    }

    var currentActive = $('.news-menu-link.active');
    var caTarget = '.' + currentActive.attr('data-menu-target') + '-block';
    var thisTarget = '.' + $(this).attr('data-menu-target') + '-block';

    if ($(this).hasClass('active')) {

    } else {
      currentActive.removeClass('active');
      $(caTarget).removeClass('active');
      $(this).addClass('active');
      $(thisTarget).addClass('active');
      $(caTarget).hide();
      $(thisTarget).fadeIn();
    }



  });



  /* $('.link-nav-dot').click(function() {
    var currentActive = $('.link-nav-dot.active');
    var caTarget = '.' + currentActive.attr('data-menu-target');
    var thisTarget = '.' + $(this).attr('data-menu-target');

    if ($(this).hasClass('active')) {

    } else {
      currentActive.removeClass('active');
      $(caTarget).removeClass('active');
      $(this).addClass('active');
      $(thisTarget).addClass('active');
      $('.links-feed').attr('data-current-active', thisTarget);
    }

  }); */

  $('.back-to-top').click(function() {
    $('html').animate({ scrollTop: '0px' });
  });


  $('.notifications').click(function() {

  });

  $(window).scroll(function (event) {
    var scroll = $(window).scrollTop();
    if (scroll > 56) {
      $('.header-nav').addClass('sticky');
    } else if ((scroll < 128 ) && ($('.header-nav').hasClass('sticky'))) {
      $('.header-nav').removeClass('sticky');
    }
  });

});
