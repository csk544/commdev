<?php

/**
 * 
 * Form setting up permission for contents of Builder per role.
 */
function builder_admin_content_permissions($form, &$form_state, $rid = NULL) {

  $groups = array();
  $content_load = builder_content_load_contents();
  if (!empty($content_load)) {
    foreach ($content_load as $module => $module_contents) {
      if (!empty($module_contents)) {
        foreach ($module_contents as $delta => $content) {
          if (!empty($content['group'])) {
            $groups[$content['group']][$delta] = $content;
          } else {
            $groups[t('Other')][$delta] = $content;
          }
        }
      }
    }
  }
  $role_names = user_roles();
  if (is_numeric($rid)) {
    $role_names = array($rid => $role_names[$rid]);
  }

  $role_permissions = _builder_content_permissions($role_names);

  $form['role_names'] = array(
      '#type' => 'value',
      '#value' => $role_names,
  );

  $options = array();
  foreach ($groups as $group => $contents) {
    if (!empty($contents)) {

      $form['permission'][] = array(
          '#markup' => $group,
          '#group' => TRUE,
      );
      foreach ($contents as $delta => $content) {
        if (is_numeric($delta)) {
          $delta = 'REMOVE__' . $delta;
        }
        $options[$delta] = '';
        $form['permission'][$delta] = array(
            '#type' => 'item',
            '#markup' => $content['info'],
        );
        foreach ($role_names as $rid => $name) {
          // Builds arrays for checked boxes for each role
          $new_delta = str_replace('REMOVE__', '', $delta);
          if (isset($role_permissions[$rid][$new_delta])) {
            $status[$rid][] = $delta;
          }
        }
      }
    }
  }

  // Have to build checkboxes here after checkbox arrays are built
  foreach ($role_names as $rid => $name) {
    $form['checkboxes'][$rid] = array(
        '#type' => 'checkboxes',
        '#options' => $options,
        '#default_value' => isset($status[$rid]) ? $status[$rid] : array(),
        '#attributes' => array('class' => array('rid-' . $rid)),
    );
    $form['role_names'][$rid] = array('#markup' => check_plain($name), '#tree' => TRUE);
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save permissions'));
  $form['#attached']['js'][] = drupal_get_path('module', 'builder') . '/js/builder.permissions.js';

  return $form;
}

/*
 *  Submition handle for builder_admin_content_permissions()
 */

function builder_admin_content_permissions_submit($form, &$form_state) {
  foreach ($form_state['values']['role_names'] as $rid => $name) {
    $perms = array();
    $permissions = $form_state['values'][$rid];
    if (!empty($permissions)) {
      foreach ($permissions as $k => $v) {
        $new_key = str_replace('REMOVE__', '', $k);
        $perms[$new_key] = $v;
      }
    }
    builder_contents_change_permissions($rid, $perms);
  }

  drupal_set_message(t('The changes have been saved.'));
  cache_clear_all();
}

function theme_builder_admin_content_permissions($variables) {
  $form = $variables['form'];

  $roles = user_roles();
  foreach (element_children($form['permission']) as $key) {
    $row = array();
    // Module name
    if (is_numeric($key)) {
      $row[] = array('data' => drupal_render($form['permission'][$key]), 'class' => array('module'), 'id' => 'module-' . $form['permission'][$key]['#id'], 'colspan' => count($form['role_names']['#value']) + 1);
    } else {
      // Permission row.
      $row[] = array(
          'data' => drupal_render($form['permission'][$key]),
          'class' => array('permission'),
      );
      foreach (element_children($form['checkboxes']) as $rid) {
        $form['checkboxes'][$rid][$key]['#title'] = $roles[$rid] . ': ' . $form['permission'][$key]['#markup'];
        $form['checkboxes'][$rid][$key]['#title_display'] = 'invisible';
        $row[] = array('data' => drupal_render($form['checkboxes'][$rid][$key]), 'class' => array('checkbox'));
      }
    }
    $rows[] = $row;
  }
  $header[] = (t('Builder content'));
  foreach (element_children($form['role_names']) as $rid) {
    $header[] = array('data' => drupal_render($form['role_names'][$rid]), 'class' => array('checkbox'));
  }
  $output = '<p>' . t('Setup permissions by role to able select contents when use Builder.') . '</p>';
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'permissions')));
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * @see system_settings_form()
 * Admin settings for Builder
 */
function builder_config_settings($form, &$form_state){
	global $base_url;
	$form['sfdc'] = array(
    '#type' => 'fieldset',
    '#title' => t('SFDC and BO details'),
		'#collapsible' => TRUE, 
		'#collapsed' => TRUE,			
  );
  $form['sfdc']['sfdc_basepath'] = array(
    '#type' => 'textfield',
    '#title' => t('SFDC base path'),
    '#default_value' => variable_get('sfdc_basepath', 'http://comm.gene.com/common-service/SFDCService/'),
    '#description' => t('The host name should be like <em>http://comm.gene.com/common-service/SFDCService/</em>'),	
    '#required' => TRUE
  );
  $form['sfdc']['bo_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Publication URL'),
	'#maxlength'     => 255,
    '#default_value' => variable_get('bo_url', ''),
    '#description' => t('Use this to get Data Publication URL'),	
    '#required' => TRUE
  ); 
  $form['sfdc']['data_publication_roles'] = array(
    '#type' => 'select',
    '#title' => t('Data Publication - Role Targetting'),
    '#default_value' => variable_get('data_publication_roles', ''),
    '#description' => t('Data Publication Dates block role targetting'),	
		'#multiple' => TRUE,
		'#options' => get_role_targeting(),
		'#attributes' => array('class' => array('chosen-widget'))
  ); 
	
	$form['generic'] = array(
    '#type' => 'fieldset',
    '#title' => t('Generic User Details'),
		'#collapsible' => TRUE, 
		'#collapsed' => TRUE,		
  );
  $form['generic']['generic_unix'] = array(
    '#type' => 'textfield',
    '#title' => t('Generic User'),
    '#default_value' => variable_get('generic_unix', 'COMMPORT'),
    '#required' => TRUE
  );
  $form['generic']['generic_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('generic_pass'),   
    '#required' => TRUE		
  );	
	
	$form['common'] = array(
		'#type' => 'fieldset',
		'#title' => t('Common details'),
		'#collapsible' => TRUE, 
		'#collapsed' => TRUE,		
  );
  $node_types = array('select' => 'select');
  $node_type_objs = node_type_get_types();
  foreach($node_type_objs as $node_type_obj) {
		$node_types[$node_type_obj->type] = $node_type_obj->name;
  }
	
  $form['common']['content_types_theme'] = array(
		'#title' => t('Commportal theme for add/edit pages'),
		'#type' => 'select',
		'#multiple' => TRUE,
		'#options' => $node_types,
		'#default_value' => variable_get('content_types_theme'),
		'#attributes' => array('class' => array('chosen-widget'))
  );

	$form['common']['calendar_googleapi'] = array(
		'#type' => 'textfield',
		'#title' => t('Google API Key'),
		'#default_value' => variable_get('calendar_googleapi', ''),  
		'#description' => t('Key for server applications - <a href="https://developers.google.com/console/help/new/#usingkeys" target="_blank">Click Here</a> to see the steps to generate new key.'),
		'#required' => TRUE,
	);	

	$form['common']['homepage_collage_nid'] = array(
		'#type' => 'textfield',
		'#title' => t('Homepage Collage Node ID'),
		'#default_value' => variable_get('homepage_collage_nid', ''),  
		'#description' => t('DO NOT CHANGE UNTIL UNLESS IF IT IS REQUIRED.'),
		'#required' => TRUE,
	);
	$form['common']['dept_term_id'] = array(
		'#type' => 'textfield',
		'#title' => t('Channel Departments Term ID'),
		'#default_value' => variable_get('dept_term_id', ''),  
		'#description' => t('DO NOT CHANGE UNTIL UNLESS IF IT IS REQUIRED.'),
		'#required' => TRUE,
	);		
	$form['common']['us_medical_dept_term_id'] = array(
		'#type' => 'textfield',
		'#title' => t('U.S.Medical Affairs Channel Departments Term ID'),
		'#default_value' => variable_get('us_medical_dept_term_id', ''),  
		'#description' => t('DO NOT CHANGE UNTIL UNLESS IF IT IS REQUIRED.'),
		'#required' => TRUE,
	);		
	$form['common']['ga_dept_term_id'] = array(
		'#type' => 'textfield',
		'#title' => t('Government Affairs Channel Departments Term ID'),
		'#default_value' => variable_get('ga_dept_term_id', ''),  
		'#description' => t('DO NOT CHANGE UNTIL UNLESS IF IT IS REQUIRED.'),
		//'#required' => TRUE,
	);
	$form['common']['us_medical_landingpage_nid'] = array(
		'#type' => 'textfield',
		'#title' => t('U.S.Medical Affairs Landing Page NID'),
		'#default_value' => variable_get('us_medical_landingpage_nid', ''),  
		'#description' => t('DO NOT CHANGE UNTIL UNLESS IF IT IS REQUIRED.'),
		//'#required' => TRUE,
	);	
	$form['common']['us_medical_landingpage_tid'] = array(
		'#type' => 'textfield',
		'#title' => t('U.S.Medical Affairs Landing Page TID'),
		'#default_value' => variable_get('us_medical_landingpage_tid', ''),  
		'#description' => t('DO NOT CHANGE UNTIL UNLESS IF IT IS REQUIRED.'),
		//'#required' => TRUE,
	);
	$form['common']['secured_term_id'] = array(
		'#type' => 'textfield',
		'#title' => t('Secured Tools Departments Term ID'),
		'#default_value' => variable_get('secured_term_id', ''),  
		'#description' => t('DO NOT CHANGE UNTIL UNLESS IF IT IS REQUIRED.'),
		'#required' => TRUE,
	);		
	$form['common']['commercial_cc_ids'] = array(
	'#title' => t("Commercial Cost Centre Id's"),
	'#type' => 'textarea',
	'#description' => t
	("Commercial Cost Centre Id's to use in Search"),
	'#default_value' => variable_get('commercial_cc_ids', ''),
	);
	$form['common']['career_landing_page'] = array(
		'#type' => 'textfield',
		'#title' => t('Career & Learning Landing page ID'),
		'#default_value' => variable_get('career_landing_page', ''),  
		'#description' => t('DO NOT CHANGE UNTIL UNLESS IF IT IS REQUIRED.'),
		'#required' => TRUE,
	);
	$form['common']['about_commercial_landing_page'] = array(
		'#type' => 'textfield',
		'#title' => t('About Commercial Landing page ID'),
		'#default_value' => variable_get('about_commercial_landing_page', ''),  
		'#description' => t('DO NOT CHANGE UNTIL UNLESS IF IT IS REQUIRED.'),
		'#required' => TRUE,
	);
	$form['common']['about_comm_term_id'] = array(
		'#type' => 'textfield',
		'#title' => t('About Commercial Term ID'),
		'#default_value' => variable_get('about_comm_term_id', ''),  
		'#description' => t('DO NOT CHANGE UNTIL UNLESS IF IT IS REQUIRED.'),
		'#required' => TRUE,
	);
	$form['common']['tools_term_id'] = array(
		'#type' => 'textfield',
		'#title' => t('Tools Term ID'),
		'#default_value' => variable_get('tools_term_id', ''),  
		'#description' => t('DO NOT CHANGE UNTIL UNLESS IF IT IS REQUIRED.'),
		'#required' => TRUE,
	);
	$form['common']['career_learning_term_id'] = array(
		'#type' => 'textfield',
		'#title' => t('Career & Learning Term ID'),
		'#default_value' => variable_get('career_learning_term_id', ''),  
		'#description' => t('DO NOT CHANGE UNTIL UNLESS IF IT IS REQUIRED.'),
		'#required' => TRUE,
	);
	$form['common']['solr_search_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Search URL'),
    '#default_value' => variable_get('solr_search_url'),
    '#required' => TRUE,
  );
	$form['common']['km_search_microsites'] = array(
    '#type' => 'textfield',
    '#title' => t('KM Microsite NIDs'),
    '#default_value' => variable_get('km_search_microsites'),    
  );
	$form['common']['feedback_nid_exclude'] = array(
    '#type' => 'textarea',
    '#title' => t('Exclude feedback block from NIDs'),
    '#default_value' => variable_get('feedback_nid_exclude',''),   
		'#description' => t('Add the nids to remove feedback block from the rightside bar (comma seperated)'),
  );
  $form['common']['partners_megamenu_logo'] = array(
    '#type' => 'textfield',
    '#title' => t('Partners Megamenu Logo URL'),
    '#default_value' => variable_get('partners_megamenu_logo',''),    
  );
  $form['common']['partners_white_megamenu_logo'] = array(
    '#type' => 'textfield',
    '#title' => t('Partners Megamenu white Logo URL'),
    '#default_value' => variable_get('partners_white_megamenu_logo',''),    
  );	
	$form['microsite'] = array(
    '#type' => 'fieldset',
    '#title' => t('Microsite details'),
		'#collapsible' => TRUE, 
		'#collapsed' => TRUE,			
  );	
	$form['microsite']['data_field_microsite_header'] = array(
		'#type' => 'textarea',
		'#title' => t('Microsite Header'),
		'#default_value' => variable_get('data_field_microsite_header', ''),
		'#description' => t('Enter JSON serialize data to get default settings in Microsite Header region.'),
	);
	
 	$form['microsite']['data_field_microsite_builder'] = array(
		'#type' => 'textarea',
		'#title' => t('Microsite Content'),
		'#default_value' => variable_get('data_field_microsite_builder', ''),
		'#description' => t('Enter JSON serialize data to get default settings in Microsite Content region.'),		
	); 
	
	$form['microsite']['data_field_microsite_rightside'] = array(
		'#type' => 'textarea',
		'#title' => t('Right Column'),
		'#default_value' => variable_get('data_field_microsite_rightside', ''),
		'#description' => t('Enter JSON serialize data to get default settings in Microsite Rightside column region.'),		
	);
	$form['calendar'] = array(
    '#type' => 'fieldset',
    '#title' => t('Calendar Settings'),
		'#collapsible' => TRUE, 
		'#collapsed' => TRUE,			
  );		
	$form['calendar']['application_name'] = array(
		'#type' => 'textfield',
		'#title' => t('Application Name'),
		'#default_value' => variable_get('application_name', ''),  
	);
	$form['calendar']['client_secret_json'] = array(
		'#type' => 'textarea',
		'#rows' => 3,
		'#title' => t('Client Secret'),
		'#default_value' => variable_get('client_secret_json', ''),
	);
	if(variable_get('access_token', '') == '') {
		if(variable_get('application_name', '') != '' && variable_get('client_secret_json', '') != '') {
			$form['calendar']['client_secret_json']['#suffix'] = '<a href="'.$base_url.'/admin/config/services/authorize_calendar">Click here to generate Access Token for Google Calendar API</a>';
		}
	} else {
		$form['calendar']['access_token'] = array(
			'#type' => 'textarea',
			'#rows' => 3,
			'#title' => t('Access Token'),
			'#default_value' => variable_get('access_token', ''),
		);
	}
  $form['usma_assets'] = array(
    '#type' => 'fieldset',
    '#title' => t('USMA Styles'),
		'#collapsible' => TRUE, 
		'#collapsed' => TRUE,			
  );
  $form['usma_assets']['nodeids'] = array(
		'#type' => 'textfield',
		'#title' => t('Node ids of content for which USMA styles to be loaded'),
    '#description' => 'Enter comma separated Node IDs',
		'#default_value' => variable_get('nodeids', ''),  
	);
  return system_settings_form($form);
}