(function ($) {
  Drupal.behaviors.misc = {
    attach: function (context, settings) {
			/* Apache Solr Search Filters */
			$('#search_filters_form').off('change').on('change', '#search_sort_by', function(e){				
				this.form.submit();			
			});				
			$('#search_filters_form').off('click').on('click', '.search-submit', function(e){
				$pager = 0;
				$("#search_filters_form input[name='start']").val($pager);
				$("#search_filters_form").submit();
			});				
			
			$('.pager_next').off('click').on('click', function(e){
				$pager = 10 + Number($("#search_filters_form input[name='start']").val());
				$("#search_filters_form input[name='start']").val($pager);
				$("#search_filters_form").submit();
			});
			$('.pager_prev').off('click').on('click', function(e){
				$pager = Number($("#search_filters_form input[name='start']").val()) - 10;
				$("#search_filters_form input[name='start']").val($pager);
				$("#search_filters_form").submit();
			});	
			
			/* KM Search SpellCheck */ 
			/*$('#spellCheck').off('click').on('click', function(e){				
			  var spellCheck = $('#spellCheck').text();
				$("#search_filters_form .queryTerm").val(spellCheck);
				$("#search_filters_form").submit();
			});*/
			
			/* Filters by file type */ 
			$('#search_filters_form #select_all').on('click',function(){
			if(this.checked){
				$('#search_filters_form .checkbox').each(function(){
					this.checked = true;
				});
			}	else	{
				$('#search_filters_form .checkbox').each(function(){
					this.checked = false;
				});
			}
		});
		$('#search_filters_form .checkbox').on('click',function(){
			if($('#search_filters_form .checkbox:checked').length == $('#search_filters_form .checkbox').length){
				$('#search_filters_form #select_all').prop('checked',true);
			}	else	{
				$('#search_filters_form #select_all').prop('checked',false);
			}
		});
		
		}	
  };
}(jQuery));