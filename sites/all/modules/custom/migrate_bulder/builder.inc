<?php

class DrupalBuilderMigration extends Migration {

  protected $sourceConnection;

  protected $sourceFields = array();

  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->sourceConnection = $this->arguments['source_connection'];
    if (!empty($this->arguments['source_database'])) {
      Database::addConnectionInfo($this->sourceConnection, 'default', $this->arguments['source_database']);
    }

    if (!empty($arguments['dependencies'])) {
      $this->dependencies = $arguments['dependencies'];
    }

    $this->sourceFields += array(
      'bid' => t('Builder Bid'),
      'uid' => t('User Id'),
      'entity_id' => t('Entity Id'),
      'type' => t('Type'),
      'field_name' => t('Field Name'),
      'data' => t('Builder Data'),
      'status' => t('Status'),
      'created' => t('Created')
    );

    $this->source = new MigrateSourceSQL($this->query(), $this->sourceFields, NULL, array('map_joinable' => FALSE));
    $this->destination = new MigrateDestinationBuilder();

    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'bid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'Source Builder ID',
          'alias' => 'bld',
        ),
      ),
      MigrateDestinationBuilder::getKeySchema()
    );

    $this->addSimpleMappings(array('entity_id', 'type', 'field_name', 'data', 'status', 'created'));

    if (isset($arguments['default_uid'])) {
      $default_uid = $arguments['default_uid'];
    }
    else {
      $default_uid = 1;
    }
    if (isset($user_migration)) {
      $this->addFieldMapping('uid', 'uid')
          ->sourceMigration($user_migration)
          ->defaultValue($default_uid);
    }
    else {
      $this->addFieldMapping('uid')
           ->defaultValue($default_uid);
    }
  }

  protected function query() {
    return Database::getConnection('default', $this->sourceConnection)
             ->select('builder_data', 'bld')
             ->fields('bld', array('bid', 'uid', 'entity_id', 'type', 'field_name', 'data', 'status', 'created'))
             //->condition('bld.bid', '17290', '<=')
             ->orderBy('bld.bid');
  }

  public function prepareRow($row) {
    if (parent::prepareRow($row) === FALSE) {
      return FALSE;
    }

    //$this->version->getSourceValues($row, $row->bid);
    return TRUE;
  }
}

class MigrateBuilderFieldHandler extends MigrateFieldHandler {

  public function __construct() {
    $this->registerTypes(array('builder'));
  }

  public function fields($type, $instance, $migration = NULL) {
    return array(
      'source_type' => t('Option: <a href="@doc">Set to \'bid\' when the value is a source ID</a>',
        array('@doc' => 'http://drupal.org/node/1224042#source_type')),
    );
  }

  public function prepare($entity, array $field_info, array $instance, array $values) {
    $result = array();
    $migration = Migration::currentMigration();
    $soft_dependencies = $migration->getSoftDependencies();
    $builder_dependency = preg_grep('/([a-zA-Z0-9]+)builder$/i', $soft_dependencies);

    if (!empty($builder_dependency)) {
      $table = array_shift($builder_dependency);
      if (db_table_exists('migrate_map_' . $table)) {
        $bid = db_select('migrate_map_' . $table, 'mb')
                  ->fields('mb', array('destid1'))
                  ->condition('mb.sourceid1', $values[0], '=')
                  ->execute()->fetchField();

      }
    }
    $bid = empty($bid) ? $values[0] : $bid;

    $data = db_select('builder_data', 'bld')->fields('bld', array('data'))->condition('bld.bid', $bid)->execute()->fetchField();

    if (empty($data)) {
      $obj = new stdClass;
      $data = serialize($obj);
    }

    $builder_cache_id = builder_get_cache_id($bid, 0);
    builder_cache_set($builder_cache_id, unserialize($data));

    $result['und'][] = array('bid' => $bid);
    return $result;
  }
}

class MigrateBuilderNodeDestinationHanlder extends MigrateDestinationHandler {
  public function __construct() {
    $this->registerTypes(array('node'));
  }

  public function complete($entity, stdClass $row) {
    $existing = db_select('migrated_nodes', 'mn')
        ->fields('mn', array('destination_nid'))
        ->condition('mn.source_nid', $row->nid, '=')
        ->condition('mn.destination_nid', $entity->nid, '=')
        ->execute()->fetchField();
    if (empty($existing)) {
      db_insert('migrated_nodes')->fields(array(
        'source_nid' => $row->nid,
        'destination_nid' => $entity->nid
      ))->execute();
    }
    else {
      db_update('migrated_nodes')->fields(array(
        'source_nid' => $row->nid,
        'destination_nid' => $entity->nid
      ))->condition('source_nid', $row->nid)->execute();
    }
  }
}