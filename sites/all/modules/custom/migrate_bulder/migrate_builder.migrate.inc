<?php

class MigrateDestinationBuilder extends MigrateDestination {
  
  static public function getKeySchema() {
    return array(
      'bid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'unsigned' => TRUE,
        'description' => 'ID of destination builder',
        'alias' => 'bld',
      ),
    );
  }

  public function __construct() {
    parent::__construct();
  }

  public function fields($migration = NULL) {
    $fields = array();

    $fields['bid'] = t('builder ID');
    $fields['uid'] = t('<a href="@doc">Authored by (uid)</a>', array('@doc' => 'http://drupal.org/node/1349696#uid'));
    $fields['entity_id'] = t('The entity ID using this builder row.');
    $fields['type'] = t('The entity type');
    $fields['field_name'] = t('Field Name');
    $fields['data'] = t('Data of each row.');
    $fields['status'] = t('Row status.');
    $fields['created'] = t('Unix timestamp');

    return $fields;
  }

  public function import(stdClass $builder, stdClass $row) {
    $migration = Migration::currentMigration();
    if (isset($row->migrate_map_destid1)) {
      if (isset($builder->bid)) {
        if ($row->migrate_map_destid1 != $builder->bid) {
          throw new MigrateException(t("Incoming bid !bid and map destination bid !destid1 don't match",
            array('!bid' => $builder->bid, '!destid1' => $row->migrate_map_destid1)));
        }
      }
      else {
        $builder->bid = $row->migrate_map_destid1;
      }
    }
    /* else {
      $existing_builder = db_select('builder_data', 'bld')->fields('bld', array('bid'))->condition('bid', $row->bid)->execute()->fetchField();
      if ($existing_builder) {
        $builder->bid = $row->bid;
      }
    } */

    if ($migration->getSystemOfRecord() == Migration::SOURCE) {
      $builder->created = isset($builder->created) ? MigrationBase::timestamp($builder->created) : REQUEST_TIME;
      if (!$builder->uid) {
        $builder->uid = 1;
      }
    }

    $this->prepare($builder, $row);

    if (isset($builder->bid)) {
      if (isset($row->migrate_map_destid1)) {
        migrate_instrument_start('builder_update');
        db_update('builder_data')->fields((array)$builder)->condition('bid', $builder->bid)->execute();
        migrate_instrument_stop('builder_update');
      }
    }
    else {
      migrate_instrument_start('builder_insert');
      $bid = db_insert('builder_data')->fields((array)$builder)->execute();
      migrate_instrument_stop('builder_insert');
      $builder->bid = $bid;
    }

    /* $builder_cache_id = builder_get_cache_id($builder->bid, 0);
    builder_cache_set($builder_cache_id, unserialize($builder->data)); */
    return array($builder->bid);
  }

  protected function prepare(stdClass $builder, stdClass $row) {

    $nids = array();

    $data = unserialize($builder->data);
    if (!empty($data->rows)) {
      foreach ($data->rows as $data_row) {
        if (isset($data_row['columns'])) {
          foreach ($data_row['columns'] as $column) {
            if (isset($column['contents'])) {
              foreach ($column['contents'] as $content) {
                if (isset($content['settings']) && !empty($content['settings']['nid'])) {
                  $nids[$content['settings']['nid']] = $content['settings']['nid'];
                }
                else {
                  continue;
                }
              }
            }
          }
        }
      }

      if (!empty($nids)) {
        $existing_nids = db_select('migrated_nodes', 'mn')->fields('mn')->execute()->fetchAll(PDO::FETCH_KEY_PAIR|PDO::FETCH_GROUP);
        foreach ($data->rows as &$data_row) {
          if (isset($data_row['columns'])) {
            foreach ($data_row['columns'] as &$column) {
              if (isset($column['contents'])) {
                foreach ($column['contents'] as &$content) {
                  if (!empty($content['settings']['nid'])) {
                    $nid = $content['settings']['nid'];
                    $content['settings']['nid'] = isset($existing_nids[$nid]) ? $existing_nids[$nid] : $nid;
                  }
                  else {
                    continue;
                  }
                }
              }
            }
          }
        }
      }
    }

    $builder->data = serialize($data);
  }

  public function prepareRollback($bid) {
    $migration = Migration::currentMigration();
    migrate_handler_invoke_all('Builder', 'prepareRollback', $bid);
    if (method_exists($migration, 'prepareRollback')) {
      $migration->prepareRollback($bid);
    }
  }

  public function bulkRollback(array $bids) {
    migrate_instrument_start('builder_delete_multiple');
    $this->deleteMultipleBuilders($bids);
    $this->completeRollback($bids);
    migrate_instrument_stop('builder_delete_multiple');
  }

  public function deleteMultipleBuilders(array $bids) {
    if (!empty($bids)) {
      db_delete('builder_data')->condition('bid', $bids, 'IN')->execute();
    }
  }

  public function completeRollback($bid) {
    $migration = Migration::currentMigration();
    migrate_handler_invoke_all('Builder', 'completeRollback', $bid);
    if (method_exists($migration, 'completeRollback')) {
      $migration->completeRollback($bid);
    }
  }
  public function __toString() {
    return t('Builder');
  }
}