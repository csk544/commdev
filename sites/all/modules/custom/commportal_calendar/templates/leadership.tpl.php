<?php
	$ceo_unix_id = $data['ceo']['unix_id'];
	$ceo_name = $data['unix_ids_details'][$ceo_unix_id]->preferred_fullname;
	$ceo_admin_details = $data['unix_ids_details'][$data['unix_id_admins'][$ceo_unix_id]];
?>
<section class="entry">

            <ul class="staff">
              <li class="row no-margin">
                <div class="member-half">
                  <div class="member__block contextual-links-region">
                    <div class="member__profile-image">
                      <img src="<?php echo $data['heads'][$ceo_unix_id]['headshot_url'];?>" alt="<?php echo $ceo_name;?>" title="<?php echo $ceo_name;?>">
                    </div>
                    <!--/col-->
                    <div class="member__profile-info">
                      <h4><?php echo $ceo_name;?></h4>
                      <span><?php echo ($data['heads'][$ceo_unix_id]['role_title'] != '') ? $data['heads'][$ceo_unix_id]['role_title'] : $data['unix_ids_details'][$ceo_unix_id]->job_title;?></span>
                      <div class="member__bio-links more_info">
                        <div class="member__more-info">
                          <span class="icon-info-circled hasTooltip"></span>
                          <div class="tooltip-content">
                            <ul>
                              <li><strong>UnixID:</strong> <?php echo $data['unix_ids_details'][$ceo_unix_id]->unixid;?></li>
                              <li><a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=<?php echo $data['unix_ids_details'][$ceo_unix_id]->email;?>"  title="<?php echo t("Mail ". $ceo_name);?>"><?php echo $data['unix_ids_details'][$ceo_unix_id]->email;?></a>
                              </li>
                              <li><strong>p:</strong> <?php echo $data['unix_ids_details'][$ceo_unix_id]->phone_number;?></li>
                              <li><strong>m:</strong> <?php echo $data['unix_ids_details'][$ceo_unix_id]->mobile_number;?></li>
                              <li><strong>Mail Stop:</strong> <?php echo $data['unix_ids_details'][$ceo_unix_id]->mail_stop;?></li>
                              <li><strong>Building:</strong> <?php echo $data['unix_ids_details'][$ceo_unix_id]->building;?>, Room <?php echo $data['unix_ids_details'][$ceo_unix_id]->room_number;?></li>
                            </ul>
                          </div>
                        </div>
                        <a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=<?php echo $data['unix_ids_details'][$ceo_unix_id]->email;?>" title="<?php echo t("Mail ". $ceo_name);?>"><span class="icon-mail" aria-hidden="true"></span></a><a href="<?php echo $data['heads'][$ceo_unix_id]['view_more'];?>"><?php echo empty($data['heads'][$ceo_unix_id]['view_more']) ? "" : t("View Bio") ;?></a><a href="<?php echo $data['heads'][$ceo_unix_id]['related_link_url'];?>"><?php echo $data['heads'][$ceo_unix_id]['related_link_title'];?></a>
                      </div>
						 <?php if (!empty($ceo_admin_details->unixid)) { ?>
					  <div class="member__bio-links">
						<div class="member_admin_info">
							<p class="small">
								<i>
									Admin: <?php echo $ceo_admin_details->preferred_fullname . "&nbsp;";?>
								</i>
							</p>
						</div>
						<div class="member__more-info">
                          <span class="icon-info-circled hasTooltip"></span>
                          <div class="tooltip-content">
                            <ul>
                              <li><strong>UnixID:</strong> <?php echo $ceo_admin_details->unixid;?></li>
                              <li><a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=<?php echo $ceo_admin_details->email;?>"  title="<?php echo t("Mail ". $ceo_admin_details->preferred_fullname);?>"><?php echo $ceo_admin_details->email;?></a>
                              </li>
                              <li><strong>p:</strong> <?php echo $ceo_admin_details->phone_number;?></li>
                              <li><strong>m:</strong> <?php echo $ceo_admin_details->mobile_number;?></li>
                              <li><strong>Mail Stop:</strong> <?php echo $ceo_admin_details->mail_stop;?></li>
                              <li><strong>Building:</strong> <?php echo $ceo_admin_details->building;?>, Room <?php echo $ceo_admin_details->room_number;?></li>
                            </ul>
                          </div>
                        </div>
					  </div>
						 <?php } ?>
                    </div>
                    <!--/col-->
					<?php if($data['heads'][$ceo_unix_id]['edit_link'] != ''):?>
					<div class="contextual-links-wrapper contextual-links-processed"><a href="javascript:void(0);" class="contextual-links-trigger">Configure</a>
						<ul class="contextual-links">
							<li class="edit-link first last">
								<a href="<?php echo $data['heads'][$ceo_unix_id]['edit_link']; ?>?destination=leadership">edit</a>
							</li>
							<li class="delete-link first last">
								<a href="<?php echo $data['heads'][$ceo_unix_id]['delete_link']; ?>?destination=leadership">delete</a>
							</li>
						</ul>
					</div>
					<?php endif;?>
                  </div>
                </div>
                <!-- ======================== -->
                <!-- STAFF SUB GROUP -->
                <!-- ======================== -->
				<?php if(isset($data['units_details']) && count($data['units_details'])>0):?>
                <ul class="staff__subgroup row no-padding">
					<?php 
					$units_count = count($data['units_details']);
					$index = 0;
					foreach($data['units_details'] as $unit_details):
						$unit_unix_ids = $data['unit_unix_ids'][$unit_details['tid']];
						$next_index = $index + 1;
						$unit_class = ($index++%2 == 0 && $next_index == $units_count) ? 'full' : 'col-md-6';
					?>
                  <li class="<?php echo $unit_class;?>">
                    <h2 class="hd-title"><?php echo $unit_details['unit_name'];?></h2>
                    <ul class="subgroup__member-halfs">
						<?php foreach($unit_unix_ids as $unix_id): 
						$unix_id_details = $data['unix_ids_details'][$unix_id];
						$name = $unix_id_details->preferred_fullname;
						$admin_details = isset($data['unix_id_admins'][$unix_id]) ? $data['unix_ids_details'][$data['unix_id_admins'][$unix_id]] : '';
						?>
                      <!--STAFF member-half -->
                      <li class="member-half unit-staff">
                        <div class="member__block">
                          <div class="member__profile-image">
                            <img src="<?php echo $data['heads'][$unix_id]['headshot_url'];?>" alt="<?php echo $name;?>" title="<?php echo $name;?>">
                          </div>
                          <!--/col-->
													<div class="margin-left">
                          <div class="member__profile-info">
                            <h4><?php echo $name;?></h4>
                            <span><?php echo ($data['heads'][$unix_id]['role_title'] != '') ? $data['heads'][$unix_id]['role_title'] : $unix_id_details->job_title;?></span>
                            <div class="member__bio-links more_info">
                              <div class="member__more-info">
                                <span class="icon-info-circled hasTooltip"></span>
                                <div class="tooltip-content">
                                  <ul>
                                    <li><strong>UnixID:</strong> <?php echo $unix_id_details->unixid;?></li>
                                    <li><a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=<?php echo $unix_id_details->email;?>"  title="<?php echo t("Mail ". $name);?>"><?php echo $unix_id_details->email;?></a>
                                    </li>
                                    <li><strong>p:</strong> <?php echo $unix_id_details->phone_number;?></li>
                                    <li><strong>m:</strong> <?php echo $unix_id_details->mobile_number;?></li>
                                    <li><strong>Mail Stop:</strong> <?php echo $unix_id_details->mail_stop;?></li>
                                    <li><strong>Building:</strong> <?php echo $unix_id_details->building;?>, Room <?php echo $unix_id_details->room_number;?></li>
                                  </ul>
                                </div>
                              </div>
                              <a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=<?php echo $unix_id_details->email;?>"  title="<?php echo t("Mail ". $name);?>"><span class="icon-mail" aria-hidden="true"></span></a><a href="<?php echo $data['heads'][$unix_id]['view_more'];?>"><?php echo empty($data['heads'][$unix_id]['view_more']) ? "" : t("View Bio") ;?></a><a href="<?php echo $data['heads'][$unix_id]['related_link_url'];?>"><?php echo $data['heads'][$unix_id]['related_link_title'];?></a>
                            </div>
                          </div>
					<?php if($admin_details != ''):?>
					  <div class="member__bio-links">
						<div class="member_admin_info">
							<p class="small">
								<i>
									Admin: <?php echo $admin_details->preferred_fullname . "&nbsp;";?>
								</i>
							</p>
						</div>
						<div class="member__more-info">
                          <span class="icon-info-circled hasTooltip"></span>
                          <div class="tooltip-content">
                            <ul>
                              <li><strong>UnixID:</strong> <?php echo $admin_details->unixid;?></li>
                              <li><a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to=<?php echo $admin_details->email;?>"  title="<?php echo t("Mail ". $admin_details->preferred_fullname);?>"><?php echo $admin_details->email;?></a>
                              </li>
                              <li><strong>p:</strong> <?php echo $admin_details->phone_number;?></li>
                              <li><strong>m:</strong> <?php echo $admin_details->mobile_number;?></li>
                              <li><strong>Mail Stop:</strong> <?php echo $admin_details->mail_stop;?></li>
                              <li><strong>Building:</strong> <?php echo $admin_details->building;?>, Room <?php echo $admin_details->room_number;?></li>
                            </ul>
                          </div>
                        </div>
					  </div>
						</div>
					  <?php endif;?>
                          <!--/col-->
					<?php if($data['heads'][$unix_id]['edit_link']):?>
						<div class="contextual-links-wrapper contextual-links-processed"><a href="javascript:void(0);" class="contextual-links-trigger">Configure</a>
						<ul class="contextual-links">
							<li class="edit-link first last">
								<a href="<?php echo $data['heads'][$unix_id]['edit_link']; ?>?destination=leadership">edit</a>
							</li>
							<li class="delete-link first last">
								<a href="<?php echo $data['heads'][$unix_id]['delete_link']; ?>?destination=leadership">delete</a>
							</li>
						</ul>
					</div>
					<?php endif;?>
                        </div>
                      </li>
                      <!--/STAFF member-half -->
					  <?php endforeach;?>                      
                    </ul>
                  </li>
				  <?php endforeach;?>
                </ul>
				<?php endif;?>
              </li>
            </ul>
          </section>