(function ($) {
	Drupal.behaviors.commportal_leadership = {
		attach: function (context, settings) {
			$(".member__block").mouseover(function(){
				$(this).addClass("contextual-links-region-active");
				$(".contextual-links-trigger", this).addClass("contextual-links-trigger-active");
			});
			$(".member__block").mouseleave(function(){
				$(this).removeClass("contextual-links-region-active");
				$(this).removeClass("contextual-links-active");
				$(".contextual-links-trigger", this).removeClass("contextual-links-trigger-active");
				$(".contextual-links", this).removeClass("show");
			});
			$(".contextual-links-trigger").click(function(){
				var parentDiv = $(this).parent();
				$(parentDiv).parent().toggleClass("contextual-links-active");
				$(".contextual-links", parentDiv).toggleClass("show");
			});
		}
	};

}(jQuery));