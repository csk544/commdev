<?php

/**
 * @file
 * Admin page callback file for commportal calendar module.
 */

/**
 * Form builder; Configure Google API Key settings for this site.
 *
 * @ingroup forms
 * @see system_settings_form()
 * Admin settings for event calendar
 
function calendar_admin_googleapi($form, &$form_state){
	$form['calendar_googleapi'] = array(
		'#type' => 'textfield',
		'#title' => t('Google API Key'),
		'#default_value' => variable_get('calendar_googleapi', ''),  
		'#description' => t('Key for server applications - <a href="https://developers.google.com/console/help/new/#usingkeys" target="_blank">Click Here</a> to see the steps to generate new key.'),
		'#required' => TRUE,
	);
  return system_settings_form($form);
}
*/
/*function fetch_leadership_page(){	
	$data = array();
	$unix_ids = array();
	$unix_id_admins = array();
	$unit_unix_ids = array();
	$units_heads = node_load_multiple(NULL, array('type' => "leadership", 'status' => 1));
	$contextual_links_access = false;
	if(is_array($units_heads) && count($units_heads)>0){
		foreach($units_heads as $nid_details){
			$head_details = array();
			$nid = $nid_details->nid;
			$head_details['edit_link'] = "";
			$head_details['delete_link'] = "";
			if(drupal_valid_path("node/" . $nid . "/edit")){
				$contextual_links_access = true;
				$head_details['edit_link'] = "node/" . $nid . "/edit";
			}
			if(drupal_valid_path("node/" . $nid . "/delete")){
				$head_details['delete_link'] = "node/" . $nid . "/delete";
			}
			$unix_ids[] = $nid_details->title;
			$admin = isset($nid_details->field_clc_admin['und'][0]['value']) ? $nid_details->field_clc_admin['und'][0]['value'] : '';
			if($admin != ''){
				$unix_ids[] = $admin;
				$unix_id_admins[$nid_details->title] = $admin;
			}			
			$head_details['role_title'] = isset($nid_details->field_role_title['und'][0]['value']) ? $nid_details->field_role_title['und'][0]['value'] : '';
			$head_details['view_more'] = isset($nid_details->field_view_bio_link['und'][0]['url']) ? $nid_details->field_view_bio_link['und'][0]['url'] : '';
			$head_details['related_link_title'] = isset($nid_details->field_related_link['und'][0]['title']) ? $nid_details->field_related_link['und'][0]['title'] : '';
			$head_details['related_link_url'] = isset($nid_details->field_related_link['und'][0]['url']) ? $nid_details->field_related_link['und'][0]['url'] : '';
			$head_details['headshot_url'] = isset($nid_details->field_headshot['und'][0]['uri']) ? file_create_url($nid_details->field_headshot['und'][0]['uri']) : '';
			$data['heads'][$nid_details->title] = $head_details;
			if(isset($nid_details->field_unit['und'][0]['tid'])){			
				$unit_unix_ids[$nid_details->field_unit['und'][0]['tid']][] = $nid_details->title;
			}else{
				$data['ceo']['unix_id'] = $nid_details->title;
			}
		}
	}
	if(count($unix_ids)>0){
		$data['unix_ids_details'] = fetch_details_by_unixids($unix_ids);
	}
	if(count($unix_id_admins)>0){
		$data['unix_id_admins'] = $unix_id_admins;
	}
	if(count($unit_unix_ids)>0){
		$units_details = taxonomy_term_load_multiple(array_keys($unit_unix_ids));
		if(is_array($units_details) && count($units_details)>0){
			foreach($units_details as $unit_details_obj){
				$unit_details['unit_name'] = $unit_details_obj->name;
				$unit_details['weight'] = $unit_details_obj->weight;
				$unit_details['tid'] = $unit_details_obj->tid;
				$data['units_details'][] = $unit_details;
			}
			usort($data['units_details'], function($a, $b) {
				return $a['weight'] - $b['weight'];
			});
			$data['unit_unix_ids'] = $unit_unix_ids;
		}
	}
	if($contextual_links_access){
		drupal_add_js(drupal_get_path('module', 'commportal_calendar') . '/scripts/leadership.js');
		drupal_add_css(drupal_get_path('module', 'contextual') . '/contextual.css');
	}
	return theme('leadership',array('data' => $data));
}*/
