<?php
 
/**
 * Menu callback; Search results on user key press in search box of header region of all pages
 */
/*function search_results_autocomplete() {
  $matches = array();
  global $base_url;
  $searchtext = isset($_REQUEST['searchtext']) ? $_REQUEST['searchtext'] : '';
  $search_results_html = ""; 
  $picture_web = $base_url . "/sites/default/files/pictures/no-profile-pic.jpg";
  if (!empty($searchtext)) {		
		$search_results_html .= '<div class="search-suggestions_results">';
		$comm_query = search_api_query('nodes_index');
		$comm_query->keys($searchtext);
		$comm_query->range(0,4);
		$comm_results = $comm_query->execute();
		$content_search = $people_search = FALSE;
		if($comm_results['result count'] > 0) {
			$content_search = TRUE;
			$search_results_html .= '<div class="search-suggestions__header margin-bottom-default"><a href="' . url('search-results') . '?type=commercial&sort_by=search_api_relevance&keyword=' . $searchtext . '" class="btn btn-sm btn-success pull-right btn-icon">View All Commercial Results <span class="icon icon-triangle-right"></span></a><h3>Commercial Results</h3></div><ul class="list-clean">';
			foreach ($comm_results['results'] as $result_details) {
				$node_details = $result_details['entity'];
				$attributes = array();
				$attributes['html'] = TRUE;
				$node_path = $base_url . url('node/' . $node_details->nid);
			  if(!empty($node_details->field_external_url)){
					$node_path = $node_details->field_external_url['und'][0]['value'];
					$ext_url_params = parse_url($node_path);
					if(isset($ext_url_params['host']) && $ext_url_params['host'] != $_SERVER['HTTP_HOST']){
						$attributes['target'] = '_blank';
					}
				}     
				$node_link = l($node_details->title, $node_path, array('attributes' => $attributes));
				if($node_details->type == "career_learning") {
					if(isset($node_details->field_requires_gene_vpn_['und'][0]['value']) && $node_details->field_requires_gene_vpn_['und'][0]['value'] == 1 && !empty($node_details->field_external_url)) {
						$node_link .= '<span class="pulse"></span>';
					}
				}
				$search_results_html .= '<li><h4>' . $node_link . '</h4><small class="search-suggestions__page-location">' . date('m.d.Y', $node_details->created) . ' / '. $node_path .'</small></li>';
			}
			$search_results_html .= '</ul>'; 
		}
		
		// LDAP People Search Results
		db_set_active('gene_ldap');    
		$query = db_select('ldap_users', 'u');
		$query->fields('u', array('unixid', 'email', 'phone_number', 'mobile_number', 'mail_stop', 'work_location', 'building', 'guid', 'preferred_fullname', 'cost_center_name', 'room_number'));
		$c = db_or()
			->condition('associatedetails', db_like($searchtext) . '%', 'LIKE')
			->condition('associatedetails', '% ' . db_like($searchtext) . '%', 'LIKE')
			->condition('indexsearch', db_like($searchtext) . '%', 'LIKE')
			->condition('indexsearch', '% ' . db_like($searchtext) . '%', 'LIKE');
		$query->condition('status', 1, '=');
		$query->condition($c);
		$query->addExpression('CASE WHEN associatedetails LIKE :db_condition_placeholder_1 THEN 2 WHEN associatedetails LIKE :db_condition_placeholder_2 THEN 1 ELSE 0 END','order_col');
		$query->orderBy('order_col','DESC');
		$query->range(0, 10);
		$result = $query->execute();
		db_set_active('default');
		
		if($result->rowCount() > 0) {
			$people_search = TRUE;
			$search_results_html .= '<div class="search-suggestions__header margin-bottom-default"><a href="' . url('search-people') . '?keyword=' . $searchtext . '" class="btn btn-sm btn-success pull-right btn-icon">View All People Results <span class="icon icon-triangle-right"></span></a><h3>People Results</h3></div><div class="people_results row no-gutters">';

			$default_profile_pic = file_create_url("public://filegPQEFT");
			foreach ($result as $userdet) {
				$display_name = $userdet->preferred_fullname . " (" . $userdet->unixid . ")";
				$email_id = $userdet->unixid . '@gene.com';
				$user_path = $base_url .  "/user";
				$picture_fid = db_select('users','u')
					->fields('u',array('picture'))
					->condition('name',$userdet->unixid,'=')
					->execute()
					->fetchField();
				if($picture_fid>0){
					$picture_info = file_load($picture_fid);
					$picture_web = file_create_url($picture_info->uri);
				}
				else{
					//$picture_web = 'http://gwiz4.gene.com/databases/empdir/portraits/' . $userdet->guid . '_1.jpg';
					$picture_web = $default_profile_pic;
				}	  
				$phone_number = !empty($userdet->phone_number) ? $userdet->phone_number : 'n/a';
				$mobile_number = !empty($userdet->mobile_number) ? $userdet->mobile_number : 'n/a';
				$mail_stop = !empty($userdet->mail_stop) ? $userdet->mail_stop : 'n/a';
				$work_location = !empty($userdet->work_location) ? $userdet->work_location : 'n/a';
				$building = !empty($userdet->building) ? $userdet->building : 'n/a';
				$room_number = !empty($userdet->room_number) ? $userdet->room_number : 'n/a';
				$on_pic_load_error = 'onerror=this.onerror=null;this.src="' . $default_profile_pic . '"';
				$search_results_html .=  '<div class="people_results__block col-sm-6 col-md-6 col-lg-4">
										<div class="people_results__photo">
											<img class="search-suggestions__person-photo" src="'. $picture_web .'" alt="'. $display_name . '" title="'. $display_name . '" ' . $on_pic_load_error . '/>
										</div>
										<div class="people_results__info">
											<h4 class="margin-00">'. $display_name . '</h4>
											<address>
												<a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to='. $email_id. '">'. $email_id. '</a><br>
												<span><strong>p.</strong> '. $phone_number .'</span>
												<span><strong>m.</strong> '. $mobile_number .'</span>
												<span><strong>Mail Stop:</strong>'. $mail_stop .'</span>
												<span><strong>Location:</strong> '. $work_location . '</span>
												<span><strong>Building:</strong> '. $building .', Room ' . $room_number . '</span>
											</address>
										</div>
									</div>';
			}
			$search_results_html .= '</div>';
		}
		
		//If no commercial and people search results
		if($content_search == FALSE && $people_search == FALSE){
			$search_results_html .= '<div class="search-suggestions__header"><h3 class="pull-left">No results found</h3></div>';
		}		
		$search_results_html .= '</div>';
  }
  echo $search_results_html;
  exit();
}*/

/**
 * Menu callback; Search results on user key press in quick search box of media archive, toolbox and home page orgchart page.
 */
function quick_search_autocomplete() {
  $searchtext = $_REQUEST['searchtext'];
  $type = $_REQUEST['type'];
  $search_results_html = "";
  global $base_url;	
  if (!empty($searchtext) && $type == "media") {
      $result = db_select('node')->fields('node', array('title', 'nid'))->condition('type', 'image_gallery', '=')->condition('status', 1, '=')->condition('title', '%'. db_like($searchtext) . '%', 'LIKE')->range(0, 5)->execute();
    foreach ($result as $nodedet) {
      $url = $base_url . "/media-archive/" . $nodedet->nid;
      $title = $nodedet->title;
      $search_results_html .= "<li><a href=\"$url\"><h5 class=\"margin-00\">$title</h5></a></li>";
    }
  }
  else if (!empty($searchtext) && ($type == "career_learning")) {
	$query = db_select('node','n');
	$query->leftJoin('field_data_field_external_url', 'eu', 'n.nid=eu.entity_id');
	$query->leftJoin('field_data_field_requires_gene_vpn_','rv','rv.entity_id=eu.entity_id');
	$query->fields('n', array('title', 'nid'))->fields('eu', array('field_external_url_value'))->fields('rv', array('field_requires_gene_vpn__value'))->condition('type', $type)->condition('status', 1)->condition('n.created',REQUEST_TIME,'<='); 

	$termquery = db_select('taxonomy_term_data','td');
	$termquery->fields('td', array('name', 'tid'))->condition('vid', 6); 
		$result = $query->condition('title', '%'. db_like($searchtext) . '%', 'LIKE')->range(0, 10)->execute();
		$termresult = $termquery->condition('name', '%'. db_like($searchtext) . '%', 'LIKE')->range(0, 10)->execute();
		$i = 1;
    foreach ($result as $nodedet) {
		$attributes = array();
		$dest_url = url('node/' . $nodedet->nid);
		if(!is_null($nodedet->field_external_url_value)){
			$dest_url = $nodedet->field_external_url_value;
			$ext_url_params = parse_url($nodedet->field_external_url_value);
			if(isset($ext_url_params['host']) && $ext_url_params['host'] != $_SERVER['HTTP_HOST']){
				$attributes['target'] = '_blank';
			}
			$title = (isset($nodedet->field_requires_gene_vpn__value) && $nodedet->field_requires_gene_vpn__value == 1)  ? $nodedet->title."<span class='pulse'></span>" : $nodedet->title;
		} else {
			$title = $nodedet->title; 
		}
      $search_results_html .= "<li>" . l( "<h5 class='margin-00'>" . $title . "</h5>", $dest_url, array('html' => TRUE, 'attributes' =>$attributes)) . "</li>";				
	  $i++;
    }
	if($i < 5) {
		$j = $i;
		//Adding taxonomy term to quick search autocomplete of career and learning
		foreach ($termresult as $termdet) {
			$attributes = array();
			if($j > 5) {
				break;	
			}
			$dest_url = url('career-and-learning/' . $termdet->tid);
			$search_results_html .= "<li>" . l( "<h5 class='margin-00'>" . $termdet->name . "</h5>", $dest_url, array('html' => TRUE)) . "</li>";				
			$j++;
		}
	}
  }
  else if (!empty($searchtext) && ($type == "toolbox")) {
	$query = db_select('node','n');
	$query->leftJoin('field_data_field_external_url', 'eu', 'n.nid=eu.entity_id');
	$query->fields('n', array('title', 'nid'))->fields('eu', array('field_external_url_value'))->condition('type', $type)->condition('status', 1)->condition('n.created',REQUEST_TIME,'<='); 
/*     if($searchtext == "all") {
	    $result = $query->orderBy('nid', 'DESC')->range(0, 5)->execute();
    }else { */
		$result = $query->condition('title', '%'. db_like($searchtext) . '%', 'LIKE')->range(0, 5)->execute();
    //}
    foreach ($result as $nodedet) {
		$attributes = array();
		$dest_url = url('node/' . $nodedet->nid);
		if(!is_null($nodedet->field_external_url_value)){
			$dest_url = $nodedet->field_external_url_value;
			$ext_url_params = parse_url($nodedet->field_external_url_value);
			if(isset($ext_url_params['host']) && $ext_url_params['host'] != $_SERVER['HTTP_HOST']){
				$attributes['target'] = '_blank';
			}			
		}
      $search_results_html .= "<li>" . l( "<h5 class='margin-00'>" . $nodedet->title . "</h5>", $dest_url, array('html' => TRUE, 'attributes' =>$attributes)) . "</li>";				
    }
  }
  echo $search_results_html;
  exit();	
}


/**
 * To fetch left sidebar search block
 */
function get_search_by_categories(){
	$data = array();
	$categoriesVocabDetails = taxonomy_vocabulary_machine_name_load("categories");
	$data['categories'] = array();
	if(isset($categoriesVocabDetails->vid)){
		$data['categories'] = taxonomy_get_tree($categoriesVocabDetails->vid);		
	}
	$announcementTypeVocabDetails = taxonomy_vocabulary_machine_name_load("announcement_type");
	$data['announcement_type'] = array();
	if(isset($announcementTypeVocabDetails->vid)){
		$data['announcement_type'] = taxonomy_get_tree($announcementTypeVocabDetails->vid);		
	}
	$searchedIds = array();
	if(isset($_GET['field_category']) && is_array($_GET['field_category']) && count($_GET['field_category'])>0){
		foreach($_GET['field_category'] as $field){
			if($field > 0){
				$searchedIds[] = $field;
			}
		}
	}
	if(isset($_GET['field_announcement_type']) && is_array($_GET['field_announcement_type']) && count($_GET['field_announcement_type'])>0){
		foreach($_GET['field_announcement_type'] as $field){
			if($field > 0){
				$searchedIds[] = $field;
			}
		}
	}
	$data['searched_ids'] = $searchedIds;
	$searchedContentTypes = array();
	if(isset($_GET['contenttype']) && is_array($_GET['contenttype']) && count($_GET['contenttype'])>0){
		$pages = array('microsite', 'career_learning', 'toolbox');
		foreach($_GET['contenttype'] as $contentType){
			if(in_array($contentType, $pages)){
				$searchedContentTypes[] = 'pages';
			}else if($contentType == 'downloads'){
				$searchedContentTypes[] = 'downloads';
			}
		}
	}
	$data['searched_content_types'] = $searchedContentTypes;
	return theme('search_by_categories', array('data' => $data));
}

/**
 * Gives Search form
 */
function get_header_search_form(){
	$data = array();
	$placeholder = isset($_GET['queryTerm']) ? $_GET['queryTerm'] : t('');
	$data['placeholder'] = $placeholder;
	return theme('header_search_form', array('data' => $data));
}

/**
 * Menu Callback; searches text in top 20 gwiz news.
 */
function search_gwiz() {
  $data = gwiz_search_results();
  $my_data = "<p class='gwiz_text'>Commercial Portal will redirect you to gWiz for your search results. To access gWiz content, please connect via Pulse (<span class='pulse'></span>) Secure.</p>";
  $keyword = !empty($_GET['queryTerm']) ? $_GET['queryTerm'] : '';
  $results_data = array();
  if(!empty($data->response)) {
    $gwiz_results = $data->response;
    if(count($gwiz_results->docs) > 0){
      $count = 0;
      //dpm($gwiz_results->docs);
      foreach ($gwiz_results->docs as $key => $value) {
        $desc = $published_date = '';
        $date_arr = array();
        $title = $value->title[0];
        $url = $value->id;
        $published_date = $value->timestamp;
        $date_arr = explode("T", $published_date);
        $published_date = $date_arr[0];
        $description = !empty($value->content) ? $value->content : '';
        $searchtext = $title . " " . $description;
        if(!empty($description)) {$desc = truncate_utf8(strip_tags($description),200, TRUE); }
        if(!empty($keyword)) {
          if(stristr($searchtext, $keyword)) {
            $desc = preg_replace("/\w*?".preg_quote($keyword)."\w*/i", "<b>$0</b>", $desc);
            $results_data[] = "<h3 class='result-list__title'><a target=\"_BLANK\" href=\"$url\">$title<span class='pulse'></span></a></h3><p><span class='result-list__date'>$published_date</span> | <span class='result-list__location'>$url</span></p><p>" . $desc .  "</p>";
          }
        }
      }
    }
  }

  $limit = 10;
  $page = pager_default_initialize(count($gwiz_results->docs), $limit, 0);
  $offset = $limit * $page;
  $output = array(
    array(
      '#theme' => 'item_list',
      '#items' => array_slice($results_data, $offset, $limit),
      '#attributes'=>array('class'=>'result-list'),
    ),
    array(
      '#theme' => 'pager',
    ),
  );
  $final_output = drupal_render($output);
  $final_output =  $my_data.$final_output;
  if (!empty($keyword) && !empty($data->response)) {
    $gwiz_url = "http://gwiz.gene.com/gwp/site/gwiz/search/?site=gwiz-coll-news&client=gwiz-frontend&proxystylesheet=gwiz-frontend&output=xml_no_dtd&q=" . $keyword;
    $final_output .= "<p><a target='_BLANK' href=\"$gwiz_url\">View More On $keyword<span class='pulse'></span></a></p></li>";    	  
  }
  return $final_output;
}

/**
 * To fetch gWiz results for showing in block based on keyword
 */
function get_gwiz_results() {
	$data = gwiz_search_results();
  $keyword = !empty($_GET['queryTerm']) ? $_GET['queryTerm'] : '';
  $results_data = "";
  if(isset($data->response)){
    $gwiz_results = $data->response;
    if(count($gwiz_results->docs) > 0){
      $results_data = "<div class='module'><ol class='result-list'>";
      $i=1;
      foreach($gwiz_results->docs as $key => $res_val) {
        $title = $res_val->title[0];
        $url = $res_val->id;
        $content = $res_val->content;
        $content = !empty($content) ? truncate_utf8($content,100, TRUE) : '';
        $content .= (!empty($content) && strlen($content)>100) ? '...' : '';
        $results_data .= "<li><span class='people-list__name'><a target=\"_BLANK\" href=\"$url\">$title<span class='pulse'></span></a></span></li>";
        $results_data .= "<li><span>" . $content .  "</span></li>";
        $i++;
        if($i > 5){
          break;
        }
      }
      $results_data .= "</ol>";
      if($i > 5){
        $view_url = "/view_more_gwiz_results/$keyword";
        $results_data .= "<p><a class='pull-right view-more margin-ll' target='_BLANK' href=\"$view_url\">View More<span class='icon-triangle-right'></span><span class='pulse'></span></a></p>";
      }
      $results_data .= "</div>";
    }
  }
  return $results_data;
}

/**
 * Function to get Epa results.
 */
function epa_reports($param = NULL) {
	global $user;
	$questions = array(
		'1qa'=>'1. Throughout the lifetime of the partnership I have invested time and energy fostering trust and open communication with my partner.',
		'1qb' =>'2. At the outset of the partnership I communicated my team\'s capabilities and value proposition to my partner.',
		'1qc'=>'3. I understand the formal and informal organizational structure so I can connect others within the organization and act as an effective resource between functions and projects.',
		'1qd' =>'4. As new options arise I have invited people to try out and test new things rather than use authority or pressure to compel them whenever possible.',
		'1qe'=>'5. I have taken deliberate steps to create safe ways for my partner to get help and information without feeling embarrassed or being put on the spot.',
		'2qa'=>'6. At the outset of the partnership I used strategic questions to understand the business needs and desired outcomes of my partner.',
		'2qb'=>'7. I have identified and established critical touch points to ensure sufficient communication throughout the life of the partnership.',
		'2qc'=>'8. I have identified goals, interests, and ideal outcomes in advance of each interaction I hold with my partner.',
		'2qd'=>'9. When my partner\'s expectations are not aligned I have discussed the situation with my partner and my manager.',
		'2qe'=>'10. At the outset of the partnership I helped my partner break up long-term goals into more frequent milestones that encourage steady progress to help achieve challenging goals.',
		'3qa'=>'11. At the outset of each project with this partner I developed and got agreement on a communication plan and a detailed project timeline with built-in touch points.',
		'3qb'=>'12. At the outset of each project with this partner I clarified timeline, budgets, responsibilities, headcount, and resources and provide clear updates when details change.',
		'3qc'=>'13. At the outset of each project with this partner I ensured everyone was clear and committed to the results we are trying to achieve.',
		'3qd'=>'14. During each project with this partner I have gathered and shared frequent measures of progress, milestones and/or results to ensure projects stay on track.',
		'3qe'=>'15. During each project with this partner I have gathered and shared frequent measures of progress, milestones and/or results to ensure projects stay on track.',
		'4qa'=>'16. Throughout the partnership I have identified the toughest moments or biggest obstacles to success and created contingency plans to overcome these challenges. ',
		'4qb'=>'17. I have reviewed and understand my partner\'s one to three year plans so my advice is timely and strategic.',
		'4qc'=>'18. I have made decisions concerning the partnership with a long-term focus in mind.',
		'4qd'=>'19. I have tracked effort, budgets, challenges, successes, and other data from past or current activities and use the data to inform future decisions and strategic direction.',
		'4qe'=>'20. I have provided data-backed advice to influence my partner\'s future business decisions.',
	);
	$query = "SELECT Question,Section, SUM(CASE WHEN (Response = 'disagree') THEN 1 ELSE 0 END) AS Disagree, SUM(CASE WHEN (Response = 'neutral') THEN 1 ELSE 0 END) AS Neutral, SUM(CASE WHEN (Response =  'strongly disagree') THEN 1 ELSE 0 END) AS Strongly_Disagree, SUM(CASE WHEN (Response =  'agree')
THEN 1 ELSE 0 END) AS Agree, SUM(CASE WHEN (Response =  'strongly agree') THEN 1 ELSE 0 END) AS Strongly_Agree FROM epa_sections_responses GROUP BY Question";
$result = db_query($query);
$data = $result->fetchAll();
 $header = array(
		'question'=>t('Question'),
		'section' => t('Section'),
    'stronglydisagree' => t('Strongly Disagree'),
    'disagree' => t('Somewhat Disagree'),
    'neutral'    => t('Neutral'),
    'agree'  => t('Somewhat Agree'),
		'stronglyagree'    => t('Strongly Agree'),
  );
	$table_data = array();
	foreach($data as $key => $val) {
		$table_data[$key]['question'] = $questions[$val->Question];
		$table_data[$key]['section'] = $val->Section;
		$table_data[$key]['stronglydisagree'] = $val->Strongly_Disagree;
		$table_data[$key]['disagree'] = $val->Disagree;
		$table_data[$key]['neutral'] = $val->Neutral;
		$table_data[$key]['agree'] = $val->Agree;
		$table_data[$key]['stronglyagree'] = $val->Strongly_Agree;
		
		$table_rows[$key] = array(
      'data' => array(
        'question'   => array(
          'data'  =>$questions[$val->Question],
        ),
				'section'   => array(
          'data'  =>$val->Section,
        ),
				 'stronglydisagree'   => array(
          'data'  => $val->Strongly_Disagree,
        ),
				 'disagree'   => array(
          'data'  =>$val->Disagree ,
        ),
				 'neutral'   => array(
          'data'  =>$val->Neutral ,
        ),
				 'agree'   => array(
          'data'  => $val->Agree,
        ),
				 'stronglyagree'   => array(
          'data'  => $val->Strongly_Agree,
        ),
			),
		);
	}
	$attributes = array('id'=>'table-articles','class'=>'articles');
	$table = array('header' => $header, 'rows' => $table_rows);
	$html = "<div class='excel-export'><a href='/admin/structure/epa-survey-results-export'>Export Data to Excel</a></div>";
	$html .= theme('table', $table,$attributes);
	
	if($param) {
		return $table_data;
	} else {
		return $html;
	}
}

/**
 * EPA Survey Excel Report
 */
function epa_survey_results_export() {
	$header = array(t('Question'),t('Section'),t('Strongly Disagree'),t('Somewhat Disagree'),t('Neutral'),t('Somewhat Agree'),t('Strongly Agree'));  
	$results = epa_reports('report_data');
	$xls_content_row = $xls_content_header = $xls_content = " ";
	foreach ($results as $result) {
		$data =  array();
		$data[] = $result['question'];
		$data[] = $result['section']; 
		$data[] = $result['stronglydisagree'];
		$data[] = $result['disagree'];
		$data[] = $result['neutral'];
		$data[] = $result['agree'];
		$data[] = $result['stronglyagree'];
		$xls_content_row .= implode("\t", array_values($data)) . "\r\n";
	}
	$xls_content_header = implode("\t", array_values($header)) . "\r";
  $xls_content .= $xls_content_header . "\n" . $xls_content_row;
	
	/* EPA detailed user report with Score */
	$epa_user_results = db_query("SELECT * FROM {epa_sections}")->fetchAll();
	$xls_content .= "\r\n\r\n\r\n";	
	$header = array(t('Unix Id'),t('Section'),t('Score'),t('RQAA Level'),t('Submitted On')); 
	$xls_content .= implode("\t", array_values($header)) . "\r";	
	foreach($epa_user_results as $row) {
		$data =  array();
		$data[] = $row->UnixId;
		$data[] = $row->Section;
		$data[] = $row->Score;
		$data[] = $row->RQAA_Level;
		$data[] = $row->Timestamp;		
		$xls_content .= implode("\t", array_values($data)) . "\r\n";		
	}
	
	$filename = 'EPA_Survey_Results_'. date('mdY') .'.xls';
	
	drupal_add_http_header('Content-type', 'application/vnd.ms-excel; charset=ISO-8859-1');
	drupal_add_http_header('Content-Disposition', 'attachment; filename="'.$filename.'"');
	drupal_add_http_header('Cache-Control', 'max-age=0');
	print $xls_content;
	exit();	 
}
/**
 * Implements System settings form.
 */
function featured_courses() {
  $featured_classes = get_featured_classes();
	$total = 0;
  $total = count($featured_classes);
  $form['total_no_of_featured'] = array (
    '#markup' => '<div>Total no. of featured courses are: '.$total.'</div>',
	);
  $form['featured_course_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of courses to display on the Find a class page'),
    '#default_value' => variable_get('featured_course_limit', ''), 
    '#element_validate' => array('element_validate_integer_positive'),
  );
  return system_settings_form($form);
}
/**
 * Page callback for arrangement of Featured courses.
 */
function goto_draggable_view() {
  return views_embed_view('featured_courses_draggable_view','page');
}

/**
 * To fetch left sidebar solr search block
 */
function get_solr_search_by_categories(){
	$data = array();
	/* $categoriesVocabDetails = taxonomy_vocabulary_machine_name_load("categories");
	$data['categories'] = array();
	if(isset($categoriesVocabDetails->vid)){
		$data['categories'] = taxonomy_get_tree($categoriesVocabDetails->vid);		
	} */
	$announcementTypeVocabDetails = taxonomy_vocabulary_machine_name_load("announcement_type");
	$data['announcement_type'] = array();
	if(isset($announcementTypeVocabDetails->vid)){
		$data['announcement_type'] = taxonomy_get_tree($announcementTypeVocabDetails->vid);		
	}
	$searchedIds = array();
	/* if(isset($_GET['field_category']) && is_array($_GET['field_category']) && count($_GET['field_category'])>0){
		foreach($_GET['field_category'] as $field){
			if($field > 0){
				$searchedIds[] = $field;
			}
		}
	} */
	if(isset($_GET['field_announcement_type']) && is_array($_GET['field_announcement_type']) && count($_GET['field_announcement_type'])>0){
		foreach($_GET['field_announcement_type'] as $field){
			if($field > 0){
				$searchedIds[] = $field;
			}
		}
	}
	$data['searched_ids'] = $searchedIds;
	$searchedContentTypes = array();
	if(isset($_GET['contenttype']) && is_array($_GET['contenttype']) && count($_GET['contenttype'])>0){
		$pages = array('microsite', 'career_learning', 'toolbox');
		foreach($_GET['contenttype'] as $contentType){
			if(in_array($contentType, $pages)){
				$searchedContentTypes[] = 'pages';
			}else if($contentType == 'downloads'){
				$searchedContentTypes[] = 'downloads';
			}
		}
	}
	$data['searched_content_types'] = $searchedContentTypes;
	return theme('solr_search_by_categories', array('data' => $data));
}

function gwiz_search_results() {
  $keyword = !empty($_GET['queryTerm']) ? $_GET['queryTerm'] : '';
  $data = "";
  $params = array('queryTerm' => $keyword);
  $gwiz_solr_url = variable_get('gwiz_solr_url') . http_build_query($params);
  $options = array(
   'headers' => array(
     'Content-Type' => 'application/json',
   ),
   'method' => 'GET',
  );
  $response = drupal_http_request($gwiz_solr_url, $options);
  $data = json_decode($response->data);
  return $data;
}