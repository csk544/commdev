<?php
	$sortFields = array();
	$sortFields['search_api_relevance'] = t("Relevance");
	$sortFields['created'] = t("Most Recent");
	$sortFieldValue = isset($_GET['sort_by']) ? $sortFields[$_GET['sort_by']] : t("Relevance");
	$sortField = (isset($_GET['sort_by']) && isset($sortFields[$_GET['sort_by']])) ? $_GET['sort_by'] : 'search_api_relevance';
?>
<div id="sidebar-first" class="sidebar">
          <h2 class="hd-title margin-bottom5 hidden-xs hidden-sm"><?php echo t('Filter Your Search');?></h2>
          <div class="module">
            <div>
              <!-- Header/Collapse Trigger Small Down -->
              <h2 class="hd-title margin-bottom5 visible-xs visible-sm"><?php echo t('Filter Your Search');?></h2>

              <div class="search-filter">
			  <?php 
			    $sortClass = (arg(0) == "search-results") ? "" : "hide";
			  ?>
                <h3 class="hd-title <?php echo $sortClass;?>"><?php echo t('Sort by:');?></h3>
                <div class="module <?php echo $sortClass;?>">
                  <div class="input-group drop-input">
                    <input type="text" class="form-control input-btn-right em-holder" id="sortfield" data-sortfield="<?php echo $sortField;?>" value="<?php echo $sortFieldValue;?>" readonly placeholder="<?php echo $sortFieldValue;?>" />
                    <div class="input-group-btn">
                      <button class="btn btn-clean" type="button"><span class="icon-triangle-down"></span>
                      </button>
                    </div>
                  </div>
                  <div class="drop-panel">
                    <div class="drop-inner">
                      <ul>
                        <li><a href="javascript:void(0);" id="relevance"><h5 class="margin-00"><?php echo t('Relevance');?></h5></a>
                        </li>
                        <li><a href="javascript:void(0);" id="mostrecent"><h5 class="margin-00"><?php echo t('Most Recent');?></h5></a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <h3 class="hd-title no-margin"><?php echo t("By Source:");?></h3>
                <div class="checkbox no-margin-top">
				<?php
				$checked = 'checked="checked"';
				$disabled = 'disabled="disabled"';
				$commercialchecked = (isset($_GET['type']) && $_GET['type'] == "commercial") ? $checked : '';
				$peoplechecked = (arg(0) == "search-people" && empty($commercialchecked)) ? $checked : '';
				$gwizchecked = (arg(0) == "search-gwiz") ? $checked : '';
				$disabled = (arg(0) == "search-results") ? '' : $disabled;
				$commercialNewsChecked = (arg(0) == "search-results" && isset($_GET['commercialNews']) && $_GET['commercialNews'] == 1) ? $checked : '';
				$peopleAnnouncmentsChecked = (arg(0) == "search-results" && isset($_GET['peopleAnnouncments']) && $_GET['peopleAnnouncments'] == 1) ? $checked : '';
				$micrositePagesChecked = (arg(0) == "search-results" && in_array('pages', $data['searched_content_types'])) ? $checked : '';
				$documentsPagesChecked = (arg(0) == "search-results" && in_array('downloads', $data['searched_content_types'])) ? $checked : '';
				?>
                  <label><input type="checkbox" id="commercial-page" <?php echo $commercialchecked;?> value="<?php echo t("Commercial");?>" /><?php echo t("Commercial");?></label>
                </div>
                <div class="checkbox">
                  <label><input type="checkbox" id="gwiz-page" <?php echo $gwizchecked;?> value="<?php echo t("gWiz");?>"><?php echo t("gWiz");?><span class="pulse"></span></label>
                </div>
                <!--<div class="checkbox">
                  <label>
                    <input type="checkbox" value="USMA">USMA</label>
                </div>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" value="other source">Other Source TBD</label>
                </div>-->
                <div class="checkbox">
                  <label><input type="checkbox" id="people-page" <?php echo $peoplechecked;?> value="People" /><?php echo t("People");?></label>
                </div>
				<h3 class="hd-title margin-top20"><?php echo t("By Type:");?></h3>
                <div class="checkbox">
                  <label>
                    <input type="checkbox" value="People" <?php echo $disabled;?> id="downloads-content" <?php echo $documentsPagesChecked;?>  ><?php echo t("Documents");?></label>
                </div>
                <div class="announcement-category">
                  <p class="bold"><?php echo t("By Category:");?></p>
                <?php if(count($data['announcement_type'])>0):?>
                <?php foreach($data['announcement_type'] as $announcementTypeDetails):?>
                <?php $peopleCatChecked = in_array($announcementTypeDetails->tid, $data['searched_ids']) ? $checked : '';?>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" class="people-announcements-terms" <?php echo $disabled;?> value="<?php echo t($announcementTypeDetails->name); ?>" <?php echo $peopleCatChecked; ?> data-field="field_announcement_type" data-tid="<?php echo $announcementTypeDetails->tid;?>" /><?php echo t($announcementTypeDetails->name); ?>
					</label>
                  </div>
				  <?php endforeach; ?>
				  <?php endif; ?>
                </div>
                
              </div>
            </div>
          </div>
        </div>