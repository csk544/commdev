<?php
	$sortFields = array();
	$sortFields['search_api_relevance'] = t("Relevance");
	$sortFields['created'] = t("Most Recent");
	$sortFieldValue = isset($_GET['sort_by']) ? $sortFields[$_GET['sort_by']] : t("Relevance");
	$sortField = (isset($_GET['sort_by']) && isset($sortFields[$_GET['sort_by']])) ? $_GET['sort_by'] : 'search_api_relevance';
  $sortClass = (arg(0) == "solr-search-results" || arg(0) == 'solr-search') ? "" : "hide";
  $checked = 'checked="checked"';
  $disabled = 'disabled="disabled"';

  $commercialchecked = (isset($_GET['type']) && $_GET['type'] == "commercial") ? $checked : '';
  $peoplechecked = (arg(0) == "search-people" && empty($commercialchecked)) ? $checked : '';
  $gwizchecked = (arg(0) == "search-gwiz") ? $checked : '';
  $disabled = (arg(0) == "solr-search-results" || arg(0) == 'solr-search') ? '' : $disabled;

  $documentsPagesChecked = (in_array(arg(0), array("solr-search-results", "solr-search")) && in_array('downloads', $data['searched_content_types'])) ? $checked : '';
  if (isset($_GET['sort_by']) && $_GET['sort_by'] == 'created') {
    $selected = 'selected';
  }
  $queryTerm = $_GET['queryTerm'] ?: '';
?>

<div id="sidebar-first" class="sidebar">

  <div class="search-field">
    <form action="/solr-search" class="search-form">
      <input type="text" class="search-input" placeholder="Search" name="queryTerm" required id="search-term" value="<?php print $queryTerm; ?>"/>
      <button type="submit" class="search-again-button">
        <span class="icon icon-Icon_Search"></span>
      </button>
    </form>
  </div>

  <div class="drop-down-block">
    <div class="title <?php echo $sortClass;?>"><?php echo t('Sort By');?></div>
    <div class="drop-down-styling <?php echo $sortClass;?>">
      <select class="sort-by">
        <option value="search_api_relevance" <?php ?> ><?php echo t('Relevance');?></option>
        <option value="created" <?php print $selected; ?>><?php echo t('Most Recent');?></option>
      </select>
    </div>
  </div>

  <div class="filter-by-block">
    <div class="title"><?php echo t("By Source");?></div>
    <div class="checkbox-style">
      <input type="checkbox" id="commercial-page" <?php echo $commercialchecked;?> value="<?php echo t("Commercial");?>" class="by_source" data-target="#results" />
      <label for="filter-by-commercial"><?php echo t("Genentech US CMG");?></label>
      <span class="checkmark"></span>
    </div>
    <div class="checkbox-style">
      <input type="checkbox" id="gwiz-page" <?php echo $gwizchecked;?> value="<?php echo t("gWiz");?>" class="by_source" data-target="#gwiz" />
      <label for="filter-by-gwiz"><?php echo t("gWiz");?><span class="pulse"></span></label>
      <span class="checkmark"></span>
    </div>
    <div class="checkbox-style">
      <input type="checkbox" id="people-page" <?php echo $peoplechecked;?> value="People" class="by_source" data-target="#people"/>
      <label for="filter-by-people"><?php echo t("People");?></label>
      <span class="checkmark"></span>
    </div>
  </div>

  <div class="filter-by-block">
    <div class="title"><?php echo t("By Type");?></div>
    <div class="checkbox-style">
      <input type="checkbox" value="People" <?php echo $disabled;?> id="downloads-content" <?php echo $documentsPagesChecked;?>>
      <label for="filter-by-documents"><?php echo t("Documents");?></label>
      <span class="checkmark"></span>
    </div>
  </div>

  <?php if (!empty($data['announcement_type'])): ?>
    <div class="filter-by-block">
      <div class="title"><?php echo t("By Category:");?></div>
      <?php foreach ($data['announcement_type'] as $type): ?>
        <?php $peopleCatChecked = in_array($type->tid, $data['searched_ids']) ? $checked : ''; ?>
        <?php $id = drupal_html_id($type->name); ?>
        <div class="checkbox-style">
          <input type="checkbox" class="people-announcements-terms" <?php echo $disabled;?> value="<?php echo t($type->name); ?>" <?php echo $peopleCatChecked; ?> data-field="field_announcement_type" data-tid="<?php echo $type->tid;?>" id="<?php print $id?>"/>
          <label for="<?php print $id?>"> <?php echo t($type->name); ?> </label>
          <span class="checkmark"></span>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
  
</div>
