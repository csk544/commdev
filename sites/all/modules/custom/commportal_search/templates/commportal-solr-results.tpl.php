<div class="result-list">
  <div class="search-tab-header">
    <div class="title"><?php print t("Results"); ?> <span class="num">(<?php print $total;?>)</span></div>
    <a href="#results" class="tab-toggle"><div class="view-all">View All</div></a>
  </div>
  <div class="view-content">
    <?php foreach ($data as $result): ?>
    <article class="search-result">
      <div class="item-content">
        <h3 class="result-list__title">
          <?php print l(t($result['title'][0]), $result['url'], array('target' => '_blank')); ?>
        </h3>
        <p>
          <span class="result-list__date"> <?php print $result['created']; ?> </span> | <span class="result-list__location"> <?php print $result['url']; ?> </span>
        </p>
        <div class="excerpt">
          <p> <?php print $result['excerpt']; ?> </p>
        </div>
      </div>
    </article>
    <?php endforeach; ?>
  </div>
</div>