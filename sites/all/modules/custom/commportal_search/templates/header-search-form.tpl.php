<div class="container">
	<div class="row">
	<!-- //Search Again -->
		<div class="col-sm-12 search-again">
		  <form class="form-horizontal">
			<div class="form-group">
			  <label for="search-term" class="col-md-4 col-lg-3 control-label h1 no-padding-right no-margin-top margin-bottom15">Search Results:</label>
			  <div class="col-md-5 col-lg-7 margin-bottom10">
				<input type="text" class="form-control input-lg" autocomplete="off" id="search-term" placeholder="" value="<?php echo $data['placeholder'];?>">
				<!--<div class="search-suggestions">
				</div>-->
			  </div>
			  <div class="col-md-3 col-lg-2 margin-bottom10">
				<button type="submit" class="btn btn-primary btn-block btn-lg search-again-button">Search Again</button>
			  </div>
			</div>
		  </form>
		</div>
	<!-- //End Search Again -->
	</div>
</div>