<?php

/**
 * @file
 * page callback file for the commportal secured module - Activate/Decativate authoring mode, handline google+ response attributes.
 */
 
/* Menu callback; Response function after authentication to handle Google+ attributes */
function response_googleplus() {
  if (isset($_GET['code'])) {
    $code = $_GET['code'];
    global $base_url;	
    global $user;	
    $url = 'https://accounts.google.com/o/oauth2/token';
    $params = array(
      "code" => $code,
      "client_id" => "921157342837-788tovrffjmksjm4cbdl4agdk4orpdu9.apps.googleusercontent.com",
      "client_secret" => "DNSj8HxY78HIAnVz2wnZiBcx", 
      "redirect_uri" => $base_url . "/response-googleplus",
      "grant_type" => "authorization_code"
    );
	
	
	$url = "https://accounts.google.com/o/oauth2/token";
    $ch = curl_init();
    $options = array( 
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_VERBOSE => true,
      CURLOPT_URL => $url ,
      CURLOPT_POST => true,
	  CURLOPT_POSTFIELDS => $params,
	);
    curl_setopt_array($ch , $options);
    $output = curl_exec($ch);
	// $request = new HttpRequest($url, HttpRequest::METH_POST);
    //$request->setPostFields($params);
    //$request->send();
    $responseObj = json_decode($output);
    $result = file_get_contents("https://www.googleapis.com/plus/v1/people/me?access_token=" . $responseObj->access_token);
    $result_data = json_decode($result, true);
    /* Saving response from google+ to user profile details */
    $account = user_load($user->uid);
    $update_account = array();
    if(array_key_exists('displayName', $result_data)) {
      $update_account = array('field_display_name' => array('und' => array(0 => array('value' => $result_data['displayName']))),
        'field_given_name' => array('und' => array(0 => array('value' => $result_data['name']['givenName']))),
        'field_family_name' => array('und' => array(0 => array('value' => $result_data['name']['familyName']))));  
    }
    if(array_key_exists('image', $result_data)) {
	  if(!empty($result_data['image']['url'])) {
	    $splitimg = explode("?", $result_data['image']['url']);
		$imageresponse = drupal_http_request($splitimg[0]);	   
		$file = file_save_data($imageresponse->data, 'public://');	
		$update_account['picture'] = $file;
	  }	
    }	
	user_save($account, $update_account);
	drupal_set_message("Google+ Profile Picture got Updated");
	drupal_goto("home");	

  }

}

/**
 * Menu callback; Update Google+ Picture
 */
function udpate_googlepicture() {
	global $base_url;
	if (stristr($base_url, 'comm.gene.com')) {			
	  //Google+ Authorization after SAML authentication 
	  $url = "https://accounts.google.com/o/oauth2/auth";
	  $params = array(
		"response_type" => "code",
		"client_id" => "921157342837-788tovrffjmksjm4cbdl4agdk4orpdu9.apps.googleusercontent.com",
		"redirect_uri" => $base_url . "/response-googleplus",
		"scope" => "profile",
	  );
	  $request_to = $url . '?' . http_build_query($params);
	  header("Location: " . $request_to);
	  exit; 	 
	} else {
	  header("Location: https://plus.google.com/me");
	  exit; 	 	
	}	
}

/**
 * Menu callback; Activate / Deactivate Authoring Mode
 */
function author_mode() {  
  global $user;	
	$user = user_load($user->uid);
  $groups = $user->field_author_groups['und'][0]['value'];	
  if (!empty($groups) && $user->uid != 1) {
    $account = user_load($user->uid);
    if (count($user->roles) > 1) {
      $user->roles = array(DRUPAL_AUTHENTICATED_RID => 'authenticated user');			
    }
    else {
      if(stristr($groups, 'drupal_author')) {
        $user->roles[4] = 'Author';
      }
      if(stristr($groups, 'drupal_superuser')){
        $user->roles[5] = 'Super Author';
      }
      if(stristr($groups, 'drupal_business_admin')){
        $user->roles[7] = 'Business Administrator';
      }	  
      if(stristr($groups, 'drupal_it_admin')){
        $user->roles[3] = 'IT Administrator';
      }
      if(stristr($groups, 'drupal_career_learning_admin')){
        $user->roles[8] = 'Career & Learning Admin';
      }
      if(stristr($groups, 'drupal_microsite_creator')){
        $user->roles[10] = 'Cross Functional Author';
      }
    }			
	user_save($account, array('roles' => $user->roles));			
  } 
	if (isset($_GET['dest']) && !empty($_GET['dest'])) {
		drupal_goto($_GET['dest']);
	}	
	else {
		drupal_goto();
	}
}

/**
 * Menu callback; Impersonate Users Form
 */
function impersonate_users($form, &$form_state){
	global $user;
	$form = array();    
	$form['impersonate_unixid'] = array(
		'#type' => 'textfield',
		'#title' => t('Unix ID'),
		'#description' => t('Choose Unix ID'),
		'#autocomplete_path' => 'autosuggest-unixid',
		'#required' => TRUE
	);  
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Impersonate')
	);
	return $form;
}

/**
 * Menu Callback; Impersonate Users Form Submit
 */
function impersonate_users_submit($form, &$form_state) {
  $unixid = $form_state['values']['impersonate_unixid'];
  $uload = user_load_by_name($unixid);    
  if(!empty($uload) && $uload->uid != 1) {
     user_external_login_register($unixid, 'secured_content');
     drupal_goto();
  } else {
     drupal_set_message("User doesn't exist!", 'error');  
  }
}

/**
 * Menu callback; Role Targeting admin form
 */
function role_targeting_form($form, &$form_state) {
  $form['grant_role_items']['#tree'] = TRUE;
  $result = db_query('SELECT id, ldapgroup, roledisplayname, weight FROM {role_targeting} ORDER BY weight ASC');
  $max_query = db_query('SELECT max(id) FROM {role_targeting}');
  $max_index = $max_query->fetchField();
  $addn_items = 10;
  $form['grant_role_max_index'] = array(
    '#type' => 'hidden',
    '#default_value' => $max_index,
  );
  foreach ($result as $item) {
    $form['grant_role_items'][$item->id] = array(
      'description' => array(
        '#type' => 'textfield',
        '#default_value' => trim($item->roledisplayname),
        '#size' => 40,
        '#maxlength' => 255,
      ),
      'ldap' => array(
        '#type' => 'textfield',
        '#default_value' => trim($item->ldapgroup),
        '#size' => 40,
        '#maxlength' => 255,
      ),
    );
  }
  
  for ($i = 1; $i <= $addn_items; $i++) {
    $form['grant_role_items'][$max_index + $i] = array(
      'description' => array(
        '#type' => 'textfield',
        '#default_value' => '',
        '#size' => 40,
        '#maxlength' => 255,
      ),
      'ldap' => array(
        '#type' => 'textfield',
        '#default_value' => '',
        '#size' => 40,
        '#maxlength' => 255,
      ),
    );
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array( '#type' => 'submit', '#value' => t('Save Changes'));
  return $form;
}

/**
 * Menu callback; submit handler for role Targeting admin form
 */ 
function role_targeting_form_submit($form, &$form_state) {
  foreach ($form_state['values']['grant_role_items'] as $id => $item) {
    if ($id > $form_state['values']['grant_role_max_index']) {
      if ($item['ldap']) {
        db_insert('role_targeting')
          ->fields(array('id' => $id, 'ldapgroup' => $item['ldap'], 'roledisplayname' => $item['description']))
          ->execute();
      }
    }
    else {
      if ($item['ldap']) {
        db_update('role_targeting')
          ->condition('id', $id)
          ->fields(array('ldapgroup' => $item['ldap'], 'roledisplayname' => $item['description']))
          ->execute();
      }
      else {
        db_delete('role_targeting')
          ->condition('id', $id)
          ->execute();
      }
    }
  }
  drupal_set_message("Saved Successfully!");
}

/**
 * Menu callback; Theme function for role Targeting admin form
 */ 
function theme_role_targeting_form($variables) {
  $form = $variables['form'];
  $rows = array();
  $count = 1;
  foreach (element_children($form['grant_role_items']) as $id) {
    $form['grant_role_items'][$id]['weight']['#attributes']['class'] = array('item-weight');
    $rows[] = array(
      'data' => array(
	    $count,
        drupal_render($form['grant_role_items'][$id]['description']),
		drupal_render($form['grant_role_items'][$id]['ldap']),
      ),
     // 'class' => array('draggable'),
    );
	$count++;
  }
  $header = array('S.No', 'Role Display Name', 'LDAP Group');
  $table_id = 'items-table';
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => $table_id)));
  $output .= drupal_render_children($form);
  //drupal_add_tabledrag($table_id, 'order', 'sibling', 'gri-item-weight');
  return $output;
}

/**
 * Menu callback; Custom message for access denied page. The users will be redirected to this function if ldap groups and role targeting mismatches.
 */ 
function access_denied_users(){
  return "<strong>Sorry, You are not authorized to access this webpage!</strong>";	
}

/**
 * Menu callback; Create Users Form
 */
function create_users($form, &$form_state){
	//global $user;
	$form = array(); 
	$form['info'] = array(
		'#markup' => t('Add user in portal by fetching details from LDAP.'),
	);	
	$form['create_unixid'] = array(
		'#type' => 'textfield',
		'#title' => t('Unix ID'),
		'#description' => t('Choose Unix ID'),
		'#autocomplete_path' => 'autosuggest-unixid',
		'#required' => TRUE
	);  
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Add User')
	);
	return $form;
}

/**
 * Menu Callback; Create Users Form Submit
 */
function create_users_submit($form, &$form_state) {
  $unixid = $form_state['values']['create_unixid'];
	$duplicate_query = db_select('users', 'u');
	$duplicate_query->fields('u', array('name'));
	$duplicate_query->condition('name', $unixid, '=');
	$duplicate_results = $duplicate_query->execute()->fetchAll();
	if($duplicate_results)	{
			drupal_set_message(t('User "'. $unixid . '" already exist in portal.'), 'error');	
	}
  else {	
		db_set_active('gene_ldap');    
		$query = db_select('ldap_users', 'u');
		$query->fields('u', array('unixid','email','first_name','last_name'));
		$query->condition('unixid', $unixid, '=');
		$query->condition('status', 1);
		$results = $query->execute()->fetchAll();
		db_set_active('default');
		if ($results) {
			$new_user = array(
			'name' => $results[0]->unixid, 'mail' => $results[0]->email, 'status' =>'1', 'pass' => rand(), 'init' => $results[0]->email);
			$new_user['field_first_name'][LANGUAGE_NONE][0]['value'] = $results[0]->first_name;
			$new_user['field_last_name'][LANGUAGE_NONE][0]['value'] = $results[0]->last_name;
			$new_user['field_display_name'][LANGUAGE_NONE][0]['value'] = $results[0]->first_name . ' ' . $results[0]->last_name;
			$account= user_save(NULL, $new_user);
			user_set_authmaps($account, array('simplesamlphp_auth' => $account->name));
			if($account){
				drupal_set_message(t('User "'. $unixid . '"   has been added successfully.'));			
				drupal_goto("admin/structure/manage-roles");
			}
		}
		else {
			drupal_set_message(t('User "'. $unixid . '" doesn\'t exist in LDAP.'), 'error');	
		}
	}	
}