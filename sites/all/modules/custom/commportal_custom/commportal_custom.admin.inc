<?php

/**
 * @file
 * Admin page callback file for commportal custom module.
 */

/**
 * Form builder; Configure emergency notice settings for this site.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function emergency_notice() { 
  //configuration settings for emergency notice
  drupal_set_title("Emergency Notice");
  $notice = variable_get('emergency_notice', '');
	$form['notice_title'] = array(
		'#type' => 'textfield',
		'#title' => t('Title'),
		'#default_value' => variable_get('notice_title', ''),
		'#required' => TRUE
  ); 
  $form['emergency_notice'] = array(
    '#type' => 'text_format',
    '#title' => t('Emergency Notice'),
    '#default_value' => isset($notice['value']) ? $notice['value'] : '',	
		'#format' => isset($notice['format']) ? $notice['format'] : 'full_html',	 
		'#description' => t('Emergency notices will be displayed across the entire portal, directly below the header.'),    
  );    
  $form['notice_type'] = array(
		'#type' => 'select',
		'#title' => t('Notice Type'),
		'#options' => array(1 => 'Warning', 0 => 'Error'),
		'#default_value' => variable_get('notice_type', ''),
		'#required' => TRUE
  ); 
  $form['notice_status'] = array(
		'#type' => 'select',
		'#title' => t('Status'),
		'#options' => array(1 => 'Active', 0 => 'Inactive'),
		'#default_value' => variable_get('notice_status', ''),
		'#required' => TRUE
  );
  return system_settings_form($form);
}

/**
 * Home Page -  Career & Learning News block
 */
function career_learning_settings() { 
	global $base_url;
  drupal_set_title("Career & Learning News");
	
	$form['section_1'] = array(
		'#type' => 'fieldset', 
		'#title' => t('Section 1'), 
		'#weight' => 4, 
		'#collapsible' => TRUE, 
		'#collapsed' => FALSE,
	);
	$form['section_1']['category_title_1'] = array(
		'#type' => 'textfield',
		'#title' => t('Category Title'),
		'#default_value' => variable_get('category_title_1', ''),
		'#required' => TRUE,
		'#maxlength' => 55,
		'#description' => t('Max character length is 55'),
  ); 
	$form['section_1']['news_title_1'] = array(
		'#type' => 'textfield',
		'#title' => t('Title'),
		'#maxlength' => 55,
		'#description' => t('Max character length is 55'),
		'#default_value' => variable_get('news_title_1', ''),
  );
	$form['section_1']['desc_1'] = array(
		'#type' => 'textfield',
		'#title' => t('Description'),
		'#maxlength' => 55,
		'#description' => t('Max character length is 55'),
		'#default_value' => variable_get('desc_1', ''),
  ); 
	$form['section_1']['url_1'] = array(
		'#type' => 'textfield',
		'#title' => t('URL'),
		'#default_value' => variable_get('url_1', ''),
		'#description' => t('Provide internal or external URL. Example: http://gwiz.gene.com'),
		'#required' => TRUE
  ); 
	$form['section_1']['image_1'] = array(
		'#type' => 'managed_file',
		'#title' => t('Background image'),
		'#widget' => 'imce',
		'#upload_location' => 'public://assets',
		'#styles' => FALSE,
		'#preview' => TRUE,
		'#upload_validators' => array('file_validate_extensions' => array('jpeg jpg png')),
		'#default_value' => variable_get('image_1', ''),
	);
	$form['section_2'] = array(
		'#type' => 'fieldset', 
		'#title' => t('Section 2'), 
		'#weight' => 5, 
		'#collapsible' => TRUE, 
		'#collapsed' => FALSE,
	);
	$form['section_2']['category_title_2'] = array(
		'#type' => 'textfield',
		'#title' => t('Category Title'),
		'#maxlength' => 55,
		'#description' => t('Max character length is 55'),
		'#default_value' => variable_get('category_title_2', ''),
		'#required' => TRUE
  ); 
	$form['section_2']['news_title_2'] = array(
		'#type' => 'textfield',
		'#title' => t('Title'),
		'#maxlength' => 55,
		'#description' => t('Max character length is 55'),
		'#default_value' => variable_get('news_title_2', ''),
  );
	$form['section_2']['desc_2'] = array(
		'#type' => 'textfield',
		'#title' => t('Description'),
		'#maxlength' => 55,
		'#description' => t('Max character length is 55'),
		'#default_value' => variable_get('desc_2', ''),
  ); 
	$form['section_2']['url_2'] = array(
		'#type' => 'textfield',
		'#title' => t('URL'),
		'#default_value' => variable_get('url_2', ''),
		'#description' => t('Provide internal or external URL. Example: http://gwiz.gene.com'),		
		'#required' => TRUE
  ); 
	$form['section_2']['image_2'] = array(
		'#type' => 'managed_file',
		'#title' => t('Background image'),
		'#widget' => 'imce',
		'#styles' => FALSE,
		'#upload_location' => 'public://assets',
		'#preview' => TRUE,
		'#upload_validators' => array('file_validate_extensions' => array('jpeg jpg png')),
		'#default_value' => variable_get('image_2', ''),
	);
  $form['#submit'][] = 'career_learning_settings_form_submit';
  return system_settings_form($form);
}
function career_learning_settings_form_submit($form, &$form_state) {
  global $user;
	$files['image_1'] = file_load($form_state['values']['image_1']);
	$files['image_2'] = file_load($form_state['values']['image_2']);
	foreach($files as $index => $file) {
		if ($file) {
		  $file->status = FILE_STATUS_PERMANENT;
		  file_save($file);
		  variable_set($index, $file->fid);
		  file_usage_add($file, 'user', 'user', $user->uid);
		  unset($form_state['values'][$index]);
		}
	}
}

/**
 * Returns HTML for a managed file element with thumbnail.
 */
function theme_image_multifield_multitype($variables) {
  $element = $variables['element'];
  $output = '';
  $base = drupal_render_children($element); // renders element as usual
	if (isset($element['#file']->uri)) {
		$output .= '<div id="edit-logo-ajax-wrapper"><div class="form-item form-type-managed-file form-item-logo"><span class="file">';
		$output .= '<img height="50px" src="' . file_create_url($element['#file']->uri) . '" />';
		$output .= '</span><input type="submit" id="edit-' . $element['#name'] . '-remove-button" name="' . $element['#name'] . '_remove_button" value="Remove" class="form-submit ajax-processed">';
		$output .= '<input type="hidden" name="' . $element['#name'] . '[fid]" value="' . $element['#file']->fid . '">';
	}
	return $output;
}
/**
 * Menu Callback; Image editor option directly in administration menu.
 */
function image_editor() { 
  global $base_url;
  global $user;
  drupal_set_title("Image Editor");
  $image_output = "<iframe src=\"$base_url/imce?app=ckeditor&dir=assets\" style=\"width:100%;height:600px;\" width=\"100%\" height=\"600px;\"></iframe>";
  return $image_output;
}

/**
 * Menu Callback; Form builder; Page Edit - Home, Sitemap etc for changing approver group, author.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function general_page_settings($form, &$form_state) {
	$form['home'] = array(
		'#type' => 'fieldset', 
		'#title' => t('Area Authors list'),
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,
	);
	$authors_list = get_authors_list();
	$form['home']["home_authors_list"] = array(
		'#type' => 'select',
		'#title' => t('Home Page - Authors List'),
		'#options'=> $authors_list,
		'#multiple' => TRUE,
		'#default_value' => variable_get("home_authors_list", ''),
		'#description' => t("Choose authors to have editable access for this page."),
	);	
	$form['home']["career_learning_authors_list"] = array(
		'#type' => 'select',
		'#title' => t('Career & Learning - Authors List'),
		'#options'=> $authors_list,
		'#multiple' => TRUE,
		'#default_value' => variable_get("career_learning_authors_list", ''),
		'#description' => t("Choose authors to have editable access for Career & Learning content."),
	);
	$form['home']["about_commercial_authors_list"] = array(
		'#type' => 'select',
		'#title' => t('About Commercial - Authors List'),
		'#options'=> $authors_list,
		'#multiple' => TRUE,
		'#default_value' => variable_get("about_commercial_authors_list", ''),
		'#description' => t("Choose authors to have editable access for About Commercial content."),
	);
	$form['home']["tools_authors_list"] = array(
		'#type' => 'select',
		'#title' => t('Secure Tools - Authors List'),
		'#options'=> $authors_list,
		'#multiple' => TRUE,
		'#default_value' => variable_get("tools_authors_list", ''),
		'#description' => t("Choose authors to have editable access for Secure Tools content."),
	);	
	
	$form['general'] = array(
		'#type' => 'fieldset', 
		'#title' => t('Content Owners - General Pages'),
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);
	$form['general']["home_content_owner"] = array(
		'#title' => t('Home Page - Content Owner'),		
		'#type' => 'textfield',
		'#autocomplete_path' => 'user/autocomplete',	
		'#default_value' => variable_get("home_content_owner", 'admin'),
		'#description' => t("Author who receives notifications about the content."),
		'#required' => TRUE,
	);	
	$form['general']["search_results_content_owner"] = array(
		'#title' => t('Search Results Page - Content Owner'),		
		'#type' => 'textfield',
		'#autocomplete_path' => 'user/autocomplete',	
		'#default_value' => variable_get("search_results_content_owner", 'admin'),
		'#description' => t("Author who receives notifications about search content."),
		'#required' => TRUE,
	);
	$form['general']["tools_content_owner"] = array(
		'#title' => t('Tools Landing Page - Content Owner'),		
		'#type' => 'textfield',
		'#autocomplete_path' => 'user/autocomplete',	
		'#default_value' => variable_get("tools_content_owner", 'admin'),
		'#description' => t("Author who receives notifications about the content."),
		'#required' => TRUE,
	);
	$form['general']["media_archive_content_owner"] = array(
		'#title' => t('Multimedia Archive - Content Owner'),		
		'#type' => 'textfield',
		'#autocomplete_path' => 'user/autocomplete',	
		'#default_value' => variable_get("media_archive_content_owner", 'admin'),
		'#description' => t("Author who receives notifications about the content."),
		'#required' => TRUE,
	);		
	$form['general']["leadership_content_owner"] = array(
		'#title' => t('Leadership Page - Content Owner'),		
		'#type' => 'textfield',
		'#autocomplete_path' => 'user/autocomplete',	
		'#default_value' => variable_get("leadership_content_owner", 'admin'),
		'#description' => t("Author who receives notifications about the content."),
		'#required' => TRUE,
	);	
	
  return system_settings_form($form);
}

/**
 * Menu Callback; Home Page sidebar block.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function home_page_settings($form, &$form_state) {
	$form['home_page_block'] = array(
		'#type' => 'textfield',
		'#title' => t('Home Page - Sidebar Block'),
		'#autocomplete_path' => 'nodeid/autocomplete',
		'#default_value' => variable_get("home_page_block", ''),
		'#description' => t("Choose which content to display on the Home page sidebar block."),
		'#element_validate' => array('link_validate'),
	);	
  return system_settings_form($form);
}

/**
 * Menu Callback; Leadership Page Settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function leadership_page_settings($form, &$form_state) {
	$form['leadership_commercial_partner'] = array(
		'#type' => 'textfield',
		'#title' => t('Leadership Commercial Partners label'),
		'#default_value' => variable_get("leadership_commercial_partner", ''),
	);	
  return system_settings_form($form);
}

/**
 * Filter files - Nodes.
 */
function getAllFilefields($fid) {
	$filefield_nids = $node_ids = $fileFields = array();  
	$field_types = field_info_field_types();
	foreach (field_info_instances() as $existing_entity_type => $bundles) {
		foreach ($bundles as $existing_bundle => $instances) {
			foreach ($instances as $instance) {
				$field = field_info_field($instance['field_name']);
				if($field['type'] == 'image' || $field['type'] == 'file') {
					$fileFields[$instance['field_name']] = array(
						'type' => $field['type'],
						'type_label' => $field_types[$field['type']]['label'],
						'field' => $field['field_name'],
						'label' => $instance['label'],
						'widget_type' => $instance['widget']['type'],
					);
				}
			}
		}
	}
	foreach($fileFields as $key => $field) {
		$filefield_nids = db_query('SELECT entity_id FROM {field_data_'.$field["field"].'} WHERE '.$field["field"].'_fid = :fid', array(':fid' => $fid))->fetchAll();
		$node_ids = array_merge($node_ids, $filefield_nids);
	}
	return $node_ids;
}
/**
 * Page callback for autocomplete 
 * 
 */
function nodeid_autocomplete($string = '') {	
  $matches = array();
  $content_types = array('advpoll','containers','image','image_gallery','people','preview_blurb','thumbnail_text','video');
  if ($string) {
   $or = db_or()
          ->condition('nid', $string, '=')
          ->condition('title', '%' . db_like($string) . '%', 'LIKE');
    $and = db_and()
					->condition('status', 1, '=')
					->condition('type', $content_types, 'IN');						
    $result = db_select('node')->fields('node', array('nid', 'title'))->orderBy('created', 'DESC')->condition($and)->condition($or)->range(0, 10)->execute();
    foreach ($result as $node) {
      $matches[$node->nid] = check_plain($node->title) . ' [NID:' . check_plain($node->nid) . ']';
    }
  }
  drupal_json_output($matches);
}

function _querycomponent_uploaded_files_nodes($form, &$form_state) {
  $form = array();
  $file_fid = arg(4);
  $fileuri = db_query("SELECT fm.uri FROM {file_managed} fm INNER JOIN {file_usage} fu ON fm.fid = fu.fid WHERE fm.fid = :fid AND fu.module = 'imce'", array(':fid' => $file_fid))->fetchField();
  if (!empty($fileuri)) {
    if (strstr($fileuri, "public://")) {
      $filepath = preg_replace_callback('#public://([^/]+)/([^?]+)#', 'fileurlencode', $fileuri);
      if (!strcmp($fileuri, $filepath)) {
        $filepath = preg_replace_callback('#public://([^/]+)#', 'fileurlencode', $fileuri);
      }
    }
    else {
      $filepath = preg_replace_callback('#sites/default/files/([^/]+)/([^?]+)#', 'fileurlencode', $fileuri);
    }
    $wysiwyg_nids = db_query('SELECT entity_id FROM {field_data_body} WHERE body_value LIKE :filename', array(':filename' => '%' . db_like($filepath) . '%'))->fetchAll();
  }
	$node_ids = getAllFilefields($file_fid);
  if (!empty($node_ids)) {
    $node_titles = array();
    foreach ($node_ids as $value) {
      $node_title    = db_query('SELECT title FROM {node} WHERE nid = :nodeid', array(':nodeid' => $value->entity_id))->fetchField();
      $node_titles[] = "<a href = '/node/" . $value->entity_id . "' target = '_blank'>" . $node_title . "</a>";
    }
    $form['uploaded_file_nodes'] = array(
      '#markup' => theme('item_list', array('items' => $node_titles)) . '<br/>',
    );
  }
  else {
    $form['uploaded_file_nodes'] = array(
      '#markup' => t('No Nodes <br/><br/>'),
    );
  } 
  
  $form['actions']['cancel'] = array(
    '#type'   => 'submit',
    '#value'  => t('Back'),
    '#access' => TRUE,
    '#weight' => -10,
    '#submit' => array('_uploaded_files_nodes_back'),
    '#limit_validation_errors' => array(),
  );
  return $form;
}
function fileurlencode($match) {
	if (array_key_exists('2', $match)) {
		return $match[1] . '/' . join('/', array_map('rawurlencode', explode('/', $match[2])));
	} else {
		return rawurlencode($match[1]);
	}
}

/**
 * Custom back button callback.
 */
function _uploaded_files_nodes_back($form, &$form_state) {
  $url = @$_GET['destination'] ? @$_GET['destination'] : 'admin/find-content/find-files';
  drupal_goto($url);
}

function _querycomponent_uploaded_files_title($file_fid) {
  $filename = db_query('SELECT filename FROM {file_managed} WHERE fid = :fid', array(':fid' => $file_fid))->fetchField();
  return !empty($filename)? t('Views pages for @filename', array('@filename' => $filename)): t('No Nodes');
}

/**
 * Menu Callback; Departments Settings.
 */
function department_settings($form, &$form_state) {
	$departments = taxonomy_get_children(variable_get('dept_term_id',10));
	$usma_departments = taxonomy_get_children(variable_get('us_medical_dept_term_id'));
	$ga_departments = taxonomy_get_children(variable_get('ga_dept_term_id'));
	$partners = array();
	if(is_array($departments) && count($departments)>0){
		$form['unit_short_names'] = array(
			'#type' => 'fieldset',
			'#title' => t('Department Short Names'),
			'#collapsible' => TRUE,
			'#collapsed' => TRUE,
		); 
		foreach($departments as $dept_term_id => $department_details){
			$partners[$dept_term_id] = $department_details->name;
			$form['unit_short_names']['department_' . $dept_term_id . '_shorten_form'] = array(
				'#type' => 'textfield',
				'#title' => t($department_details->name . ' Shorten Form'),
				'#default_value' => variable_get('department_' . $dept_term_id . '_shorten_form', 'Our'),
				'#description' => t('Provide Shortened form for Department ' . $department_details->name)
			);
		}
		$form['department_partners'] = array(
			'#type' => 'select',
			'#title' => t('Partners'),
			'#options' => $partners,
			'#default_value' => variable_get('department_partners', array()),
			'#multiple' => TRUE,
		);
	}
	if(is_array($usma_departments) && count($usma_departments)>0){
		$form['usma_unit_short_names'] = array(
			'#type' => 'fieldset',
			'#title' => t('USMA Department Short Names'),
			'#collapsible' => TRUE,
			'#collapsed' => TRUE,
		); 
		foreach($usma_departments as $dept_term_id => $department_details){
			$partners[$dept_term_id] = $department_details->name;
			$form['usma_unit_short_names']['department_' . $dept_term_id . '_shorten_form'] = array(
				'#type' => 'textfield',
				'#title' => t($department_details->name . ' Shorten Form'),
				'#default_value' => variable_get('department_' . $dept_term_id . '_shorten_form', 'Our'),
				'#description' => t('Provide Shortened form for USMA Department ' . $department_details->name)
			);
		}
	}
	if(is_array($ga_departments) && count($ga_departments)>0){
		$form['ga_unit_short_names'] = array(
			'#type' => 'fieldset',
			'#title' => t('GA Department Short Names'),
			'#collapsible' => TRUE,
			'#collapsed' => TRUE,
		); 
		foreach($ga_departments as $dept_term_id => $department_details){
			$partners[$dept_term_id] = $department_details->name;
			$form['ga_unit_short_names']['department_' . $dept_term_id . '_shorten_form'] = array(
				'#type' => 'textfield',
				'#title' => t($department_details->name . ' Shorten Form'),
				'#default_value' => variable_get('department_' . $dept_term_id . '_shorten_form', 'Our'),
				'#description' => t('Provide Shortened form for GA Department ' . $department_details->name)
			);
		}
	}
  return system_settings_form($form);
}

/**
 *  To Redirect to Subnav and Misc content creation pages
 */
function redirect_microsite($arg2){
	$array = array('subnav', 'misc', 'kmmicrosite', 'kmmisc', 'kmsubnav');
	if(in_array($arg2, $array)){
		$link = 'node/add/microsite/' . $arg2;
	}else{
		$link = '';
	}
	drupal_goto($link);
}

/**
 * To show the Tags Subscribed Count
 */
function tags_subscribe_count(){
	global $user;
	$uid = $user->uid;
	$query = db_select('users', 'u');						
  $query->join('field_data_field_follow_tags', 'ft', 'u.uid=ft.entity_id');
	$results = $query->fields('ft', array('field_follow_tags_value'))
							->fields('u', array('uid'))
							->condition('status', 1)
							->execute()->fetchAll();
	$total_tags = array();
	foreach($results as $result){
		$tags_str = $result->field_follow_tags_value;
		$tags_arr = explode(',', $tags_str);
		if(is_array($tags_arr) && count($tags_arr)>0){
			foreach($tags_arr as $tag){
				if(isset($total_tags[$tag]) && isset($total_tags[$tag]['count'])){
					$total_tags[$tag]['count']++;
				}else{
					$term_details = taxonomy_term_load((int)$tag);
					if(isset($term_details->name)){
						$total_tags[$tag]['name'] = $term_details->name;
						$total_tags[$tag]['count'] = 1;
					}
				}
			}
		}
	}
	$headers = array(
      array('data' => 'Name', 'field' => 'tag_name'),
      array('data' => 'Count', 'field' => 'tag_count', 'sort' => 'desc'),
  );
	$table_rows = array();
  if(is_array($total_tags) && count($total_tags)>0){
		foreach($total_tags as $tag_id => $tag_arr){
			$table_rows[] = array(
												'tag_name' => $tag_arr['name'],
												'tag_count' => $tag_arr['count']
											);			
		}
	}
	// getting the current sort and order parameters from the url
	$order = tablesort_get_order($headers);
	$sort = tablesort_get_sort($headers);
	// sort the table data accordingly (write your own sort function)
	$table_rows = tags_sort($table_rows, $order['sql'], $sort);
	$rows = array();
	$skip = false;
	if($_SERVER['REDIRECT_URL'] != '/admin/tags-subscribe-details' && count($table_rows)>10){
		$skip = true;
	}
	foreach ($table_rows as $entry) {
		if($skip && count($rows)>9){
			break;
		}
		$rows[] = array(
				array('data' => $entry['tag_name']),
				array('data' => $entry['tag_count']),
		);
	}
	if($skip){
		$rows[] = array(
				array('data' => ''),
				array('data' => l('More', url('admin/tags-subscribe-details')), 'class' => array('more-link')),
		);
	}else if($_SERVER['REDIRECT_URL'] == '/admin/tags-subscribe-details'){
		$rows[] = array(
				array('data' => ''),
				array('data' => l('Back to Dashboard', url('user/' . $uid . '/dashboard')), 'class' => array('more-link')),				
		);
	}
	$table = array('header' => $headers, 'attributes' => array(), 'rows' => $rows);
	return theme('table', $table);
}

/**
 * Sort Tags Subscribe List
 */
function tags_sort($table_rows, $sql, $sort){
	$sort_arr = array(
								'desc' => SORT_DESC,
								'asc' => SORT_ASC,
							);
	$sorted_data = array();
	foreach ($table_rows as $key => $row) {
    $sorted_data[$key]  = $row[$sql];
	}
	array_multisort($sorted_data, $sort_arr[$sort], $table_rows);
	return $table_rows;
}

/**
 * Returns HTML for a list of available entity bundles.
 *
 * @param array $variables
 *   An associative array containing:
 *   - content: An array of content types.
 *
 * @ingroup themeable
 * To change the Order based on Label name instead of Machine Name
 */
function custom_entityconnect_entity_add_list($variables) {
	$entities = $variables['items']; 
  $cache_id = $variables['cache id'];
	$sorted_data = array(); 
	if(array_key_exists('containers', $entities)) {
		$desc = isset($entities['containers']['description']) ? $entities['containers']['description'] : " ";
		unset($entities['containers']);
		$containers_list = array('career_profiles_list'=>'Career Profiles List:node/add/containers/career_profiles/'.  $cache_id,'carousel'=>'Carousel:node/add/containers/image/'.$cache_id,'downloads_list'=>'Downloads List:node/add/containers/downloads/'.$cache_id,'news_list'=>'News List:node/add/containers/article/'.$cache_id,'people_announcements_list'=>'People Announcements List:node/add/containers/announcements/'.$cache_id);
		foreach($containers_list as $key => $href) {
			$split = explode(":", $href);
			$label = $split[0];
			$href = $split[1];
			$entities[$key]['href']= $href;
			$entities[$key]['label']= $label; 
			$entities[$key]['description']= $desc;
		}
		foreach ($entities as $key => $row) { 
			$sorted_data[$key]  = $row['label']; 
		}
	} else { 
		foreach ($entities as $key => $row) { 
			$sorted_data[$key]  = $row['label'];
		}
	}
	array_multisort($sorted_data, SORT_ASC, $entities);
  if ($entities) { 
    $output = '<ul class="admin-list">';
    foreach ($entities as $item) {
			$output .= '<li class="clearfix"><span class="label">' . l($item['label'], $item['href'], array(
			'query' => array("build_cache_id" => $cache_id, "child" => TRUE))) . '</span>';
			if (array_key_exists('description', $item)) {
				$output .= '<div class="description">' . filter_xss_admin($item['description']) . '</div>';
			}
      $output .= '</li>';
    }
    $output .= '</ul>';
  }
  else {
    $output = '<p>' . t('You have not selected any entities.') . '</p>';
  }
  return $output;
}
