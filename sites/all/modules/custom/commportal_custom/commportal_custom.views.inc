<?php
/**
 * Implements hook_views_data().
 */
function commportal_custom_views_data() {   
    $data['suggest_tags']['table']['group'] = t('Suggest Tags');
    $data['suggest_tags']['table']['base'] = array(
		'field' => 'nid',
        'title' => t('Suggest Tags'),        
    );
	  $data['suggest_tags']['table']['join'] = array(
		'node' => array(
		  'left_field' => 'nid', 
		  'field' => 'nid',
		),
	  );
	$data['suggest_tags']['sid'] = array(
		'title' => t('Suggest Tags SID'),
		'help' => t('SID'),
		'field' => array(
			'handler' => 'views_handler_field_numeric',
			'click sortable' => TRUE,
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
		'filter' => array(
			'handler' => 'views_handler_filter_numeric',
		),
	);	  
  $data['suggest_tags']['nid'] = array(
    'title' => t('Suggest Tags NID'), 
    'help' => t('NID'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship', 
      'label' => t('Suggest Tags NID'),
    ),
  );
	$data['suggest_tags']['uid'] = array(
		'title' => t('Suggested By'),
		'help' => t('UID'),
		'field' => array(
			'handler' => 'views_handler_field_numeric',
			'click sortable' => TRUE,
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
		'filter' => array(
			'handler' => 'views_handler_filter_numeric',
		),
		'argument' => array(
			'handler' => 'views_handler_argument_numeric',
		),
	);	
	$data['suggest_tags']['tag'] = array(
		'title' => t('Tag'),
		'help' => t('TAG'),
		'field' => array(
			'handler' => 'views_handler_field',
			'click sortable' => TRUE,
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
		'filter' => array(
			'handler' => 'views_handler_filter_string',
		),
	);
	$data['suggest_tags']['comments'] = array(
		'title' => t('Admin Comments'),
		'help' => t('Comments'),
		'field' => array(
			'handler' => 'views_handler_field',
			'click sortable' => TRUE,
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
		'filter' => array(
			'handler' => 'views_handler_filter_string',
		),
	);

	$data['suggest_tags']['status'] = array(
		'title' => t('Status'),
		'help' => t('Status'),
		'field' => array(
			'handler' => 'views_handler_field',
			'click sortable' => TRUE,
		),
		'sort' => array(
			'handler' => 'views_handler_sort',
		),
		'filter' => array(
			'handler' => 'views_handler_filter_string',
		),
	);	
	$data['suggest_tags']['date'] = array(
		'title' => t('Date Submitted'),
		'help' => t('Date Submitted'),
		'field' => array(
			'handler' => 'views_handler_field_date',
			'click sortable' => TRUE,
		),
		'sort' => array(
			'handler' => 'views_handler_sort_date',
		),
		'filter' => array(
			'handler' => 'views_handler_filter_date',
		),
	);	
	$data['suggest_tags']['p_date'] = array(
		'title' => t('Date Processed'),
		'help' => t('Date Processed'),
		'field' => array(
			'handler' => 'views_handler_field_date',
			'click sortable' => TRUE,
		),
		'sort' => array(
			'handler' => 'views_handler_sort_date',
		),
		'filter' => array(
			'handler' => 'views_handler_filter_date',
		),
	);
  return $data;
}