(function($) {
  Drupal.behaviors.commportal = {
    attach: function(context) {
	  var base_url = Drupal.settings.commportal_custom.base_url;
		var uid = Drupal.settings.commportal_custom.uid;	
		//Hide text formats & row weights except for admin
		if (uid != 1){			
			$(".text-format-wrapper fieldset").hide();
			$(".tabledrag-toggle-weight").addClass("element-invisible");
			$("body").addClass("not-admin");
		}
	//Manage Suggestion Tags	
	$('.manage-suggestion-tags a.approve,.manage-suggestion-tags button.reject').unbind().click(function(event) {
		event.preventDefault();			
		var $this = $(this).parent().parent(),action = "approve",term;												
		term = $this.attr('term'); 
		if ($(this).hasClass('reject')) {
			action = "reject"; 
			//var comments = $this.find('textarea[name="pending_comments"]').val();
			var comments = $(this).siblings('textarea.comments').val();
		}	
		$this.text('Loading...');	
		$.ajax({type: 'POST',url: base_url + '/manage_tags/' + action + '/' + $this.attr('sid')  + '/' + $this.attr('nid'),data: {term: term, comments: comments},
			success: function(data) {					
				$this.text(data);
				$this.closest('td').prev().html(data);				
			}
		}); 	
	});
	
	$('table tr th.views-field-webform-component-2 a').each(function( index ) {
		var webform_comp = $(this).text();
		if (webform_comp[webform_comp.length-1] === ".") {
			webform_comp = webform_comp.slice(0,-1);
			$(this).text(webform_comp);
		}
	});
	
	$('.manage-suggestion-tags a.reject').unbind().click(function(event) {
	  $(this).parent().next().removeClass('element-invisible');
		$(this).closest("div.actions_links").addClass('element-invisible');
		event.preventDefault();			
		var $this = $(this).parent().next();
		/*$this.html("<textarea class='comments'>Similar Tag Already Exists.</textarea><br /><button class='reject'>Ok</button>");*/
		$this.children('textarea').select();
	});	
	//Pending Tasks	
	$('.home-pending-tasks a.approve,.home-pending-tasks button.reject').unbind().click(function(event) {
		event.preventDefault();			
		var $this = $(this).parent().parent(),action = "Approved",comments;
		if ($(this).hasClass('reject')) {
			action = "Rejected"; 
			comments = $("#pending_comments").val();	
		}	
		$this.text('Loading...');	
		$.ajax({type: 'POST',url: base_url + '/pending_tasks/' + action + '/' + $this.attr('nid'),data: {comments: comments},
			success: function(data) {					
				$this.text(data);							
			}
		});	 	
	});
	$('.home-pending-tasks a.reject').unbind().click(function(event) {
	  $(this).parent().next().removeClass('element-invisible');
		$(this).closest("div.actions_links").addClass('element-invisible');	
		event.preventDefault();			
		var $this = $(this).parent().next();
		/* $this.html("<textarea class='comments'></textarea><br /><button class='reject'>Ok</button>"); */
		$this.children('textarea').select();
	});	

	//Add reset button for popular links	 
	$("div#edit-field-links .link-field-subrow").each(function(index) {
		if ($(this).find("button").length == 0) {
			$('<img class = "add_icon" title="Upload Document" src = "/sites/default/files/assets/add.png"><button type="submit" class="clear_links">Clear</button>').appendTo($(this));  
		}
	});	
		
	$("#links-node-form #edit-field-links .link-attributes").each(function(index) {			
			$(this).find("label").text("Requires Gene VPN?");
	});	
	
	/* List of Links + Add documents using Popup */
	$('div.form-type-link-field .add_icon').click(function(event){           
			event.preventDefault(); 
			var url_id = "";
			$(this).closest('div.link-field-subrow').find('input').each(function() {
				url_id = $(this).attr('id');
			});	 
			var val = '/node/add/downloads/popup/'+ url_id;
			window.open(val, "popupWindow",  "width=600,height=600,scrollbars=yes");           
  });
	
	
	$('button.clear_links').unbind().click(function(event) {
		event.preventDefault();
		var $this = $( this ).parent().find('input');
		$this.val("");
		//$(this).closest('.draggable').remove();
	});
	//Feedback reply mail.
	$('.reply_feedback').click(function (event) {
		event.preventDefault();
		window.open($(this).attr("href"), "popupWindow", "width=600,height=600,scrollbars=yes");
	});
	
	//Tool tip for google analytics content
	
	//$("th.no-of-visits-help-text").attr("title", "Visits are the periods of time (also known as “sessions”) that visitors spend on a page. A visit is ended either after 30 minutes of inactivity or if the user leaves a page for more than 30 minutes (if a user leaves the page and returns within 30 minutes, this is counted as part of the original visit).");
	
    $("th.no-of-visits-help-text").append("<span class='num_of_visits'>Visits are the periods of time (also known as “sessions”) that visitors spend on a page. A visit ends either after 30 minutes of inactivity or if the user leaves a page for more than 30 minutes (if a user leaves the page and returns within 30 minutes, this is counted as part of the original visit).</span>" );

	$("th.no-of-views-help-text").append("<span class='num_of_views'>Pageviews is the number of times users open a page within the Commercial Portal. This covers all views…so if a user refreshes the page, or navigates away from the page to other Portal pages and then returns, all of these instances are counted as individual page views.</span>");
	
	$("th.time-on-page-help-text").append("<span class='time_on_page'>Time on Page is the average time between a particular visitor loading a page, and then loading a second page within the Commercial Portal. This metric has a maximum value of 30 minutes (see Visits). Each site within the Commercial Portal will have a different value for Time on Page calculated for it.</span>");
	
	$("th.bounce-rate-help-text").append("<span class='bounce_rate'>A “bounce” is a single view, in which the visitor enters a page and does not click or navigate anywhere else within the Commercial Portal. The “bounce rate” therefore is the percentage of single-site visits. In other words, bounce rate shows when users leave the Commercial Portal after viewing that page, regardless of how they got there or how long they stayed on that page. It is calculated as entrances divided by bounces. </span>");
	
	$("th.entrances-help-text").append("<span class='entrances'>Entrances counts the number of times a particular page within the Commercial Portal was the first one a user visited during their visit. Each page on the Commercial Portal will have a different entrance rate associated with it. </span>");
	
	$("th.exit-rate-help-text").append("<span class='exit_rate'>Exit Rate is the percentage of site exits from the page. In other words, this shows the percentage of users for whom the page was the last one they viewed before exiting the Commercial Portal. </span>");
	
	//Textfield clear when clicked on the clear button.
	$("#views-exposed-form-manage-roles-page #edit-reset").click(function (event) {	
		$('#edit-uid').val('');
	});
	// Mouseover image links
	$(".row-class").hover(function () { 	 	
		$(this).find(".views-field-nothing").show(); 
	}, function () {   	
		$(this).find(".views-field-nothing").hide();	 
	});	
	
	//Help text for related link in leadership content type.
	$("#edit-field-related-link-und-0-url").attr("placeholder", "Example: http://gwiz.gene.com");
	
	$(".find-files .views-reset-button").click(function (event) {		
		$(".find-files #edit-uid").val("");
		$(".find-files #edit-filename").val("");
	});
	
	//Limit teaser char count
	if ($("#edit-field-teaser .description").length != 0) {
		var characters = $("#edit-field-teaser #counter strong").text();
		$("#edit-field-teaser textarea").keyup(function(){ 
			if($(this).val().length > characters){
				$(this).val($(this).val().substr(0, characters));
			}
			var remaining = characters -  $(this).val().length;
			$("#edit-field-teaser .description #remaining").html("Remaining <b>"+remaining+"</b>.");
			if(remaining <= 10){
				$("#counter, #remaining").css("color","red");
			} else {
				$("#counter, #remaining").css("color","#666");
			}
		});
	}
	
	//Limit text char count in Highlight content type.
	if($(".node-preview_blurb-form #edit-field-text .description").length != 0) {
		var characters = $("#edit-field-text #counter strong").text();		
		$(".node-preview_blurb-form #edit-field-text input:text").keyup(function(){
			if($(this).val().length > characters){
				$(this).val($(this).val().substr(0, characters));
			}
			var remaining = characters -  $(this).val().length;
			$("#edit-field-text .description #remaining").html("Remaining <b>"+remaining+"</b>.");
			if(remaining <= 10){
				$("#counter, #remaining").css("color","red");
			} else {
				$("#counter, #remaining").css("color","#666");
			}
		});
	} 	
	
	// Position of Promo Image Carousel
		function setPosition() {
			if($('#promo-image-carousel-node-form table.field-multiple-table thead tr th').length == 2) {
				$('#promo-image-carousel-node-form table.field-multiple-table thead tr th:first').after().removeAttr('colspan');
				$('#promo-image-carousel-node-form table.field-multiple-table thead tr th:first').before('<th colspan="2" class="field-label"><label>POSITION</label></th>');
			}
			var count = 0; 
			$('#promo-image-carousel-node-form table.field-multiple-table tbody tr').each(function() {
				if($(this).children('td').length == 3) {
					count++; 
					$(this).find('td:first').next().before('<td class="position">'+count+'</td>');
				}
			});
		}
		function changeOrder() {
			var count = 0; 
			$('#promo-image-carousel-node-form table.field-multiple-table tbody tr').each(function() {
				$(this).children('td').each(function() {
					if($(this).hasClass('position')) {
						count++;
						$(this).html(count);
					}
				});
			});
		}
		var obj = $('body').find('#promo-image-carousel-node-form');
		if(obj.length) {
			setPosition();
			Drupal.tableDrag.prototype.row.prototype.onSwap = function () {
				changeOrder();
			};
		}
		$('.node-form .form-item-title > label').removeClass('element-invisible');
		$('.node-form').bind('mousedown : click : keyup', '.edit-field-tags-und', function() {
			if($('#edit_field_tags_und_chosen .chosen-results > li:first').text() == '- None -') {
				$('#edit_field_tags_und_chosen .chosen-results > li:first').remove();
			}
		});
		$("#taxonomy-form-term").submit(function(e) {
			if($(this).attr('action').match('tags/add') == 'tags/add') {
				e.preventDefault();
				var form = this;
				$.ajax({
					type: "POST",
					url: base_url + '/check_tag',
					data: {tag: $('#edit-name').val()},
					cache: false,
					success : function(result){callback(result, form)}
				}); 
			}      
		});
		var callback = function(result, form){
			if(result) {
				var status = confirm('Are you sure? Do you want to add a tag that is already exists?');
				if(status) {
					form.submit();
				}
			} else {
				form.submit();
			}
		};								 	

		/* To hide edit link when content is not available */
		$(".entityconnect-edit.autocomplete", context).each(function() {
			var edit = this;
			var first_val = $(this).siblings('.form-type-textfield').children("input").first().val();
			if(typeof first_val != 'undefined'){
				if(!$(this).siblings('.form-type-textfield').children("input").first().val().trim()){
					$(edit).addClass('hide');
				}
			}			
		});
		$(".field-widget-entityreference-autocomplete input[id^='edit-field-choose-']", context).bind('autocompleteSelect', function(event, node) {
			if($(this).val().trim()){
				$(this).siblings('.entityconnect-edit.autocomplete').removeClass('hide');
			}
		});
		$(".field-widget-entityreference-autocomplete input[id^='edit-field-choose-']", context).bind('blur', function(event, node) {
			if(!$(this).val().trim() && !$(this).siblings('.entityconnect-edit.autocomplete').hasClass('hide')){
				$(this).siblings('.entityconnect-edit.autocomplete').addClass('hide');
			}
		});	

		/* Remove icon for containers list */				
		$("div.field-type-entityreference .entityconnect-edit") .each(function(index) {
			if ($(this).find("img.remove-icon").length == 0) { 
				$('<img class = "remove-icon" title="Remove from list" src = "/sites/default/files/assets/remove-icon.png">').
				insertAfter($(this).children("input"));
				$(this).children("input").addClass('edit-icon');
			}
		}); 
		$("div.field-type-entityreference .entityconnect-add").children("input").addClass('add-icon');		
		$('img.remove-icon').click(function(event) {	  	
		event.preventDefault();
		var $this = $( this ).parent().parent().find('input');
		$this.val("");			
		$( this ).closest('tr').addClass('hide');
		});	
		
		
		}
  }
})(jQuery);