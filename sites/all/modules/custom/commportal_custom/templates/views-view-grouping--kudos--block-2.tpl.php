<?php

/**
 * @file
 * This template is used to print a single grouping in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $grouping: The grouping instruction.
 * - $grouping_level: Integer indicating the hierarchical level of the grouping.
 * - $rows: The rows contained in this grouping.
 * - $title: The title of this grouping.
 * - $content: The processed content output that will normally be used.
*/
$markup = '<li><strong class="level-0">'.$title.'<span class="accordion-open icon icon-triangle-down"></span><span class="accordion-closed icon icon-triangle-right"></span></strong>';
$markup .= '<div><ol class="media-archive-year">';
foreach($rows as $val) {
	$markup .= '<li>'.$val["group"].'</li>';
}
$markup .= '</ol></div></li>';
print $markup; 
?>

