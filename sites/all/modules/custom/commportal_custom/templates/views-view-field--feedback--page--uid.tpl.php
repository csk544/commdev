<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php 
	global $base_url;
	$output = "";
	$output .= '<div>';
	if(!empty($row->field_field_feedback_path[0]['raw']['value'])) {
		$path = trim($row->field_field_feedback_path[0]['raw']['value']);
		if ($row->users_node_uid > 0 && !empty($row->node_title)) {
			$user = user_load($row->users_node_uid);
			$to = $user->mail;
			$title = str_replace('&','and',$row->node_title);
			$url = "https://mail.google.com/mail/?view=cm&to=".$to."&su= " .$title;
			$output .= "<a href='".$url."' class='reply_feedback' onclick='return false;'>Reply</a>";
		}
		if(drupal_valid_path($path) && preg_match('/node/', $path)) {
			if ($row->users_node_uid > 0 && !empty($title)) {
				$output .= "<span> / </span>";
			}
			$node_path = "/".$path."/edit";
			$output .= "<a href = '".@$node_path."'>Edit page</a>";
		}
	}
	$output .= '</div>';
	print $output;
?>