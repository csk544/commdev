<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
 $results = $view->result;
 global $user;
 $tags = drupal_explode_tags($user->field_follow_tags['und'][0]['value']);
	if(count(variable_get('users_default_tags'))>0){
		foreach(variable_get('users_default_tags') as $default_tags_val){
			if(! in_array($default_tags_val,$tags)){
				$tags[] = $default_tags_val;
			}
		}
	}
?>
<div class="following-title">
	<h3><?php print $view->display['block']->display_title; ?></h3>
		<?php if($more){ print $more; } ?>
</div>


	<!-- Article Block (this part repeats) -->
	<?php 
	if(empty($results)){
		print "<p>There is no feed to show</p>";
	}else{	
    ?> <div class="scroll-feed fancy-scrollbar"> <?php
	
	foreach($results as $key => $val){ 
	    $following_tags = array();;
		foreach($val->field_field_tags as $key_tag => $val_tag){
			if(in_array($val_tag['raw']['tid'],$tags)){
				$following_tags[] = $val_tag['rendered']['#title'];
			}
		}
		$news_feed_source = "";
		if($val->field_field_is_this_a_usma_page[0]['raw']['value'] == 0){
		$news_feed_source = "Commercial";
		}else{
			$news_feed_source = "US Medical Affairs";
		}
		if(strlen($val->node_title) > 40) {	// Strip the length of title
			$wrapped = wordwrap($val->node_title, 40, "...#", true);
			$wrapped = explode("#", $wrapped);
			$title = $wrapped[0];	
		}else{
			$title = $val->node_title;
		}
		if(strlen($val->field_field_teaser[0]['raw']['value']) > 110) {	// Strip the length of teaser
			$wrapped = wordwrap($val->field_field_teaser[0]['raw']['value'], 110, "...#", true);
			$wrapped = explode("#", $wrapped);
			$teaser = $wrapped[0];	
		}else{
			$teaser = $val->field_field_teaser[0]['raw']['value'];
		}
	
	?>
		<article class="following-feed-item">
				<div class="container">
					<div class="row">
						<div class="following-feed-img" style="background-image: url('/sites/all/themes/cmgportal/library/img/USMA Awards 2018_USMA.jpg')"></div>
						<div class="following-feed-content">
							<h4 class="following-feed-title"><?php print $title; ?></h4>
							<span class="following-feed-date"><?php print date("F d, Y",$val->node_created); ?></span>
							<span class="following-feed-source"><?php print implode(", ",$following_tags); ?></span>
							<p class="following-feed-excerpt"><?php print $teaser; ?></p>
						</div>
					</div>
				</div>
		<?php 
			$attributes = array();
			$attributes['class'][] = 'following-feed-item-link';
			print l("", "node/".$val->nid, array('attributes' => $attributes)); 
		?>
	</article>
		
	<?php } ?>
	
	</div>
	<?php } ?>