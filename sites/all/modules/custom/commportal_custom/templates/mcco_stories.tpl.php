<?php
	global $user,$base_url;
	print '<img alt="" src="/sites/default/files/assets/mcco_Stories of Opp.jpeg" class="mcco_embedded_img"/>';
	print $data['add_story'];
	unset($data['add_story']);
	print '<div class = "mcco_story_page">';
	foreach($data as $k => $v) {
		$src = file_create_url($v['image']);
		$text = $v['text'];
		$title = strip_tags(substr($text, 0, 50))."...";
		$submitted_by = $v['submitted_by'];
		print '<div class="mcco_story col-sm-6">
			<a class="story_link" href="node/'.$k.'"><img alt="" src="'.$src.'" class="story_image" /></a>
			<div class="tile_content_wrapper" style="position:relative;">
				<div class="tile__social"> 
					<a href="mailto:?subject='.$title.'&body='.$base_url.'/node/'.$k.'" class="mail_icon"><span aria-hidden="true" class="icon-mail"></span></a>
					<span class="comment_count">('.$v["comment_count"].')</span><span class="icon-chat"></span><span class="like_count">('.$v["rate_results"].')</span><span class="icon-like"></span>
      </div>
			<a class="story_link" href="node/'.$k.'"><p>'.$text.'</p><span><strong>[Submitted by: '.$submitted_by.']</strong></span></a>
			</div>
		</div>';
	}
	print '</div>';
?>