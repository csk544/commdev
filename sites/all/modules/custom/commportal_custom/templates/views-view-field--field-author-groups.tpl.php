<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php
$roles_arr = array(
	'drupal_author' => 'Author',
	'drupal_superuser' => 'Super Author',
	'drupal_business_admin' => 'Business Administrator',
	'drupal_it_admin' => 'IT Administrator',
  'drupal_career_learning_admin' => 'Career & Learning Admin',
  'drupal_service_desk' => 'Service Desk',
  'drupal_microsite_creator' => 'Cross Functional Author',
);
if(count($row->field_field_author_groups) > 0) {
	$roles = array();
	$groups = drupal_explode_tags($row->field_field_author_groups[0]['raw']['value']);
	foreach($groups as $key => $val) {
		$roles[$val] = $roles_arr[$val];
	}
	asort($roles);
	$items['items'] = $roles;
	$items['title'] = '';
	$items['type'] = 'ul';
	print theme('item_list', $items);
}
 ?>
