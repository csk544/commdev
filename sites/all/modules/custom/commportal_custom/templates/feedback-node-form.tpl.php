<h2 class="hd-title no-border-top">Feedback</h2>
<p class="margin-bottom-default">Your feedback helps make CMG Portal better! Your comments will be sent to the author of this page.</p>
  <div class="row">
    <div class="col-sm-6">
      <div class="input-group">
			<span id="basic-addon1" class="input-group-addon">Author</span>
				<?php print render($form['feedback_author']);?>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="input-group">
			<span class="input-group-addon">
          <input type="radio" aria-label="..." class="toggle-readonly" checked="checked">
        </span>
			<?php print render($form['feedback_from']);?>
      </div>
    </div>
  </div>
  <!--<div class="row">
    <div class="col-sm-12">
      <div class="checkbox">
        <label><?php //print render($form['submit_anonymous']); ?>Submit anonymously</label>
      </div> 
    </div>
  </div>-->
  <div class="form-group ">
		<p class="feedback_error" style='color:red;display:none'>Comments cannot be left empty</p>
		<?php
		print render($form['feedback_desc']);?>
		<?php print render($form['submit']); ?>
		<?php print render($form['clear']);
		print drupal_render_children($form);
	  ?>
    <div class="clearfix"></div>
  </div>