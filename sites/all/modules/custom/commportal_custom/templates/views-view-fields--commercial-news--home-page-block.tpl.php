<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

  $img_url = file_create_url($row->field_field_image[0]['raw']['uri']);
  if (isset($row->field_field_image[0]['raw']['is_default']) && !empty($fields['field_department_news_thumbnail'])) {
    $img_url = file_create_url($row->field_field_department_news_thumbnail[0]['raw']['uri']);
  }
?>
<article class="news-feed-item">
  <div class="container">
    <div class="row">
      <div class="news-feed-img" style="background-image: url('<?php print $img_url; ?>')"></div>
      <div class="news-feed-content">
        <h4 class="news-feed-title">
          <?php print $fields['title']->content; ?>
        </h4>
        <span class="news-feed-date">
          <?php print $fields['created']->content; ?>
        </span>
        <span class="news-feed-source">
          <?php print empty($row->field_field_is_this_a_usma_page[0]['raw']['value']) ? t("Commercial") : t("US Medical Affairs"); ?>
        </span>
        <p class="news-feed-excerpt">
          <?php print $fields['field_teaser']->content; ?>
        </p>
      </div>
    </div>
  </div>
  <?php print l("", "node/" . $row->nid, array('attributes' => array('class' => array('news-feed-item-link'))));?>
</article>