 <?php
	global $theme_path;
	if($ajax_testing=='true'){
		if(count($choices)>0) {
			$output .= '<div><section>
      <!--/Class List Section--><ol class="find-class-list">';
			foreach($choices as $val) {
				$node = node_load($val);
				$month = array();
        //Preparing an array of 12 months from current month.
        for ($i = 0; $i <= 11; $i++) {
          $months[] = date("Y-m", strtotime( date( 'Y-m-01' )." +$i months"));
        }
				$months_list = array();
				$current_year = date('y');
				$next_year = $current_year+1;
        //Todo: Check the below foreach.
        foreach($node->field_course_date['und'] as $date_value){
          if(!(date('y', strtotime($date_value['value'])) < $current_year || date('y', strtotime($date_value['value'])) > $next_year)){
            $months_list[] = date('Y-m', strtotime($date_value['value']));	
          }
        }
				$intersect = array_intersect($months, $months_list);
        foreach($intersect as $month_numbers){
          $month[] = date('m', strtotime($month_numbers));	
        }			
				$result = array_intersect($month_num,$month);
				$month_name = array();
        foreach($result as $num){
          $month_name[] = date('F', mktime(0, 0, 0, $num, 10));		
        }
        $team_tid = getIntactteam_termid();
				$comp = array();
        foreach($node->field_career_learning_topic['und'] as $comp_value){
          $comp[] = $comp_value['tid'];		
        }
				$result1 =array_intersect($comp_tid,$comp);
				$name = array();
				foreach($result1 as $competency_id){
          $term = taxonomy_term_load($competency_id);
          $name[] = $term->name;	
        }
        $output .= '<li class="row-col"><div class="row"><div class="col-sm-12"><h3 class="result-list__title margin-bottom10">';
        if(!empty($node->field_external_url['und'][0]['value'])){
          $output .= l($node->title, $node->field_external_url['und'][0]['value'],array('attributes' => array('target'=> '_blank')));
        }else{
          $output .= l($node->title, 'node/'.$node->nid);
        }
        $output .= '</h3><p>'.$node->field_teaser_text['und'][0]['value'].'</p></div>';
        if (in_array($team_tid, $comp)) {
          $output .= '<div class="col-sm-6"><div class="row">';
          if(!empty($node->field_external_url['und'][0]['value'])){
            $output .= '<div class="col-sm-4">'.l('Read More', $node->field_external_url['und'][0]['value'], array('attributes' => array('target'=> '_blank','class'=> array('btn',' btn-block', 'btn-primary margin-lx')))).'</div>';
          }else{
            $output .= '<div class="col-sm-4">'.l('Read More', 'node/'.$node->nid, array('attributes' => array('class'=> array('btn',' btn-block', 'btn-primary margin-lx')))).'</div>';
          }
          $output .= '<div class="col-sm-12"><a href="https://docs.google.com/forms/d/1HmIgsdbOZjsOuoj1ZhYJtfdLUT_kjUWglpJ7pogsptM/viewform?c=0&w=1" target="_blank"class="btn btn-block btn-primary margin-lx">Initiate a Request for Team Training</a></div></div></div><div class="col-sm-6"><div class="row"><div class="col-sm-12"><div class="class-categories-container pull-right text-uppercase">'; 
        }else{
          $output .= '<div class="col-sm-3"><div class="row">';
          if(!empty($node->field_external_url['und'][0]['value'])){
            $output .= '<div class="col-sm-8">'.l('Read More', $node->field_external_url['und'][0]['value'], array('attributes' => array('target'=> '_blank','class'=> array('btn',' btn-block', 'btn-primary margin-lx')))).'</div>';
          }else{
            $output .= '<div class="col-sm-8">'.l('Read More', 'node/'.$node->nid, array('attributes' => array('class'=> array('btn',' btn-block', 'btn-primary margin-lx')))).'</div>';
          }
						$output .= '</div></div><div class="col-sm-9"><div class="row"><div class="col-sm-12"><div class="class-categories-container pull-right text-uppercase">';
				}
        if(!empty($month_name) || !empty($result1)){
          foreach($month_name as $month){
            $output .= '<span>'.$month.'</span>';					 
          }
          foreach($name as $competency){ 
            $output .= '<span>'.$competency.'</span>';
          }
        }
				$output.= '</div></div></div></div></div></li>';
			}
			$output .= '</ol><!--/End Class List Section--></section></div>'; 
		}else {
			$output .= "<h3>No Results found!!</h3>";
		}	 
		echo $output;
	}
	else {
		echo '<div id="replace-find-results"><h2 style="color:#6FA140;">Browse available classes for yourself or your team by competency or month.</h2></div>';
	}
?>