<?php

/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
 $results = $view->result;
?>
<div class="news-block-title">
	<h4>News</h4>
		<?php 
	if(!empty($results)){ ?>	
		<span class="view-all"><a href="javascript:void(0);">View All</a></span>
	<?php } ?>
</div>
<div class="scroll-feed fancy-scrollbar">

	<!-- Article Block (this part repeats) -->
	<?php 
	if(empty($results)){
		print "<p>There is no News content to show</p>";
	}	
	foreach($results as $key => $val){ 
		if(strlen($val->node_title) > 40) {	// Strip the length of title
			$wrapped = wordwrap($val->node_title, 40, "...#", true);
			$wrapped = explode("#", $wrapped);
			$title = $wrapped[0];	
		}else{
			$title = $val->node_title;
		}
		if(strlen($val->field_field_teaser[0]['raw']['value']) > 110) {	// Strip the length of teaser
			$wrapped = wordwrap($val->field_field_teaser[0]['raw']['value'], 110, "...#", true);
			$wrapped = explode("#", $wrapped);
			$teaser = $wrapped[0];	
		}else{
			$teaser = $val->field_field_teaser[0]['raw']['value'];
		}
	
	?>
		<article class="news-feed-item">
		<div class="container">
			<div class="row">
				<div class="news-feed-img" style="background-image: url('/sites/all/themes/cmgportal/library/img/USMA Awards 2018_USMA.jpg')"></div>
				<div class="news-feed-content">
					<h3 class="news-feed-title"><?php print $title; ?></h3>
					<span class="news-feed-date"><?php print date("F d, Y",$val->node_created); ?></span>
					<span class="news-feed-source">Commercial</span>
					<p class="news-feed-excerpt"><?php print $teaser; ?></p>
				</div>
			</div>
		</div>
		<?php 
			$attributes = array();
			$attributes['class'][] = 'news-feed-item-link';
			print l("", "node/".$val->nid, array('attributes' => $attributes)); 
		?>
	</article>
		
	<?php } ?>
</div>