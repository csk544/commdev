<?php

/**
 * @file
 * Admin page callback file for cmg custom module.
 */
 
/**
 * Menu Callback; My Links Settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function my_links_settings($form, &$form_state) {
	$form['My_links_description'] = array(
		'#type' => 'textarea',
		'#title' => t('My Links Description'),
		'#default_value' => variable_get("My_links_description", ''),
	);	
  return system_settings_form($form);
}
