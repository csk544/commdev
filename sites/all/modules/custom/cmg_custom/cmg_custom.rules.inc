<?php

/**
 * Implements hook_rules_action_info() on behalf of the user module.
 */
function cmg_custom_rules_action_info() {
  return array(
    'user_update_department' => array(
      'label' => t('Update Users Department on login'),
      'parameter' => array(
        'account' => array(
          'type' => 'user',
          'label' => t('User'),
          'description' => t('The user whose department will update.'),
          'save' => TRUE,
        ),
        'default_cost_center' => array(
          'type' => 'integer',
          'label' => t('Default Cost Center for User'),
          'description' => t('Default Cost Center for User.'),
          'save' => TRUE,
        ),
      ),
      'group' => t('User'),
    ),
  );
}

function user_update_department($account, $default_cost_center) {
  global $_simplesamlphp_auth_saml_attributes;
  $terms = variable_get('terms_list_cost_center', array());

  if ($account->uid != 1) {
    $saml = $_simplesamlphp_auth_saml_attributes;
    if (!empty($saml['unixid']) && array_key_exists(0, $saml['unixid'])) {
      if (!empty($saml['cost_center_number']) && array_key_exists(0, $saml['cost_center_number'])) {
        $cost_center = $saml['cost_center_number'];
      }
      else {
        $cost_center = $default_cost_center; 
      }
      $account->field_cost_center['und'][0]['value'] = $cost_center;
      $super_function = db_query('SELECT SuperFunction FROM {cost_center_details} where CostCenter = :CostCenter', array(':CostCenter' => $cost_center))->fetchField();
      if(!empty($super_function)) {
        $account->field_cost_center_super_function['und'][0]['value'] = $super_function;
      }
    }
  }

  if (!isset($account->field_user_department)) {
    field_attach_load('user', array($account->uid => $account));
  }

  if (empty($account->field_user_department['und'][0]['target_id'])) {
    $dept_tid = db_query('SELECT TID FROM cost_center_departments WHERE CostCenter = :cid', array(':cid' => $account->field_cost_center['und'][0]['value']))->fetchField();
    if (!empty($dept_tid)) {
      $account->field_user_department['und'][0]['target_id'] = $dept_tid;
    }
  }

  field_attach_presave('user', $account);
  field_attach_update('user', $account);
  entity_get_controller('user')->resetCache(array($account->uid));
}