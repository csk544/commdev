<?php
/*
 * load custom form form to create webform node
*/

function custom_webform_form($form, $form_state) { 
  $frm_id = arg(3);
	if($frm_id != '') { 
	  $result = db_select('custom_webform_details', 'cw')
		->fields('cw')
		->condition('wid', $frm_id,'=')
		->execute()
		->fetchAssoc();
    $title = $result['title'];
    $form_desc = $result['form_desc'];
    $email = $result['email'];
    $subject = $result['subject'];
    $message = $result['message'];
	}
  $form = array();
  $form['webform_title'] = array(
    '#title' => t('Title'),
    '#description' => t('Enter the title for webform.'),
    '#type' => 'textfield',
    '#size' => 20,
    '#default_value' => isset($title) ? $title:'',
  );
  $form['form_description'] = array(
    '#title' => t('Description'),
    '#description' => t('Enter the text to be displayed as description above the form.'),
    '#type' => 'textarea',
    '#rows' => 3,
    '#cols' => 20,
    '#resizable' => FALSE,
    '#default_value' => isset($form_desc) ? $form_desc:'',
  );
  $form['thank_you_message'] = array(
    '#title' => t('Submission Message'),
    '#description' => t('Enter the the text to be displayed as thank you message on form submission.'),
    '#type' => 'textarea',
    '#rows' => 3,
    '#cols' => 20,
    '#resizable' => FALSE,
    '#default_value' => isset($message) ? $message:'',
  );
  $form['form_email'] = array(
    '#title' => t('Destination Email Address'),
    '#description' => t('Enter the destination email address to receive form submission.'),
    '#type' => 'textfield',
    '#size' => 60,
    '#default_value' => isset($email) ? $email:'',
  );
  $form['form_subject'] = array(
    '#title' => t('Email Subject Header'),
    '#description' => t('Enter the subject to display for the email.'), 
    '#type' => 'textfield',
    '#size' => 60,
    '#default_value' => isset($subject) ? $subject:'',
  ); 
  $form['submit'] = array(
   '#type' => 'submit',
   '#value' => 'Submit',
  );
  return $form; 
}
/*
 * function to set the configurations to create webform node
*/
function custom_webform_configurations() {
  $form = array();
  $form['custom_webform_nid'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter the node id of existing webform'),
    '#default_value' => variable_get('custom_webform_nid', ''),
  );
  $form['description_cid'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter the Component id of the Description id of existing webform'),
    '#default_value' => variable_get('description_cid', ''),
  );
  return system_settings_form($form); 
}


/**
 * function to list out all webforms createed from custom webform
 */
function list_of_added_webforms() {
	$result = db_select('custom_webform_details', 'cw')
    ->fields('cw')
    ->execute()
    ->fetchAll();
	$id = array();
	$rows = array();

	foreach ($result as $key => $val) {
		$id[] = $val->wid;
		$wid[] = $val->wnid;
		$title[] = $val->title;
		$form_desc[] = $val->form_desc;
		$email[] = $val->email;
		$subject[] = $val->subject;
    $message[] = $val->message;
	}
	$header = array("Title","Edit","Delete"); 
	for($i=0;$i<count($id);$i++) {
    $node_title = l($title[$i], 'node/'.$wid[$i], array('attributes' => array('class' => array('title-link')))); 
    $edit = l(t('Edit'), 'admin/custom_webform/edit/'.$id[$i].'/'.$wid[$i], array('attributes' => array('class' => array('edit-link'))));
		$delete = l(t('Delete'), 'admin/custom_webform/delete/'.$id[$i].'/'.$wid[$i], array('attributes' => array('class' => array('del-link'))));
		$row = array($node_title,$edit,$delete);
		$rows[] = $row;
	}
	$output = theme('table', array('header' => $header,'rows' => $rows ));
	return $output;
}

/**
 * Function to delete already added calendars
 */ 
function custom_webform_delete($form, &$form_state, $id) {
	if(is_numeric($id)) {
				$result = db_select('custom_webform_details', 'cd')
				->fields('cd')
				->condition('wid',$id,'=')
				->execute()
				->fetchAssoc();
		$form['view_id'] = array('#type' => 'value', '#value' => $result['wid']);
		$form['webform_title'] = array('#type' => 'value', '#value' => $result['title']);
      return confirm_form($form, t('Are you sure you want to delete %title?', array('%title' => $result['title'])), 'admin/custom_webform/view', t('This action cannot be undone.'), t('Delete'), t('Cancel')
		);
	}
	return $form;
}

/**
 * On confirmation deleting rows from the DB tables
 */
function custom_webform_delete_submit($form, &$form_state) {
 	
  if ($form_state['values']['confirm'] == 1) {
    $del_nid = arg(4);
    node_delete($del_nid); 	
    $del_id = $form_state['values']['view_id'];
    $del_title = $form_state['values']['webform_title'];
    $del_rec = db_delete('custom_webform_details')
      ->condition('wid',$del_id,'=')
      ->condition('title',$del_title,'=')
      ->execute();
    if ($del_rec) {
      drupal_set_message('Your webform has been deleted');
      drupal_flush_all_caches();
      drupal_goto('admin/custom_webform/view');
    }
  }
}

/**
 * submit function for custom_webform_form
 *
 * @param $form An associative array containing the structure of the form
 * @param $form_state A keyed array containing the current state of the form
 */
function custom_webform_form_submit($form,$form_state){ 
  global $user;
  global $languages;
  $title = $form_state['values']['webform_title'];
  $form_desc = $form_state['values']['form_description'];
  $email = $form_state['values']['form_email'];
  $subject = $form_state['values']['form_subject'];
  $message = $form_state['values']['thank_you_message'];
  if(arg(3) == ''){
    if (drupal_multilingual() || module_exists('locale')) {
      $lang_code = language_default();
    }
    else {
      $lang_code =  LANGUAGE_NONE; 
    }
    $node = new stdClass();
    $node->type = 'webform';
    node_object_prepare($node);
    $node->language = $lang_code;
    $node->title = $form_state['values']['webform_title'];
    $node->body[$lang_code][0]['format']  = 'full_html';
    //$node->field_show_title[$lang_code][0]['value'] = 1;	
    $node->uid = $user->uid;
    //Newly added - CMG portal
    $node->field_is_this_a_usma_page['und'][0]['value'] = 1;
    $node->field_channel['und'][0]['tid'] = variable_get('us_medical_dept_term_id');
    $node->field_formdescription['und'][0]['value'] = $node->field_formdescription['und'][0]['safe_value'] = $form_state['values']['webform_title'];
    //End - CMG portal
    $node->promote = 0;
    $node->comment = 0;
    $node_id = variable_get('custom_webform_nid', '');
    $node_new = node_load($node_id);
	// Create the webform components.
    $components = $node_new->webform['components'];
    $cid = variable_get('description_cid', 8);
    $components[$cid]['value'] =  '<b>'.$form_state['values']['form_description'].'</b>';
	// Setup notification email.
		$template = "<p>Submitted on [submission:date:long]</p>
	<p>Submitted by user: [current-user:name]</p>
	<p>Submitted values are:</p>
	[submission:values]";
  $emails = array(
    array(
      'email' => $form_state['values']['form_email'],
      'subject' => $form_state['values']['form_subject'],
      'from_name' => 'default',
      'from_address' => 'default',
      'template' => $template,
			'html'=> 1,
      'excluded_components' => array(),
    ),
  );
  // Attach the webform to the node.
  $node->webform = array(
    'confirmation' => $form_state['values']['thank_you_message'],
    'confirmation_format' => NULL,
    'redirect_url' => '<confirmation>',
    'status' => '1',
    'block' => '0',
    'teaser' => '0',
    'allow_draft' => '0',
    'auto_save' => '0',
    'submit_notice' => '1',
    'submit_text' => '',
    'submit_limit' => '-1', // User can submit more than once.
    'submit_interval' => '-1',
    'total_submit_limit' => '-1',
    'total_submit_interval' => '-1',
    'record_exists' => TRUE,
    'roles' => array(
      0 => '1', // Anonymous user can submit this webform.
      1 => '2',
    ),
    'emails' => $emails,
    'components' => $components,
  );
  // Save the node.
    node_save($node);
    $webform_nid = $node->nid;
  } 
  // Insert or Updating the table   
  if(arg(3)) {
    $current_webform_nid = arg(4);
    $node_update = node_load($current_webform_nid);
    $node_update->title = $form_state['values']['webform_title'];
	//$node_update->body['und'][0]['value'] = $form_state['values']['form_description'];
	$node_update->field_show_title['und'][0]['value'] = 1;
  // Attach the webform to the node.
    $node_update->webform['confirmation'] = $form_state['values']['thank_you_message'];
  // update notification email.
    $node_update->webform['emails'][1]['email'] = $form_state['values']['form_email']; 
    $node_update->webform['emails'][1]['subject'] = $form_state['values']['form_subject'];
	$cid = variable_get('description_cid', 8);
    $node_update->webform['components'][$cid]['value'] = '<b>'.$form_state['values']['form_description'].'</b>';
    node_save($node_update);
  //Storing the custom webform details in custom table    
    db_update('custom_webform_details') 
	->fields(array(
	  'title' =>$title,
	  'form_desc'=>$form_desc,
	  'email' =>$email,
	  'subject'=>$subject,
	  'message' =>$message,
	))
	->condition('wid', arg(3), '=')
	->execute();
  } else {
      db_insert('custom_webform_details')
	    ->fields(array(
	      'wnid' => $webform_nid, 
	      'title' =>$title,
	      'form_desc'=>$form_desc,
	      'email' =>$email,
	      'subject'=>$subject,
	      'message' =>$message,
	    ))
	      ->execute();
  }
  if(arg(3)) {
    drupal_set_message(t('Your webform ').l($title, 'node/'.arg(4)). t(' details has been updated')); 		
	} else {
	  drupal_set_message(t('You have created a webform ').l($title, 'node/'.$webform_nid).t(' successfully'));
	}
	  drupal_flush_all_caches();
	  drupal_goto('admin/custom_webform');	 
 
  }