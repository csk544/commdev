(function($) {
  Drupal.behaviors.webform_changes = {
    attach: function(context, settings) {
		$('.webform-client-form .form-actions .webform-next').attr('value', 'Submit'); 
		//$('.webform-client-form.preview .form-actions .webform-submit').attr('value', 'Submit'); 
		//$('.webform-client-form.preview .form-actions > #edit-cancel-button').hide();
		if($('.webform-client-form').hasClass('preview')){ 
	      $('.webform-component--medical-objective-and-gap-addressed-per-current-year-medical-plan').after('<div class="form-item webform-component webform-component-markup webform-component--cancel-modal"> <p>Are you sure you want to cancel before completing this form? None of your entries will be saved if you cancel.</p></div>');
	    }
	    if($('.webform-component').hasClass('webform-component--cancel-modal') || $('.webform-client-form').hasClass('preview')) { 
			$('.webform-client-form .form-actions > #edit-cancel-button').click(function() {
			$('.webform-component--cancel-modal').dialog({
				modal: true,
				minWidth: 550,
				minHeight: 380,
				dialogClass: 'fixed-dialog',
				title: '',
				buttons: [
					{text: "Yes, Cancel", click: function() {
					 window.close();  
					window.history.back();
				   
					$(this).dialog("close");
					}
					},
					{text: "No, Return to form", click: function() {
					$(this).dialog("close");
					}
					}
				]
			});
			return false;
			});
		} else {
			$('.webform-client-form .form-actions > #edit-cancel-button').click(function() {
			var status = confirm("Are you sure you want to cancel before completing this form? None of your entries will be saved if you cancel.");
			if(status) {
				return true;
			} else {
				return false;
			}
			});
		} 
		

		//changes to concept tracker item
	
		

		var newURL = window.location.pathname;
		var pathArray = newURL.split( '/' );
		var secondLevelLocation = pathArray[2];
		if(secondLevelLocation == 'concept-tracker'){
         $('.messages.status').hide();
		}
		var concept_tracker_id =  Drupal.settings['concept_tracker_nid'];

		if(secondLevelLocation == concept_tracker_id){ 
		   $('ul.primary').hide();
		}
		
		

		if(window.location.href.match('adverse-event') == 'adverse-event') {
			var data_arr = new Array();
			$('#webform-component-event--date-you-were-notified-of-the-event > .webform-container-inline > div').each(function() {
				data_arr.push($(this).html());
			});
			$('#webform-component-event--date-you-were-notified-of-the-event > .webform-container-inline').html(data_arr[2]+data_arr[1]+data_arr[0]);
			var dob = new Array();
			$('#webform-component-patient-details--patient-dob > .webform-container-inline > div').each(function() {
				dob.push($(this).html());
			});
			$('#webform-component-patient-details--patient-dob > .webform-container-inline').html(dob[2]+dob[1]+dob[0]);
		}
		$('#edit-submitted-reason :selected').text('- Select one -');
	}
	};
	
	//changes to the date field of concept tracker
	$( document ).ready(function() {
		$("[class*='webform-component--tumor-type']").each(function(){
    		if($.trim($(this).text()) == 'Tumor Type'){ 
			$(this).css('display','none');
		}

	});
	
   
   var cdi_val = $('#edit-submitted-date-of-concept-discussion-with-investigator-year').val()+'-'+$('#edit-submitted-date-of-concept-discussion-with-investigator-month').val()+'-'+$('#edit-submitted-date-of-concept-discussion-with-investigator-day').val();  
   if($('#edit-submitted-date-of-concept-discussion-with-investigator-year').val() != ''){
   $('#edit-submitted-date-of-concept-discussion-with-investigator').val(cdi_val); 
   }
   var srt_val = $('#edit-submitted-srrt-proposal-presentation-date-year').val()+'-'+$('#edit-submitted-srrt-proposal-presentation-date-month').val()+'-'+$('#edit-submitted-srrt-proposal-presentation-date-day').val(); 
  if($('#edit-submitted-srrt-proposal-presentation-date-year').val() != '')   {
   $('#edit-submitted-srrt-proposal-presentation-date').val(srt_val); 
   }

	if (document.getElementById('messages')) {
		if($('#edit-submitted-date-of-concept-discussion-with-investigator').val() == ""){
	   $('#edit-submitted-date-of-concept-discussion-with-investigator').addClass('error');
	 }
	}  

	
	$("#edit-submitted-tumor-type-gi,#edit-submitted-tumor-type-b,#edit-submitted-tumor-type-cns, #edit-submitted-tumor-type-d,#edit-submitted-tumor-type-gu,#edit-submitted-tumor-type-gyn,#edit-submitted-tumor-type-h,#edit-submitted-tumor-type-l,#edit-submitted-tumor-type-n").click(function() {
	if($(this).val() == 'Other'){ 
		   $('.form-item.webform-component.webform-component-textfield.webform-component--other-tumor-type.webform-container-inline').show();   
		   $('.form-item.webform-component.webform-component-textfield.webform-component--other-tumor-type.webform-container-inline > label').html("<label for='edit-submitted-other-tumor-type'>Other Tumer Type <span title='This field is required.' class='form-required'>*</span></label>");
		}
		else{
		  $('.form-item.webform-component.webform-component-textfield.webform-component--other-tumor-type.webform-container-inline').hide();  
		}
});		
	});
})(jQuery);


