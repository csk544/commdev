<?php

function comm_cdn_admin($form) {
  
  $form['media_portal_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Media Portal Orign Host'),
    '#default_value' => variable_get('media_portal_server', NULL),
    '#description' => t('Media Portal Orign Host for Content Upload'),
    '#required' => TRUE,
  );
  $form['media_portal_server_egress'] = array(
    '#type' => 'textfield',
    '#title' => t('Media Portal Alias'),
    '#default_value' => variable_get('media_portal_server_egress', NULL),
    '#description' => t('Media Portal Egress Property or Alias'),
    '#required' => TRUE,
  );
  $form['media_portal_server_ftp_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Media Portal FTP UserName'),
    '#default_value' => variable_get('media_portal_server_ftp_username', NULL),
    '#description' => t('Media Portal FTP UserName'),
    '#required' => TRUE,
  );
  $form['media_portal_server_ftp_password'] = array(
    '#type' => 'password',
    '#title' => t('Media Portal FTP Password'),
    '#default_value' => variable_get('media_portal_server_ftp_password', NULL),
    '#description' => t('Media Portal FTP Password'),
    '#required' => TRUE,
  );
  $form['media_portal_folder'] = array(
    '#type' => 'textfield',
    '#title' => t('Folder Name'),
    '#default_value' => variable_get('media_portal_folder', NULL),
    '#description' => t('The folder name within Google Cloud Storage. Required in order to separate out environments. For example, local,
      dev, staging, prod etc.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}