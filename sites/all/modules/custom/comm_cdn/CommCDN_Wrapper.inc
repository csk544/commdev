<?php

/**
 * @file
 * Implements DrupalStreamWrapperInterface.
 */

class CommCDN_Wrapper extends DrupalLocalStreamWrapper {

  /**
   * Object Media Portal CDN FTP Connection.
   */
  protected $ftp_connection;

  /**
   * String Media Portal CDN FTP Connection Hostname.
   */
  protected $server;
  
  /**
   * String Media Portal CDN FTP Connection Username.
   */
  protected $username;
  
  /**
   * String Media Portal CDN FTP Connection Password.
   */
  protected $password;

  /**
   * String Google Cloud Store folder name.
   */
  protected $folder;

  public function __construct() {
    $this->server = variable_get('media_portal_server');
    $this->username = variable_get('media_portal_server_ftp_username');
    $this->password = variable_get('media_portal_server_ftp_password');
    $this->folder = variable_get('media_portal_folder');
    $this->getCDN();

    if (@ftp_login($this->ftp_connection, $this->username, $this->password)) {
      ftp_pasv($this->ftp_connection, TRUE);
      if ($this->folder) {
        ftp_chdir($this->ftp_connection, $this->folder);
      }
    }
    else {
      throw new Exception("Unable to Login to Portal");
    }
  }

  public function getExternalUrl() {
    $url = '';
    $local_path = $this->getLocalPath();

    if ($local_path) {
      $url = $this->generateSignedURL($local_path);
    }

    $path = explode('/', $local_path);

    if ($path[0] == 'styles') {
      $urls = cache_get("styles_urls");
      if (isset($urls->data[urldecode($url)])) {
        return $url;
      }
      else {
        $headers = @get_headers($url);
        if (strpos($headers[0], '200') !== FALSE) {
          $urls->data[urldecode($url)] = $url;
          cache_set("styles_urls", $urls->data);
          return $url;
        }
      }

      $target = $this->getTarget($this->uri);
      if ($target) {
        array_shift($path);
        return url('system/files/styles/' . implode('/', $path), array('absolute' => TRUE));
      }
    }

    return $url;
  }

  public function stream_open($uri, $mode, $options, &$opened_url) {
    $this->uri = $uri;
    $this->handle = tmpfile();
    return (bool) $this->handle;
  }

  public function url_stat($uri, $flags) {
    return $this->_stat($uri);
  }

  public function stream_read($count) {
    try {
      $stat = fstat($this->handle);
      if (!$stat['size']) {
        $ret = ftp_nb_fget($this->ftp_connection, $this->handle, $this->getLocalPath($this->uri), FTP_BINARY);
        while ($ret == FTP_MOREDATA) {
          $ret = ftp_nb_continue($this->ftp_connection);
        }
        rewind($this->handle);
      }
      return parent::stream_read($count);
    }
    catch(Exception $e) {
      watchdog_exception('cdn', $e, 'CommCDN_Wrapper::stream_read');
    }
  }

  public function stream_flush() {
    $local_path = $this->getLocalPath($this->uri);

    $directory = dirname($local_path);

    if (@ftp_chdir($this->ftp_connection, $directory)) {
      ftp_chdir($this->ftp_connection, $directory);
    }
    else {
      array_walk(explode("/", $directory), function($dir_name) {
        ftp_mkdir($this->ftp_connection, $dir_name);
        ftp_chmod($this->ftp_connection, octdec(str_pad(777, 4, '0', STR_PAD_LEFT)), $dir_name);
        ftp_chdir($this->ftp_connection, $dir_name);
      });
    }
    rewind($this->handle);
    $ret = ftp_nb_fput($this->ftp_connection, basename($local_path), $this->handle, FTP_BINARY);
    while ($ret == FTP_MOREDATA) {
      $ret = ftp_nb_continue($this->ftp_connection);
    }

    if (strpos($this->uri, "styles")) {
      $url = $this->generateSignedURL($local_path);
      $urls = cache_get("styles_urls");
      $urls->data[$url] = $url;
      cache_set("styles_urls", $urls->data);
    }
  }

  public function stream_close() {
    parent::stream_close();
    return ftp_close($this->ftp_connection);
  }

  public function getLocalPath($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }

    $path  = str_replace('cdn://', '', $uri);
    $path = trim($path, '/');
    return $path;
  }

  public function getDirectoryPath() {
    return "";
  }

  public function mkdir($uri, $mode, $options) {
    return TRUE;
  }

  public function chmod($mode) {
    return TRUE;
  }

  public function unlink($uri) {
    $local_path = $this->getLocalPath($uri);
    $directory = dirname($local_path);
    try {
      ftp_chdir($this->ftp_connection, $directory);
      ftp_delete($this->ftp_connection, basename($uri));
      return TRUE;
    }
    catch(Exception $e) {
      watchdog_exception('cdn', $e, 'CommCDN_Wrapper::unlink');
    }
    return FALSE;
  }

  protected function generateSignedURL($file_path) {
    $url = "http://" . implode("/", array(variable_get('media_portal_server_egress'), $this->folder, $file_path));
    return $url;
  }

  private function getCDN() {
    if (empty($this->server)) {
      throw new Exception("Please provide Server Information for CDN.");
    }
    else {
      $this->ftp_connection = ftp_connect($this->server);
    }
  }

  protected function _stat($uri = NULL) {
    $stat = FALSE;

    try {
      if (empty($uri)) {
        $uri = $this->uri;
      }
      $this->uri = $uri;
      $target = $this->getTarget($uri);

      if ($target) {
        $path_info = pathinfo($uri);
        if (!isset($path_info['extension'])) {
          $mode = 0040000;
          $size = 1;
          $mode |= 0777;
          $stat = $this->createFileMode($mode, $size);
        }
        else {
          $dir = dirname($this->getLocalPath($uri)) ?: '.';
          $resources = ftp_nlist($this->ftp_connection, $dir) ?: array();
          foreach($resources as $resource) {
            if (basename($resource) == basename($uri)) {
              $mode = 0100000;
              $mode |= 0777;
              $size = ftp_size($this->ftp_connection, $this->getLocalPath($uri));
              $stat = $this->createFileMode($mode, $size);
              break;
            }
          }
        }
      }
    }
    catch(Exception $e) {
      watchdog_exception('cdn', $e, 'CommCDN_Wrapper::_stat');
    }

    return $stat;
  }

  public function dirname($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }
    $scheme = file_uri_scheme($uri);
    $dirname = dirname(file_uri_target($uri));

    if ($dirname == '.') {
      $dirname = '';
    }

    return $scheme . '://' . $dirname;
  }

  protected function createFileMode($mode, $size) {
    $stat = array();

    $stat[0] = $stat['dev'] = 0;
    $stat[1] = $stat['ino'] = 0;
    $stat[2] = $stat['mode'] = $mode;
    $stat[3] = $stat['nlink'] = 0;
    $stat[4] = $stat['uid'] = 0;
    $stat[5] = $stat['gid'] = 0;
    $stat[6] = $stat['rdev'] = 0;
    $stat[7] = $stat['size'] = $size;
    $stat[8] = $stat['atime'] = 0;
    $stat[9] = $stat['mtime'] = 0;
    $stat[10] = $stat['ctime'] = 0;
    $stat[11] = $stat['blksize'] = 0;
    $stat[12] = $stat['blocks'] = 0;

    return $stat;
  }
}