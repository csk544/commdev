<?php
/*
 * To create  Literature Search form
*/

function literature_search_form($form, &$form_state) {
    $form['#attributes']['class'][] = 'literature_search_form';	
		$form['title'] = array(
			'#type' => 'item',
			'#markup' => t('<h2>Literature Search Form</h2>'), 
			'#description' => t('Required fields are marked with *'),
		);
	  // User info fieldset collapsible
		$form['user_information'] = array( 
			'#type' => 'fieldset',
			'#title' => t('User Information'), 
			'#description' => t('This form is for use by USMA Knowledge Enhancement only.  The form for other users/departments is located at: http://gwiz.gene.com/groups/Library/services/search_forms.html'),
		);
		$form['user_information']['first_name'] = array(
			'#type' => 'textfield',
			'#title' => t('First Name'),
			'#size' => 30,
			'#required' => TRUE,
		);
		$form['user_information']['last_name'] = array(
			'#type' => 'textfield',
			'#title' => t('Last Name'),
			'#size' => 30,
			'#required' => TRUE,
		);
		$format = 'd-m-Y';
		$form['user_information']['date_needed'] = array(
				'#title' => t('Date Needed'),
				'#type' => 'date_popup',
				'#date_format' => $format,
				'#date_label_position' => 'within',
				'#date_year_range' => '0:+2',
				'#default_value' => format_date(time(), 'custom', $format), 
				//'#datepicker_options' => array('maxDate' => 0),
				'#required' => TRUE,
				'#size' => 30,				
			 );

	  // Questions To Be Answered fieldset collapsible
		$form['lsf_questions'] = array(
			'#type' => 'fieldset',
			'#title' => t('Questions To Be Answered'),
			'#description' => t('Write as much as you like. Too much information is just fine. The better the understanding of the information need, the better the search and the more precise will be the output. If keywords are given - and they are welcome - please indicate the relationship between them.'),
		);
		$form['lsf_questions']['questions'] = array(
			'#title' => 'Questions/Topics',
			'#type' => 'textarea',
			'#rows' => 3,
			'#cols' => 10,
			'#resizable' => FALSE,
			'#required' => TRUE,
		);
		
		$form['lsf_questions']['search_time'] = array(
			'#type' => 'radios',
			'#title' => t('How far back should the search go?'),
			'#options' => array(
				  '5 Years' => t('5 Years'),
				  '2 Years' => t('2 Years'), 
				  'Year of drug approval' => t('Year of drug approval'),
				  'other' => t('other'),
               ),
			'#required' => TRUE,
		);
		 
	  	$form['lsf_questions']['time_period'] = array(
			'#type' => 'textfield',
			'#title' => t('Time period requested'),
			'#size' => 30,
			'#states' => array(
				'visible' => array(
					':input[name="search_time"]' => array('value' => 'other'),
					'#required' => TRUE,
				),
			),
		);     	
   	    $form['lsf_questions']['population'] = array(
			'#type' => 'checkboxes',
			'#title' => t('Population<br /><i class="label-ext">(Check all that apply)</i>'),
			'#options' => array(
				  'Human' => t('Human'),
				  'Animal' => t('Animal'),
				  'In vitro' => t('In vitro'),
               ),
			'#required' => TRUE,   
		);			
      	$form['lsf_questions']['language'] = array(
			'#type' => 'radios',
			'#title' => t('Language Requirements'),
			'#options' => array(
				  'English only' => t('English only'),
				  'Any, with English abstract ' => t('Any, with English abstract '),
				  'Any' => t('Any'),
               ),
			'#required' => TRUE,   
		);	
   	    $form['lsf_questions']['publication_type'] = array(
			'#type' => 'checkboxes',
			'#title' => t('Publication Types'),
			'#options' => array(
				  'Literature/Narrative Reviews' => t('Literature/Narrative Reviews'),
				  'Editorial/Commentary' => t('Editorial/Commentary'),
				  'Case Reports' => t('Case Reports'),
				  'Conference Abstracts' => t('Conference Abstracts'),
               ),
		);
      	/*$form['lsf_questions']['preference'] = array(
			'#type' => 'radios',
			'#title' => t('Would you prefer that the searcher call to get a better understanding of your request?'),
			'#options' => array(
				  'yes' => t('yes'),
				  'no' => t('no'),
               ),
		);*/
		$form['background'] = array(
			'#type' => 'fieldset',
			'#title' => t('Background'),			
		);
		$form['background']['reason'] = array(
			'#title' => 'Reason/background of request<br /><i class="label-ext">(contact center case fulfillment [high priority, reimbursement, etc.], new global standard response, dossier, KE-authored publication/presentation, KE-initiated requests for task forces/medical teams/directors.)</i>',
			'#type' => 'textarea',
			'#rows' => 3,
			'#cols' => 20,
			'#resizable' => FALSE,
			'#required' => TRUE,
			
		);
		$form['background']['instructions'] = array(
			'#title' => 'Note/Special Instructions',
			'#type' => 'textarea',
			'#rows' => 3,
			'#cols' => 20,
			'#resizable' => FALSE,
		);
        		$form['buttons'] = array(
			'#type' => 'fieldset',
		);
		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Submit'),
			'#validate' => array('custom_contactus_form_validate'), 
		);
		$form['cancel'] = array(
			'#name' => 'cancel',
			'#type' => 'button',
			'#value' => t('Cancel'),
			'#attributes' => array('onclick' => 'window.location="http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '"; return false;'),
	  ); 		
return $form; 		
}
function custom_contactus_form_validate($form, &$form_state) {
	if($form_state['input']['search_time'] == "other") {
		if($form_state['input']['time_period'] == "") {
			form_set_error('time_period', 'Time Period field is required.');
		}
	}
}
/**
 * Function called from within a custom submit handler.
 */
function literature_search_form_submit($form, &$form_state) {	
  literature_search_form_mail_send($form_state['input']);
}

/**
* Function to send the email 
*/
function literature_search_form_mail_send($params) {

	$checked_publications = array();
	foreach($params['publication_type'] as $key => $val) {
		array_push($checked_publications, $val);
	}	
	$pub_list = implode(",", $checked_publications);
	
	$checked_population = array();
	foreach($params['population'] as $key => $val) {
		array_push($checked_population, $val);
	}	
	$pop_list = implode(",", $checked_population);
	$subject = "Literature Search Form";
	$body = $params['first_name']." has submitted the following details.\n\n";
	$body .= "First Name = ".$params['first_name']."\n";
	$body .= "Last Name = ".$params['last_name']."\n";
	$body .= "Date Needed = ".$params['date_needed']['date']."\n";
	$body .= "Questions/Topics = ".$params['questions']."\n";
	if($params['search_time'] == "other") {		
		$time_period = $params['time_period'];
	} else {
		$time_period = $params['search_time'];
	}
	$body .= "How far back should the search go? = ".$time_period."\n";		
	$body .= "Population = ".trim($pop_list, ",")."\n";
	$body .= "Language Requirements = ".$params['language']."\n";
	if(empty($params['publication_type']['Conference Abstracts']) && empty($params['publication_type']['Literature/Narrative Reviews']) && empty($params['publication_type']['Editorial/Commentary']) && empty($params['publication_type']['Case Reports']))
	{
		$publications_list = "exclude review articles";
	}
	else {	
		$publications_list = trim($pub_list, ",");
	
	}
	$body .= "Publication Types = ".$publications_list."\n";
	$body .= "Reason/background of request = ".$params['reason']."\n";
	$body .= "Note/Special Instructions = ".($params['instructions'] ? $params['instructions']:"--")."\n\n";	
  $module = 'custom_contactus'; 
  $key = 'literature_form_message'; 
  $to = variable_get('to_addr', '');
  $from = variable_get('frm_addr', 'admin@example.com');
  $language = language_default();
  $send = TRUE;
  $params = array(
    'body' => $body,
    'subject' => $subject,
    'headers' => array(
        'MIME-Version' => '1.0',
		'Content-Type' => 'text/plain; charset=UTF-8; format=flowed',
		'Content-Transfer-Encoding' => '8Bit',
		'X-Mailer' => 'Drupal',
		

    )
);
  $result = drupal_mail($module, $key, $to, $language, $params, $from, $send);
  if ($result['result'] == TRUE) {
	  //custom_contactus_auto_reply_mail_send($params);
    drupal_goto('search_form/success');
  } else {
    drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
  }
}
/**
 * Implements hook_mail.
 */
function custom_contactus_mail($key, &$message, $params) { 
  $message['headers']['Content-Type'] = 'text/plain; charset=UTF-8; format=flowed';
	$message['headers']['Content-Transfer-Encoding'] = '7bit';	
  $options = array(
    'langcode' => $message['language']->language,
  );
  switch ($key) {
		case 'literature_form_message': 
			$message['subject'] = variable_get('literature_form_subject', '');
			//$message['body'][] = replace_token_body(variable_get('literature_form_mail_body', ''),$params, 'literature_form_mail');
			$message['body'][] = $params['body'];
			//echo "<pre>"; print_r($params);
			//exit;
    break;
  }
  
  
}
