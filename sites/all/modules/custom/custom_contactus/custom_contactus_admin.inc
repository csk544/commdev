<?php
/*
 * load custom form form to create custom contact us form
*/

function custom_contactus_form($form, &$form_state, $form_type) {
	$sal_list = get_sal_list();
	$product_list = get_product_list();
	$credential_list = get_credential_list($form_type);
	$practice_list = get_practice_list($form_type);
	$state_list = get_state_list();
	$form_types = array('commercial-representatives', 'care-affairs', 'medical-affairs');
	if(in_array($form_type, $form_types)) {
		$form['form_type'] = array('#type' => 'hidden', '#value' => $form_type);
		$form['form-disclaimer'] = array(
			'#type' => 'item',
			'#markup' => variable_get('disclaimer', ''),
		); 
		if($form_type == 'commercial-representatives') {
			$form['code'] = array('#type' => 'hidden', '#value' => variable_get('code_commrep_form'));
			$form['title'] = array(
				'#type' => 'item', 
				'#markup' => variable_get('title1', '<h3>Medical Information Request(s) Form for Commercial Representatives (CS, COS, TLL, etc.)</h3>'),
			);
		} 
    else if($form_type == 'care-affairs') {
			$form['code'] = array('#type' => 'hidden', '#value' => variable_get('code_mcl_form'));
			$form['title'] = array(
				'#type' => 'item',
				'#markup' => variable_get('title2', '<h3>Medical Information Request(s) Form for Managed Care and Government Affairs Representatives (PFT, MCL,CFT,GAM,FRM,etc.) and Access Solutions)</h3>'),
			); 
		} 
    else if($form_type == 'medical-affairs') {
			$form['code'] = array('#type' => 'hidden', '#value' => variable_get('code_msl_form'));
			$form['title'] = array(
				'#type' => 'item',
				'#markup' => variable_get('title3', '<h3>Medical Information Request Form for Medical Affairs Personnel (MSL)</h3>'),
			); 
		}
		$form['fdate'] = array('#type' => 'hidden', '#value' => format_date(time(), 'custom', 'd-m-Y'));
		$form_state['storage']['addquestions'] = isset($form_state['storage']['addquestions']) ? $form_state['storage']['addquestions'] : 0;
		$form['#attributes']['class'][] = 'custom_contact_form';	
		$form['intro'] = array(
			'#type' => 'item',
			'#markup' => variable_get('introduction', 'This form is for the submission of unsolicited medical questions. Any other use is strictly prohibited.<br />Required fields are marked with * (Company Name is required if Delivery Method is U.S. Mail or Overnight.)'),
		);
		  // User info fieldset collapsible
		$form['user_info'] = array(
			'#type' => 'fieldset',
			'#title' => t('User Information'), 
			//'#description' => t('Enter at least 3 characters (all searches use a wildcard search by default)'),
		);
		$form['user_info']['unixid'] = array(
			'#type' => 'textfield',
			'#title' => t('Your Genentech UNIX ID '),
			'#size' => 30,
			'#required' => TRUE,
		);
	  // Requestor info fieldset collapsible
		$form['rdi_info'] = array(
			'#type' => 'fieldset',
			'#title' => t('Requestor Demographic Information'),
		);
		$form['rdi_info']['salutation'] = array(
			'#type' => 'select',
			'#title' => t('Salutation '),
			'#options' => $sal_list,
			'#empty_option'=>t('--Select One--'),
			'#required' => TRUE,
			'#chosen' => FALSE,
		);
		$form['rdi_info']['fname'] = array(
			'#type' => 'textfield',
			'#title' => t('First name '),
			'#size' => 30,
			'#required' => TRUE,
		);
		$form['rdi_info']['lname'] = array(
			'#type' => 'textfield',
			'#title' => t('Last name '),
			'#size' => 30,
			'#required' => TRUE,
		);
		$form['rdi_info']['credential'] = array(
			'#type' => 'select',
			'#title' => t('Credential '),
			'#options' => $credential_list,
			'#empty_option'=>t('--Select One--'),
			'#required' => TRUE,
			'#chosen' => FALSE,
		);
		$form['rdi_info']['job_title'] = array(
			'#type' => 'textfield',
			'#title' => t('Job Title '),
			'#size' => 30,
		);
		$form['rdi_info']['practice'] = array(
			'#type' => 'select',
			'#title' => t('Practice setting '),
			'#options' => $practice_list,
			'#empty_option'=>t('--Select One--'),
			'#required' => TRUE,
			'#chosen' => FALSE,
		);
		$form['rdi_info']['cname'] = array(
			'#type' => 'textfield',
			'#title' => t('Company name '),
			'#size' => 30,
		);
		$form['rdi_info']['saddr1'] = array(
			'#type' => 'textfield',
			'#title' => t('Street Address1 '),
			'#size' => 30,
			'#required' => TRUE,
		);
		$form['rdi_info']['saddr2'] = array(
			'#type' => 'textfield',
			'#title' => t('Street Address2'),
			'#size' => 30,
		);
		$form['rdi_info']['city'] = array(
			'#type' => 'textfield',
			'#title' => t('City '),
			'#size' => 30,
			'#required' => TRUE,
		);
		$form['rdi_info']['state'] = array(
			'#type' => 'select',
			'#title' => t('State '),
			'#options' => $state_list,
			'#empty_option'=>t('--Select One--'),
			'#required' => TRUE,
			'#chosen' => FALSE,
		);
		$form['rdi_info']['zip'] = array(
			'#type' => 'textfield',
			'#title' => t('Zip '),
			'#size' => 30,
			'#element_validate' => array('element_validate_number'),
			'#required' => TRUE,
		);
		$form['rdi_info']['phone'] = array(
			'#type' => 'textfield',
			'#title' => t('Phone '),
			'#size' => 3,
			'#maxlength' => 3,
			'#required' => TRUE,
			'#element_validate' => array('element_validate_number'),
			'#attributes' => array('class' => array('container-inline', 'phone')),
		);
		$form['rdi_info']['phone2'] = array(
			'#type' => 'textfield',
			'#title' => t('Phone2 '),
			'#size' => 3,
			'#maxlength' => 3,
			'#required' => TRUE,
			'#element_validate' => array('element_validate_number'),
			'#attributes' => array('class' => array('container-inline', 'phone')),
		);  
		$form['rdi_info']['phone3'] = array(
			'#type' => 'textfield',
			'#title' => t('Phone3 '),
			'#size' => 4,
			'#maxlength' => 4,
			'#required' => TRUE,
			'#prefix' => '<span class="container-inline">&nbsp;-&nbsp;</span>',
			'#element_validate' => array('element_validate_number'),
			'#attributes' => array('class' => array('phone')),
		);
		$form['rdi_info']['extension'] = array(
			'#type' => 'textfield',
			'#title' => t('Extension '),
			'#size' => 10,
			'#element_validate' => array('element_validate_number'),
		);
		// Questions fieldset 
		$form['questions'] = array(
			'#type' => 'fieldset',
			'#title' => t('Questions'),
		);
		$form['questions']['delivery_method'] = array(
			'#type' => 'select',
			'#title' => t('Delivery Method for Requestor '),
			'#options' => array(
				'Email' => t('Email'),
				'US Mail' => t('US Mail'),
				'Overnight' => t('Overnight'),
				'Fax' => t('Fax'),
			),
			'#empty_option' => t('--Select One--'),
			'#required' => TRUE,
			'#chosen' => FALSE,
		);
		$form['questions']['email'] = array(
			'#type' => 'textfield',
			'#title' => t('Email'),
			'#size' => 30,
			'#states' => array(
				'visible' => array(
					':input[name="delivery_method"]' => array('value' => 'Email'),
				),
			),
		);  
		$form['questions']['fax'] = array(
			'#type' => 'textfield',
			'#title' => t('Fax'),
			'#size' => 3,
			'#maxlength' => 3,
			'#element_validate' => array('element_validate_number'),
			'#states' => array(
				'visible' => array(
					':input[name="delivery_method"]' => array('value' => 'Fax'),
				),
			),
			'#attributes' => array('class' => array('container-inline', 'phone')),
		);
		$form['questions']['fax2'] = array(
			'#type' => 'textfield',
			'#title' => t('Fax2'),
			'#size' => 3,
			'#maxlength' => 3,
			'#element_validate' => array('element_validate_number'),
			'#states' => array(
				'visible' => array(
					':input[name="delivery_method"]' => array('value' => 'Fax'),
				),
			),
			'#attributes' => array('class' => array('container-inline', 'phone')),
		);  
		$form['questions']['fax3'] = array(
			'#type' => 'textfield',
			'#title' => t('Fax3'),
			'#size' => 4,
			'#maxlength' => 4,
			'#prefix' => '<span class="container-inline hyphen" style="display:none;">&nbsp;-&nbsp;</span>',
			'#element_validate' => array('element_validate_number'),
			'#states' => array(
				'visible' => array(
					':input[name="delivery_method"]' => array('value' => 'Fax'),
				),
			),		
			'#attributes' => array('class' => array('phone')),
		);
		if($form_type == 'medical-affairs') {
			$format = 'd-m-Y';
			$form['questions']['request_date'] = array(
				'#title' => 'Request Date',
				'#type' => 'date_popup',
				'#date_format' => $format,
				'#date_label_position' => 'within',
				'#date_year_range' => '0:-1',
				'#default_value' => format_date(time(), 'custom', $format), 
				'#datepicker_options' => array('maxDate' => 0),
				'#required' => TRUE,	  
			 );
		}
		if($form_type != 'medical-affairs') {
			$form['questions']['instructions'] = array(
				'#type' => 'item',
				'#markup' => '<div class="instructions" style="clear: both; padding-bottom: 10px; width: 600px;">
				<b>In the question fields(s) below, please submit the following:</b>
				<ul><li>The customers specific question or request for medical information; please use the customers own words</li>
				<li>The indication or treatment setting of interest and any additional information you feel is pertinent</li>
				</ul>
				</div>',
			);
		 }
		$form['questions']['product'] = array(
			'#type' => 'select',
			'#title' => t('Product '),
			'#options' => $product_list,
			'#empty_option'=>t('--Select One--'),
			'#required' => TRUE,
			'#chosen' => FALSE,	
		); 
		$form['questions']['question'] = array(
			'#type' => 'textarea',
			'#title' => t('Question '),
			'#rows' => 3,
			'#cols' => 20,
			'#resizable' => FALSE,
			'#required' => TRUE,
		);   
		if($form_type == 'medical-affairs') {
			$form['questions']['response'] = array(
				'#title' => 'Response',
				'#type' => 'textarea',
				'#rows' => 3,
				'#cols' => 20,
				'#resizable' => FALSE,
			);
		}
	 // Container to add the questions components on Adding another product
		$form['addquestions'] = array(
			'#type' => 'container',
			'#tree' => TRUE,
			'#prefix' => '<div id="addquestions">',
			'#suffix' => '</div>',
		);
		if ($form_state['storage']['addquestions']) {
			for ($i = 1; $i <= $form_state['storage']['addquestions']; $i++) {
				$form['addquestions'][$i] = array(
					'#type' => 'fieldset',
					'#tree' => TRUE,
				);
				$form['addquestions'][$i]['product'] = array(
					'#title' => t('Product'),
					'#type' => 'select',
					'#options' => $product_list,
					'#empty_option'=>t('--Select One--'),
					'#required' => TRUE,
					'#chosen' => FALSE,
					);
				$form['addquestions'][$i]['question'] = array(
					'#title' => t('Question'),
					'#type' => 'textarea',
					'#rows' => 3,
					'#cols' => 20,
					'#resizable' => FALSE,
					'#required' => TRUE,
				);
				if($form_type == 'medical-affairs') {
					$form['addquestions'][$i]['response'] = array(
						'#title' => 'Response',
						'#type' => 'textarea',
						'#rows' => 3,
						'#cols' => 20,
						'#resizable' => FALSE,
					);
				}
			}
		}
	//ajax call back to add another product
		$form['add_product'] = array(
			'#type' => 'button',
			'#value' => t('Add Another Product'),
			'#limit_validation_errors' => array(),  
				'#ajax' => array(
				'callback' => 'custom_registration_ajax_add_participant',
				'wrapper' => 'addquestions',
				'#executes_submit_callback' => FALSE,
			 ),
		);
		$form['buttons'] = array(
			'#type' => 'fieldset',
		);
		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Submit'),
			'#validate' => array('custom_contactus_form_validate'),
		);
		$form['cancel'] = array(
			'#name' => 'cancel',
			'#type' => 'button',
			'#value' => t('Cancel'),
			'#attributes' => array('onclick' => 'window.location="http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '"; return false;'),
	  ); 
		$form_state['storage']['addquestions']++;
		return $form; 
	} 
  else {
		drupal_set_message('Page not found!', 'error');
	}
} 

function custom_form_clear($form, &$form_state){
  $form_state['rebuild'] = TRUE;
}

/*
 * function to set the configurations related to contact us form
*/
function custom_contactus_configurations($form, $form_state) { 
  $form = array();
// Title configurations  fieldset collapsible  
  $form['title_config'] = array(
		'#type' => 'fieldset',
		'#title' => t('Title Configurations'),
		'#collapsible' => TRUE, 
		'#collapsed' => FALSE,
  );  
  $form['title_config']['title1'] = array(
		'#type' => 'textfield',
		'#title' => 'Commercial Representatives form title',
		'#size' => 170,
		'#default_value' => variable_get('title1', 'Medical Information Request(s) Form for Commercial Representatives (CS, COS, TLL, etc.)'),  
	); 
  $form['title_config']['title2'] = array(
    '#title' => 'Managed Care and Government Affairs Representatives form title',
    '#type' => 'textarea',
    '#rows' => 3,
    '#cols' => 20,
    '#resizable' => FALSE,
    '#default_value' => variable_get('title2', 'Medical Information Request(s) Form for Managed Care and Government Affairs Representatives (PFT, MCL,CFT,GAM,FRM,etc.) and Access Solutions)'),  
	);
  $form['title_config']['title3'] = array(
    '#type' => 'textfield',
    '#title' => 'Medical Affairs form title',
    '#size' => 170,
    '#default_value' => variable_get('title3', 'Medical Information Request Form for Medical Affairs Personnel (MSL)'),  
	);
// Node configurations  fieldset collapsible  	
  $form['node_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node Configurations'),
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );
  $form['node_config'] ['success_page_nid'] = array(
    '#title' => t('Node id of the page'),
    '#type' => 'textfield',
    '#size' => 10,
    '#default_value' => variable_get('success_page_nid', 938),  
  );
  
  $form['node_config'] ['success_page'] = array(
    '#title' => t('Body of the page'),
    '#type' => 'textarea',
    '#rows' => 3,
    '#cols' => 20,
    '#resizable' => FALSE,
    '#default_value' => variable_get('success_page', '<h3>Medical Information Request Form Submitted</h3><h3>Thank You</h3><p>The request you submitted on @day@, @date@, at @time@ has been sent to the Medical communications department&gt; You will be contacted if there are any questions about your request.</p>'),  
  );
  
// Form configurations  fieldset collapsible  
	
  $form['custom_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form Configurations'),
	'#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );
  $form['custom_config'] ['introduction'] = array(
    '#title' => t('Introduction Text'),
    '#type' => 'textarea',
    '#rows' => 3,
    '#cols' => 20,
    '#resizable' => FALSE,
    '#default_value' => variable_get('introduction', 'This form is for the submission of unsolicited medical questions. Any other use is strictly prohibited.<br />Required fields are marked with * (Company Name is required if Delivery Method is U.S. Mail or Overnight.)'),  
  );
  $form['custom_config'] ['sal'] = array(
    '#title' => t('Salutation'),
    '#type' => 'textarea',
    '#rows' => 3,
    '#cols' => 20,
    '#resizable' => FALSE,
    '#default_value' => variable_get('sal', 'Dr.|Mr.|Ms.|Mrs.|Miss.|Other'), 
  );
  $form['custom_config'] ['state'] = array(
    '#title' => t('State'),
    '#type' => 'textarea',
    '#rows' => 3,
    '#cols' => 20,
    '#resizable' => FALSE,
    '#default_value' => variable_get('state', 'AK|AL|AR|AZ|CA|CO|CT|DC|DE|FL|GA|HI|IA'),  
  );
  $form['custom_config'] ['product'] = array(
    '#title' => t('Product'),
    '#type' => 'textarea',
    '#rows' => 3,
    '#cols' => 20,
    '#resizable' => FALSE,
    '#default_value' => variable_get('product', 'Actemra|Activase|Avastin - Breast|Avastin - Breast|Avastin - Cervical|Avastin - General|Avastin - Lung|Avastin - Ocular|Boniva|Cathflo Activase|CellCept|Erivedge'), 
  );
  $form['custom_config'] ['credential'] = array(
    '#title' => t('Credential'),
    '#type' => 'textarea',
    '#rows' => 3,
    '#cols' => 20,
    '#resizable' => FALSE,
    '#default_value' => variable_get('credential', 'DDS|DO|LVN|MD|MD PHD|Non HCP|NP|OD|PA'), 
  );
  $form['custom_config'] ['pract_setting'] = array(
    '#title' => t('Practice Setting'),
    '#type' => 'textarea',
		'#rows' => 3,
		'#cols' => 20,
		'#resizable' => FALSE,
		'#default_value' => getPracticeSettingVarible(), 
  );
  $form['custom_config'] ['disclaimer'] = array(
    '#title' => t('Desclaimer text Text'),
    '#type' => 'textarea',
    '#rows' => 3,
    '#cols' => 20,
    '#resizable' => FALSE,
    '#default_value' => variable_get('disclaimer', ''),  
  );
// Unique code configurations  fieldset collapsible  	
  $form['code_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Code Configurations'),
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );
  $form['code_config'] ['code_commrep_form'] = array(
    '#title' => t('Code for Commercial form'),
    '#type' => 'textfield',
    '#size' => 10,
    '#default_value' => variable_get('code_commrep_form', 'USMASR'),  
  );
  
  $form['code_config'] ['code_msl_form'] = array(
    '#title' => t('Code for MSL form'),
    '#type' => 'textfield',
    '#size' => 10,
    '#default_value' => variable_get('code_msl_form', 'USMMSL'),  
  ); 
  $form['code_config'] ['code_mcl_form'] = array(
    '#title' => t('Code for MCL form'),
    '#type' => 'textfield',
    '#size' => 10,
    '#default_value' => variable_get('code_mcl_form', 'USMMCL'),  
  );
  
// Mail configurations  fieldset collapsible  
  $form['mail_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mail Configurations'),
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );
  $form['mail_config']['from_addr'] = array(
    '#title' => t('From'),
    '#type' => 'textfield',
    '#size' => 60,
    '#default_value' => variable_get('from_addr', 'noreply@gene.com'),
  );

  $form['mail_config']['to_addr_1'] = array(
    '#title' => t('To address Commercial Representatives Form'),
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 200,
    '#default_value' => variable_get('to_addr_1', ''),
  );
  $form['mail_config']['to_addr_2'] = array(
    '#title' => t('To address Care Affairs Form'),
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 200,
    '#default_value' => variable_get('to_addr_2', ''),
  );
  $form['mail_config']['to_addr_3'] = array(
    '#title' => t('To address Medical Affairs Form'),
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 200,
    '#default_value' => variable_get('to_addr_3', ''),
  );
  $form['mail_config']['subject'] = array(
    '#title' => t('Subject'),
    '#type' => 'textfield',
    '#size' => 60,
    '#default_value' => variable_get('subject', ''),
  );
  $form['mail_config']['mail_body'] = array(
    '#title' => t('Email body'),
    '#type' => 'textarea',
    '#rows' => 3,
    '#cols' => 20,
    '#resizable' => FALSE,
    '#default_value' => variable_get('mail_body', ''),
  );
  $form['mail_config']['auto_reply_subject'] = array(
    '#title' => t('Auto Reply Subject'),
    '#type' => 'textfield',
    '#size' => 60,
    '#default_value' => variable_get('auto_reply_subject', 'Medical Information Request'),
  );  
  $form['mail_config']['auto_reply_body'] = array(
    '#title' => t('Auto Reply Email body '),
    '#type' => 'textarea',
    '#rows' => 3,
    '#cols' => 20,
    '#resizable' => FALSE,
    '#default_value' => variable_get('auto_reply_body', ''),
  ); 
  
 // Mail configurations  fieldset collapsible  
  $form['mail_config_literature_form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Literature form Mail Configurations'),
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
  );
  $form['mail_config_literature_form']['frm_addr'] = array(
    '#title' => t('From Adress'),
    '#type' => 'textfield',
    '#size' => 60,
    '#default_value' => variable_get('frm_addr', 'noreply@gene.com'),
  );
  $form['mail_config_literature_form']['to_addr'] = array(
    '#title' => t('To address'),
    '#type' => 'textfield',
    '#size' => 60,
    '#maxlength' => 200,
    '#default_value' => variable_get('to_addr', ''),
  );
  $form['mail_config_literature_form']['literature_form_subject'] = array(
    '#title' => t('Subject'),
    '#type' => 'textfield',
    '#size' => 60,
    '#default_value' => variable_get('literature_form_subject', ''),
  ); 
  
  $form['mail_config_literature_form']['literature_form_mail_body'] = array(
    '#title' => t('Email body'),
    '#type' => 'textarea',
    '#rows' => 3,
    '#cols' => 20,
    '#resizable' => FALSE,
    '#default_value' => variable_get('literature_form_mail_body', ''),
  );
  $form['mail_config_literature_form'] ['search_form_success_page'] = array(
    '#title' => t('Body of the page'),
    '#type' => 'textarea',
    '#rows' => 3,
    '#cols' => 20,
    '#resizable' => FALSE,
    '#default_value' => variable_get('search_form_success_page', '<h3>Literature Search Form Submitted</h3><h3>Thank You</h3><p>The request you submitted on  @custom_date@, at @custom_time@ PDT has been sent to Literature Search database and/or Medical Communications department. You will be contacted if there are any questions about your request.</p>'),  
  );
		/* $form['product_mail_recievers'] = array(
    '#title' => t('Email Ids for product change notifications'),
    '#type' => 'textfield',
		'#description' => 'Enter Comma seperated email ids',
	'#size' => 60,
	'#maxlength' => 200,
    '#default_value' => variable_get('product_mail_recievers', ''),
  ); */
  $form['clear'] = array(
    '#name' => 'clear',
    '#type' => 'button',
    '#value' => t('Reset'),
    '#attributes' => array('onclick' => 'this.form.reset(); return false;'),
  );

  $form['#submit'][] = 'custom_contactus_configurations_submit';
  return system_settings_form($form);
}

/*
*  Submit function for custom_contactus_configurations to add product details to db
*/
function custom_contactus_configurations_submit(&$form, &$form_state){
	global $user;
	$result = array();
	$default_product = explode("|",$form['custom_config']['product']['#default_value']);
	$submitted_product = explode("|",$form_state['values']['product']);
	$removed_products = array_diff($default_product,$submitted_product);
	$added_products = array_diff($submitted_product,$default_product);
	if(count($removed_products)>0 || count($added_products)>0){
		$body="";
		if(isset($removed_products) && count($removed_products) > 0){
			$result['removed'] = implode(" | ", $removed_products); 
			$body .= "The product ".$result['removed']. "has been removed by ".$user->name." on " .date("Y-m-d H:i:s", time());
		}
		if(isset($added_products)&& count($added_products) > 0){
			$result['added'] = implode(" | " ,$added_products);
			$body .= "The product ".$result['added']. "has been added by ".$user->name." on " .date("Y-m-d H:i:s", time());
		}
		foreach($result as $key => $val){
		/*			db_insert('custom_audit_product_details')
				->fields(array(
					'Products' => $val,
					'Date' => date("Y-m-d H:i:s", time()),
					'User' => $user->name,
					'Action' => $key,
				))->execute(); */
		//}
		/*$to = $form_state['values']['product_mail_recievers'];
		$from = variable_get('site_mail', '');
		$key = 'custom_contactus_notifications';
		$params = array(
				'context' => array(
					'subject' => 'Product change notification',
					'body' => $body,
					'bcc' => $to,
				),
				'attachments' => array(),
				'sender' => $from,
			);
		
		try {
				$answer = drupal_mail('custom_contactus', $key, $to, language_default(), $params, $from, TRUE);

				if (!$answer['result']) {
					throw new Exception("The message could not be sent.");
				}
			}
			catch (Exception $e) {
				//throw new Exception("The message could not be sent.");
		 }*/
	  }	 
  }
}  
/**
 * Function  callback  to the ajax
 */
function custom_registration_ajax_add_participant($form, $form_state) {
  return $form['addquestions'];
}
/**
 * Validate function  of the contact us form
 */
function custom_contactus_form_validate($form, &$form_state) { 
  if ($form_state['values']['email'] == 'email') {
    if (!valid_email_address($form_state['values']['email'])) {
			form_set_error('email', t('That e-mail address is not valid.'));
    }
  }
  if(($form_state['values']['delivery_method'] == 'US Mail' || $form_state['values']['delivery_method'] == 'Overnight') &&    $form_state['values']['cname'] == '' ){ 
	  form_set_error('cname', t('The Company name field is required.'));  
  }
	if($form_state['values']['email'] == '' && $form_state['values']['delivery_method'] == 'Email'){ 
	  form_set_error('email', t('Email field is required.'));  
  }
	$fax_empty = false;
	if($form_state['values']['fax'] == '' || $form_state['values']['fax2'] == '' || $form_state['values']['fax3'] == '') {
		$fax_empty = true;
	}
	$fax_arr = array('fax','fax2','fax3');
	foreach($fax_arr as $fax) {
		if($form_state['values'][$fax] == '' && $form_state['values']['delivery_method'] == 'Fax'){
			form_set_error($fax, t(ucfirst($fax).' field is required.'));  
		}
  }
  if(isset($form_state['input']['request_date'])){
    if($form_state['input']['request_date']['date'] > $form_state['values']['fdate'])	{
      form_set_error('request_date', t('The Request date should not be a future date'));   
    }
  }
}
/**
 * Function called from within a custom submit handler.
 */
 function custom_contactus_form_submit($form, &$form_state) {
  custom_contactus_mail_send($form_state['input']);
} 
/**
 * Implements hook_mail.
 */
function custom_contactus_mail($key, &$message, $params) { 
   $message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed';
  //$message['headers']['Content-Type'] = 'text/plain; charset=UTF-8; format=flowed';
	$message['headers']['Content-Transfer-Encoding'] = '7bit';	
  $options = array(
    'langcode' => $message['language']->language,
  );
  switch ($key) {
    case 'contact_message':
			$message['subject'] = variable_get('subject', 'Unsolicited Medical Information Request');
			$message['body'][] = replace_token_body(variable_get('mail_body', ''),$params, 'mail');
    break;
	    case 'contact_auto_reply_message':
			$message['subject'] = variable_get('auto_reply_subject', 'Medical Information Request');
			$message['body'][] = replace_token_body(variable_get('auto_reply_body', ''),$params, 'auto_reply_mail');
    break;
		case 'literature_form_message': 
			$message['subject'] = variable_get('literature_form_subject', '');
			$message['body'][] = replace_token_body(variable_get('literature_form_mail_body', ''),$params, 'literature_form_mail');
    break;
  }
}
/**
* Function to send the email 
*/
 function custom_contactus_mail_send($params) {
  $module = 'custom_contactus'; 
  $key = 'contact_message';
  if($params['form_type'] == 'commercial-representatives'){
		$to = variable_get('to_addr_1', 'admin@example.com');
  }
  if($params['form_type'] == 'care-affairs'){
		$to = variable_get('to_addr_2', 'admin@example.com');
  }
  if($params['form_type'] == 'medical-affairs'){
		$to = variable_get('to_addr_3', 'admin@example.com');
  } 
  $from = variable_get('from_addr', 'admin@example.com');
  $language = language_default();
  $send = TRUE;
  $result = drupal_mail($module, $key, $to, $language, $params, $from, $send);
  if ($result['result'] == TRUE) {
	  custom_contactus_auto_reply_mail_send($params);
    drupal_goto('contactus/success');
  } 
  else {
    drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
  }
} 

/**
* Function to send the the auto reply email 
*/
function custom_contactus_auto_reply_mail_send($params) {
  $module = 'custom_contactus'; 
  $key = 'contact_auto_reply_message';
  $to = $params['unixid'].'@gene.com';
  $from = variable_get('from_addr', 'do-not-reply@gene.com');
  $language = language_default();
  $send = TRUE;
  $result = drupal_mail($module, $key, $to, $language, $params, $from, $send);
  if ($result['result'] == TRUE) {
    drupal_set_message(t('Mail sent.'));
  }
  else {
    drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
  }
}

function get_sal_list(){
  $sal_get_values =  variable_get('sal', 'Dr.|Mr.|Ms.|Mrs.|Miss.|Other');
  $sal_values = explode("|", $sal_get_values);
  $sal_list = array_combine($sal_values, $sal_values);
  return $sal_list;
}

function get_product_list(){ 
  $product_get_values =  variable_get('product', 'Actemraaaaaaa|Activase|Avastin - Breast|Avastin - Breast|Avastin - Cervical|Avastin - General|Avastin - Lung|Avastin - Ocular|Boniva|Cathflo Activase|CellCept|Erivedge');
  $product_values = explode("|", $product_get_values);
  $product_list = array_combine($product_values, $product_values);
  return $product_list;
}

function get_credential_list($form_type) {
	if($form_type == 'care-affairs') {
		$credential_get_values =  variable_get('credential', 'DDS|DO|LVN|MD|MD PHD|Non HCP|NP|OD|PA')."|HCP-Other";
	} 
  else {
		$credential_get_values =  variable_get('credential', 'DDS|DO|LVN|MD|MD PHD|Non HCP|NP|OD|PA');
	}
  $credential_values = explode("|", $credential_get_values);
	asort($credential_values);
  $credential_list = array_combine($credential_values, $credential_values);
  return $credential_list;
}
function getPracticeSettingVarible() {
	$practice_get_values =  variable_get('pract_setting', 'Alle|Allergy
	Card|Cardiology
	CliPhar|Clinical Pharmacy
	Derm|Dermatology
	EmMed|Emergency Medicine
	Endo|Endocrinology
	Gastro|Gastroenterology
	Hema|Hematology
	Immu|Immunology
	IntRad|Interventional Radiology
	Neph|Nephrology
	Neur|Neurology
	NurEdu|Nurse Educator
	NurPrac|Nurse Practitioner
	Onco|Oncology
	HemOnc|Hematology/Oncology
	Pulm|Pulmonology
	RetSpe|Retinal Specialist
	Rheu|Rheumatology
	SpecPhar|Specialty Pharmacy
	Transplant|Transplantation Specialist
	VasSur|Vascular Surgery');
	return $practice_get_values;
}
function get_practice_list($form_type) {
	if($form_type == 'care-affairs') {
    $practice_get_values =  getPracticeSettingVarible()."
      Z-Com/Gui|Compendia/Publishers
      Z-HMO/HP|HMO/Health Plan/Private Payor
      Z-Medicare|Medicare/Medicaid.Public Payor
      Z-PBM|PBM";
  } else {
    $practice_get_values =  getPracticeSettingVarible();	
  }
  $practice_values = explode("\n", $practice_get_values);
	foreach($practice_values as $index => $practice_value) {
		$tmpPrac = explode('|', $practice_value);
		$practices[$tmpPrac[0]] = $tmpPrac[1];
	}
	asort($practices);
	$practices['Other'] = 'Other';
  //$practice_list = array_combine($practice_values, $practice_values);
	//asort($practice_list);
  return $practices;
}

function get_state_list(){
  $state_get_values =  variable_get('state', 'AK|AL|AR|AZ|CA|CO|CT|DC|DE|FL|GA|HI|IA');
  $state_values = explode("|", $state_get_values);
  $state_list = array_combine($state_values, $state_values);
  return $state_list;
} 