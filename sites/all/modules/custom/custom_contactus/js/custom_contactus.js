(function ($) {
	$(document).ready(function() {
		if(($('#edit-delivery-method').val() == 'usmail') || ($('#edit-delivery-method').val() == 'overnight')) {
			$('.form-item.form-type-textfield.form-item-cname > label').html("<label for='edit-cname'>Company name <span title='This field is required.' class='form-required'>*</span></label>");
		} else if($('#edit-delivery-method').val() == 'fax') {
			$('span.hyphen').css({'display':'block'});
		}
		$('#edit-delivery-method').change(function() {
			if($(this).val() == 'usmail' || $(this).val() == 'overnight') {
					var str = $('.form-item.form-type-textfield.form-item-cname > label').html();
					$('.form-item.form-type-textfield.form-item-cname > label').html("<label for='edit-cname'>Company name <span title='This field is required.' class='form-required'>*</span></label>");
			} else {
					$('.form-item.form-type-textfield.form-item-cname > label').html("<label for='edit-cname'>Company name</label>");
					if($(this).val() == 'fax') {
						$('.form-item.form-type-textfield.form-item-fax > label').html("<label for='edit-fax'>Fax <span title='This field is required.' class='form-required'>*</span></label>");
					}
					if($(this).val() == 'mail') {
						$('.form-item.form-type-textfield.form-item-email > label').html("<label for='edit-email'>Email <span title='This field is required.' class='form-required'>*</span></label>");
					}
			}
			if($(this).val() == 'fax') {
				$('span.hyphen').css({'display':'block'});
			} else {
				$('span.hyphen').css({'display':'none'});
			}
		});
		$('.form-item.form-type-textfield.form-item-fax > label').html("<label for='edit-fax'>Fax <span title='This field is required.' class='form-required'>*</span></label>");
		$('.form-item.form-type-textfield.form-item-email > label').html("<label for='edit-email'>Email <span title='This field is required.' class='form-required'>*</span></label>");
		$('#edit-phone, #edit-fax').before('<span style="float:left;margin-top:2px;">(</span>').after('<span style="float:left;margin-top:2px;">)</span>')
		$('.phone').keyup(function() {
			if($(this).val().length == $(this).attr('maxlength')) {
				var id = $(this).attr('id').slice(-1); 
				var element = 'fax';
				if($(this).attr('id').match('phone') == 'phone') {
					element = 'phone';
				}
				if(id == 2) {
					$('#edit-'+element+'3').focus();
				} else if($(this).attr('id') == 'edit-phone' || $(this).attr('id') == 'edit-fax'){
					$('#edit-'+element+'2').focus();
				}
			}
		});
	
	//clear the form elements after submit
	 $('.custom_contact_form .button_cancel').click(function() {
     $(this).closest('form').find('input[type=text], textarea, select').val('');
     return false;
     });
	
	});
    
  
 	
})(jQuery);


