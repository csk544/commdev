<?php
/*
 * To create  Literature Search form
*/

function literature_search_form($form, &$form_state) {
    $form['#attributes']['class'][] = 'literature_search_form';	
		$form['title'] = array(
			'#type' => 'item',
			'#markup' => t('<h2>Literature Search Form</h2>'), 
			'#description' => t('Required fields are marked with *'),
		);
	  // User info fieldset collapsible
		$form['user_information'] = array( 
			'#type' => 'fieldset',
			'#title' => t('User Information'), 
			//'#description' => t('Enter at least 3 characters (all searches use a wildcard search by default)'),
		);
		$form['user_information']['first_name'] = array(
			'#type' => 'textfield',
			'#title' => t('First Name'),
			'#size' => 30,
			'#required' => TRUE,
		);
		$form['user_information']['last_name'] = array(
			'#type' => 'textfield',
			'#title' => t('Last Name'),
			'#size' => 30,
			'#required' => TRUE,
		);
		$format = 'd-m-Y';
		$form['user_information']['date_needed'] = array(
				'#title' => t('Date Needed'),
				'#type' => 'date_popup',
				'#date_format' => $format,
				'#date_label_position' => 'within',
				'#date_year_range' => '0:+2',
				'#default_value' => format_date(time(), 'custom', $format), 
				//'#datepicker_options' => array('maxDate' => 0),
				'#required' => TRUE,	  
			 );

	  // Questions To Be Answered fieldset collapsible
		$form['lsf_questions'] = array(
			'#type' => 'fieldset',
			'#title' => t('Questions To Be Answered'),
			'#description' => t('Write as much as you like. Too much information is just fine. The more the searcher understands the information need, the better the search and the more precise will be the editing. If keywords are given - and they are welcome - please indicate the relationship between them.'),
		);
		$form['lsf_questions']['questions'] = array(
			'#title' => 'Questions',
			'#type' => 'textarea',
			'#rows' => 3,
			'#cols' => 10,
			'#resizable' => FALSE,
			'#required' => TRUE,
		);
		
		$form['lsf_questions']['search_time'] = array(
			'#type' => 'radios',
			'#title' => t('How far back should the search go?'),
			'#options' => array(
				  '5 Years' => t('5 Years'),
				  '2 Years' => t('2 Years'), 
				  'year of drug approval' => t('year of drug approval'),
				  'other' => t('other'),
               ),
			'#required' => TRUE,
		);
		 
	  	$form['lsf_questions']['time_period'] = array(
			'#type' => 'textfield',
			'#title' => t('Time period requested'),
			'#size' => 30,
			'#states' => array(
				'visible' => array(
					':input[name="search_time"]' => array('value' => 'other'),
				),
			),
		);
     	$form['lsf_questions']['usage'] = array(
			'#type' => 'radios',
			'#title' => t('Substance Usage'),
			'#options' => array(
				  'therapeutic use only' => t('therapeutic use only'),
				  'any mention of compound' => t('any mention of compound'),
           	    ),
			'#required' => TRUE,  	
		);
      	$form['lsf_questions']['source'] = array(
			'#type' => 'radios',
			'#title' => t('Substance Source'),
			'#options' => array(
				  'administered' => t('administered'),
				  'endogenous' => t('endogenous'),
				  'both' => t('both'),
               ),
			'#required' => TRUE,   
		);
   	    $form['lsf_questions']['population'] = array(
			'#type' => 'checkboxes',
			'#title' => t('Population<br /><i class="label-ext">(Check all that apply)</i>'),
			'#options' => array(
				  'human' => t('human'),
				  'animal ' => t('animal'),
				  'in vitro' => t('in vitro'),
               ),
			'#required' => TRUE,   
		);			
      	$form['lsf_questions']['language'] = array(
			'#type' => 'radios',
			'#title' => t('Language Requirements'),
			'#options' => array(
				  'English only' => t('English only'),
				  'any, with English abstract ' => t('any, with English abstract '),
				  'any' => t('any'),
               ),
			'#required' => TRUE,   
		);	
   	    $form['lsf_questions']['publication_type'] = array(
			'#type' => 'checkboxes',
			'#title' => t('Publication Type'),
			'#options' => array(
				  'include review articles' => t('include review articles'),
               ),
		);
      	$form['lsf_questions']['preference'] = array(
			'#type' => 'radios',
			'#title' => t('Would you prefer that the searcher call to get a better understanding of your request?'),
			'#options' => array(
				  'yes' => t('yes'),
				  'no' => t('no'),
               ),
		);
		$form['lsf_questions']['reason'] = array(
			'#title' => 'Reason/background of request<br /><i class="label-ext">(e.g.: HCP inquiry, patient involvement, drug safety, other)</i>',
			'#type' => 'textarea',
			'#rows' => 3,
			'#cols' => 20,
			'#resizable' => FALSE,
			'#required' => TRUE,
			
		);
		$form['lsf_questions']['instructions'] = array(
			'#title' => 'Note/Special Instructions',
			'#type' => 'textarea',
			'#rows' => 3,
			'#cols' => 20,
			'#resizable' => FALSE,
		);
        		$form['buttons'] = array(
			'#type' => 'fieldset',
		);
		$form['submit'] = array(
			'#type' => 'submit',
			'#value' => t('Submit'),
			//'#validate' => array('custom_contactus_form_validate'), 
		);
		$form['cancel'] = array(
			'#name' => 'cancel',
			'#type' => 'button',
			'#value' => t('Cancel'),
			'#attributes' => array('onclick' => 'window.location="http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '"; return false;'),
	  ); 		
return $form; 		
}

/**
 * Function called from within a custom submit handler.
 */
function literature_search_form_submit($form, &$form_state) {
  literature_search_form_mail_send($form_state['input']);
}

/**
 * Implements hook_mail.
 */
function custom_contactus_mail($key, &$message, $params) { 
  $message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed';
  $options = array(
    'langcode' => $message['language']->language,
  );
  switch ($key) {

		case 'literature_form_message': 
			$message['subject'] = variable_get('literature_form_subject', '');
			$message['body'][] = replace_token_body(variable_get('literature_form_mail_body', ''),$params, 'literature_form_mail');
    break;
  }
}

/**
* Function to send the email 
*/
function literature_search_form_mail_send($params) {
  $module = 'custom_contactus'; 
  $key = 'literature_form_message'; 
  $to = variable_get('to_addr', 'barajus@gene.com');
  $from = variable_get('frm_addr', 'admin@example.com');
  $language = language_default();
  $send = TRUE;
  $result = drupal_mail($module, $key, $to, $language, $params, $from, $send);
  if ($result['result'] == TRUE) {
	  //custom_contactus_auto_reply_mail_send($params);
    drupal_goto('search_form/success');
  } else {
    drupal_set_message(t('There was a problem sending your message and it was not sent.'), 'error');
  }
}