(function ($) {
	var fetchFuncTitle = false, unixId = "", nid = 0, orgObj = {}, resetOrgForm = true;
  Drupal.behaviors.commportal_org_chart = {
    attach: function (context, settings) {
		$('#org-nid', context).once(function(){
			nid = $("#org-nid").val();
			$.ajax({
					method: "POST",
					url: Drupal.settings.basePath + "get-org-info",
					data: { nid: nid}
				}).done(function(data) {
					orgObj = data;
				});
		});
		$(".add-person").click(function(){
			if(resetOrgForm){
				$("#edit-unixid").val("");
				$("#edit-functional-title").val("");
				$("#edit-parent").val("");
				$('#edit-parent').trigger('chosen:updated');
				$(".form-item-parent").removeClass("hide");
				$(".form-item-admin").removeClass("hide");
				$('#edit-admin').prop('checked', false);
				$('#edit-open').prop('checked', false);
				$("input[name=edit]").val(false);
				$("#edit-unixid").prop('disabled', false);
				$("#edit-delete").addClass("hide");
				$(".show-org-messages").html("");
			}
			resetOrgForm = true;			
		});
		$("input#edit-unixid", context).bind('autocompleteSelect', function(event, node) {
			unixId = $(node).data('autocompleteValue');
			$(".show-org-messages").html(Drupal.t("Fetching Functional Title..."));
				$.ajax({
					method: "POST",
					url: Drupal.settings.basePath + "fetch-func-title",
					data: { unixId: unixId}
				}).done(function(functionalTitle) {
					fetchFuncTitle = false;
					$("#edit-functional-title").val(functionalTitle);
					$(".show-org-messages").html("");
				});
		});
		$('#edit-open').change(function(){
			if($("#edit-open").is(":checked")){
				$("#edit-unixid").val("");
				$("#edit-unixid").prop('disabled', true);
			}else{
				$("#edit-unixid").prop('disabled', false);
			}
		});
		$(".employee-container").click(function(){
			$(".show-org-messages").html("");
			$("#edit-delete").removeClass("hide");
			var selectedIndex = $(this).attr("data-index");
			var selectedUnixId = orgObj.associates[selectedIndex];						
			var parent = 0;
			if(typeof orgObj.associatesData[selectedIndex].parent == "undefined"){
				$(".form-item-parent").addClass("hide");
				$(".form-item-admin").addClass("hide");
				var selectedFuncTitle = orgObj.associatesData[selectedIndex].FuncTitle;
				var selectedIsAdmin = orgObj.associatesData[selectedIndex].isAdmin;
				var selectedIsOpen = orgObj.associatesData[selectedIndex].isOpen;
			}else{
				parent = orgObj.associatesData[selectedIndex].parent;
				$("#edit-parent").val(parent);
				$('#edit-parent').trigger('chosen:updated');
				$(".form-item-parent").removeClass("hide");
				$(".form-item-admin").removeClass("hide");
				var selectedPersonObj = {};
				if(typeof orgObj.admins[selectedIndex] != "undefined"){
					selectedPersonObj = orgObj.associatesData[parent].admin[selectedIndex];					
				}else{
					selectedPersonObj = orgObj.associatesData[parent].sub[selectedIndex];					
				}
				var selectedFuncTitle = selectedPersonObj.FuncTitle;
				var selectedIsAdmin = selectedPersonObj.isAdmin;
				var selectedIsOpen = selectedPersonObj.isOpen;
			}			
			$("#edit-unixid").val(selectedUnixId);
			$("#edit-functional-title").val(selectedFuncTitle);
			$('#edit-admin').prop('checked', selectedIsAdmin);
			$('#edit-open').prop('checked', selectedIsOpen);
			if(selectedIsOpen){
				$("#edit-unixid").val("");
				$("#edit-unixid").prop('disabled', true);
			}
			$("input[name=edit]").val(selectedIndex);
			resetOrgForm = false;
			$(".add-person").click();
		});
		$(".save-org-chart").click(function() {
			unixId = $.trim($("#edit-unixid").val());
			var funcTitle = $.trim($("#edit-functional-title").val());
			var associate = $("#edit-parent").val();
			var availableAssociatesCount = $("#edit-parent option").length;
			var isAdmin = $("#edit-admin").is(":checked");
			var isOpen = $("#edit-open").is(":checked");
			var edit = $("input[name=edit]").val();
			var error = false;
			$(".show-org-messages").html("");
			$(".show-org-messages").removeClass("success");
			if(availableAssociatesCount > 1 && associate == 0 && !$(".form-item-parent").hasClass("hide")){
				error = true;
				$(".show-org-messages").append(Drupal.t("Please select the Person to Associate. "));
			}
			if(unixId == "" && isOpen == false){
				error = true;
				$(".show-org-messages").append(Drupal.t("Please enter either UNIX ID or select as Open position."));
			}			
			if(error == false){
				$.ajax({
					method: "POST",
					url: Drupal.settings.basePath + "save-org-info",
					data: { nid: nid, edit: edit, unixId: unixId, funcTitle: funcTitle, associate:associate, isAdmin: isAdmin, isOpen: isOpen }
				}).done(function(result) {
					var status = result.status;
					var message = result.message;
					if(status == "success"){
						window.location.reload();
					}else{
						$(".show-org-messages").html(message);
					}
				});
			}			
			return false;
		});
		$(".delete-current-associate").click(function(){
			var edit = $("input[name=edit]").val();
			$.ajax({
				method: "POST",
				url: Drupal.settings.basePath + "delete-current-associate",
				data: { nid: nid, edit: edit}
			}).done(function(result) {
				var status = result.status;
				var message = result.message;
				if(status == "success"){
					window.location.reload();
				}else{
					$(".show-org-messages").html(message);
				}
			});
			return false;
		});
    }
  };
}(jQuery));

