<?php
$name = ($data->isOpen) ? t("Open") : ((trim($data->preferred_fullname) == "") ? $data->first_name . ' ' . $data->last_name : $data->preferred_fullname);
$profilePic = ($data->guid != 0) ? "http://gwiz4.gene.com/databases/empdir/portraits/" . $data->guid . "_1.jpg" : "http://www.electricvelocity.com.au/Upload/Blogs/smart-e-bike-side_2.jpg";
?>
<div class="employee-image">
	<img src="<?php echo $profilePic;?>" class="rounded" />
</div>
<div class="employee-title-container">
	<div class="employee"><?php echo $name;?></div>
	<div class="fictional-title"><?php echo $data->FuncTitle;?></div>
</div>
<?php if(!$data->isOpen):?>
<div class="member__bio-links">
	<div class="member__more-info">
		<span class="icon-info-circled hasTooltip"></span>
		<div class="tooltip-content">
			<div class="col-md-4 member__more-info-image">
				<div class="employee-image">
					<img src="<?php echo $profilePic;?>" class="rounded" />
				</div>
			</div>
			<div class="col-md-8 member__more-info-contact">
				<ul>                                
					<li><strong><?php echo t("UnixID:");?></strong> <?php echo $data->unixid;?></li>
					<li><a target="_blank" href="https://mail.google.com/mail/?view=cm&fs=1&to='. <?php echo $data->email;?>"><?php echo $data->email;?></a></li>
					<li><strong>p:</strong> <?php echo $data->phone_number;?></li>
					<li><strong>m:</strong> <?php echo $data->mobile_number;?></li>
					<li><strong><?php echo t("Mail Stop:");?></strong> <?php echo $data->mail_stop;?></li>
					<li><strong><?php echo t("Building:");?></strong> <?php echo $data->building;?>, Room <?php echo $data->room_number;?></li>
				</ul>
			</div>                                   
		</div>
	</div>
</div>
<?php endif;?>