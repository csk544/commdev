<?php

/**
 * Implementation of hook_form() for Organisation Chart.
 */
function commportal_org_chart_form($form, &$form_state, $associates, $admins, $unixIdsDetails){
	$hideClass = count($associates)>0 ? "" : "hide";
	$managers = array_diff($associates, $admins);
	if(count($managers)>0 && count($unixIdsDetails)>0){
		foreach($managers as $index => $unixId){
			if(isset($unixIdsDetails[$unixId])){
				$managers[$index] = $unixIdsDetails[$unixId]->preferred_fullname . " (" . $unixId . ")";
			}
		}
	}
	$nid = arg(1);
	drupal_add_js(drupal_get_path('module', 'commportal_org_chart') . '/js/commportal_org_chart.js');
	$form['unixid'] = array(
		'#type' => 'textfield',
		//'#title' => t('UNIX ID'),
		'#size' => 30,
		'#attributes' => array('placeholder' => t('UNIX ID')),
		'#autocomplete_path' => 'autosuggest-unixid',
		//'#required' => TRUE
	);
	$form['parent'] = array(
		'#type' => 'select',
		'#title' => t('Reports to: '),
		'#options' => $managers,
		'#attributes' => array('class' => array('chosen-select')),
		'#prefix' => "<div class=". $hideClass .">",
		'#suffix' => "</div>",
		'#required' => TRUE
	);
	$form['functional_title'] = array(
		'#type' => 'textfield',
		//'#title' => t('Functional Title'),
		'#size' => 30,
		'#attributes' => array('placeholder' => t('Functional Title')),
		//'#required' => TRUE
	);
	//$parentOptions = array('--select--');
	$form['admin'] = array(
		'#type' =>'checkbox', 
		'#title' => t('Admin'),
		'#default_value' => 0,
		'#prefix' => "<div class=". $hideClass .">",
		'#suffix' => "</div>"
	);
	$form['open'] = array(
		'#type' =>'checkbox', 
		'#title' => t('Open'),
		'#default_value' => 0,
	);
	$form['nid'] = array(
		'#type' => 'hidden', 
		'#value' => $nid,
		'#attributes' => array('id' => 'org-nid'),
	);
	$form['edit'] = array(
		'#type' => 'hidden', 
		'#value' => false
	);
	$form['submit'] = array(
		'#type' => 'submit', 
		'#value' => t('Save'),
		'#attributes' => array('class' => array('save-org-chart')),
	);
	$form['delete'] = array(
		'#type' => 'submit', 
		'#value' => t('Delete'),
		'#attributes' => array('class' => array('delete-current-associate')),
	);	
	return $form;
}

/**
 * To Get Organisation Info
 */
function get_org_info(){
	$nid = $_POST['nid'];
	$query = db_select('field_data_field_org_chart_content', 'f');
    $query->fields('f', array('field_org_chart_content_value'));
    $query->condition('entity_id', $nid);
    $result = $query->execute()->fetchField();
	drupal_json_output(unserialize($result));
}

/**
 * Menu callback; Auto suggestions on user key press in Unix Id box of Organisation Chart
 */
function fetch_func_title() {
  $matches = array();
  global $base_url;
  $unixId = $_POST['unixId'];
  if (!empty($unixId)) {
	db_set_active('gene_ldap');    
    $query = db_select('ldap_users', 'u');
    $query->fields('u', array('job_title'));
    $query->condition('unixid', $unixId);
    $functionalTitle = $query->execute()->fetchField();
    echo $functionalTitle;
    db_set_active('default'); 
	exit;
  }
}

/**
 * To Save / Update Organisation Chart Info
 */
function save_org_info(){
	global $user;
	$error = false;
	$message = "Invalid Request";
	$response = new stdClass();
	$response->status = "error";
	if(isset($_POST['nid']) && is_numeric($_POST['nid'])){
		$node = node_load($_POST['nid']);
		$dataArr = field_get_items('node', $node, 'field_org_chart_content');
		$defaultDataArr = array();
		$defaultDataArr['associates'] = array();
		$defaultDataArr['openPositions'] = array();
		$defaultDataArr['associatesData'] = array();
		$defaultDataArr['admins'] = array();
		$data = isset($dataArr[0]['value']) ? unserialize($dataArr[0]['value']) : $defaultDataArr;
		$unixId = trim($_POST['unixId']);
		$edit = $_POST['edit'] ? (int)$_POST['edit'] : false;
		if($unixId != ""){
			if(in_array($unixId,$data['associates']) && $data['associates'][$edit] != $unixId){
				$error = true;
				$message = "Unix Id already exists";								
			}else{
				db_set_active('gene_ldap');    
				$query = db_select('ldap_users', 'u');
				$query->fields('u', array('unixid'));
				$query->condition('unixid', $unixId);
				$dbUnixId = $query->execute()->fetchField();
				db_set_active('default');
				if($unixId != $dbUnixId){
					$error = true;
					$message = "Unix Id does not exist or inactive";
				}
			}			
		}
		if(!$error){
			$funcTitle = $_POST['funcTitle'];
			$associate = $_POST['associate'];			
			$isOpen = ($_POST['isOpen'] == "true") ? true : false;
			$isAdmin = ($_POST['isAdmin'] == "true") ? true : false;						
			$index = isset($data['associates']) ? count($data['associates'])+1 : 1;
			if(is_numeric($edit) && $edit>0){
				$index = $edit;
				if(!$isOpen && isset($data['openPositions'][$index])){
					unset($data['openPositions'][$index]);
				}
				$previousManager = $data['associatesData'][$index]['parent'];
				if(isset($data['admins'][$index])){
					unset($data['admins'][$index]);
					unset($data['associatesData'][$previousManager]['admin'][$index]);
				}else{
					unset($data['associatesData'][$previousManager]['sub'][$index]);
				}				
			}
			if($isOpen){
				$data['openPositions'][$index] = $index;
				$unixId = t("Open");
			}						
			$associateArr['unixId'] = $unixId;
			$associateArr['FuncTitle'] = $funcTitle;
			$associateArr['isAdmin'] = $isAdmin;
			$associateArr['isOpen'] = $isOpen;
			$data['associates'][$index] = $unixId;			
			if($associate>0){
				$data['associatesData'][$index]['parent'] = $associate;
				if($isAdmin){
					$data['associatesData'][$associate]['admin'][$index] = $associateArr;
					$data['admins'][$index] = $unixId;
				}else{
					$data['associatesData'][$associate]['sub'][$index] = $associateArr;
				}					
			}else{
				$data['associatesData'][$index]['unixId'] = $unixId;
				$data['associatesData'][$index]['FuncTitle'] = $funcTitle;
				$data['associatesData'][$index]['isAdmin'] = $isAdmin;
				$data['associatesData'][$index]['isOpen'] = $isOpen;
			}						
			$existVid = $node->vid;
			$node_wrapper = entity_metadata_wrapper('node', $node);
			$node_wrapper->field_org_chart_content->set(serialize($data));
			$node_wrapper->revision->set(1);
			$node_wrapper->log->set(t("Added ".$unixId." Info to Organisation Chart"));
			//$node_wrapper->uid->set($user->uid); TODO: Need to check whether the node revision is taking the current logged in user uid
			$node_wrapper->save();
			$newVid = $node->vid;
			if($existVid == $newVid){
				$message = "Organisation chart update failed, please contact administrator";
			}else{
				$response->status = "success";
				$message = "Success";
			}			
		}
	}
	$response->message = $message;
	drupal_json_output($response);
}

/**
 * To Convert Numbers to Words
 */
 function convert_number_to_words($number) {
    
    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );
    
    if (!is_numeric($number)) {
        return false;
    }
    
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
    }
    
    $string = $fraction = null;
    
    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }
    
    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens   = ((int) ($number / 10)) * 10;
            $units  = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds  = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . convert_number_to_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int) ($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= convert_number_to_words($remainder);
            }
            break;
    }
    
    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }
    
    return $string;
}

/**
 * To Prepare Organisation Chart View
 */
function prepareOrgChart($data = array(), $associatesData = array(), $subAssociatesData = array(), $unixIdsDetails = array(), $level = 1, $content = '', $debtId = 1){
	$class = ($level>1) ? "hidden" : "";
	$content = '<div class="department-groups ' . convert_number_to_words(count($subAssociatesData)) . '-groups cf ' . $class . '" data-level="' . $level . '" data-dept-id="' . $debtId . '">';
	$content .= '<div class="hanger"></div>';
	$carousel = false;
	if(count($subAssociatesData)>6){
		$carousel = true;
		$content .= '<div class="row">';
		$content .= '<div class="col-md-12">';
		$content .= '<div class="owl-carousel promo-slider org-chart">';
	}
	foreach($subAssociatesData as $index => $associateData){
		$adminsInfo = isset($associatesData[$index]['admin']) ? $associatesData[$index]['admin'] : array();
		if($carousel){
			$content .= '<div class="item">';
		}
		$itemClass = ($associateData['isOpen']) ? "open-position" : "";
		$content .= '<div class="department-group ' . $itemClass . '" data-dept-id="' . $index . '">';
		$content .= '<div class="department-group-container">';
		$containerClass = ($unixIdsDetails[$associateData['unixId']]->manager_unixid != $data['associates'][$associatesData[$index]['parent']]) ? "diff-manager" : "";
		$content .= '<div class="employee-container cursor ' . $containerClass . '" data-index="' . $index . '">';
		$unixIdsDetails[$associateData['unixId']]->FuncTitle = $associateData['FuncTitle'];
		$unixIdsDetails[$associateData['unixId']]->isOpen = $associateData['isOpen'];
		$content .= theme('employee_container',array('data'=>$unixIdsDetails[$associateData['unixId']]));
		$content .= '</div>';
		$overviewClass = '';
		if(count($adminsInfo)>0){
			$content .= '<div class="admin">';
			foreach($adminsInfo as $adminIndex => $adminInfo){
				$content .= '<div class="admin-left-attachment">';
				$adminContainerClass = ($unixIdsDetails[$adminInfo['unixId']]->manager_unixid != $data['associates'][$associatesData[$adminIndex]['parent']]) ? "diff-manager" : "";
				$content .= '<div class="employee-container cursor ' . $adminContainerClass . '" data-index="' . $adminIndex . '">';
				$unixIdsDetails[$adminInfo['unixId']]->FuncTitle = $adminInfo['FuncTitle'];
				$unixIdsDetails[$adminInfo['unixId']]->isOpen = $adminInfo['isOpen'];
				$content .= theme('employee_container',array('data'=>$unixIdsDetails[$adminInfo['unixId']]));
				$content .= '</div>';
				$content .= '</div>';
			}
			$content .= '</div>';
			$overviewClass = "has-admin";
		}
		if(isset($associatesData[$index]['sub']) && count($associatesData[$index]['sub'])>0){
			$content .= '<div class="department-overview-container ' . $overviewClass . '">';
            $content .= '<div class="department-overview oc-count">+' . count($associatesData[$index]['sub']) . '</div>';
            $content .= '</div>';
		}				
		$content .= '</div>';
		$content .= '</div>';
		if($carousel){
			$content .= '</div>';
		}
	}
	if($carousel){
		$content .= '</div>';
		$content .= '</div>';
		$content .= '</div>';
	}
	$content .= '</div>';
	foreach($subAssociatesData as $index => $associateData){
		if(isset($associatesData[$index]['sub']) && count($associatesData[$index]['sub'])>0){
			$content .= prepareOrgChart($data, $associatesData, $associatesData[$index]['sub'], $unixIdsDetails, ++$level, $content, $index);
		}
	}	
	return $content;
}

function delete_current_associate(){
	global $user;
	$error = false;
	$message = "Invalid Request";
	$response = new stdClass();
	$response->status = "error";
	if(isset($_POST['nid']) && is_numeric($_POST['nid']) && isset($_POST['edit']) && is_numeric($_POST['edit'])){
		$node = node_load($_POST['nid']);
		$dataArr = field_get_items('node', $node, 'field_org_chart_content');
		$defaultDataArr = array();
		$defaultDataArr['associates'] = array();
		$defaultDataArr['openPositions'] = array();
		$defaultDataArr['associatesData'] = array();
		$defaultDataArr['admins'] = array();
		$data = isset($dataArr[0]['value']) ? unserialize($dataArr[0]['value']) : $defaultDataArr;
		$index = $_POST['edit'];		
		if(isset($data['associatesData'][$index]['sub']) && count($data['associatesData'][$index]['sub'])>0){
			$error = true;
			$message = "Please re-assign the associates before deletion of Manager position";
		}
		if(!$error){
			$previousManager = $data['associatesData'][$index]['parent'];
			if(isset($data['admins'][$index])){
				unset($data['admins'][$index]);
				unset($data['associatesData'][$previousManager]['admin'][$index]);
			}else{
				unset($data['associatesData'][$previousManager]['sub'][$index]);
			}
			if(isset($data['openPositions'][$index])){
				unset($data['openPositions'][$index]);
			}
			$existVid = $node->vid;
			$node_wrapper = entity_metadata_wrapper('node', $node);
			$node_wrapper->field_org_chart_content->set(serialize($data));
			$node_wrapper->revision->set(1);
			$node_wrapper->log->set(t("Deleted ".$unixId." Info to Organisation Chart"));
			//$node_wrapper->uid->set($user->uid); TODO: Need to check whether the node revision is taking the current logged in user uid
			$node_wrapper->save();
			$newVid = $node->vid;
			if($existVid == $newVid){
				$message = "Organisation chart update failed, please contact administrator";
			}else{
				$response->status = "success";
				$message = "Success";
			}
		}		
	}
	$response->message = $message;
	drupal_json_output($response);
}