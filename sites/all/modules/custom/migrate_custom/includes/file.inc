<?php

class MigrateFileMappedFid extends MigrateFileFid {
  public function processFile($value, $owner) {
    $migration = Migration::currentMigration();
    $soft_dependencies = $migration->getSoftDependencies();
    $dependency = preg_grep('/([a-zA-Z0-9]+)file$/i', $soft_dependencies);
    
    $table = array_shift($dependency);

    $fid = db_select('migrate_map_' . $table, 'fl')->fields('fl', array('destid1'))->condition('fl.sourceid1', $value, '=')->execute()->fetchField();
    if ($fid) {
      $value = $fid;
    }
    return parent::processFile($value, $owner);
  }
}