<?php

class MigrateCustomMigrateDestinationNode extends MigrateDestinationHandler {

  public function __construct() {
    $this->registerTypes(array('node'));
  }

  public function prepare($entity, stdClass $source_row) {
    //$migration = Migration::currentMigration();
    $node = node_load($source_row->nid);

    if ($node && trim($node->title) == trim($source_row->title) && ($entity->type == $node->type)) {
      $entity->nid = $node->nid;
      $entity->vid = $node->vid;
    }
  }
}
