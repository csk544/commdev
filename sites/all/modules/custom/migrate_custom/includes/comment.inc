<?php

class MigrateCustomMigrateDestinationComment extends MigrateDestinationHandler {

  public function __construct() {
    $this->registerTypes(array('comment'));
  }

  public function prepare($entity, stdClass $source_row) {
    $comment = comment_load($source_row->cid);
    
    if ($comment && $comment->subject == $source_row->subject) {
      $entity->cid = $comment->cid;
    }
  }
}
