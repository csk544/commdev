<?php

class MigrateEntityReferenceFieldHandlerCustom extends MigrateFieldHandler {

  public function __construct($options = array()) {
    $this->registerTypes(array('entityreference'));
  }

  public function prepare($entity, array $field_info, array $instance, array $values) {
    $result = array();
    if ($instance['entity_type'] == 'node' && !empty($values)) {

      $arguments = (isset($values['arguments']))? $values['arguments']: array();
      $language = $this->getFieldLanguage($entity, $field_info, $arguments);
      
      $target_nodes = db_select('migrated_nodes', 'md')->fields('md')->condition('md.source_nid', $values, 'IN')->execute()->fetchAllKeyed();
      $delta = 0;
      foreach ($values as $value) {
        $result[$language][$delta]['target_id'] = !empty($target_nodes[$value]) ? $target_nodes[$value] : $value;
        $delta++;
      }
    }
    else {
      $result = NULL;
    }
    return $result;
  }
}

class MigrateLinkFieldHandlerCustom extends MigrateLinkFieldHandler {
  public function __construct($options = array()) {
    $this->registerTypes(array('link_field'));
  }

  public function prepare($entity, array $field_info, array $instance, array $values) {
    $result = parent::prepare($entity, $field_info, $instance, $values);
    if (!empty($result)) {
      $language = $this->getFieldLanguage($entity, $field_info, array());
      foreach ($result[$language] as $key => $item) {
        $result[$language][$key]['url'] = str_replace('comm.gene.com', 'cmg.gene.com', $result[$language][$key]['url']);
      }
    }
    return $result;
  }
}