<?php
/**
 * My custom filter handler
 */
class search_feedback_handler_filter_user_response extends views_handler_filter_in_operator {

  /**
   * Override parent get_value_options() function.
   *
   * @return
   *   Return the stored values in $this->value_options if someone expects it.
   */
  function get_value_options() {
    $response['Yes'] = 'Yes';
    $response['No'] = 'No';
    $this->value_options = $response;
    return $response;
  }
}