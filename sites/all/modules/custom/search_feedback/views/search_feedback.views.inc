<?php

/**
 * @file
 * Views integration for the search feedback custom module
 */
 
/**
 * Implements hook_views_data()
 *
 */
function search_feedback_views_data() {
 
 $table = array(
    'search_feedback' => array(  
       'table' => array(
          'group' => 'Feedback',   
             'base' => array(
               'field' => 'sid', 
               'title' => 'Search Feedback',
               'help' => 'Search Feedback database table'
            )
        ),
     //Description of sid
        'sid' => array(
           'title' => t('SId'),
           'help' => t('Search Feedback table Id field'),
           'field' => array(
              'click sortable' => TRUE,
            ),
           'filter' => array(
               'handler' => 'views_handler_filter_numeric'
            ),
 
           'sort' => array(
              'handler' => 'views_handler_sort'
           )
         ),
      //Description of user_unixid field
        'user_unixid' => array(
           'title' => t('User Unix Id'),
           'help' => t('Search Feedback table Unix id field'),
           'field' => array(
              'click sortable' => TRUE,
           ),
           'filter' => array(
              'handler' => 'views_handler_filter_string'
           ),
 
           'sort' => array(
              'handler' => 'views_handler_sort'
           )
        ),
     //Description of user_name field
       'user_name' => array(
           'title' => t('Username'),
           'help' => t('Search Feedback table User name field'),
           'field' => array(
                'click sortable' => TRUE,
             ),
           'filter' => array(
                 'handler' => 'views_handler_filter_string'
             ),
          'sort' => array(
             'handler' => 'views_handler_sort'
          )
        ),
			//Description of search_keyword field
       'search_keyword' => array(
           'title' => t('Search Keyword'),
           'help' => t('Search Feedback table Search Keyword field'),
           'field' => array(
                'click sortable' => TRUE,
             ),
           'filter' => array(
                 'handler' => 'views_handler_filter_string'
             ),
          'sort' => array(
             'handler' => 'views_handler_sort'
          )
        ),
			//Description of user_response field
       'user_response' => array(
           'title' => t('Response'),
           'help' => t('Search Feedback table Response field'),
           'field' => array(
                'click sortable' => TRUE,
             ),
           'filter' => array(
                 'handler' => 'search_feedback_handler_filter_user_response'
             ),
          'sort' => array(
             'handler' => 'views_handler_sort'
          )
        ),
				//Description of user_comment field
       'user_comment' => array(
           'title' => t('Comment'),
           'help' => t('Search Feedback table Comment field'),
           'field' => array(
                'click sortable' => TRUE,
             ),
           'filter' => array(
                 'handler' => 'views_handler_filter_string'
             ),
          'sort' => array(
             'handler' => 'views_handler_sort'
          )
        ),
				//Description of user_comment field
       'submission_date' => array(
           'title' => t('Date of submission'),
           'help' => t('Search Feedback table Date field'),
           'field' => array(
                'click sortable' => TRUE,
             ),
           'filter' => array(
                 'handler' => 'views_handler_filter_date'
             ),
          'sort' => array(
             'handler' => 'views_handler_sort'
          )
        )
      )
    );
    return $table;
}