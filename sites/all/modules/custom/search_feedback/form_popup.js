(function($, Drupal) {
  function showPopup(feedback_response) { 
		if(feedback_response > 0) {
			$( "div#search-feedback-modal" ).text( "Thank you! We will work to enhance the search experience." );
			$('a#search-feedback').trigger('click');
			//$('div.form-item-search-feedback').find(':input[type=radio]').attr('checked',false).val('');
			//alert("Thank you! We will work to enhance the search experience.");
			$("form#search-feedback-form")[0].reset();
		} else {
			$( "div#search-feedback-modal" ).text( "Thank you! Your feedback has been submitted." );
			$('a#search-feedback').trigger('click');
			$("form#search-feedback-form")[0].reset();
			//alert("Thank you! Your feedback has been submitted.");
		}
  }
  // Create the callback for the command we used in our ajax response
  Drupal.ajax.prototype.commands.formPopupTriggerPopup = function(ajax, response)
  {
    // response.name contains the value the user submitted in the form.
    // We will pass this to our function showPopup, so it can be shown in the popup.
    showPopup(response.feedback_response);
  };
}(jQuery, Drupal));