<?php
/**
 * SAML 2.0 remote IdP metadata for simpleSAMLphp.
 *
 * Remember to remove the IdPs you don't use from this file.
 *
 * See: https://rnd.feide.no/content/idp-remote-metadata-reference
 */

/*
 * Guest IdP. allows users to sign up and register. Great for testing!
$metadata['https://openidp.feide.no'] = array(
	'name' => array(
		'en' => 'Feide OpenIdP - guest users',
		'no' => 'Feide Gjestebrukere',
	),
	'description'          => 'Here you can login with your account on Feide RnD OpenID. If you do not already have an account on this identity provider, you can create a new one by following the create new account link and follow the instructions.',

	'SingleSignOnService'  => 'https://openidp.feide.no/simplesaml/saml2/idp/SSOService.php',
	'SingleLogoutService'  => 'https://openidp.feide.no/simplesaml/saml2/idp/SingleLogoutService.php',
	'certFingerprint'      => 'c9ed4dfb07caf13fc21e0fec1572047eb8a7a4cb'
);
 */


/*
 * Feide, the norwegian federation. Test and production metadata.
$metadata['https://idp-test.feide.no'] = array(
	'name' => array(
		'en' => 'Feide Test environment',
		'no' => 'Feide testmiljø',
	),
	'description'                  => 'Feide test environment (idp-test.feide.no). Authenticate with your identity from a school or university in Norway.',

	'SingleSignOnService'          => 'https://idp-test.feide.no/simplesaml/saml2/idp/SSOService.php',
	'SingleLogoutService'          => 'https://idp-test.feide.no/simplesaml/saml2/idp/SingleLogoutService.php',

	'certFingerprint'              => 'fa982efdb69f26e8073c8f815a82a0c5885960a2',
	'hint.cidr'                    => '158.38.0.0/16',
);

$metadata['https://idp.feide.no'] = array(
	'name' => 'Feide',
	'description' => array(
		'en' => 'Authenticate with your identity from a school or university in Norway.',
		'no' => 'Logg inn med din identitet fra skolen eller universitetet du er tilknyttet (i Norge).',
	),
	'SingleSignOnService'          => 'https://idp.feide.no/simplesaml/saml2/idp/SSOService.php',
	'SingleLogoutService'          => 'https://idp.feide.no/simplesaml/saml2/idp/SingleLogoutService.php',
	'certFingerprint'              => 'cde69e332fa7dd0eaa99ee0ddf06916e8942ac53',
	'hint.cidr'                    => '158.38.0.0/16',
);
 */



/*
 * Wayf, the danish federation metadata.
$metadata['https://wayf.wayf.dk'] = array(
	'name' => array(
		'en' => 'DK-WAYF Production server',
		'da' => 'DK-WAYF Produktionsmiljøet',
	),
	'description'          => 'Login with your identity from a danish school, university or library.',
	'SingleSignOnService'  => 'https://wayf.wayf.dk/saml2/idp/SSOService.php',
	'SingleLogoutService'  => 'https://wayf.wayf.dk/saml2/idp/SingleLogoutService.php',
	'certFingerprint'      => 'c215d7bf9d51c7805055239f66b957d9a72ff44b'
);

$metadata['https://betawayf.wayf.dk'] = array(
	'name' => array(
		'en' => 'DK-WAYF Quality Assurance',
		'da' => 'DK-WAYF Quality Assurance miljøet',
	),
	'description'          => 'Login with your identity from a danish school, university or library.',
	'SingleSignOnService'  => 'https://betawayf.wayf.dk/saml2/idp/SSOService.php',
	'SingleLogoutService'  => 'https://betawayf.wayf.dk/saml2/idp/SingleLogoutService.php',
	'certFingerprint'      => 'c215d7bf9d51c7805055239f66b957d9a72ff44b'
);

$metadata['https://testidp.wayf.dk'] = array(
	'name' => array(
		'en' => 'DK-WAYF Test Server',
		'da' => 'DK-WAYF Test Miljøet',
	),
	'description'          => 'Login with your identity from a danish school, university or library.',
	'SingleSignOnService'  => 'https://testidp.wayf.dk/saml2/idp/SSOService.php',
	'SingleLogoutService'  => 'https://testidp.wayf.dk/saml2/idp/SingleLogoutService.php',
	'certFingerprint'      => '04b3b08bce004c27458b3e85b125273e67ef062b'
);


$metadata['http://localhost/simplesaml/saml2/idp/metadata.php'] = array (
  'metadata-set' => 'saml20-idp-remote',
  'entityid' => 'http://localhost/simplesaml/saml2/idp/metadata.php',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'http://localhost/simplesaml/saml2/idp/SSOService.php',
    ),
  ),
  'SingleLogoutService' => 'http://localhost/simplesaml/saml2/idp/SingleLogoutService.php',
  'certData' => 'MIICgTCCAeoCCQCbOlrWDdX7FTANBgkqhkiG9w0BAQUFADCBhDELMAkGA1UEBhMCTk8xGDAWBgNVBAgTD0FuZHJlYXMgU29sYmVyZzEMMAoGA1UEBxMDRm9vMRAwDgYDVQQKEwdVTklORVRUMRgwFgYDVQQDEw9mZWlkZS5lcmxhbmcubm8xITAfBgkqhkiG9w0BCQEWEmFuZHJlYXNAdW5pbmV0dC5ubzAeFw0wNzA2MTUxMjAxMzVaFw0wNzA4MTQxMjAxMzVaMIGEMQswCQYDVQQGEwJOTzEYMBYGA1UECBMPQW5kcmVhcyBTb2xiZXJnMQwwCgYDVQQHEwNGb28xEDAOBgNVBAoTB1VOSU5FVFQxGDAWBgNVBAMTD2ZlaWRlLmVybGFuZy5ubzEhMB8GCSqGSIb3DQEJARYSYW5kcmVhc0B1bmluZXR0Lm5vMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDivbhR7P516x/S3BqKxupQe0LONoliupiBOesCO3SHbDrl3+q9IbfnfmE04rNuMcPsIxB161TdDpIesLCn7c8aPHISKOtPlAeTZSnb8QAu7aRjZq3+PbrP5uW3TcfCGPtKTytHOge/OlJbo078dVhXQ14d1EDwXJW1rRXuUt4C8QIDAQABMA0GCSqGSIb3DQEBBQUAA4GBACDVfp86HObqY+e8BUoWQ9+VMQx1ASDohBjwOsg2WykUqRXF+dLfcUH9dWR63CtZIKFDbStNomPnQz7nbK+onygwBspVEbnHuUihZq3ZUdmumQqCw4Uvs/1Uvq3orOo/WJVhTyvLgFVK2QarQ4/67OZfHd7R+POBXhophSMv1ZOo',
  'NameIDFormat' => 'urn:oasis:names:tc:SAML:2.0:nameid-format:transient',
);

 */


//Check whether base url is secured or non-secured based on that send SAML request
if(array_key_exists('HTTPS', $_SERVER) && !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") {
  $baseurlpath = "https://" . $_SERVER['HTTP_HOST'];
} else { 
  $baseurlpath = "http://" . $_SERVER['HTTP_HOST'];
}


$metadata['gene.com'] = array(
	'name' => array(
		'en' => 'Genentech - Authtentication',
		'no' => 'Genentech Commportal',
	), 
	'description'          => 'Here you can login with your account on commportal',
  'SingleSignOnService'  => 'https://b2bdev.gene.com/idp/SSO.saml2',
	'SingleLogoutService'  => "https://b2bdev.gene.com/siteminderagent/forms/logoff.fcc?TARGET=https://b2bdev.gene.com/idp/startSSO.ping?PartnerSpId=genentechcmgportal",
	'certData'      => 'MIID2jCCAsICCQCZacx4y70SUTANBgkqhkiG9w0BAQsFADCBrjELMAkGA1UEBhMCQ0gxDjAMBgNV
BAgTBUJhc2VsMQ4wDAYDVQQHEwVCYXNlbDEgMB4GA1UEChMXRi4gSG9mZm1hbm4tTGEgUm9jaGUg
QUcxEDAOBgNVBAsTB0FNUy1XQUMxGTAXBgNVBAMTEFJvY2hlIEZlZGVyYXRpb24xMDAuBgkqhkiG
9w0BCQEWIWdsb2JhbC53ZWJhY2Nlc3Njb250cm9sQHJvY2hlLmNvbTAeFw0xNzAzMDMwODExMTZa
Fw0yNzAzMDEwODExMTZaMIGuMQswCQYDVQQGEwJDSDEOMAwGA1UECBMFQmFzZWwxDjAMBgNVBAcT
BUJhc2VsMSAwHgYDVQQKExdGLiBIb2ZmbWFubi1MYSBSb2NoZSBBRzEQMA4GA1UECxMHQU1TLVdB
QzEZMBcGA1UEAxMQUm9jaGUgRmVkZXJhdGlvbjEwMC4GCSqGSIb3DQEJARYhZ2xvYmFsLndlYmFj
Y2Vzc2NvbnRyb2xAcm9jaGUuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuXf6
aAs5SdwSC/zch54EdNeyvGQkYuGPHzjL4V0dpGLlWH+lwcdSTdeud35+Q+0th3rMphqEzWFsCIV/
fvEz88b7gwegAnm83td4aQaa903kHBtj7wgk/6ETIY3/b5R4R5up2uz5hH+aUuDSObUFUY9F7/rJ
Z2fYqrSiAd65BnolEtsyB58dgouj8KLV1ydRbPlGoDLaBS2cE/B/bbcIbeT9lCz/RT7kd3fxXIuh
24ko6TeRKUtSeYotb+QEuGV3LT8VAVsBzObAhe6zi6kcQVlNIJJhk/AUJRy1VHBO0fOEKvd4wqQF
/u6CStuUPFOv16+zlO9qoehDog6aju7/0wIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQAV3UzlmERb
Q3gqgMp24RYIFnYETWKZ5/s++83ZNjK1iNlekA6EIPnlCq5KxF77zlOckgMiTNuzoe/E8APeNWOV
EuZqbozZJymXAneKMRIetEDqK+qkT02iH9WMxxmm3iFAa/JpJa0Wp+O54EyL5sWmqqinN8Zigx4z
DA0ubRNr5tCREHigr3WVL31cqWF9wpUV9wulq+HGSWG4dPJrzsBpwzKzQuleuZZk9ukErWbkSegF
Iwk2afKpWGS6LmTLJkkQcK+g6AEVkdj1mYFancsKYRvS1yV5SF9VcWc6zIyYMC9ww4cfnmJn2PRg
VlfE5HLi3Fw0k6zdzWkNwpzkxfyI'
);
